package io.leandev.appstack.exception;

import java.util.Map;

public class Violation {
    private String message;
    private String[] props = new String[0];
    private Object params;

    public Violation(String[] props, String message, Object... params) {
        this.message = message;
        this.params = params;
        this.props = props;
    }

    public Violation(String prop, String message, Object... params) {
        String[] props = new String[1];
        props[0] = prop;

        this.message = message;
        this.params = params;
        this.props = props;
    }

    public Violation(String prop, String message, Map<String, Object> params) {
        String[] props = new String[1];
        props[0] = prop;

        this.message = message;
        this.params = params;
        this.props = props;
    }

    public Violation(String message, Object... params) {
        this.message = message;
        this.params = params;
    }

    public String getMessage() {
        return message;
    }

    public String[] getProps() {
        return props;
    }

    public Object getParams() {
        return params;
    }
}
