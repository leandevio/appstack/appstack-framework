package io.leandev.appstack.html;

import io.leandev.appstack.doc.Heading;

public class HtmlHeading extends HtmlBlock implements Heading {
    private int level;

    public static HtmlHeading of(Heading heading) {
        HtmlHeading htmlHeader = new HtmlHeading(heading.level());
        heading.chunks().stream().forEach(htmlHeader::appendChunk);
        return htmlHeader;
    }

    public HtmlHeading() {
        this(1);
    }

    public HtmlHeading(int level) {
        super(String.format("h%d", level));
        this.level = level;
    }

    @Override
    public int level() {
        return this.level;
    }
}
