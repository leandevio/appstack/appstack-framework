package io.leandev.appstack.logging;

public enum Level {
    ERROR(4), WARN(3), INFO(2), DEBUG(1), TRACE(0);
    private final int value;

    Level(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}