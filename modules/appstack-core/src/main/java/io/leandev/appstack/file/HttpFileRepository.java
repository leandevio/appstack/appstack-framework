package io.leandev.appstack.file;

import io.leandev.appstack.bean.Model;
import io.leandev.appstack.converter.Converters;
import io.leandev.appstack.json.JsonParser;
import io.leandev.appstack.text.TextReader;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.classic.methods.*;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.ProtocolException;
import org.apache.hc.core5.http.io.entity.ByteArrayEntity;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.hc.core5.net.URIBuilder;

import javax.ws.rs.core.MediaType;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

@Slf4j
public class HttpFileRepository implements FileRepository {
    public static class FileType {
        public static final String HEADER = "FileType";
        public static final String REGULAR = "-";
        public static final String DIRECTORY = "d";
        public static final String SYMBOLIC_LINK = "l";
        public static final String SOCKET = "s";
        public static final String PIPE = "p";
        public static final String CHARACTER_DEVICE = "c";
        public static final String BLOCK = "b";
    }

    public static class HttpHeader {
        public static final String FILE_TYPE = "FileType";
        public static final String FILE_SIZE = "FileSize";
    }

    private final PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
    private JsonParser jsonParser = new JsonParser();
    private Converters converters = new Converters();
    private URL url;
    private String username;
    private String password;

    public HttpFileRepository(URL url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }
    public HttpFileRepository(String url, String username, String password) throws MalformedURLException {
        this.url = new URL(url);
        this.username = username;
        this.password = password;
    }

    @Override
    public boolean exists(Path path) {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(path));
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }
        HttpHead http = new HttpHead(uri);
        http.setHeader("Authorization", this.getAuthorization());

        int code;
        try {
            CloseableHttpResponse response = httpClient.execute(http);
            code = response.getCode();
        } catch (IOException e) {
            throw new HttpFileRepositoryException("Failed to execute the request.", e);
        }

        return (code == 200);
    }
    @Override
    public boolean isDirectory(Path path) {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(path));
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }
        HttpHead http = new HttpHead(uri);
        http.setHeader("Authorization", this.getAuthorization());

        Header fileType;
        try {
            CloseableHttpResponse response = httpClient.execute(http);
            fileType = response.getHeader(HttpHeader.FILE_TYPE);
        } catch (ProtocolException | IOException e) {
            throw new HttpFileRepositoryException("Failed to execute the request.", e);
        }

        return fileType.getValue().equals(FileType.DIRECTORY);
    }

    @Override
    public boolean isRegularFile(Path path) {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(path));
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }
        HttpHead http = new HttpHead(uri);
        http.setHeader("Authorization", this.getAuthorization());

        Header fileType;
        try {
            CloseableHttpResponse response = httpClient.execute(http);
            fileType = response.getHeader(HttpHeader.FILE_TYPE);
        } catch (ProtocolException | IOException e) {
            throw new HttpFileRepositoryException("Failed to execute the request.", e);
        }

        return fileType.getValue().equals(FileType.REGULAR);
    }

    @Override
    public long size(Path path) {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(path));
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }
        HttpHead http = new HttpHead(uri);
        http.setHeader("Authorization", this.getAuthorization());

        Header contentLength;
        try {
            CloseableHttpResponse response = httpClient.execute(http);
            contentLength = response.getHeader(HttpHeader.FILE_SIZE);
        } catch (ProtocolException | IOException e) {
            throw new HttpFileRepositoryException("Failed to execute the request.", e);
        }

        return Long.parseLong(contentLength.getValue());
    }

    public OutputStream newOutputStream(Path path, Model options) throws IOException {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(path));
            Set<String> keySet = options.keySet();
            for(String key : keySet) {
                if(options.getAsString(key) == null) continue;
                uriBuilder.addParameter(key, options.getAsString(key));
            }
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }
        HttpPost httpPost = new HttpPost(uri);
        httpPost.setHeader("Authorization", this.getAuthorization());

        CloseableHttpResponse response = httpClient.execute(httpPost);
        response.getEntity().getContent();

        return null;
    }

    private String getAuthorization() {
        String auth = this.username + ":" + this.password;
        String encodedAuth = Base64.getEncoder().encodeToString(auth.getBytes(StandardCharsets.ISO_8859_1));
        return "Basic " + encodedAuth;
    }


    @Override
    public OutputStream newOutputStream(Path path, OpenOption... openOptions) throws IOException {
        return null;
    }


    @Override
    public void write(Path path, byte[] bytes, OpenOption... openOptions) throws IOException {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(path));
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }

        boolean truncate_existing = false;
        boolean append = false;
        for(OpenOption openOption : openOptions) {
            if(openOption.equals(StandardOpenOption.TRUNCATE_EXISTING)) {
                truncate_existing = true;
            } else if(openOption.equals(StandardOpenOption.APPEND)) {
                append = true;
            }
        }

        HttpUriRequest httpRequest;
        if(append) {
            httpRequest = new HttpPatch(uri);
        } else if(truncate_existing) {
            httpRequest = new HttpPut(uri);
        } else {
            httpRequest = new HttpPost(uri);
        }

        HttpEntity httpEntity = new ByteArrayEntity(bytes, null);
        httpRequest.setEntity(httpEntity);
        httpRequest.setHeader("Content-type", MediaType.APPLICATION_OCTET_STREAM);
        httpRequest.setHeader("Authorization", this.getAuthorization());

        log.debug(String.format("Sending request to %s(%s)", httpRequest.getRequestUri(), httpRequest.getMethod()));

        try(CloseableHttpResponse response = httpClient.execute(httpRequest)) {
            if(response.getCode() != 200 && response.getCode() != 204) {
                int status = response.getCode();
                String statusText = parseStatusText(response);

                throw new HttpFileRepositoryException(status, statusText);
            }
        }
    }

    @Override
    public void write(Path path, String text, OpenOption... openOptions) throws IOException {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(path));
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }

        boolean truncate_existing = false;
        boolean append = false;
        for(OpenOption openOption : openOptions) {
            if(openOption.equals(StandardOpenOption.TRUNCATE_EXISTING)) {
                truncate_existing = true;
            } else if(openOption.equals(StandardOpenOption.APPEND)) {
                append = true;
            }
        }

        HttpUriRequest httpRequest;
        if(append) {
            httpRequest = new HttpPatch(uri);
        } else if(truncate_existing) {
            httpRequest = new HttpPut(uri);
        } else {
            httpRequest = new HttpPost(uri);
        }

        HttpEntity httpEntity = new StringEntity(text, StandardCharsets.UTF_8);
        httpRequest.setEntity(httpEntity);
        httpRequest.setHeader("Content-type",MediaType.TEXT_PLAIN);
        httpRequest.setHeader("Authorization", this.getAuthorization());

        try(CloseableHttpResponse response = httpClient.execute(httpRequest)) {
            if(response.getCode() != 200 && response.getCode() != 204) {
                int status = response.getCode();
                String statusText = parseStatusText(response);

                throw new HttpFileRepositoryException(status, statusText);
            }
        }
    }

    @Override
    public InputStream newInputStream(Path path) throws IOException {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(path));
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }

        HttpUriRequest httpRequest = new HttpGet(uri);
        httpRequest.setHeader("Authorization", this.getAuthorization());


        CloseableHttpResponse response = httpClient.execute(httpRequest);
        if(response.getCode() != 200 && response.getCode() != 204) {
            int status = response.getCode();
            String statusText = parseStatusText(response);

            throw new HttpFileRepositoryException(status, statusText);
        }
        return response.getEntity().getContent();
    }

    @Override
    public boolean deleteIfExists(Path path) throws IOException {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(path));
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }

        HttpUriRequest httpRequest = new HttpDelete(uri);
        httpRequest.setHeader("Authorization", this.getAuthorization());
        httpRequest.setHeader("Ignore", converters.convert(true, String.class));

        CloseableHttpResponse response = httpClient.execute(httpRequest);
        if(response.getCode() != 200 && response.getCode() != 204) {
            int status = response.getCode();
            String statusText = parseStatusText(response);

            throw new HttpFileRepositoryException(status, statusText);
        }
        try {
            String totalElements = response.getHeader("TotalElements").getValue();
            if(totalElements == null) {
                totalElements = "0";
            }
            if(converters.convert(totalElements, Integer.class) > 0) {
                return true;
            } else {
                return false;
            }
        } catch (ProtocolException e) {
            return false;
        }
    }

    @Override
    public void delete(Path path) throws IOException {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(path));
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }

        HttpUriRequest httpRequest = new HttpDelete(uri);
        httpRequest.setHeader("Authorization", this.getAuthorization());
        httpRequest.setHeader("Ignore", converters.convert(false, String.class));

        CloseableHttpResponse response = httpClient.execute(httpRequest);
        if(response.getCode() != 200 && response.getCode() != 204) {
            int status = response.getCode();
            String statusText = parseStatusText(response);

            throw new HttpFileRepositoryException(status, statusText);
        }
    }

    @Override
    public Path move(Path source, Path target) throws IOException {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(source));
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }

        HttpUriRequest httpRequest = new HttpPatch(uri);
        httpRequest.setHeader("Authorization", this.getAuthorization());
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("command", "move"));
        nvps.add(new BasicNameValuePair("destination", toURI(target)));
        httpRequest.setEntity(new UrlEncodedFormEntity(nvps));

        CloseableHttpResponse response = httpClient.execute(httpRequest);
        if(response.getCode() != 200 && response.getCode() != 204) {
            int status = response.getCode();
            String statusText = parseStatusText(response);

            throw new HttpFileRepositoryException(status, statusText);
        }
        return target;
    }

    @Override
    public Stream<Path> list(Path path, boolean hidden) throws IOException {
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();
        URI uri;
        try {
            URIBuilder uriBuilder = new URIBuilder(url.toURI());
            uriBuilder.appendPath(toURI(path));
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new FileRepositoryException(e);
        }

        HttpUriRequest httpRequest = new HttpGet(uri);
        httpRequest.setHeader("Authorization", this.getAuthorization());
        httpRequest.setHeader("Hidden", converters.convert(hidden, String.class));

        CloseableHttpResponse response = httpClient.execute(httpRequest);
        if(response.getCode() != 200 && response.getCode() != 204) {
            int status = response.getCode();
            String statusText = parseStatusText(response);

            throw new HttpFileRepositoryException(status, statusText);
        }

        List<String> paths;
        try(InputStream inputStream = response.getEntity().getContent()) {
            paths = jsonParser.readValueAsList(inputStream, String.class);
        }
        return paths.stream().map(encodedPath -> {
            String decodedPath;
            try {
                decodedPath = URLDecoder.decode(encodedPath, StandardCharsets.UTF_8.toString());
            } catch (UnsupportedEncodingException e) {
                decodedPath = encodedPath;
            }
            return Paths.get(decodedPath);
        });
    }

    @Override
    public void close() {}

    private String parseStatusText(CloseableHttpResponse response) throws IOException {
        String statusText = response.getReasonPhrase();
        if(statusText != null && statusText.length() > 0) {
            return statusText;
        }
        try(TextReader textReader = new TextReader(response.getEntity().getContent())) {
            String content = textReader.readAll();
            String contentType = parseContentType(response);
            if(contentType.contains(String.format("%s/%s", MediaType.APPLICATION_JSON_TYPE.getType(), MediaType.APPLICATION_JSON_TYPE.getSubtype()))) {
                Model model = jsonParser.readValue(content, Model.class);
                statusText = model.getString("message");
            } else {
                statusText = content;
            }
        }

        return statusText;
    }

    private String parseContentType(CloseableHttpResponse response) {
        Header header = null;
        try {
            header = response.getHeader("Content-Type");
        } catch (ProtocolException e) {
            log.debug("Failed to get the content type");
            return null;
        }
        return header.getValue();
    }

    private String toContentType(MediaType mediaType) {
        return String.format("%s/%s", mediaType.getType(), mediaType.getSubtype());
    }

    private String toURI(Path path) {
        String uri = path.toString();
        if(!File.separator.equals("/")) {
            uri = uri.replace(File.separator, "/");
        }
        return uri;
    }
}
