package io.leandev.appstack.doc;

import javax.ws.rs.core.MediaType;

public interface Image<T> extends Chunk<T> {
    @Override
    default MediaType mediaType() {
        return new MediaType("font", "*");
    }
}
