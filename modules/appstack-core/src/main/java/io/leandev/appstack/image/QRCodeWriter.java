package io.leandev.appstack.image;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import io.leandev.appstack.measure.Length;

import java.awt.image.BufferedImage;
import java.util.EnumMap;
import java.util.Map;

public class QRCodeWriter {
    private static final int PPI = 300;

    private com.google.zxing.qrcode.QRCodeWriter writer = new com.google.zxing.qrcode.QRCodeWriter();
    private int ppi = PPI;

    public QRCodeWriter (int ppi) {
        this.ppi = ppi;
    }

    public QRCodeWriter() {}

    public BufferedImage encode(String text, int width, int height, int margin) throws ImageException {

        BitMatrix bitMatrix;
        try {
            Map<EncodeHintType, Object> hints = new EnumMap<>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hints.put(EncodeHintType.MARGIN, margin); /* default = 4 pixels*/
            bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE, height, width, hints);
        } catch (WriterException e) {
            throw new ImageException(e);
        }

        return MatrixToImageWriter.toBufferedImage(bitMatrix);
    }

    public BufferedImage encode(String text, Length width, Length height, Length margin) throws ImageException {
        return this.encode(text, width.toPixel(ppi), height.toPixel(ppi), margin.toPixel(ppi));
    }

    public BufferedImage encode(String text, int width, int height) throws ImageException {
        return this.encode(text, width, height, 2);
    }

    public BufferedImage encode(String text, Length width, Length height) throws ImageException {
        return this.encode(text, width.toPixel(ppi), height.toPixel(ppi));
    }

    public int getPpi() {
        return ppi;
    }

    public void setPpi(int ppi) {
        this.ppi = ppi;
    }
}
