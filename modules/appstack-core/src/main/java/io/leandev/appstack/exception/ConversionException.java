package io.leandev.appstack.exception;

public class ConversionException extends ApplicationException {
    private static final String MESSAGE = "Conversion failed.";

    public ConversionException() {
        this(MESSAGE);
    }
    public ConversionException(String message) {
        super(message);
    }

    public ConversionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConversionException(Throwable cause) {
        this(MESSAGE, cause);
    }

    public ConversionException(String message, Object... params) {
        super(message, params);
    }

    public ConversionException(String message, Throwable cause, Object... params) {
        super(message, cause, params);
    }

    public String toString() {
        return this.getMessage();
    }

    @Override
    public String defaultMessage() {
        return MESSAGE;
    }
}