package io.leandev.appstack.search;

public enum ComparisonOperator {
    EQUAL("=="), NOT_EQUAL("!="), GREATER_THAN(">"), GREATER_THAN_OR_EQUAL(">="), LESS_THAN("<"),
    LESS_THAN_OR_EQUAL("<="), IN("=in="), NOT_IN("=out="), HAS("=has="), BETWEEN("=between="), IS("=is="), IS_NOT("=isnt=");

    private String operator;

    ComparisonOperator(final String operator) {
        this.operator = operator;
    }
}
