package io.leandev.appstack.bean;

import org.apache.commons.collections.map.HashedMap;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * ObjectCopier
 *
 * clone object or copy object's properties.
 *
 * @ TODO: 2022/2/22 remove the dependency on Model. Model could depends on ObjectCopier, but not the other way around.
 */
public class ObjectCopier {
    public <T> T copyObject(T obj) {
        ObjectWriter objectWriter = new ObjectWriter();
        byte[] bytes = objectWriter.writeObject(obj);
        ObjectReader objectReader = new ObjectReader();
        Object $clone = objectReader.readObject(bytes, obj.getClass());
        T clone;
        if($clone.getClass().equals(obj.getClass())) {
            clone = (T) $clone;
        } else {
            try {
                clone = (T) obj.getClass().newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new ObjectReadWriteException(e);
            }
            copyProperties(clone, $clone);
        }
        return clone;
    }

    public <S, T> void copyProperties(T target, S source, String... excludes) {
        List<String> excludeList = Arrays.asList(excludes);
        Model<T> t = new Model<>(target);
        Model<S> s = new Model<>(source);
        for(String key : t.keySet()) {
            if(excludeList.contains(key)) continue;
            if(!s.containsKey(key)) continue;
            if(!t.isWritable(key)) continue;
            t.put(key, s.get(key));
        }
    }

    public <S, T> void copyPropertiesIgnoreCase(T target, S source, String... excludes) {
        List<String> excludeList = Arrays.stream(excludes)
                .map(exclude -> exclude.toLowerCase(Locale.ROOT))
                .collect(Collectors.toList());
        Map<String, String> map = new HashedMap();
        Model<S> s = new Model<>(source);
        for(String sKey : s.keySet()) {
            map.put(sKey.toLowerCase(Locale.ROOT), sKey);
        }
        Model<T> t = new Model<>(target);
        for(String key : t.keySet()) {
            String lKey = key.toLowerCase(Locale.ROOT);
            if(excludeList.contains(lKey)) continue;
            String sKey = map.get(lKey);
            if(sKey == null) continue;
            if(!t.isWritable(key)) continue;
            t.put(key, s.get(sKey));
        }
    }

    public <S, T> void defaultProperties(T target, S source, String... excludes) {
        List<String> excludeList = Arrays.asList(excludes);
        Model<T> t = new Model<>(target);
        Model<S> s = new Model<>(source);
        for(String key : t.keySet()) {
            if(t.get(key) != null) continue;
            if(excludeList.contains(key)) continue;
            if(!s.containsKey(key)) continue;
            if(!t.isWritable(key)) continue;
            t.put(key, s.get(key));
        }
    }

    public <S, T> void defaultPropertiesIgnoreCase(T target, S source, String... excludes) {
        List<String> excludeList = Arrays.stream(excludes)
                .map(exclude -> exclude.toLowerCase(Locale.ROOT))
                .collect(Collectors.toList());
        Map<String, String> map = new HashedMap();
        Model<S> s = new Model<>(source);
        for(String sKey : s.keySet()) {
            map.put(sKey.toLowerCase(Locale.ROOT), sKey);
        }
        Model<T> t = new Model<>(target);
        for(String key : t.keySet()) {
            if(t.get(key) != null) continue;
            String lKey = key.toLowerCase(Locale.ROOT);
            if(excludeList.contains(lKey)) continue;
            String sKey = map.get(lKey);
            if(sKey == null) continue;
            if(!t.isWritable(key)) continue;
            t.put(key, s.get(sKey));
        }
    }
}
