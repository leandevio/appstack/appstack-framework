package io.leandev.appstack.file;

public class HttpFileRepositoryException extends FileRepositoryException {
    private int status;
    public HttpFileRepositoryException(int status, String message) {
        super(message);
    }
    public HttpFileRepositoryException(String message) {
        super(message);
    }

    public HttpFileRepositoryException(Throwable cause) {
        super(cause);
    }

    public HttpFileRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getStatus() {
        return this.status;
    }
}
