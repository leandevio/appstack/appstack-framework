package io.leandev.appstack.doc.delta;

import io.leandev.appstack.util.Model;

public interface Operation {
    Content content();

    Model attributes();
}
