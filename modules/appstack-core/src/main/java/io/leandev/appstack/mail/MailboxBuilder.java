package io.leandev.appstack.mail;

import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.Session;
import java.security.GeneralSecurityException;
import java.util.Properties;

public class MailboxBuilder {

    private IMAPSessionBuilder imapSessionBuilder;
    public static MailboxBuilder newMailbox(String host, int port) {
        return new MailboxBuilder(host, port);
    }

    public static MailboxBuilder newOffice365Mailbox() throws GeneralSecurityException {
        String host = "outlook.office365.com";
        int port = 993;
        Properties props = new Properties();
        props.put("mail.imap.ssl.enable", "true");
        props.put("mail.imap.starttls.enable", "true");
        props.put("mail.imap.auth", "true");
        props.put("mail.imap.auth.mechanisms", "XOAUTH2");

        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        props.put("mail.imap.ssl.trust", "*");
        props.put("mail.imap.ssl.socketFactory", sf);

        return new MailboxBuilder(host, port, props);
    }

    public static MailboxBuilder newGMailMailbox() {
        final String host = "imap.gmail.com";
        final int port = 993;
        Properties props = new Properties();
        props.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.imap.socketFactory.fallback", "false");
        props.setProperty("mail.imap.socketFactory.port", String.valueOf(port));
        return new MailboxBuilder(host, port, props);
    }
    public MailboxBuilder(String host, int port) {
        this(host, port, new Properties());
    }

    public MailboxBuilder(String host, int port, Properties properties) {
        IMAPSessionBuilder imapSessionBuilder = new IMAPSessionBuilder(host, port, properties);
        this.imapSessionBuilder = imapSessionBuilder;
    }

    public Mailbox build() {
        Session session = imapSessionBuilder.build();
        Mailbox mailbox = new Mailbox(session);
        return mailbox;
    }


    public MailboxBuilder withDebug(boolean debug) {
        imapSessionBuilder.withDebug(debug);
        return this;
    }


    public MailboxBuilder withSSL() {
        imapSessionBuilder.withSSL();
        return this;
    }

    public MailboxBuilder withProperty(String key, String value) {
        imapSessionBuilder.withProperty(key, value);
        return this;
    }
}
