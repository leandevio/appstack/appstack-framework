package io.leandev.appstack.xml;

public class XmlParserException  extends Exception {
    public XmlParserException(String message) {
        super(message);
    }
    public XmlParserException(Throwable cause) {
        super(cause);
    }
    public XmlParserException(String message, Throwable cause) {
        super(message, cause);
    }
}

