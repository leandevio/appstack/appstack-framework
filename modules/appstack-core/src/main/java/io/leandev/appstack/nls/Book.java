package io.leandev.appstack.nls;

import java.util.HashMap;

public class Book extends HashMap<String, String> {
    @Override
    public String put(String key, String value) {
        return super.put(key.toLowerCase(), value);
    }

    @Override
    public String get(Object key) {
        return super.get(key.toString().toLowerCase());
    }
}