package io.leandev.appstack.file;

import io.leandev.appstack.content.ContentDetector;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.core.MediaType;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;


@Slf4j
public class LocalFileRepository implements FileRepository {
    private ContentDetector contentDetector = new ContentDetector();
    private Path repository;
    public LocalFileRepository(Path path) {
        this.repository = path;
        if(!Files.exists(path)) {
            throw new FileException(String.format("Repository not found. Path = %s", path));
        }
    }

    public LocalFileRepository(URL url) {
        this(url.getPath());
    }

    public LocalFileRepository(String path) {
        this(Paths.get(path));
    }

    @Override
    public OutputStream newOutputStream(Path uri, OpenOption... openOptions) throws IOException {
        Path path = this.resolve(uri);
        return Files.newOutputStream(path, openOptions);
    }

    @Override
    public void write(Path uri, byte[] bytes, OpenOption... openOptions) throws IOException {
        Path path = this.resolve(uri);
        Path folder = path.getParent();
        if(!Files.exists(folder)) {
            Files.createDirectories(folder);
        }
        Files.write(path, bytes, openOptions);
    }

    @Override
    public void write(Path uri, String text, OpenOption... openOptions) throws IOException {
        byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
        write(uri, bytes, openOptions);
    }

    @Override
    public InputStream newInputStream(Path uri) throws IOException {
        Path path = this.resolve(uri);
        if(!Files.exists(path)) {
            throw new FileNotFoundException(String.format("File not found. Path: %s", path));
        }
        return Files.newInputStream(path);
    }

    @Override
    public boolean exists(Path uri) {
        Path path = this.resolve(uri);
        return Files.exists(path);
    }

    @Override
    public boolean isDirectory(Path uri) {
        Path path = this.resolve(uri);
        return Files.isDirectory(path);
    }

    @Override
    public boolean isRegularFile(Path uri) {
        Path path = this.resolve(uri);
        return Files.isRegularFile(path);
    }

    @Override
    public long size(Path uri) throws IOException {
        Path path = this.resolve(uri);
        return Files.size(path);
    }

    public FileTime getLastModifiedTime(Path uri) throws IOException {
        Path path = this.resolve(uri);
        return Files.getLastModifiedTime(path);
    }

    @Override
    public boolean deleteIfExists(Path uri) throws IOException {
        if(this.exists(uri)) {
            this.delete(uri);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void delete(Path uri) throws IOException {
        Path path = this.resolve(uri);
        if(Files.isDirectory(path)) {
            this.deleteDirectory(path);
        } else {
            Files.delete(path);
        }
    }

    @Override
    public Path move(Path source, Path target) throws IOException {
        Path _source = this.resolve(source);
        Path _target = this.resolve(target);
        Path folder = _target.getParent();
        if(!Files.exists(folder)) {
            Files.createDirectories(folder);
        }
        Path location = Files.move(_source, _target);
        log.debug(String.format("The file is moved from %s to %s.", _source, location));
        return target;
    }

    @Override
    public Stream<Path> list(Path uri, boolean hidden) throws IOException {
        Path path = this.resolve(uri);
        Stream<Path> stream = Files.list(path)
                .filter(file -> {
                    try {
                        return hidden ? true : !Files.isHidden(file);
                    } catch (IOException e) {
                        return false;
                    }
                })
                .map(file -> this.repository.relativize(file));
        return stream;
    }

    @Override
    public void close() {}

    public Path createDirectories(Path uri) throws IOException {
        Path path = this.resolve(uri);
        Files.createDirectories(path);
        return uri;
    }

    public Path resolve(Path uri) {
        Path path;
        int count = uri.getNameCount();
        if(count == 0) {
            return this.repository;
        }

        if(uri.startsWith("..")) {
            // prevent from being hack outside the repository.
            path = uri.subpath(1, count);
        } else if(uri.startsWith("/")) {
            path = uri.subpath(0, count);
        } else {
            path = uri;
        }
        return this.repository.resolve(path);
    }

    public MediaType detect(Path uri) throws IOException {
        Path file = this.resolve(uri);
        return contentDetector.detect(file);
    }

    private String getBasename(String filename, boolean removeAllExtensions) {
        if (filename == null || filename.isEmpty()) {
            return filename;
        }

        String extPattern = "(?<!^)[.]" + (removeAllExtensions ? ".*" : "[^.]*$");
        return filename.replaceAll(extPattern, "");
    }

    private Optional<String> getExtension(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    private void deleteDirectory(Path pathToBeDeleted) throws IOException {
        Files.walk(pathToBeDeleted)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }
}
