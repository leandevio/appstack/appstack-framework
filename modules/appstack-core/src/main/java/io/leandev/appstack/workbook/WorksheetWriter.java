package io.leandev.appstack.workbook;

import io.leandev.appstack.util.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WorksheetWriter {
    private Worksheet worksheet;

    public WorksheetWriter(Worksheet worksheet) {
        this.worksheet = worksheet;
    }

    public void writeRow(Object bean) {
        Item<?> item = new Item(bean);
        List<String> keys = item.keySet().stream().sorted().collect(Collectors.toList());
        List<Object> values = new ArrayList<>();
        for(String key : keys) {
            values.add(item.get(key));
        }
        this.write(values);
    }

    public void write(List<Object> values) {
        Row row = worksheet.createRow();
        for(int i=0; i<values.size(); i++) {
            Cell cell = row.createCell();
            cell.setValue(values.get(i));
        }
    }
}
