package io.leandev.appstack.mail;

import io.leandev.appstack.search.*;
import io.leandev.appstack.util.Item;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EmailFilter {
    private Predicate predicate = Predicate.empty();
    private List<Email> emails;

    public static EmailFilter query(List<Email> emails) {
        return new EmailFilter(emails);
    }

    protected EmailFilter(List<Email> emails) {
        this.emails = emails;
    }

    public EmailFilter receivedDateAfter(Date receivedDate) {
        this.predicate = this.predicate.and(Predicate.gt("receivedDate", receivedDate));
        return this;
    }

    public List<Email> list() {
        List<Email> findings = new ArrayList<>();
        for(Email email : emails) {
            if(match(email, predicate)) {
                findings.add(email);
            }
        }
        return null;
    }

    private boolean match(Email email, Predicate predicate) {
        Item<Envelope> item = new Item(email);
        return this.compare(item, predicate.node());
    }

    private boolean compare(Item<Envelope> item, Node node) {
        boolean test = true;
        if(node instanceof ComparisonNode) {
            String selector = ((ComparisonNode) node).selector();
            Object expection = ((ComparisonNode) node).expectation();
            ComparisonOperator operator = ((ComparisonNode) node).operator();
            if(operator==ComparisonOperator.GREATER_THAN) {
                if(expection instanceof Date) {
                    test = ((Date) item.get(selector)).after((Date) expection);
                } else if(expection instanceof Number) {
                    test = (((Number)item.get(selector)).doubleValue() > ((Number) expection).doubleValue());
                }
            } else if(operator==ComparisonOperator.EQUAL) {
                if(expection instanceof String) {
                    test = (item.get(selector).equals(expection));
                } else {
                    test = (((Number)item.get(selector)).doubleValue() == ((Number) expection).doubleValue());
                }
            }
        } else if(node instanceof AndNode) {
            for(Node childNode : ((AndNode) node).children()) {
                test = compare(item, childNode);
                if(!test) break;
            }
        }
        return test;
    }
}
