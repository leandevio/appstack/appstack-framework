package io.leandev.appstack.workbook;

import org.apache.poi.hpsf.CustomProperties;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

public class XlsWorkbook implements Workbook {
    private HSSFWorkbook workbook;

    public XlsWorkbook() {
        this.workbook = new HSSFWorkbook();
    }

    public XlsWorkbook(InputStream in) throws IOException {
        this.workbook = new HSSFWorkbook(in);
    }

    public void autoSizeColumns() {
        for(int i=0; i<this.numberOfWorksheets(); i++) {
            this.getWorksheetAt(i).autoSizeColumns();
        }
    }

    public void setLocale(Locale locale) {
        if(this.workbook.getDocumentSummaryInformation()==null) {
            this.workbook.createInformationProperties();
        }
        CustomProperties customProperties = this.workbook.getDocumentSummaryInformation().getCustomProperties();
        if (customProperties == null) {
            customProperties = new CustomProperties();
            this.workbook.getDocumentSummaryInformation().setCustomProperties(customProperties);
        }
        this.workbook.getDocumentSummaryInformation().getCustomProperties().put("locale", locale.toLanguageTag());
    }

    public Locale locale() {
        if(this.workbook.getDocumentSummaryInformation()==null) {
            this.workbook.createInformationProperties();
        }
        CustomProperties customProperties = this.workbook.getDocumentSummaryInformation().getCustomProperties();
        String property;
        if(customProperties==null) {
            property = null;
        } else {
            property = (String) customProperties.get("locale");
        }
        return (property==null) ? null : Locale.forLanguageTag(property);
    }

    public XlsCellStyle createCellStyle() {
        HSSFCellStyle hssfCellStyle = workbook.createCellStyle();
        HSSFFont hssfFont = workbook.createFont();
        hssfCellStyle.setFont(hssfFont);
        XlsCellStyle cellStyle = new XlsCellStyle(hssfCellStyle, this);
        return cellStyle;
    }

    public XlsCellStyle createCellStyle(HSSFCellStyle style) {
        HSSFCellStyle hssfCellStyle = workbook.createCellStyle();
        hssfCellStyle.cloneStyleFrom(style);
        HSSFFont hssfFont = workbook.createFont();
        hssfCellStyle.setFont(hssfFont);
        XlsCellStyle cellStyle = new XlsCellStyle(hssfCellStyle, this);
        return cellStyle;
    }

    @Override
    public int numberOfWorksheets() {
        return this.workbook.getNumberOfSheets();
    }

    @Override
    public XlsWorksheet createWorksheet(String name) {
        return toWorksheet(workbook.createSheet(name));
    }

    @Override
    public XlsWorksheet createWorksheet() {
        return toWorksheet(workbook.createSheet());
    }

    @Override
    public XlsWorksheet getWorksheetAt(int i) {
        HSSFSheet sheet;
        try {
            sheet = workbook.getSheetAt(i);
        } catch(IllegalArgumentException ex) {
            sheet = null;
        }
        return toWorksheet(sheet);
    }

    @Override
    public XlsWorksheet getWorksheet(String name) {
        return toWorksheet(workbook.getSheet(name));
    }

    @Override
    public void close() throws IOException {
        workbook.close();
    }

    @Override
    public void write(OutputStream outputStream) throws IOException {
        workbook.write(outputStream);
    }

    private XlsWorksheet toWorksheet(HSSFSheet sheet) {
        if(sheet==null) return null;

        XlsWorksheet worksheet = new XlsWorksheet(sheet, this);
        return worksheet;
    }

    protected HSSFWorkbook data() {
        return this.workbook;
    }
}
