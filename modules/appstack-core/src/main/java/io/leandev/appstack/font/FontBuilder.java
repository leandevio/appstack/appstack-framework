package io.leandev.appstack.font;

import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class FontBuilder {
    private static final Logger LOGGER = LogManager.getLogger(FontBuilder.class);
    private static final Class clazz = FontBuilder.class;
    private static final ClassLoader LOADER = clazz.getClassLoader();

    private final Map<String, FontFamily> LOCALEFONTFAMILY = new HashMap<>();

    private String fontFamily = Font.SANS_SERIF;
    private int style = Font.PLAIN;
    private int size = 12;

    public FontBuilder() {
        LOCALEFONTFAMILY.put(Locale.TRADITIONAL_CHINESE.toLanguageTag(), FontFamily.NotoSansTC);
        LOCALEFONTFAMILY.put(Locale.ENGLISH.toLanguageTag(), FontFamily.SANS_SERIF);
    }

    public FontBuilder withFontFamily(FontFamily fontFamily) {
        this.fontFamily = fontFamily.getValue();
        return this;
    }

    public FontBuilder withFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
        return this;
    }

    public FontBuilder withLocale(Locale locale) {
        FontFamily fontFamily = LOCALEFONTFAMILY.get(locale.toLanguageTag());
        if(fontFamily==null) {
            fontFamily = LOCALEFONTFAMILY.getOrDefault(locale.getLanguage(), FontFamily.SANS_SERIF);
        }
        return withFontFamily(fontFamily);
    }

    public FontBuilder withStyle(int style) {
        this.style = style;
        return this;
    }

    public FontBuilder ofSize(int size) {
        this.size = size;
        return this;
    }

    public Font build() {
        Font font;
        Path fontFile = Paths.get("fonts").resolve(fontFamily);
        try(InputStream inputStream = LOADER.getResourceAsStream(fontFile.toString())) {
            //create the font to use. Specify the size!
            if(inputStream==null) {
                font = new Font(fontFamily, style, (int) size);
            } else {
                // size's type has to be float
                font = Font.createFont(Font.TRUETYPE_FONT, inputStream).deriveFont(style).deriveFont((float)size);
                GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
                //register the font
                ge.registerFont(font);
            }
        } catch (IOException |FontFormatException e) {
            LOGGER.warn(String.format("Failed to load the font - %s", fontFamily), e);
            font = new Font(Font.SANS_SERIF, style, (int) size);
        }
        return font;
    }
}
