package io.leandev.appstack.crypto;

import io.leandev.appstack.exception.ApplicationException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

/**
 * 將資料用指定的 Algorithm 編碼.
 * <p/>
 * Created by jasonlin on 2/21/14.
 */
public class Digester {

    private Algorithm algorithm = Algorithm.MD5;
    private MessageDigest messageDigest = null;
    private Base64.Encoder base64Encoder = Base64.getEncoder();

    public Digester() {
    }

    public Digester(Algorithm algorithm) {
        this.algorithm = algorithm;
    }


    public byte[] digest(byte[] message) {
        if(message==null) return null;
        return getMessageDigest().digest(message);
    }

    public String digest(String message) {
        if(message==null) return null;
        byte[] value = digest(message.getBytes());

        String hash = base64Encoder.encodeToString(value);

        return hash;
    }

    public boolean verify(byte[] message, byte[] hash) {
        if(hash==null) return (message==null);
        byte[] answer = digest(message);
        return Arrays.equals(answer, hash);
    }

    public boolean verify(String message, String hash) {
        if(hash==null) return (message==null);
        String answer = digest(message);
        return hash.equals(answer);
    }

    public Algorithm getAlgorithm() {
        return algorithm;
    }

    private MessageDigest getMessageDigest() {
        if(this.messageDigest==null) {
            try {
                this.messageDigest = MessageDigest.getInstance(algorithm.toString());
            } catch (NoSuchAlgorithmException e) {
                throw new ApplicationException(e);
            }
        }
        return this.messageDigest;
    }


    public enum Algorithm {
        MD5("MD5"), SHA1("SHA-1"), SHA256("SHA-256");

        private final String name;

        Algorithm(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String toString() {
            return name;
        }
    }
}
