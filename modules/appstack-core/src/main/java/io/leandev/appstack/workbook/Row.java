package io.leandev.appstack.workbook;

public interface Row {
    Object[] getValues();

    Cell getCell(int index);

    Object getValue(int index);

    Cell createCell();

    int lastCellNum();

    int firstCellNum();

    int size();

    Worksheet worksheet();
}
