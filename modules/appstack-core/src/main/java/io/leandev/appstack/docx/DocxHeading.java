package io.leandev.appstack.docx;

import io.leandev.appstack.doc.Heading;
import io.leandev.appstack.html.HtmlBlock;
import io.leandev.appstack.html.HtmlHeading;

public class DocxHeading extends DocxBlock implements Heading {
    private int level;

    public static HtmlHeading of(Heading heading) {
        HtmlHeading htmlHeader = new HtmlHeading(heading.level());
        heading.chunks().stream().forEach(htmlHeader::appendChunk);
        return htmlHeader;
    }

    public DocxHeading() {
        this(1);
    }

    public DocxHeading(int level) {
        super();
        this.level = level;
    }

    @Override
    public int level() {
        return this.level;
    }
}
