package io.leandev.appstack.text;

import io.leandev.appstack.parser.RecordMetaData;

public class TxtRecordMetaData implements RecordMetaData {
    @Override
    public String columnName(int i) {
        return Integer.toString(i);
    }
}
