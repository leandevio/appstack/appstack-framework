package io.leandev.appstack.pdf;

import org.apache.pdfbox.pdmodel.PDPage;

public class PdfPage {
    protected PDPage pdPage;

    protected PdfPage(PDPage pdPage) {
        this.pdPage = pdPage;
    }
}
