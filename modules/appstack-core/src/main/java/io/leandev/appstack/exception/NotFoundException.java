package io.leandev.appstack.exception;

public class NotFoundException extends ApplicationException {
    private static final String MESSAGE = "The requested resource or data does not exist.";

    public NotFoundException() {
        this(MESSAGE);
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        this(MESSAGE, cause);
    }

    public NotFoundException(String message, Object... params) {
        super(message, params);
    }

    public NotFoundException(String message, Throwable cause, Object... params) {
        super(message, cause, params);
    }

    public String toString() {
        return this.getMessage();
    }

    @Override
    public String defaultMessage() {
        return MESSAGE;
    }
}