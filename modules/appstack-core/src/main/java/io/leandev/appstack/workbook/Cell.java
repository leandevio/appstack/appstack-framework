package io.leandev.appstack.workbook;

public interface Cell {
    void setValue(Object value);

    Object getValue();

    Row row();
}
