package io.leandev.appstack.search;

import java.util.List;

public class AndNode extends LogicalNode {
    public AndNode(List<Node> nodes, Node... more) {
        super(LogicalOperator.AND, nodes, more);
    }

    public AndNode(List<Node> nodes, List<Node> more) {
        super(LogicalOperator.AND, nodes, more);
    }

    public AndNode(Node node, List<Node> more) {
        super(LogicalOperator.AND, node, more);
    }

    public AndNode(Node... nodes) {
        super(LogicalOperator.AND, nodes);
    }
}
