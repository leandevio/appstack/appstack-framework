package io.leandev.appstack.security.exception;

public class UserNotFoundException extends AuthenticationException {
    public UserNotFoundException(String message) {
        super(message);
    }
    public UserNotFoundException(Throwable cause) {
        super(cause);
    }
    public UserNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
