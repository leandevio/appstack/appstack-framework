package io.leandev.appstack.doc;

import javax.ws.rs.core.MediaType;

public interface Link extends Chunk<String> {
    @Override
    default MediaType mediaType() {
        return MediaType.TEXT_PLAIN_TYPE;
    }

    String href();
}
