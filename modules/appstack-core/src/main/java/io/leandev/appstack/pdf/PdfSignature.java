package io.leandev.appstack.pdf;

import io.leandev.appstack.pdf.signature.CreateSignature;
import org.apache.pdfbox.pdmodel.PDDocument;

import javax.naming.InvalidNameException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

public class PdfSignature {
    private CreateSignature signature;
    public PdfSignature(KeyStore keyStore, String password) throws PdfException {
        // sign PDF
        try {
            signature = new CreateSignature(keyStore, password.toCharArray());
        } catch (KeyStoreException | UnrecoverableKeyException | NoSuchAlgorithmException | CertificateException | IOException e) {
            throw new PdfException("Failed to initialize the signature.", e);
        }
        signature.setExternalSigning(false);
    }

    public void sign(InputStream inputStream, OutputStream outputStream) throws PdfException, IOException {
        PdfDocument pdfDocument = new PdfDocument(inputStream);
        this.sign(pdfDocument, outputStream);
    }

    public void sign(PdfDocument pdfDocument, OutputStream outputStream) throws PdfException {
        PDDocument pdDocument = pdfDocument.getPDDocument();;
        try {
            signature.signDetached(pdDocument, outputStream);
        } catch (IOException | InvalidNameException e) {
            throw new PdfException("Failed to sign the document.", e);
        }
    }
}
