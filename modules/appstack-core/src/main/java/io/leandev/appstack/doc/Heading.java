package io.leandev.appstack.doc;

public interface Heading extends Block {
    int level();
}
