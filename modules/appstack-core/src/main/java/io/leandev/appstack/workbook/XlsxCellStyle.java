package io.leandev.appstack.workbook;

import io.leandev.appstack.doc.Align;
import io.leandev.appstack.doc.BorderStyle;
import io.leandev.appstack.doc.Color;
import io.leandev.appstack.doc.VerticalAlignment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

import java.util.HashMap;
import java.util.Map;

public class XlsxCellStyle implements CellStyle {
    private static Map<BorderStyle, org.apache.poi.ss.usermodel.BorderStyle> BORDERSTYLEMAPPING = new HashMap<>();
    private static Map<Align, HorizontalAlignment> HALIGNMAPPING = new HashMap<>();

    private static Map<VerticalAlignment, org.apache.poi.ss.usermodel.VerticalAlignment> VALIGNMAPPING = new HashMap<>();

    protected XSSFCellStyle data;
    private XlsxWorkbook workbook;

    static {
        BORDERSTYLEMAPPING.put(BorderStyle.NONE, org.apache.poi.ss.usermodel.BorderStyle.NONE);
        BORDERSTYLEMAPPING.put(BorderStyle.SOLID, org.apache.poi.ss.usermodel.BorderStyle.THIN);
        BORDERSTYLEMAPPING.put(BorderStyle.DOTTED, org.apache.poi.ss.usermodel.BorderStyle.DOTTED);
        BORDERSTYLEMAPPING.put(BorderStyle.DASHED, org.apache.poi.ss.usermodel.BorderStyle.DASHED);
        BORDERSTYLEMAPPING.put(BorderStyle.DOUBLE, org.apache.poi.ss.usermodel.BorderStyle.DOUBLE);

        HALIGNMAPPING.put(Align.LEFT, HorizontalAlignment.LEFT);
        HALIGNMAPPING.put(Align.RIGHT, HorizontalAlignment.RIGHT);
        HALIGNMAPPING.put(Align.CENTER, HorizontalAlignment.CENTER);
        HALIGNMAPPING.put(Align.JUSTIFY, HorizontalAlignment.JUSTIFY);
        HALIGNMAPPING.put(Align.DISTRIBUTED, HorizontalAlignment.DISTRIBUTED);

        VALIGNMAPPING.put(VerticalAlignment.TOP, org.apache.poi.ss.usermodel.VerticalAlignment.TOP);
        VALIGNMAPPING.put(VerticalAlignment.BOTTOM, org.apache.poi.ss.usermodel.VerticalAlignment.BOTTOM);
        VALIGNMAPPING.put(VerticalAlignment.MIDDLE, org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
        VALIGNMAPPING.put(VerticalAlignment.JUSTIFY, org.apache.poi.ss.usermodel.VerticalAlignment.JUSTIFY);
        VALIGNMAPPING.put(VerticalAlignment.DISTRIBUTED, org.apache.poi.ss.usermodel.VerticalAlignment.DISTRIBUTED);
    }

    protected XlsxCellStyle(XSSFCellStyle xssfCellStyle, XlsxWorkbook workbook) {
        this.workbook = workbook;
        this.data = xssfCellStyle;
    }

    protected XSSFCellStyle data() {
        return data;
    }

    @Override
    public void setColor(Color color) {
        XSSFColor xssfColor = toXSSFColor(color);
        data.getFont().setColor(xssfColor);
    }

    @Override
    public void setBackground(Color color) {
        XSSFColor xssfColor = toXSSFColor(color);
        data.setFillForegroundColor(xssfColor);
        data.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    }

    @Override
    public void setDataFormat(String dataFormat) {
        CreationHelper helper = workbook.data().getCreationHelper();
        data.setDataFormat(helper.createDataFormat().getFormat(dataFormat));
    }

    @Override
    public String dataFormat() {
        String dataFormatString = data.getDataFormatString();
        return dataFormatString.equalsIgnoreCase("General") ? null : dataFormatString;
    }

    @Override
    public void setAlign(Align align) {
        data.setAlignment(HALIGNMAPPING.get(align));
    }

    @Override
    public void setVerticalAlignment(VerticalAlignment verticalAlignment) {
        data.setVerticalAlignment(VALIGNMAPPING.get(verticalAlignment));
    }

    @Override
    public void setBorderTopColor(Color color) {
        data.setTopBorderColor(toXSSFColor(color));
    }

    @Override
    public void setBorderBottomColor(Color color) {
        data.setBottomBorderColor(toXSSFColor(color));
    }

    @Override
    public void setBorderLeftColor(Color color) {
        data.setLeftBorderColor(toXSSFColor(color));
    }

    @Override
    public void setBorderRightColor(Color color) {
        data.setRightBorderColor(toXSSFColor(color));
    }

    @Override
    public void setBorderTopStyle(BorderStyle borderStyle) {
        data.setBorderTop(BORDERSTYLEMAPPING.get(borderStyle));
    }

    @Override
    public void setBorderBottomStyle(BorderStyle borderStyle) {
        data.setBorderBottom(BORDERSTYLEMAPPING.get(borderStyle));
    }

    @Override
    public void setBorderLeftStyle(BorderStyle borderStyle) {
        data.setBorderLeft(BORDERSTYLEMAPPING.get(borderStyle));
    }

    @Override
    public void setBorderRightStyle(BorderStyle borderStyle) {
        data.setBorderRight(BORDERSTYLEMAPPING.get(borderStyle));
    }

    @Override
    public void setFontFamily(String name) {
        data.getFont().setFontName(name);
    }

    @Override
    public void setWrapText(boolean wrapText) {
        data.setWrapText(wrapText);
    }

    public XlsxWorkbook workbook() {
        return this.workbook;
    }

    private XSSFColor toXSSFColor(Color color) {
        java.awt.Color awtColor = new java.awt.Color(color.red(), color.green(), color.blue(), color.alpha());
        return new XSSFColor(awtColor, new DefaultIndexedColorMap());
    }

}
