package io.leandev.appstack.math;

import org.apache.commons.math3.stat.regression.SimpleRegression;

public class SimpleLinearRegressionModel {
    private SimpleRegression regression;

    public SimpleLinearRegressionModel() {
        regression = new SimpleRegression(true);
    }

    public SimpleLinearRegressionModel(boolean includeIntercept) {
        regression = new SimpleRegression(includeIntercept);
    }

    public void addData(double x, double y) {
        regression.addData(x, y);
    }

    public void addData(double[][] xy) {
        regression.addData(xy);
    }

    public double predict(double x) {
        return regression.predict(x);
    }

    public double slope() {
        return regression.getSlope();
    }

    public double slopeStdErr() {
        return regression.getSlopeStdErr();
    }

    public double intercept() {
        return regression.getIntercept();
    }

    public double interceptStdErr() {
        return regression.getInterceptStdErr();
    }
}
