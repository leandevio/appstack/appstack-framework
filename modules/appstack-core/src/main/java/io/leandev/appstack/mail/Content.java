package io.leandev.appstack.mail;

import io.leandev.appstack.content.ContentDetector;
import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Content {
    private static final Logger LOGGER = LogManager.getLogger(Content.class);

    private List<Article> articles = new ArrayList<>();
    private List<Attachment> attachments = new ArrayList<>();
    private List<Content> contents = new ArrayList<>();
    private List<Email> emails = new ArrayList<>();

    public void clear() {
        articles.clear();
        attachments.clear();
        contents.clear();
        emails.clear();
    }

    public void addArticle(Article article) {
        this.articles.add(article);
    }

    public void addArticle(Object data, MediaType mediaType, Charset charset, String text) {
        Article article = new Article(data, mediaType, charset, text);
        addArticle(article);
    }

    public void addArticle(Object data, MediaType mediaType, String text) {
        addArticle(data, mediaType, StandardCharsets.UTF_8, text);
    }

    public List<Article> getArticles() {
        return this.articles;
    }

    public Article getArticle() {
        return this.articles.size() > 0 ? this.articles.get(0) : null;
    }

    public void addAttachment(Attachment attachment) {
        this.attachments.add(attachment);
    }

    public void addAttachment(byte[] bytes, String name) {
        MediaType mediaType;
        try {
            ContentDetector detector = new ContentDetector();
            mediaType = detector.detect(bytes);
        } catch(Exception ex) {
            LOGGER.warn(String.format("Can't detect the content type of %s, Reason: %s", name, ex));
            mediaType = MediaType.APPLICATION_OCTET_STREAM_TYPE;
        }

        Attachment attachment = new Attachment(bytes, name, mediaType);

        addAttachment(attachment);
    }

    public void addAttachment(InputStream inputStream, String name) throws IOException {
        int size = inputStream.available();

        byte[] bytes = new byte[size];

        inputStream.read(bytes);

        addAttachment(bytes, name);
    }

    public void addAttachment(File file) throws IOException {
        FileInputStream inputStream = new FileInputStream(file);
        addAttachment(inputStream, file.getName());
    }

    public List<Attachment> getAttachments() {
        return this.attachments;
    }

    public boolean hasAttachment() {
        return this.attachments.size()>0;
    }

    public boolean hasEmbeddedContent() {
        return this.contents.size()>0;
    }

    public void addContent(Content content) {
        this.contents.add(content);
    }

    public List<Content> getContents() {
        return this.contents;
    }

    public void addEmail(Email email) {
        this.emails.add(email);
    }

    public List<Email> getEmails() {
        return this.emails;
    }

    public boolean isMultipart() {
        return hasAttachment() || hasEmbeddedContent() || articles.size()>1 || articles.stream().anyMatch(article -> article.hasText());
    }

    public String toString() {
        Article article = this.getArticle();
        return (article==null) ? super.toString() : article.toString();
    }
}
