package io.leandev.appstack.docx;

import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;

import java.util.Optional;

public class DocxTableCell {
    private XWPFTableCell xwpfTableCell;

    protected DocxTableCell(XWPFTableCell xwpfTableCell) {
        this.xwpfTableCell = xwpfTableCell;
    }

    public void setText(String text) {
        int size = xwpfTableCell.getParagraphs().size();
        if(size>0) {
            for(int i=1; i<size-1; i++) {
                xwpfTableCell.removeParagraph(i);
            }
        }
        this.getFirstParagraph(true).get().setText(text);
    }

    public void clear() {
        int size = xwpfTableCell.getParagraphs().size();
        for(int i=0; i<size; i++) {
            xwpfTableCell.removeParagraph(i);
        }
    }

    public void setStyle(DocxStyle style) {
        for(XWPFParagraph xwpfParagraph: xwpfTableCell.getParagraphs()) {
            xwpfParagraph.setStyle(style.getId());
        }
    }

    public DocxTableCell withStyle(DocxStyle style) {
        this.setStyle(style);
        return this;
    }

    public DocxTableCell withText(String text) {
        this.setText(text);
        return this;
    }

    public String getText() {
        return xwpfTableCell.getText();
    }

    public DocxParagraph createParagraph() {
        return new DocxParagraph(xwpfTableCell.addParagraph());
    }

    public DocxParagraph createParagraph(String text) {
        return new DocxParagraph(xwpfTableCell.addParagraph()).withText(text);
    }

    public Optional<DocxParagraph> getFirstParagraph(boolean autoCreate) {
        DocxParagraph docxParagraph;
        Optional<XWPFParagraph> xwpfParagraphOptional = xwpfTableCell.getParagraphs().stream().findFirst();
        if(xwpfParagraphOptional.isPresent()) {
            docxParagraph = new DocxParagraph(xwpfParagraphOptional.get());
        } else {
            if(autoCreate) {
                docxParagraph = this.createParagraph();
            } else {
                docxParagraph = null;
            }
        }

        return Optional.ofNullable(docxParagraph);
    }
}
