package io.leandev.appstack.security.exception;

public class BadCredentialsException extends AuthenticationException {
    public BadCredentialsException(String message) {
        super(message);
    }
    public BadCredentialsException(Throwable cause) {
        super(cause);
    }
    public BadCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }
}
