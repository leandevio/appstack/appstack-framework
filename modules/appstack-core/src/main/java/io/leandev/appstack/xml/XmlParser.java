package io.leandev.appstack.xml;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import java.io.InputStream;

public class XmlParser {
    private SAXReader reader = new SAXReader();

    public XmlDocument parse(InputStream inputStream) throws XmlParserException {
        Document document;
        try {
            document = reader.read(inputStream);
        } catch (DocumentException e) {
            throw new XmlParserException(e);
        }
        XmlDocument xmlDocument = new XmlDocument(document);
        return xmlDocument;
    }
}
