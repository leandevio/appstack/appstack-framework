package io.leandev.appstack.file;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.vfs2.*;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Slf4j
public class SftpFileRepository implements FileRepository {
    private String hostname;
    private int port = 22;
    private String username;
    private String password;

    private FileSystemManager fileSystemManager;
    private FileSystemOptions opts;

    public SftpFileRepository(String hostname, int port, String username, String password) {
        this.hostname = hostname;
        this.port = port;
        this.username = username;
        this.password = password;
        this.opts = new FileSystemOptions();
        try {
            // Resets the FileSystemManager to the default to make sure it's a new start.
            VFS.reset();
            this.fileSystemManager = VFS.getManager();
            SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(
                    opts, "no");
            SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);
        } catch (FileSystemException e) {
            throw new SftpFileRepositoryException(e);
        }
    }

    public SftpFileRepository(String hostname, String username, String password) {
        this(hostname, 22, username, password);
    }

    private String resolve(Path path) {
        String password;
        try {
            password = URLEncoder.encode(this.password, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            password = this.password;
            log.warn("Failed to encode the password.", e);
        }
        return String.format("sftp://%s:%s@%s/%s",  this.username, password, this.hostname, path.toString());
    }

    @Override
    public OutputStream newOutputStream(Path path, OpenOption... openOptions) throws IOException {
        FileObject remote = fileSystemManager.resolveFile(this.resolve(path));
        OutputStream outputStream = remote.getContent().getOutputStream();
        return outputStream;
    }

    @Override
    public void write(Path path, byte[] bytes, OpenOption... openOptions) throws IOException {
        try(OutputStream outputStream = this.newOutputStream(path, openOptions)) {
            outputStream.write(bytes);
        }
    }

    @Override
    public void write(Path path, String text, OpenOption... openOptions) throws IOException {
        try(OutputStream outputStream = this.newOutputStream(path, openOptions)) {
            outputStream.write(text.getBytes(StandardCharsets.UTF_8));
        }
    }

    @Override
    public InputStream newInputStream(Path path) throws IOException {
        FileObject remote = fileSystemManager.resolveFile(this.resolve(path));
        InputStream inputStream = remote.getContent().getInputStream();
        return inputStream;
    }

    @Override
    public boolean exists(Path path) {
        try(FileObject remote = fileSystemManager.resolveFile(this.resolve(path))) {
            return remote.exists();
        } catch (FileSystemException e) {
            throw new SftpFileRepositoryException(e);
        }
    }

    @Override
    public boolean isDirectory(Path path) {
        try(FileObject remote = fileSystemManager.resolveFile(this.resolve(path))) {
            return remote.isFolder();
        } catch (FileSystemException e) {
            throw new SftpFileRepositoryException(e);
        }
    }

    @Override
    public boolean isRegularFile(Path path) {
        try(FileObject remote = fileSystemManager.resolveFile(this.resolve(path))) {
            return remote.isFile();
        } catch (FileSystemException e) {
            throw new SftpFileRepositoryException(e);
        }
    }

    @Override
    public long size(Path path) throws IOException {
        try(FileObject remote = fileSystemManager.resolveFile(this.resolve(path))) {
            return remote.getContent().getSize();
        } catch (FileSystemException e) {
            throw new SftpFileRepositoryException(e);
        }
    }

    @Override
    public boolean deleteIfExists(Path path) throws IOException {
        try(FileObject remote = fileSystemManager.resolveFile(this.resolve(path))) {
            return remote.delete();
        } catch (FileSystemException e) {
            throw new SftpFileRepositoryException(e);
        }
    }

    @Override
    public void delete(Path path) throws IOException {
        try(FileObject remote = fileSystemManager.resolveFile(this.resolve(path))) {
            remote.delete();
        } catch (FileSystemException e) {
            throw new SftpFileRepositoryException(e);
        }
    }

    @Override
    public Path move(Path source, Path target) throws IOException {
        try(FileObject sourceFileObject = fileSystemManager.resolveFile(this.resolve(source));
            FileObject targetFileObject = fileSystemManager.resolveFile(this.resolve(target))) {
            sourceFileObject.moveTo(targetFileObject);
            return target;
        } catch (FileSystemException e) {
            throw new SftpFileRepositoryException(e);
        }
    }

    @Override
    public Stream<Path> list(Path path, boolean hidden) throws IOException {
        List<Path> paths = new ArrayList<>();
        try(FileObject remote = fileSystemManager.resolveFile(this.resolve(path))) {
            for(FileObject child : remote.getChildren()) {
                paths.add(Paths.get(child.getName().getPath()));
            }
        } catch (FileSystemException e) {
            throw new SftpFileRepositoryException(e);
        }
        return paths.stream();
    }

    @Override
    public void close() {
        this.fileSystemManager.close();
    }
}
