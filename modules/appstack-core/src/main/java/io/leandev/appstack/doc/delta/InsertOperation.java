package io.leandev.appstack.doc.delta;

import io.leandev.appstack.util.Model;

public class InsertOperation implements Operation {
    private Content content;
    private Model attributes;

    public InsertOperation(Content content, Model attributes) {
        this.content = content;
        this.attributes = attributes;
    }

    public InsertOperation(Content content) {
        this(content, new Model());
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Model getAttributes() {
        return attributes;
    }

    public void setAttributes(Model attributes) {
        this.attributes = attributes;
    }

    @Override
    public Content content() {
        return getContent();
    }

    @Override
    public Model attributes() {
        return attributes;
    }
}
