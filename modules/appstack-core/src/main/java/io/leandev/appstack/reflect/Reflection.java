package io.leandev.appstack.reflect;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.annotation.Annotation;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

public class Reflection {
    private Reflections reflections;

    public Reflection() {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
                .setUrls(classpaths(staticClassLoader(), contextClassLoader()));

        this.initialize(configurationBuilder);
    }

    public Reflection(String name) {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
                .filterInputsBy(new FilterBuilder().includePackage(name))
                .setUrls(ClasspathHelper.forPackage(name));
        initialize(configurationBuilder);
    }

    private void initialize(ConfigurationBuilder configurationBuilder) {
        this.reflections = new Reflections(configurationBuilder);
    }

    public Set<Class<?>> getTypesAnnotatedWith(Class<? extends Annotation> annotation) {
        return reflections.getTypesAnnotatedWith(annotation);
    }


    public <T> Set<Class<? extends T>> getSubTypesOf(Class<T> type) {
        Set<Class<? extends T>> subTypes = new HashSet<>();

        try {
            for(Class<? extends T> subType : reflections.getSubTypesOf(type)) {
                subTypes.add((Class<? extends T>) Class.forName(subType.getName()));
            }
        } catch(ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        return subTypes;
    }

    public static ClassLoader contextClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    public static ClassLoader staticClassLoader() {
        return Reflection.class.getClassLoader();
    }

    public static Collection<URL> classpaths(ClassLoader... classLoaders) {
        Collection<URL> distinct = new HashSet<>();
        for(ClassLoader classLoader : classLoaders) {
            if(classLoader instanceof URLClassLoader) {
                URL[] urls = ((URLClassLoader) classLoader).getURLs();
                if (urls != null) {
                    distinct.addAll(Arrays.asList(urls));
                }
            }
        }
        return distinct;
    }

}
