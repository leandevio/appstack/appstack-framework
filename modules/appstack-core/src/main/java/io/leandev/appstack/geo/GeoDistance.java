package io.leandev.appstack.geo;

public interface GeoDistance {
    GeoLocation getOrigin();

    default GeoLocation origin() {
        return getOrigin();
    }

    void setOrigin(GeoLocation origin);

    GeoLocation getDestination();

    default GeoLocation distination() {
        return getDestination();
    }

    void setDestination(GeoLocation destination);

    Double getDistance();

    default Double distance() {
        return getDistance();
    }

    void setDistance(Double distance);
}
