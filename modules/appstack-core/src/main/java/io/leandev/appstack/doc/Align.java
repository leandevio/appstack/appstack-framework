package io.leandev.appstack.doc;

public enum Align {
    LEFT, RIGHT, CENTER, JUSTIFY, DISTRIBUTED;
}
