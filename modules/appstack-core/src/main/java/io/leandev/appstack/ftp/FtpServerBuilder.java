package io.leandev.appstack.ftp;

import io.leandev.appstack.util.Environment;

public class FtpServerBuilder {
    Environment environment = Environment.getDefaultInstance();
    FtpServerContext ftpServerContext = new FtpServerContext();

    public static FtpServerBuilder newFtpServer() {
        return new FtpServerBuilder();
    }

    public FtpServerBuilder() {
        ftpServerContext.setPort(environment.propertyAsInteger(FtpPropsKey.FTPPORT, FtpPropsDefault.FTPPORT));
    }

    public FtpServerBuilder onPort(int port) {
        ftpServerContext.setPort(port);
        return this;
    }

    public FtpServerBuilder withUserManager(FtpUserManager userManager) {
        ftpServerContext.setFtpUserManager(userManager);
        return this;
    }

    public FtpServer build() throws FtpException {
        return new FtpServer(ftpServerContext);
    }
}
