package io.leandev.appstack.search;

import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import cz.jirutka.rsql.parser.ast.RSQLOperators;

import java.util.Arrays;

public enum RSQLComparator {
    EQUAL(RSQLOperators.EQUAL), NOT_EQUAL(RSQLOperators.NOT_EQUAL), GREATER_THAN(RSQLOperators.GREATER_THAN), GREATER_THAN_OR_EQUAL(RSQLOperators.GREATER_THAN_OR_EQUAL), LESS_THAN(RSQLOperators.LESS_THAN), LESS_THAN_OR_EQUAL(
            RSQLOperators.LESS_THAN_OR_EQUAL), IN(RSQLOperators.IN), NOT_IN(RSQLOperators.NOT_IN),
    HAS(new ComparisonOperator("=has=", false)),
    BETWEEN(new ComparisonOperator("=between=", true)),
    IS(new ComparisonOperator("=is=", false)),
    IS_NOT(new ComparisonOperator("=isnt=", false));

    private cz.jirutka.rsql.parser.ast.ComparisonOperator operator;

    RSQLComparator(final cz.jirutka.rsql.parser.ast.ComparisonOperator operator) {
        this.operator = operator;
    }

    public static RSQLComparator get(final cz.jirutka.rsql.parser.ast.ComparisonOperator operator) {
        return Arrays.stream(RSQLComparator.values())
                .filter(operation -> operation.getOperator() == operator)
                .findAny().orElse(null);
    }

    public ComparisonOperator getOperator() {
        return operator;
    }
}
