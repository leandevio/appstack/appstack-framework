package io.leandev.appstack.docx;

import org.apache.poi.xwpf.usermodel.XWPFRun;

public class DocxTextRun {
    private XWPFRun xwpfRun;

    protected DocxTextRun(XWPFRun xwpfRun) {
        this.xwpfRun = xwpfRun;
    }

    public void setText(String text) {
        xwpfRun.setText(text);
    }

    public String getText(int position) {
        return xwpfRun.getText(position);
    }
}
