package io.leandev.appstack.ftp;

import io.leandev.appstack.crypto.PasswordEncoder;
import io.leandev.appstack.crypto.PlainPasswordEncoder;
import io.leandev.appstack.util.Environment;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;

public class FtpInMemoryUserManagerBuilder {
    private static Environment environment = Environment.getDefaultInstance();
    private PasswordEncoder passwordEncoder = new PlainPasswordEncoder();
    private Collection<FtpUser> users = Collections.emptyList();

    public static FtpInMemoryUserManagerBuilder newInMemoryUserManager() {
        return new FtpInMemoryUserManagerBuilder();
    }

    public FtpInMemoryUserManagerBuilder withPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
        return this;
    }

    public FtpInMemoryUserManagerBuilder withUsers(Collection<FtpUser> users) {
        Path ftpHome = environment.propertyAsNamedPath(FtpPropsKey.FTPHOME, FtpPropsDefault.FTPHOME, true);
        for(FtpUser user : users) {
            if(user.getHome()==null) {
                user.setHome(ftpHome.resolve(user.getUsername()).toString());
            }
        }
        this.users = users;
        return this;
    }

    public FtpInMemoryUserManager build() {
        return new FtpInMemoryUserManager(passwordEncoder, users);
    }
}
