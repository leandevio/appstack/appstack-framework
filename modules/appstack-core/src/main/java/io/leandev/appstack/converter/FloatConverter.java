package io.leandev.appstack.converter;

public class FloatConverter implements Converter<Float>{
    @Override
    public Float convert(Object value) {
        Float number;
        if(value instanceof Number) {
            number = toFloat((Number) value);
        } else if(value==null) {
            number = null;
        } else if(value instanceof String) {
            number = toFloat((String) value);
        } else {
            throw new ConversionException("Only support convert String and Number to Float.");
        }
        return number;
    }

    private Float toFloat(Number value) {
        Float n = value.floatValue();
        return n;
    }

    private Float toFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            throw new ConversionException(String.format("Failed to convert the value: %s", value), e);
        }
    }
}
