package io.leandev.appstack.constant;

public class CorePropsDefault {
    public static final String SERVERPORT = "4000";

    public static final String LOGLEVEL = "WARN";

    public static final int MAILSMTPPORT = 25;
    public static final boolean MAILSMTPAUTH = false;
    public static final boolean MAILSMTPSTARTTLSENABLE = false;
    public static final boolean MAILDEBUG = false;
    public static final boolean MAILFIREWALLENABLE = true;
    public static final String MAILWHITELIST = "";

    public static final String LOGFILEPATTERN = "${LOG_FILE}.%d{yyyy-MM-dd}.%i.gz";

}
