package io.leandev.appstack.exception;

public class ApplicationException extends RuntimeException {
    private static final String MESSAGE = "An Application Error Has Occurred.";
    private Object[] params = new Object[0];

    public ApplicationException() {
        this(MESSAGE);
    }

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationException(Throwable cause) {
        this(cause.getMessage() == null ? MESSAGE : cause.getMessage(), cause);
    }

    public ApplicationException(String message, Object... params) {
        super(message);
        this.params = params;
    }

    public ApplicationException(String message, Throwable cause, Object... params) {
        super(message, cause);
        this.params = params;
    }

    public Object[] getParams() {
        return params;
    }

    public String toString() {
        return this.getMessage();
    }

    public String defaultMessage() {
        return MESSAGE;
    }
}