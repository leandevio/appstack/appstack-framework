package io.leandev.appstack.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import io.leandev.appstack.converter.BytesConverter;

import java.io.IOException;

public class BytesDeserializer extends StdDeserializer<byte[]> {
    BytesConverter converter = new BytesConverter();

    public BytesDeserializer() {
        this(null);
    }

    public BytesDeserializer(Class<?> valueType) {
        super(valueType);
    }

    @Override
    public byte[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String bytes = jsonParser.getText();
        return converter.convert(bytes);
    }
}
