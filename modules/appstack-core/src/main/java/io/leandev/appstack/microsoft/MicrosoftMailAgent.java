package io.leandev.appstack.microsoft;

import io.leandev.appstack.bean.ObjectCopier;
import io.leandev.appstack.client.ClientAgent;
import io.leandev.appstack.client.ClientCredential;
import io.leandev.appstack.client.ClientException;
import io.leandev.appstack.json.JsonParser;
import io.leandev.appstack.text.TextReader;
import io.leandev.appstack.util.Model;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.message.BasicHeader;

import java.io.IOException;
import java.util.Optional;

@Slf4j
public class MicrosoftMailAgent implements ClientAgent {
    private static String DATE_PATTERN = "yyyy-MM-dd HH:mm:ssX";
    private JsonParser jsonParser = JsonParser.ofDatePattern(DATE_PATTERN);
    private ObjectCopier objectCopier = new ObjectCopier();
    private MicrosoftMailCredential microsoftMailCredential;
    private MicrosoftMailAccessToken microsoftMailAccessToken;

    @Override
    public MicrosoftMailAccessToken connect(ClientCredential clientCredential) {
        this.microsoftMailCredential = (MicrosoftMailCredential) clientCredential;
        return this.acquireToken();
    }

    @Override
    public MicrosoftMailAccessToken acquireToken() {
        if(this.microsoftMailAccessToken == null || this.microsoftMailAccessToken.isExpired()) {
            this.microsoftMailAccessToken = this.fetchToken(microsoftMailCredential);
        }
        return this.microsoftMailAccessToken;
    }

    public MicrosoftMailAccessToken fetchToken(ClientCredential clientCredential) throws ClientException {
        String tenantId = clientCredential.getTenantId();
        String clientId = clientCredential.getClientId();
        String clientSecret = clientCredential.getClientSecret();
        String scopes = clientCredential.getScopes();
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpRequest = new HttpPost("https://login.microsoftonline.com/" + tenantId + "/oauth2/v2.0/token");
        String encodedBody = "client_id=" + clientId + "&scope=" + scopes + "&client_secret=" + clientSecret
                + "&grant_type=client_credentials";
        httpRequest.setEntity(new StringEntity(encodedBody, ContentType.APPLICATION_FORM_URLENCODED));
        httpRequest.addHeader(new BasicHeader("cache-control", "no-cache"));
        Model payload;
        try(CloseableHttpResponse response = client.execute(httpRequest)) {
            if(response.getCode() != HttpStatus.SC_OK) {;
                Optional<MicrosoftClientError> error = readError(response);
                String reason;
                if(error.isPresent()) {
                    reason = String.format("%s(%s)", error.get().getDescription(), error.get().getCode());
                } else {
                    reason = (response == null) ? "Unknown" : response.getReasonPhrase();
                }
                String message = String.format("%d %s", response.getCode(), reason);
                log.warn(message);
                throw new ClientException(message);
            }
            payload = readResponse(response);
        } catch (Exception e) {
            String message = String.format("Failed to execute the request. Reason: %s", e.getMessage());
            log.warn(message, e);
            throw new ClientException(message, e);
        }

        MicrosoftMailAccessToken clientAccessToken = new MicrosoftMailAccessToken();
        objectCopier.copyPropertiesIgnoreCase(clientAccessToken, payload);
        return clientAccessToken;
    }

    private Model readResponse(CloseableHttpResponse response) {
        HttpEntity entity = response.getEntity();
        Model data;
        try(TextReader textReader = new TextReader(entity.getContent())) {
            String text = textReader.readAll();
            log.debug(String.format("Receive the response: %s\n%s", entity.getContentType(), text));
            Model payload = jsonParser.readValue(text, Model.class);
            data = payload;
        } catch(IOException ex) {
            throw new ClientException(String.format("Failed to read the response.", response));
        }
        return data;
    }

    private Optional<MicrosoftClientError> readError(CloseableHttpResponse response) {
        HttpEntity entity = response.getEntity();
        MicrosoftClientError error = null;
        try(TextReader textReader = new TextReader(entity.getContent())) {
            String text = textReader.readAll();
            log.debug(String.format("Receive the response: %s\n%s", entity.getContentType(), text));
            MicrosoftClientError payload = jsonParser.readValue(text, MicrosoftClientError.class);
            error = payload;
            error.setCode(error.getError_codes().get(0).toString());
            error.setDescription(error.getError_description());
        } catch(IOException ex) {
            log.trace("Failed to read the error response.", ex);
        }
        return Optional.ofNullable(error);
    }

    @Override
    public void close() {

    }
}
