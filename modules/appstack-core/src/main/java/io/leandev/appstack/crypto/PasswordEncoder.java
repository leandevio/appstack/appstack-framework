package io.leandev.appstack.crypto;

import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import io.leandev.appstack.util.FactoryProps;

public interface PasswordEncoder {
    Logger LOGGER = LogManager.getLogger(PasswordEncoder.class);
    String PASSWORDENCODER = "io.leandev.appstack.crypto.BCryptPasswordEncoder";

    /**
     * Encode the raw password.
     * @param rawPassword
     * @return
     */
    String encode(CharSequence rawPassword);

    /**
     * Verify the encoded password obtained from storage matches the submitted raw password after it too is encoded.
     * @param rawPassword
     * @param encodedPassword
     * @return
     */
    boolean matches(CharSequence rawPassword, String encodedPassword);

    class SingletonHolder {
        public static final PasswordEncoder INSTANCE = PasswordEncoder.getInstance();

    }

    static PasswordEncoder getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    static PasswordEncoder getInstance() {
        PasswordEncoder passwordEncoder;
        Class<? extends PasswordEncoder> type;
        String name = FactoryProps.getDefaultInstance().property(PasswordEncoder.class, PASSWORDENCODER);
        try {
            type = (Class<? extends PasswordEncoder>) Class.forName(name);
            passwordEncoder = type.newInstance();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to instantiate %s. Check the setting(%s) in the META-INF/appstack.factories in the classpath."
                    , name, PasswordEncoder.class.getCanonicalName()), e);
            throw new RuntimeException(e);
        }

        return passwordEncoder;
    }
}
