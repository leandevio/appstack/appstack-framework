package io.leandev.appstack.search;

public enum Unpaged implements Pageable {
    INSTANCE;

    @Override
    public int page() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException();
    }

    @Override
    public long offset() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Pageable next() {
        return this;
    }

    @Override
    public Pageable previous() {
        return this;
    }

    @Override
    public boolean hasPrevious() {
        return false;
    }

    @Override
    public Pageable go(int page) {
        return this;
    }

    @Override
    public Sort sort() {
        return Sort.unsorted();
    }

    @Override
    public int offsetAsInt() {
        return 0;
    }
}
