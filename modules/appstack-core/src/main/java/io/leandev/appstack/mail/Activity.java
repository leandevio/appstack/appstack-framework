package io.leandev.appstack.mail;

import javax.mail.internet.InternetAddress;
import java.util.*;

public class Activity {
    private String uid = UUID.randomUUID().toString();
    private String subject;
    private String description;
    private InternetAddress organizer;
    private Map<AttendeeType, List<InternetAddress>> attendees = new HashMap<>();
    private Date startDate, endDate;
    private String location;

    public Activity() {
        attendees.put(AttendeeType.REQUIRED, new ArrayList<>());
        attendees.put(AttendeeType.OPTIONAL, new ArrayList<>());
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOrganizer(InternetAddress organizer) {
        this.organizer = organizer;
    }

    public InternetAddress getOrganizer() {
        return organizer;
    }

    public Map<AttendeeType, List<InternetAddress>> getAttendees() {
        return attendees;
    }

    public void setAttendees(Map<AttendeeType, List<InternetAddress>> attendees) {
        this.attendees = attendees;
    }

    public void addAttendee(InternetAddress attendee) {
        this.attendees.get(AttendeeType.REQUIRED).add(attendee);
    }

    public List<InternetAddress> getAttendees(AttendeeType attendeeType) {
        return this.attendees.get(attendeeType);
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

}
