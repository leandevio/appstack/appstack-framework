package io.leandev.appstack.exception;

public class InvalidFormatException extends Exception {
    private static final String MESSAGE = "An exception occurred: Invalid Format.";

    public InvalidFormatException() {
        this(MESSAGE);
    }

    public InvalidFormatException(String message) {
        super(message);
    }
    public InvalidFormatException(Throwable cause) {
        super(cause);
    }
    public InvalidFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
