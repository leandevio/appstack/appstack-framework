package io.leandev.appstack.file;

public class FileRepositoryException extends RuntimeException {
    public FileRepositoryException(String message) {
        super(message);
    }

    public FileRepositoryException(Throwable cause) {
        super(cause);
    }

    public FileRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
