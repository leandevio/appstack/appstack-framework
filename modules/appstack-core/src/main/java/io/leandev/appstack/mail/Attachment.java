package io.leandev.appstack.mail;

import javax.ws.rs.core.MediaType;

public class Attachment {
    private byte[] bytes;
    private String name;
    private MediaType mediaType;

    public Attachment(byte[] bytes, String name, MediaType mediaType) {
        this.bytes = bytes;
        this.name = name;
        this.mediaType = mediaType;
    }

    public byte[] getBytes() {
        return this.bytes;
    }

    public MediaType getMediaType() {
        return this.mediaType;
    }

    public String getName() {
        return this.name;
    }

}
