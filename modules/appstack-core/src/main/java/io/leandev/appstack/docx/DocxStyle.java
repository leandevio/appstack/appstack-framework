package io.leandev.appstack.docx;


import org.apache.poi.xwpf.usermodel.XWPFStyle;

public class DocxStyle {
    private XWPFStyle xwfpStyle;

    public static final String H1 = "Heading1";
    public static final String H2 = "Heading2";
    public static final String H3 = "Heading3";
    public static final String H4 = "Heading4";

    protected DocxStyle(XWPFStyle xwfpStyle) {
        this.xwfpStyle = xwfpStyle;
    }

    public String getName() {
        return xwfpStyle.getName();
    }

    public String getId() {
        return xwfpStyle.getStyleId();
    }
}
