package io.leandev.appstack.ftp;

public class FtpException extends Exception {
    public FtpException(String message) {
        super(message);
    }
    public FtpException(Throwable cause) {
        super(cause);
    }
    public FtpException(String message, Throwable cause) {
        super(message, cause);
    }
}
