package io.leandev.appstack.font;

public enum FontFamily {
    SANS_SERIF("SANS_SERIF", "Sans Serif"),
    TimesNewRoman("times.ttf", "Times New Roman"),
    NotoSansTC("NotoSansTC-Regular.otf", "Noto Sans TC"),
    NotoSerifTC("NotoSerifTC-Regular.otf", "Noto Serif TC"),
    BiauKai("TW-Kai-98_1.ttf", "標楷體"), DFKai_SB("TW-Kai-98_1.ttf", "標楷體");


    private String value;
    private String name;

    FontFamily(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getValue() {
        return this.value;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.name;
    }
}
