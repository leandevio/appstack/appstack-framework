package io.leandev.appstack.csv;

import com.univocity.parsers.common.record.Record;
import io.leandev.appstack.parser.AbstractRecordWrapper;

import java.util.Arrays;
import java.util.List;

public class CsvRecord extends AbstractRecordWrapper<Record> {
    protected CsvRecord(Record data) {
        super(data);
    }

    @Override
    public int size() {
        return data.getValues().length;
    }

    @Override
    public List<String> getValues() {
        return Arrays.asList(data.getValues());
    }

    @Override
    public Object getValue(int index) {
        return data.getString(index);
    }
}
