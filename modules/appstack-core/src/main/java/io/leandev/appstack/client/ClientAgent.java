package io.leandev.appstack.client;

public interface ClientAgent extends AutoCloseable {
    ClientAccessToken connect(ClientCredential clientCredential);
    ClientAccessToken acquireToken() throws ClientException;
}
