package io.leandev.appstack.security.constant;

public class SecurityPropsKey {
    public static final String SECURITYENABLED = "security.enabled";
    public static final String SECURITYWILDCARD = "security.wildcard";
    public static final String SECURITYPRINCIPALS = "security.principals";
    public static final String SECURITYTOKENTTL = "security.token.ttl";
    public static final String SECURITYTOKEYKEY = "security.token.key";
    public static final String SECURITYANONYMOUSENABLED = "security.anonymous.enabled";
    public static final String SECURITYANONYMOUSPRINCIPAL = "security.anonymous.principal";
}
