package io.leandev.appstack.doc;

import java.util.HashMap;
import java.util.Map;

public class Color {
    private java.awt.Color color;

    public Color(String value) {
        String hex;
        if(value.startsWith("#")) {
            hex = value;
        } else if(palette.containsKey(value.toUpperCase())) {
            hex = palette.get(value.toUpperCase()).hex();
        } else {
            hex = Color.BLACK.hex();
        }

        this.color = java.awt.Color.decode(hex);
    }

    public Color(float r, float g, float b) {
        this.color = new java.awt.Color(r, g, b);
    }

    public Color(float r, float g, float b, float a) {
        this.color = new java.awt.Color(r, g, b, a);
    }

    public Color(int r, int g, int b) {
        this.color = new java.awt.Color(r, g, b);
    }

    public Color(int r, int g, int b, int a) {
        this.color = new java.awt.Color(r, g, b, a);
    }

    public int rgb() {
        return this.color.getRGB();
    }

    public String hex() {
        String hexColour = Integer.toHexString(this.rgb() & 0xffffff);
        if (hexColour.length() < 6) {
            hexColour = "000000".substring(0, 6 - hexColour.length()) + hexColour;
        }
        return "#" + hexColour;
    }

    public int red() {
        return color.getRed();
    }

    public int green() {
        return color.getGreen();
    }

    public int blue() {
        return color.getBlue();
    }

    public int alpha() {
        return color.getAlpha();
    }

    public static Color BLUE = new Color("#1976D2");
    public static Color DARKBLUE = new Color("#0D47A1");
    public static Color LIGHTBLUE = new Color("#E3F2FD");

    public static Color GREEN = new Color("#388E3C");
    public static Color DARKGREEN = new Color("#1B5E20");
    public static Color LIGHTGREEN = new Color("#E8F5E9");

    public static Color TEAL = new Color("#009688");
    public static Color DARKTEAL = new Color("#004D40");
    public static Color LIGHTTEAL = new Color("#E0F2F1");

    public static Color ORANGE = new Color("#F57C00");
    public static Color DARKORANGE = new Color("#E65100");
    public static Color LIGHTORANGE = new Color("#FFF3E0");

    public static Color RED = new Color("#D32F2F");
    public static Color DARKRED = new Color("#B71C1C");
    public static Color LIGHTRED = new Color("#FFEBEE");

    public static Color CYAN = new Color("#00ACC1");
    public static Color DARKCYAN = new Color("#006064");
    public static Color LIGHTCYAN = new Color("#E0F7FA");

    public static Color YELLOW = new Color("#FBC02D");
    public static Color DARKYELLOW = new Color("#F57F17");
    public static Color LIGHTYELLOW = new Color("#FFFDE7");

    public static Color PURPLE = new Color("#AB47BC");
    public static Color DARKPURPLE = new Color("#6A1B9A");
    public static Color LIGHTPURPLE = new Color("#F3E5F5");

    public static Color INDIGO = new Color("#3F51B5");
    public static Color DARKINDIGO = new Color("#303F9F");
    public static Color LIGHTINDIGO = new Color("#E8EAF6");

    public static Color GREY = new Color("#607D8B");
    public static Color DARKGREY = new Color("#37474F");
    public static Color LIGHTGREY = new Color("#ECEFF1");

    public static Color BLACK = new Color(0, 0, 0);
    public static Color WHITE = new Color(255, 255, 255);

    public static Map<String, Color> palette = new HashMap<>();
    static {
        palette.put("BLUE", BLUE);
        palette.put("GREEN", GREEN);
        palette.put("TEAL", TEAL);
        palette.put("ORANGE", ORANGE);
        palette.put("RED", RED);
        palette.put("CYAN", CYAN);
        palette.put("YELLOW", YELLOW);
        palette.put("PURPLE", PURPLE);
        palette.put("INDIGO", INDIGO);
        palette.put("GREY", GREY);
        palette.put("BLACK", BLACK);
        palette.put("WHITE", WHITE);
    }
}
