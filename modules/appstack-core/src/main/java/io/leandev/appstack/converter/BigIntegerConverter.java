package io.leandev.appstack.converter;

import java.math.BigInteger;

public class BigIntegerConverter implements Converter<BigInteger> {
    @Override
    public BigInteger convert(Object value) {
        BigInteger number;
        if(value instanceof BigInteger) {
            number = (BigInteger) value;
        } else if(value==null) {
            number = null;
        } else if(value instanceof Number) {
            number = toBigInteger((Number) value);
        } else if(value instanceof String) {
            number = toBigInteger((String) value);
        } else {
            throw new ConversionException("Only support convert String and Number to BigInteger.");
        }
        return number;
    }

    private BigInteger toBigInteger(Number value) {
        return new BigInteger(value.toString());
    }

    private BigInteger toBigInteger(String value) {
        try {
            return new BigInteger(value);
        } catch (NumberFormatException e) {
            throw new ConversionException(String.format("Failed to convert the value: %s", value), e);
        }
    }
}
