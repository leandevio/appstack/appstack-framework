package io.leandev.appstack.docx;

public class DocxException extends Throwable {
    public DocxException(String message) {
        super(message);
    }

    public DocxException(Throwable cause) {
        super(cause);
    }

    public DocxException(String message, Throwable cause) {
        super(message, cause);
    }
}