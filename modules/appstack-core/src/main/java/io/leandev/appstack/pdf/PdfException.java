package io.leandev.appstack.pdf;

public class PdfException extends Throwable {
    public PdfException(String message) {
        super(message);
    }

    public PdfException(Throwable cause) {
        super(cause);
    }

    public PdfException(String message, Throwable cause) {
        super(message, cause);
    }
}
