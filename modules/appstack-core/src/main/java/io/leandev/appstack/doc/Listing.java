package io.leandev.appstack.doc;

public interface Listing extends Block {
    boolean ordered();
}
