package io.leandev.appstack.crypto;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class KeyStoreBuilder {
    private KeyStore keyStore;

    public KeyStoreBuilder(String type) throws CryptoException {
        try {
            this.keyStore = KeyStore.getInstance(type);
        } catch (KeyStoreException e) {
            throw new CryptoException(String.format("Failed to create the keystore with the type: %s", type), e);
        }
    }

    public static KeyStoreBuilder newKeyStore(String type) throws CryptoException {
        return new KeyStoreBuilder(type);
    }

    public static KeyStoreBuilder newPKCS12KeyStore() throws CryptoException {
        return new KeyStoreBuilder("pkcs12");
    }

    public KeyStoreBuilder from(Path file, String password) throws CryptoException {
        try(InputStream inputStream = new FileInputStream(file.toFile())) {
            keyStore.load(inputStream, password.toCharArray());
            return this;
        } catch (IOException | NoSuchAlgorithmException | CertificateException e) {
            throw new CryptoException("Failed to load the keystore.", e);
        }
    }

    public KeyStore build() {
        return keyStore;
    }
}
