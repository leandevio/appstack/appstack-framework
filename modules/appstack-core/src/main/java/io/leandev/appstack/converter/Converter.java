package io.leandev.appstack.converter;

public interface Converter<T> {
    T convert(Object value);
}
