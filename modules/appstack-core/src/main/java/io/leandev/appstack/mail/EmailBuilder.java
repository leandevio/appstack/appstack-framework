package io.leandev.appstack.mail;

import javax.mail.Message;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class EmailBuilder {
    private Charset charset = StandardCharsets.UTF_8;
    private MediaType mediaType;
    private Email email ;

    public static EmailBuilder newEmail() {
        return new EmailBuilder();
    }

    public static EmailBuilder newHtmlEmail() {
        return new EmailBuilder();
    }

    public static EmailBuilder newTextEmail() {
        return new EmailBuilder(MediaType.TEXT_PLAIN_TYPE);
    }

    private EmailBuilder(MediaType mediaType) {
        this.mediaType = mediaType;
        this.email = new Email(charset);
    }

    private EmailBuilder() {
        this(MediaType.TEXT_HTML_TYPE);
    }

    public EmailBuilder from(String address, String name) throws UnsupportedEncodingException {
        InternetAddress internetAddress = new InternetAddress(address, name);
        email.setFrom(internetAddress);
        return this;
    }

    public EmailBuilder from(String address) throws AddressException {
        InternetAddress internetAddress = new InternetAddress(address);
        email.setFrom(internetAddress);
        return this;
    }

    public EmailBuilder to(String address, String name) throws UnsupportedEncodingException {
        InternetAddress internetAddress = new InternetAddress(address, name);
        email.addRecipient(Message.RecipientType.TO, internetAddress);
        return this;
    }

    public EmailBuilder to(String address) throws AddressException {
        InternetAddress internetAddress = new InternetAddress(address);
        email.addRecipient(Message.RecipientType.TO, internetAddress);
        return this;
    }

    public EmailBuilder to(List<String> addresses) throws AddressException {
        for(String address : addresses) {
            this.to(address);
        }
        return this;
    }


    public EmailBuilder cc(String address, String name) throws UnsupportedEncodingException {
        InternetAddress internetAddress = new InternetAddress(address, name);
        email.addRecipient(Message.RecipientType.CC, internetAddress);
        return this;
    }

    public EmailBuilder cc(String address) throws AddressException {
        InternetAddress internetAddress = new InternetAddress(address);
        email.addRecipient(Message.RecipientType.CC, internetAddress);
        return this;
    }

    public EmailBuilder cc(List<String> addresses) throws AddressException {
        for(String address : addresses) {
            this.cc(address);
        }
        return this;
    }

    public EmailBuilder bcc(String address, String name) throws UnsupportedEncodingException {
        InternetAddress internetAddress = new InternetAddress(address, name);
        email.addRecipient(Message.RecipientType.BCC, internetAddress);
        return this;
    }

    public EmailBuilder bcc(String address) throws AddressException {
        InternetAddress internetAddress = new InternetAddress(address);
        email.addRecipient(Message.RecipientType.BCC, internetAddress);
        return this;
    }

    public EmailBuilder bcc(List<String> addresses) throws AddressException {
        for(String address : addresses) {
            this.bcc(address);
        }
        return this;
    }

    public EmailBuilder replyTo(String address) throws AddressException {
        InternetAddress internetAddress = new InternetAddress(address);
        email.addReplyTo(internetAddress);
        return this;
    }

    public EmailBuilder replyTo(List<String> addresses) throws AddressException {
        for(String address : addresses) {
            this.replyTo(address);
        }
        return this;
    }

    public EmailBuilder withSubject(String subject) {
        email.setSubject(subject);
        return this;
    }

    public EmailBuilder withContent(Object data, String text) {
        email.addArticle(data, this.mediaType, this.charset, text);
        return this;
    }

    public EmailBuilder withContent(Object data) {
        return withContent(data, null);
    }

    public EmailBuilder withAttachment(byte[] bytes, String name) {
        email.addAttachment(bytes, name);
        return this;
    }

    public EmailBuilder withAttachment(InputStream inputStream, String name) throws IOException {
        email.addAttachment(inputStream, name);
        return this;
    }

    public EmailBuilder withAttachment(File file) throws IOException {
        email.addAttachment(file);
        return this;
    }

    public Email build() {
        return this.email;
    }
}
