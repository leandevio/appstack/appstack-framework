package io.leandev.appstack.security.domain;

public class UsernamePasswordSecurityToken extends SecurityToken {
    public UsernamePasswordSecurityToken(Object principal, Object credentials) {
        super(principal, credentials);
    }
}
