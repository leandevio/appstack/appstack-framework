package io.leandev.appstack.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Sort {
    private final List<Order> orders;

    public Sort() {
        this.orders = new ArrayList<>();
    }

    public Sort(Direction direction, String... selectors) {
        List<Order> orders = new ArrayList<>();
        for(String selector : selectors) {
            orders.add(new Order(direction, selector));
        }
        this.orders = orders;
    }

    public Sort(String... selectors) {
        List<Order> orders = new ArrayList<>();
        for(String selector : selectors) {
            orders.add(new Order(selector));
        }
        this.orders = orders;
    }

    public Sort(Order... orders) {
        this.orders = Arrays.asList(orders);
    }

    public Sort(List<Order> orders) {
        this.orders = orders;
    }

    public static Sort unsorted() {
        return new Sort();
    }

    public Sort and(Sort... sorts) {
        List<Order> orders = new ArrayList<>();
        for(Sort sort : sorts) {
            orders.addAll(sort.orders());
        }
        return new Sort(orders);
    }

    public List<Order> orders() {
        return this.orders;
    }

    public Stream<Order> stream() {
        return this.orders().stream();
    }

    public boolean isSorted() {
        return !this.orders.isEmpty();
    }

    public boolean isUnsorted() {
        return !this.isSorted();
    }

    public static Sort by(Direction direction, String... selectors) {
        return new Sort(direction, selectors);
    }

    public static Sort by(String... selectors) {
        return new Sort(selectors);
    }

    public static Sort by(Sort... sorts) {
        Sort sort = Sort.unsorted();
        for(Sort any : sorts) {
            sort = sort.and(any);
        }
        return sort;
    }

    public static class Order {
        private final String selector;
        private final Direction direction;
        private static final Direction DEFAULT_DIRECTION = Direction.ASC;

        public Order(Direction direction, String selector) {
            this.direction = direction;
            this.selector = selector;
        }

        public Order(String selector) {
            this.selector = selector;
            this.direction = DEFAULT_DIRECTION;
        }

        public String selector() {
            return this.selector;
        }

        public Direction direction() {
            return this.direction;
        }


        public boolean isAscending() {
            return direction().isAscending();
        }

        public boolean isDescending() {
            return direction().isDescending();
        }
    }

    public enum Direction {
        ASC, DESC;

        Direction() {}

        public boolean isAscending() {
            return this.equals(Direction.ASC);
        }

        public boolean isDescending() {
            return this.equals(Direction.DESC);
        }
    }
}
