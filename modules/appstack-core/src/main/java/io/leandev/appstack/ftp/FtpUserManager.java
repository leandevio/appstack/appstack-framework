package io.leandev.appstack.ftp;

import io.leandev.appstack.crypto.PasswordEncoder;

import java.util.Collection;

public interface FtpUserManager {
    PasswordEncoder passwordEncoder();

    FtpUser get(String username);

    void save(FtpUser user);

    Collection<FtpUser> users();

    default void save(Collection<FtpUser> users) {
        for(FtpUser user : users) {
            save(user);
        }
    }
}
