package io.leandev.appstack.doc;

import javax.ws.rs.core.MediaType;

public interface TextRun extends Chunk<String> {

    @Override
    default MediaType mediaType() {
        return MediaType.TEXT_PLAIN_TYPE;
    }

    TextDecoration textDecoration();

    void setTextDecoration(TextDecoration decoration);

    void setColor(Color color);

    void setBackground(Background background);

}
