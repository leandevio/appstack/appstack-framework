package io.leandev.appstack.crypto;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class PasswordGenerator {
    static List<Character> SYMBOLS = "^$*.[]{}()?-\"!@#%&/\\,><':;|_~`".chars()
            .mapToObj(c -> (char) c)
            .collect(Collectors.toList());
    static List<Character> LOWERCASE = "abcdefghijklmnopqrstuvwxyz".chars()
            .mapToObj(c -> (char) c)
            .collect(Collectors.toList());
    static List<Character> UPPERCASE = (new String("ABCDEFGHIJKLMNOPQRSTUVWXYZ")).chars()
            .mapToObj(c -> (char) c)
            .collect(Collectors.toList());
    static List<Character> NUMBERS = "0123456789".chars()
            .mapToObj(c -> (char) c)
            .collect(Collectors.toList());
    static char[] ALL_CHARS = (new String("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789^$*.[]{}()?-\"!@#%&/\\,><':;|_~`")).toCharArray();
    static Random rand = new SecureRandom();

    private boolean allowSymbol = true;
    private boolean allowLetter = true;
    private int length = 4;

    public PasswordGenerator() {
    }

    public PasswordGenerator(int length) {
        this.setLength(length);
    }

    public PasswordGenerator(int length, boolean allowSymbol) {
        this.setLength(length);
        this.setAllowSymbol(allowSymbol);
    }

    public void setLength(int length) {
        assert length >= 4;
        this.length = length;
    }

    public int getLength() {
        return this.length;
    }

    public String generate() {
        char[] password = new char[length];

        List<Character> characters = new ArrayList<>();
        List<Character> letters = new ArrayList<>();
        letters.addAll(UPPERCASE);
        letters.addAll(LOWERCASE);

        characters.addAll(NUMBERS);
        if(allowLetter) {
            characters.addAll(letters);
        }
        if(allowSymbol) {
            characters.addAll(SYMBOLS);
        }

        int start = 0;
        //get the requirements out of the way
        if(allowLetter) {
            password[start++] = letters.get(rand.nextInt(letters.size()));
        }
        if(allowSymbol) {
            password[start++] = letters.get(rand.nextInt(SYMBOLS.size()));
        }

        //populate rest of the password with random chars
        for (int i = start; i < length; i++) {
            password[i] = characters.get(rand.nextInt(characters.size()));
        }

        //shuffle it up
        for (int i = 0; i < password.length; i++) {
            int randomPosition = rand.nextInt(password.length);
            char temp = password[i];
            password[i] = password[randomPosition];
            password[randomPosition] = temp;
        }

        return new String(password);
    }

    public void setAllowSymbol(boolean allowSymbol) {
        this.allowSymbol = allowSymbol;
    }


    public void setAllowLetter(boolean allowLetter) {
        this.allowLetter = allowLetter;
    }
}
