package io.leandev.appstack.search;

public class ComparisonNode implements Node {
    private final ComparisonOperator operator;
    private final String selector;
    private final Object expectation;

    public ComparisonNode(ComparisonOperator operator, String selector, Object expectation) {
        this.operator = operator;
        this.selector = selector;
        this.expectation = expectation;
    }

    public ComparisonOperator operator() {
        return this.operator;
    }

    public String selector() {
        return this.selector;
    }

    public Object expectation() {
        return this.expectation;
    }
}
