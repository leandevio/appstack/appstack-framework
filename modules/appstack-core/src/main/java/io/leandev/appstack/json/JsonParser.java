package io.leandev.appstack.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeSerializer;
import io.leandev.appstack.bean.Model;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

public class JsonParser {
    private static final String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
    private ObjectMapper mapper = new ObjectMapper();

    public static JsonParser ofDatePattern(String datePattern) {
        return new JsonParser(datePattern);
    }

    public JsonParser() {
        this(DATE_PATTERN);
    }
    protected JsonParser(String datePattern) {
        SimpleDateFormat df = new SimpleDateFormat(datePattern);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.setDateFormat(df);

        SimpleModule module = new SimpleModule();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(datePattern);
        module.addSerializer(ZonedDateTime.class, new ZonedDateTimeSerializer(dateTimeFormatter));
        mapper.registerModule(module);

        module = new SimpleModule();
        module.addDeserializer(ZonedDateTime.class, new ZonedDateTimeDeserializer());
        mapper.registerModule(module);

        module = new SimpleModule();
        module.addDeserializer(Model.class, new ModelDeserializer());
        mapper.registerModule(module);

        module = new SimpleModule();
        module.addDeserializer(byte[].class, new BytesDeserializer());
        mapper.registerModule(module);
    }

    public void register(Class type, JsonSerializer jsonSerializer) {
        SimpleModule module = new SimpleModule();
        module.addSerializer(type, jsonSerializer);
        mapper.registerModule(module);
    }


    public void register(Class type, JsonDeserializer jsonDeserializer) {
        SimpleModule module = new SimpleModule();
        module.addDeserializer(type, jsonDeserializer);
        mapper.registerModule(module);
    }

    /**
     * Parse the json string into an object of the data type or an array of the array data type.
     *
     * @param input
     * @param type
     * @param <T>
     * @return
     * @throws IOException
     */
    public <T> T readValue(Reader input, Class<T> type) {
        try {
            return mapper.readValue(input, type);
        } catch (IOException e) {
            throw new JsonException(e);
        }
    }

    public <T> T readValue(InputStream input, Class<T> type, Charset charset) throws IOException {
        try(InputStreamReader inputStreamReader = new InputStreamReader(input, charset)) {
            return readValue(inputStreamReader, type);
        }
    }

    public <T> T readValue(InputStream input, Class<T> type) throws IOException {
        return readValue(input, type, StandardCharsets.UTF_8);
    }

    public <T> T readValue(InputStreamReader input, Class<T> type) throws IOException {
        return mapper.readValue(input, type);
    }

    public <T> T readValue(String input, Class<T> type) {
        if(input == null) return null;
        try(StringReader stringReader = new StringReader(input)) {
            return readValue(stringReader, type);
        }
    }

    public <T> List<T> readValueAsList(InputStream input, Class<T> type, Charset charset) throws IOException {
        try(InputStreamReader inputStreamReader = new InputStreamReader(input, charset)) {
            return readValueAsList(inputStreamReader, type);
        }

    }

    public <T> List<T> readValueAsList(InputStream input, Class<T> type) throws IOException {
        return readValueAsList(input, type, StandardCharsets.UTF_8);

    }

    public <T> List<T> readValueAsList(InputStreamReader input, Class<T> type) throws IOException {
        T[] objects = (T[]) Array.newInstance(type, 0);
        List<T> beans = (List<T>) Arrays.asList(mapper.readValue(input, objects.getClass()));
        return beans;
    }

    public <T> List<T> readValueAsList(String input, Class<T> type) throws IOException {
        T[] objects = (T[]) Array.newInstance(type, 0);
        List<T> beans = (List<T>) Arrays.asList(mapper.readValue(input, objects.getClass()));
        return beans;
    }

    public <T> T readValue(InputStream input, TypeReference<T> typeRef, Charset charset) throws IOException {
        try(InputStreamReader inputStreamReader = new InputStreamReader(input, charset)) {
            return (T) mapper.readValue(inputStreamReader, typeRef);
        }
    }

    public <T> T readValue(InputStream input, TypeReference<T> typeRef) throws IOException {
        return this.readValue(input, typeRef, StandardCharsets.UTF_8);
    }

    public <T> T readValue(InputStreamReader input, TypeReference<T> typeRef) throws IOException {
        return (T) mapper.readValue(input, typeRef);
    }

    public <T> T readValue(String input, TypeReference<T> typeRef) throws IOException {
        return (T) mapper.readValue(input, typeRef);
    }

//    public Map<String, Object> readValueAsMap(InputStream input) throws IOException {
//        TypeReference<HashMap<String,Object>> typeRef
//                = new TypeReference<HashMap<String,Object>>() {};
//        HashMap<String,Object> map = mapper.readValue(input, typeRef);
//        return map;
//    }
//
//    public Map<String, Object> readValueAsMap(InputStreamReader input) throws IOException {
//        TypeReference<HashMap<String,Object>> typeRef
//                = new TypeReference<HashMap<String,Object>>() {};
//        HashMap<String,Object> map = mapper.readValue(input, typeRef);
//        return map;
//    }
//
//    public Map<String, Object> readValueAsMap(String input) throws IOException {
//        TypeReference<HashMap<String,Object>> typeRef
//                = new TypeReference<HashMap<String,Object>>() {};
//        HashMap<String,Object> map = mapper.readValue(input, typeRef);
//        return map;
//    }

    public String writeValue(Object value) throws Exception {
        String string;
        try {
            string = mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new Exception(e);
        }

        return string;
    }

    public String writeValueAsString(Object value) {
        if(value == null) return null;
        String string = null;
        try {
            string = mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new JsonException(e);
        }

        return string;
    }

    public void writeValue(OutputStream outputStream, Object value, Charset charset) throws IOException {
        try(OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, charset)) {
            mapper.writeValue(outputStreamWriter, value);
        }
    }

    public void writeValue(OutputStream outputStream, Object value) throws IOException {
        this.writeValue(outputStream, value, StandardCharsets.UTF_8);
    }
}
