package io.leandev.appstack.mail;

import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;

import javax.mail.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Folder {
    public static final int READ_ONLY = javax.mail.Folder.READ_ONLY;
    public static final int READ_WRITE = javax.mail.Folder.READ_WRITE;

    private static final Logger LOGGER = LogManager.getLogger(Folder.class);

    private javax.mail.Folder _folder;

    protected static Folder of(javax.mail.Folder folder) {
        return new Folder(folder);
    }

    protected Folder(javax.mail.Folder folder) {
        this._folder = folder;
    }

    public int getEmailCount() throws EmailException {
        try {
            return this._folder.getMessageCount();
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public int getNewEmailCount() throws EmailException {
        try {
            return this._folder.getNewMessageCount();
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public int getUnreadEmailCount() throws EmailException {
        try {
            return this._folder.getUnreadMessageCount();
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public List<Email> fetch() throws EmailException {
        Message[] messages;
        try {
            messages = _folder.getMessages();
        } catch (MessagingException e) {
            throw new EmailException(e);
        }

        List<Email> emails = new ArrayList<>();

        for(Message message : messages) {
            Email email = Email.of(message);
            emails.add(email);
        }

        return emails;
    }


    public List<Email> fetch(long start, long end) throws EmailException, IOException {
        Message[] messages;
        try {
            messages = ((UIDFolder) this._folder).getMessagesByUID(start, end);
        } catch (MessagingException e) {
            throw new EmailException(e);
        }

        List<Email> emails = new ArrayList<>();

        for(Message message : messages) {
            Email email = Email.of(message);
            emails.add(email);
        }

        return emails;
    }

    public List<Email> fetch(long start) throws IOException {
        return fetch(start, UIDFolder.MAXUID);
    }

    /**
     * If this value matches the previously returned UIDVALIDITY for this folder, you can rely on the UIDs.
     * You should use the UID to detect which mails have been added or removed. Note that the content of an email can not be changed without also changing the UID.
     * @return
     */
    public long getUIDValidity() {
        try {
            return (_folder instanceof UIDFolder) ? ((UIDFolder) _folder).getUIDValidity() : -1;
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public void open(int mode) {
        try {
            _folder.open(mode);
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public void open() {
        this.open(Folder.READ_ONLY);
    }

    public void close(boolean expunge) {
        try {
            _folder.close(expunge);
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public void close() throws EmailException {
        this.close(false);
    }

    public boolean exists() {
        try {
            return this._folder.exists();
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public void copyEmail(Email email, Folder folder) {
        try {
            Message[] messages = ((UIDFolder) this._folder).getMessagesByUID(email.getUID(), email.getUID());
            this._folder.copyMessages(messages, folder._folder);
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public void deleteEmail(Email email) throws EmailException {
        try {
            Message[] messages = ((UIDFolder) this._folder).getMessagesByUID(email.getUID(), email.getUID());
            for(Message message : messages) {
                message.setFlag(Flags.Flag.DELETED, true);
            }
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }
}
