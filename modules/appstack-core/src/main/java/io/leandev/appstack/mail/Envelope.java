package io.leandev.appstack.mail;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Envelope {
    private Long uid;
    private Charset charset;
    private Date sentDate;
    private Date receivedDate;
    private String subject;
    private InternetAddress from;
    private Map<Message.RecipientType, List<InternetAddress>> recipients = new HashMap<>();
    private List<InternetAddress> replyTo = new ArrayList<>();

    public Envelope(Charset charset) {
        this.charset = charset;
        recipients.put(Message.RecipientType.TO, new ArrayList<>());
        recipients.put(Message.RecipientType.CC, new ArrayList<>());
        recipients.put(Message.RecipientType.BCC, new ArrayList<>());
    }

    public Envelope() {
        this(StandardCharsets.UTF_8);
    }

    public void setUID(Long uid) {
        this.uid = uid;
    }

    public Long getUID() {
        return this.uid;
    }

    public Charset getCharset() {
        return this.charset;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public InternetAddress getFrom() {
        return from;
    }

    public void setFrom(InternetAddress from) {
        this.from = from;
    }

    public List<InternetAddress> getRecipients(Message.RecipientType type) {
        return recipients.get(type);
    }

    public List<InternetAddress> getAllRecipients() {
        List<InternetAddress> recipients = new ArrayList<>();
        for(List<InternetAddress> addresses : this.recipients.values()) {
            recipients.addAll(addresses);
        }
        return recipients;
    }

    public void setRecipients(Message.RecipientType type, List<InternetAddress> addresses) {
        this.recipients.put(type, addresses);
    }

    public List<InternetAddress> getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(List<InternetAddress> replyTo) {
        this.replyTo = replyTo;
    }

    public void addRecipient(Message.RecipientType type, InternetAddress address) {
        recipients.get(type).add(address);
    }

    public void addRecipients(Message.RecipientType type, List<InternetAddress> addresses) {
        recipients.get(type).addAll(addresses);
    }

    public void addRecipients(Message.RecipientType type, InternetAddress[] addresses) {
        recipients.get(type).addAll(Arrays.asList(addresses));
    }

    public void addReplyTo(InternetAddress address) {
        this.replyTo.add(address);
    }

    public void setReplyTo(InternetAddress[] addresses) {
        this.replyTo.addAll(Arrays.asList(addresses));
    }
}
