package io.leandev.appstack.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import io.leandev.appstack.bean.Model;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ModelDeserializer extends StdDeserializer<Model> {
    public ModelDeserializer() {
        this(null);
    }

    public ModelDeserializer(Class<?> valueType) {
        super(valueType);
    }

    @Override
    public Model deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Map map = deserializationContext.readValue(jsonParser, LinkedHashMap.class);
        return new Model(map);
    }
}