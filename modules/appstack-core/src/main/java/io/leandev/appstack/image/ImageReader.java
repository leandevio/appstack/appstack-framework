package io.leandev.appstack.image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class ImageReader {
    private InputStream stream;

    public static BufferedImage getBufferedImage(InputStream stream) throws IOException {
        return ImageIO.read(stream);
    }

    public ImageReader(InputStream stream) {
        this.stream = stream;
    }

    public BufferedImage read() throws IOException {
        return ImageIO.read(stream);
    }

    public void close() throws IOException {
        this.stream.close();
    }

}
