package io.leandev.appstack.image;

import javax.imageio.ImageIO;
import javax.ws.rs.core.MediaType;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

public class ImageWriter {
    private OutputStream output;

    public static boolean writeBufferedImage(OutputStream output, BufferedImage bufferedImage) throws IOException {
        ImageWriter imageWriter = new ImageWriter(output);
        return imageWriter.write(bufferedImage, "png");
    }

    public static boolean writeBufferedImage(OutputStream output, BufferedImage bufferedImage, MediaType mediaType) throws IOException {
        ImageWriter imageWriter = new ImageWriter(output);
        return imageWriter.write(bufferedImage, mediaType);
    }

    public static boolean writeBufferedImage(OutputStream output, BufferedImage bufferedImage, String format) throws IOException {
        ImageWriter imageWriter = new ImageWriter(output);
        return imageWriter.write(bufferedImage, format);
    }
    public ImageWriter(OutputStream output) {
        this.output = output;
    }

    public boolean write(BufferedImage image) throws IOException {
        return ImageIO.write(image, "png", output);
    }

    public boolean write(BufferedImage image, MediaType mediaType) throws IOException {
        String format = mediaType.getSubtype();
        return ImageIO.write(image, format, output);
    }

    public boolean write(BufferedImage image, String format) throws IOException {
        return ImageIO.write(image, format, output);
    }

    public void close() throws IOException {
        this.output.close();
    }
}
