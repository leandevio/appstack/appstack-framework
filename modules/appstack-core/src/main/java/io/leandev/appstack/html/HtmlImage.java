package io.leandev.appstack.html;

import io.leandev.appstack.doc.Image;

import java.util.Base64;

public class HtmlImage extends HtmlChunk<String> implements Image<String> {
    public static HtmlImage of(Image image) {
        HtmlImage htmlImage = new HtmlImage();
        String content;
        if(image.content() instanceof byte[]) {
            content = encode((byte[]) image.content());
        } else {
            content = image.content().toString();
        }
        htmlImage.setContent(content);
        return htmlImage;
    }

    private static String encode(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    public HtmlImage() {
        super("img");
    }

    public HtmlImage(String source) {
        this();
        this.setContent(source);
    }


    @Override
    public String content() {
        return this.attr("src");
    }

    @Override
    public void setContent(String content) {
        this.attr("src", content);
    }

    @Override
    public void render() {}
}
