package io.leandev.appstack.social;

import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;

import io.leandev.appstack.security.exception.AuthenticationException;
import io.leandev.appstack.json.JsonParser;


import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Google {
    private static final Logger LOGGER = LogManager.getLogger(Google.class);
    private String id;
    private String name;
    private String email;
    private String gender;
    private Date birthday;
    private String familyName;
    private String givenName;

    private static JsonParser parser = new JsonParser();

    public static Google connect(String accessToken) throws IOException {
        String USER_AGENT = "Mozilla/5.0";

        String url = String.format("https://oauth2.googleapis.com/tokeninfo?id_token=%s", accessToken);

        URL obj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

        //add request header
        conn.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = conn.getResponseCode();
        LOGGER.info("Sending 'GET' request to URL : " + url);
        LOGGER.info("Response Code : " + responseCode);
        if(responseCode!=200) {
            Map<String, String> response = parser.readValue(conn.getErrorStream(), HashMap.class);
            String error = response.get("error");
            String error_description = response.get("error_description");
            String message = String.format("%s: %s", error, error_description);
            LOGGER.debug(message);
            throw new AuthenticationException(message);
        }

        Map<String, String> props = parser.readValue(conn.getInputStream(), HashMap.class);

        Google google = new Google(props.get("sub"));
        google.name = props.get("name");
        google.email = props.get("email");
        google.gender = props.get("gender");
        google.givenName = props.get("given_name");
        google.familyName = props.get("family_name");

        return google;
    }

    private Google(String id) {
        this.id = id;
    }

    public String id() {
        return id;
    }

    public String name() {
        return name;
    }

    public String email() {
        return email;
    }

    public String gender() {
        return gender;
    }

    public Date birthday() {
        return birthday;
    }

    public String givenName() {
        return givenName;
    }

    public String familyName() {
        return familyName;
    }
}
