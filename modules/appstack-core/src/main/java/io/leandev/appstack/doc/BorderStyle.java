package io.leandev.appstack.doc;

public enum BorderStyle {
    NONE, SOLID, DOTTED, DASHED, DOUBLE;
}
