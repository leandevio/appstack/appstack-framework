package io.leandev.appstack.image;

public enum Position {
    CENTER("center"), 
    TOP("top"), 
    TOP_LEFT("top-left"),
    TOP_RIGHT("top-right"),
    BOTTOM("bottom"),
    BOTTOM_LEFT("bottom-left"),
    BOTTOM_RIGHT("bottom-right"),
    LEFT("left"),
    LEFT_TOP("left-top"),
    LEFT_BOTTOM("left-bottom"),
    RIGHT("right"),
    RIGHT_TOP("right-top"),
    RIGHT_BOTTOM("right-bottom");

    private final String name;

    Position(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }
}
