package io.leandev.appstack.util;

import io.leandev.appstack.converter.Converters;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

public class Model extends LinkedHashMap<String, Object> implements Map<String, Object> {
    Converters converters = new Converters();

    public static Model of(Map<String, Object> map) {
        Model model = new Model();
        if(map!=null) model.putAll(map);
        return model;
    }

    public Object getValue(String name) {
        return super.get(name);
    }

    public <T> T getValue(String name, Class<T> type) {
        return converters.convert(this.getValue(name), type);
    }

    public Object getValue(int index) {
        Iterator<Entry<String, Object>> iterator = this.entrySet().iterator();
        for(int i=0; i<index; i++) {
            iterator.next();
        }
        Entry<String, Object> entry = iterator.next();
        return entry.getValue();
    }

    public <T> T getValue(int index, Class<T> type) {
        return converters.convert(this.getValue(index), type);
    }

    public List<Object> getValues() {
        return this.entrySet().stream().map(entry -> entry.getValue()).collect(Collectors.toList());
    }

    public String getAsString(String name) {
        return converters.convert(this.getValue(name), String.class);
    }

    public String getAsString(int i) {
        return converters.convert(this.getValue(i), String.class);
    }

    public String getAsString(String name, String defaultValue) {
        Object value = super.get(name);

        return value==null ? defaultValue : converters.convert(value, String.class);
    }

    public Integer getAsInteger(String name) {
        return converters.convert(super.get(name), Integer.class);
    }

    public Long getAsLong(String name) {
        return converters.convert(super.get(name), Long.class);
    }

    public BigInteger getAsBigInteger(String name) {
        return converters.convert(super.get(name), BigInteger.class);
    }

    public Float getAsFloat(String name) {
        return converters.convert(super.get(name), Float.class);
    }

    public Double getAsDouble(String name) {
        return converters.convert(super.get(name), Double.class);
    }

    public BigDecimal getAsBigDecimal(String name) {
        return converters.convert(super.get(name), BigDecimal.class);
    }

    public Boolean getAsBoolean(String name) {
        return converters.convert(super.get(name), Boolean.class);
    }

    public Date getAsDate(String name) {
        return converters.convert(super.get(name), Date.class);
    }

    public List<?> getAsList(String name) {
        return (List<?>) super.get(name);
    }

    public Map<String, ?> getAsMap(String name) {
        return (Map<String, ?>) super.get(name);
    }
}
