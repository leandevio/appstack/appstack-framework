package io.leandev.appstack.csv;

import com.univocity.parsers.common.record.Record;
import io.leandev.appstack.parser.AbstractResultWrapper;

import java.util.Iterator;

public class CsvResult extends AbstractResultWrapper<CsvRecord, Record> {
    public CsvResult(Iterator<Record> iterator) {
        super(iterator);
    }

    @Override
    protected CsvRecord toRecord(Record data) {
        CsvRecord record = new CsvRecord(data);
        return record;
    }

}
