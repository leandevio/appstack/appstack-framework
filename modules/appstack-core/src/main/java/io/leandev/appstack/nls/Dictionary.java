package io.leandev.appstack.nls;

import java.util.HashMap;
import java.util.Locale;

public class Dictionary extends HashMap<String, Book> {
    private Locale locale;

    public Dictionary(Locale locale) {
        super();
        this.locale = locale;
    }

    public Locale locale() {
        return this.locale;
    }
}