package io.leandev.appstack.xml;

import org.dom4j.Attribute;
import org.dom4j.Element;
import org.dom4j.Node;

import java.util.List;
import java.util.stream.Collectors;

public class XmlElement {
    private Element element;
    protected XmlElement(Element element) {
        this.element = element;
    }

    public String tagName() {
        return this.element.getName();
    }

    public String attr(String name) {
        Attribute attribute = this.element.attribute(name);
        return attribute==null ? null : attribute.getValue();
    }


    public XmlElement select(String xpathExpression) {
        Node node = this.element.selectSingleNode(xpathExpression);
        XmlElement xmlElement;
        if(node!=null && node instanceof Element) {
            xmlElement = new XmlElement((Element) node);
        } else {
            xmlElement = null;
        }
        return xmlElement;
    }

    public List<XmlElement> selectAll(String xpathExpression) {
        List<Node> nodes = this.element.selectNodes(xpathExpression);
        List<XmlElement> xmlElements = nodes.stream()
                .filter(node -> node!=null && node instanceof Element)
                .map(node -> new XmlElement((Element) node))
                .collect(Collectors.toList());
        return xmlElements;
    }

    public String text() {
        return this.element.getText();
    }
}
