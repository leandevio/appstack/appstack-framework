package io.leandev.appstack.processor;

import io.leandev.appstack.util.Environment;

import java.util.Map;

public interface EnvironmentProcessor {
    void postProcess(Environment environment);
    void postProcess(Environment environment, Map<String, Object> props);
}
