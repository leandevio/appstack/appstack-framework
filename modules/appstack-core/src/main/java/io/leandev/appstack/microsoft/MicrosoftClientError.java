package io.leandev.appstack.microsoft;

import io.leandev.appstack.client.ClientError;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MicrosoftClientError extends ClientError {
    private String error;
    private String error_description;
    private List<Integer> error_codes;
    private String trace_id;
    private String correlation_id;
    private String error_uri;
}
