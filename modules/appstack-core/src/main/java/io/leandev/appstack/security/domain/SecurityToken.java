package io.leandev.appstack.security.domain;

public class SecurityToken {
    private Object principal;
    private Object credentials;

    public SecurityToken(Object principal) {
        this.principal = principal;
    }

    public SecurityToken(Object principal, Object credentials) {
        this.principal = principal;
        this.credentials = credentials;
    }

    public Object principal() {
        return principal;
    }

    public Object getPrincipal() {
        return principal();
    }

    public void setPrincipal(Object principal) {
        this.principal = principal;
    }

    public Object credentials() {
        return credentials;
    }

    public Object getCredentials() {
        return credentials();
    }

    public void setCredentials(Object credentials) {
        this.credentials = credentials;
    }
}
