package io.leandev.appstack.html;

import io.leandev.appstack.doc.Chunk;

public abstract class HtmlChunk<T> extends HtmlElement implements Chunk<T> {
    public HtmlChunk(String tagName) {
        super(tagName);
    }
}
