package io.leandev.appstack.security.processor;

import io.leandev.appstack.processor.EnvironmentProcessor;
import io.leandev.appstack.security.constant.SecurityPropsDefault;
import io.leandev.appstack.security.constant.SecurityPropsKey;
import io.leandev.appstack.util.Environment;
import java.util.Map;

public class SecurityEnvironmentProcessor implements EnvironmentProcessor {

    @Override
    public void postProcess(Environment environment) {
        environment.setProperty(SecurityPropsKey.SECURITYENABLED, SecurityPropsDefault.SECURITYENABLED, false);
        environment.setProperty(SecurityPropsKey.SECURITYTOKENTTL, SecurityPropsDefault.SECURITYTOKENTTL, false);
        environment.setProperty(SecurityPropsKey.SECURITYTOKEYKEY, SecurityPropsDefault.SECURITYTOKENKEY, false);
        environment.setProperty(SecurityPropsKey.SECURITYANONYMOUSENABLED, SecurityPropsDefault.SECURITYANONYMOUSENABLED, false);
        environment.setProperty(SecurityPropsKey.SECURITYANONYMOUSPRINCIPAL, SecurityPropsDefault.SECURITYANONYMOUSPRINCIPAL, false);
    }

    @Override
    public void postProcess(Environment environment, Map<String, Object> props) {
    }

}
