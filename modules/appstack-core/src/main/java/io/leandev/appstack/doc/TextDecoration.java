package io.leandev.appstack.doc;

public class TextDecoration {
    private Line line = Line.NONE;
    private Style style = Style.NORMAL;
    private Variant variant = Variant.NORMAL;
    private Weight weight = Weight.NORMAL;
    private Size size = Size.MEDIUM;

    public void setLine(Line line) {
        this.line = line;
    }

    public Line getLine() {
        return line;
    }

    public Weight getWeight() {
        return weight;
    }

    public Size getSize() {
        return size;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Variant getVariant() {
        return variant;
    }

    public void setVariant(Variant variant) {
        this.variant = variant;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public enum Line {
        UNDERLINE,
        OVERLINE,
        STRIKE,
        NONE
    }

    public enum Variant {
        SUPER,
        SUB,
        NORMAL
    }

    public enum Weight {
        BOLDER,
        BOLD,
        NORMAL,
        LIGHTER
    }

    public enum Style {
        NORMAL,
        ITALIC,
        OBLIQUE
    }

    public enum Size {
        XXSMALL("xx-small"),
        XSMALL("x-small"),
        SMALL("small"),
        MEDIUM("medium"),
        LARGE("large"),
        XLARGE("x-large"),
        XXLARGE("xx-large");

        private String value;

        Size(String value) {
            this.value = value;
        }

        public String value() {
            return this.value;
        }

    }
}
