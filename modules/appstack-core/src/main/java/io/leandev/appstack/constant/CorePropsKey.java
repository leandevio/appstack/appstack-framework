package io.leandev.appstack.constant;

public class CorePropsKey {
    public static final String SERVERPORT = "server.port";
    public static final String LOGLEVEL = "log.level";

    public static final String MAILSMTPHOST = "mail.smtp.host";
    public static final String MAILSMTPPORT = "mail.smtp.port";
    public static final String MAILSMTPAUTH = "mail.smtp.auth";
    public static final String MAILSMTPUSERNAME = "mail.smtp.username";
    public static final String MAILSMTPPASSWORD = "mail.smtp.password";
    public static final String MAILSMTPSTARTTLSENABLE = "mail.smtp.starttls.enable";
    public static final String MAILDEBUG = "mail.debug";
    public static final String MAILFIREWALLENABLE = "mail.firewall.enable";

    public static final String MAILSMTPSSLENABLE = "mail.smtp.ssl.enable";
    public static final String MAILWHITELIST = "mail.whitelist";

    public static final String LOGFILENAME = "logging.file.name";
    public static final String LOGFILEPATTERN = "logging.logback.rollingpolicy.file-name-pattern";

}
