package io.leandev.appstack.html;

import io.leandev.appstack.doc.*;

import java.util.ArrayList;
import java.util.List;

public abstract class HtmlBlock extends HtmlElement implements Block {
    protected List<HtmlChunk> chunks = new ArrayList<>();
    private Align align = null;

    public HtmlBlock(String tagName) {
        super(tagName);
    }

    @Override
    public List<HtmlChunk> chunks() {
        return chunks;
    }

    @Override
    public void setAlign(Align align) {
        this.align = align;
        String value = (align==null) ? null : align.name();
        this.css("text-align", value);
    }

    @Override
    public HtmlChunk appendChunk(Chunk chunk) {
        HtmlChunk htmlChunk;
        if(chunk instanceof HtmlChunk) {
            htmlChunk = (HtmlChunk) chunk;
        } else if(chunk instanceof TextRun) {
            htmlChunk = HtmlTextRun.of((TextRun) chunk);
        } else if(chunk instanceof Image) {
            htmlChunk = HtmlImage.of((HtmlImage) chunk);
        } else {
            htmlChunk = HtmlTextRun.of(chunk.content().toString());
        }
        this.append(htmlChunk);

        this.chunks.add(htmlChunk);

        return htmlChunk;
    }
}
