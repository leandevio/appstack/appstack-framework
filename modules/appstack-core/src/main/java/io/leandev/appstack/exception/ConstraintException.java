package io.leandev.appstack.exception;

import java.util.HashSet;
import java.util.Set;

public class ConstraintException extends ApplicationException {
    private static final String MESSAGE = "Constraint violation. Please verify input and try again.";

    private Set<Violation> violations = new HashSet<>();

    public ConstraintException() {
        this(MESSAGE);
    }

    public ConstraintException(String message) {
        super(message);
    }

    public ConstraintException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConstraintException(Throwable cause) {
        this(MESSAGE, cause);
    }

    public ConstraintException(String message, Object... parameters) {
        super(message, parameters);
    }

    public ConstraintException(String message, Throwable cause, Object... parameters) {
        super(message, cause, parameters);
    }

    public ConstraintException(Violation violation) {
        this();
        this.violations.add(violation);
    }

    public ConstraintException(Set<Violation> violations) {
        this();
        this.violations.addAll(violations);
    }

    public ConstraintException(Set<Violation> violations, String message, Object... parameters) {
        super(message, parameters);
        this.violations.addAll(violations);
    }


    public ConstraintException(Violation violation, String message, Object... parameters) {
        super(message, parameters);
        this.violations.add(violation);
    }

    public ConstraintException(Set<Violation> violations, Throwable cause) {
        this(MESSAGE, cause);
        this.violations.addAll(violations);
    }

    public ConstraintException(Violation violation, Throwable cause) {
        this(MESSAGE, cause);
        this.violations.add(violation);
    }

    public ConstraintException(Set<Violation> violations, String message, Throwable cause, Object... parameters) {
        super(message, cause, parameters);
        this.violations.addAll(violations);
    }

    public Set<Violation> getViolations() {
        return this.violations;
    }

    public String toString() {
        return this.getMessage();
    }

    @Override
    public String defaultMessage() {
        return MESSAGE;
    }

}
