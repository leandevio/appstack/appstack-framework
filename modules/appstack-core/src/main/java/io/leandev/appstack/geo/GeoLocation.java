package io.leandev.appstack.geo;

/**
 * South latitudes are negative, east longitudes are positive
 */
public class GeoLocation {
    private double latitude;
    private double longitude;

    public GeoLocation(double latitude, double longtitude) {
        this.latitude = latitude;
        this.longitude = longtitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double latitude() {
        return getLatitude();
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }


    public double longitude() {
        return getLongitude();
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
