package io.leandev.appstack.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *  將資料用指定的 Algorithm 加密/解密.
 *
 * Created by jasonlin on 2/21/14.
 */
public interface Encryptor {

    String encrypt(String plainText) throws CryptoException;

    String decrypt(String cipherText) throws CryptoException;

    byte[] encrypt(byte[] plainText) throws CryptoException;

    byte[] decrypt(byte[] cipherText) throws CryptoException;

    // hex representation
    default String hex(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte b : bytes) {
            result.append(String.format("%02x", b));
        }
        return result.toString();
    }

    // print hex with block size split
    default String hexWithBlockSize(byte[] bytes, int blockSize) {

        String hex = hex(bytes);

        // one hex = 2 chars
        blockSize = blockSize * 2;

        // better idea how to print this?
        List<String> result = new ArrayList<>();
        int index = 0;
        while (index < hex.length()) {
            result.add(hex.substring(index, Math.min(index + blockSize, hex.length())));
            index += blockSize;
        }

        return result.toString();
    }

    void encrypt(InputStream inputStream, OutputStream outputStream) throws CryptoException, IOException;

    void decrypt(InputStream inputStream, OutputStream outputStream) throws CryptoException, IOException;
}
