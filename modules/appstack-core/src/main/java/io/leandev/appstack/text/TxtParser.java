package io.leandev.appstack.text;

import java.io.InputStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class TxtParser {
    private TxtFormat txtFormat;

    public TxtParser(TxtFormat txtFormat) {
        this.txtFormat = txtFormat;
    }

    public Iterable<TxtRecord> iterate(InputStream in) {
        TxtResult txtResult = parse(in);
        return () -> txtResult;
    }

    public Stream<TxtRecord> stream(InputStream in) {
        Iterable<TxtRecord> result = this.iterate(in);
        Stream<TxtRecord> stream  = StreamSupport.stream(result.spliterator(), false);
        return stream;
    }

    public TxtResult parse(InputStream in) {
        TextFileReader reader = new TextFileReader(in);
        TxtResult txtResult = new TxtResult(reader.lines(), this.txtFormat());
        return txtResult;
    }

    public TxtFormat txtFormat() {
        return txtFormat;
    }
}
