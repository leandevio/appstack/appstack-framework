package io.leandev.appstack.social;

public class FacebookException extends RuntimeException {
    public FacebookException(String message) {
        super(message);
    }
    public FacebookException(Throwable cause) {
        super(cause);
    }
    public FacebookException(String message, Throwable cause) {
        super(message, cause);
    }
}
