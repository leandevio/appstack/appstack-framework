package io.leandev.appstack.cache;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.expiry.ExpiryPolicy;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class CacheBuilder<K, V> {
    private static long HEAP_SIZE = 50;
    private static Duration EXPIRATION = Duration.of(5, ChronoUnit.MINUTES);

    private CacheManager cacheManager;
    private String name;
    private Class<K> keyType;
    private Class<V> valueType;
    private ResourcePoolsBuilder resourcePoolsBuilder;
    private ExpiryPolicy expiryPolicy;


    public CacheBuilder(CacheManager cacheManager, String name, Class<K> keyType, Class<V> valueType) {
        this.cacheManager = cacheManager;
        this.name = name;
        this.keyType = keyType;
        this.valueType = valueType;
        this.resourcePoolsBuilder = ResourcePoolsBuilder.newResourcePoolsBuilder().heap(HEAP_SIZE);
        this.expiryPolicy = ExpiryPolicyBuilder.timeToLiveExpiration(EXPIRATION);

    }

    public static CacheBuilder newCache(CacheManager cacheManager, String name, Class<?> keyType, Class<?> valueType) {
        return new CacheBuilder(cacheManager, name, keyType, valueType);
    }

    public CacheBuilder heap(long size) {
        resourcePoolsBuilder.heap(size);
        return this;
    }


    public CacheBuilder offheap(long size) {
        resourcePoolsBuilder.offheap(size, MemoryUnit.MB);
        return this;
    }

    public CacheBuilder disk(long size) {
        resourcePoolsBuilder.disk(size, MemoryUnit.MB);
        return this;
    }

    public CacheBuilder withTimeToLiveExpiration(Duration duration) {
        expiryPolicy = ExpiryPolicyBuilder.timeToLiveExpiration(duration);
        return this;
    }

    public CacheBuilder withTimeToLiveExpirationByMinutes(long duration) {
        return withTimeToLiveExpiration(Duration.of(duration, ChronoUnit.MINUTES));
    }

    public CacheBuilder withTimeToIdleExpiration(Duration duration) {
        expiryPolicy = ExpiryPolicyBuilder.timeToIdleExpiration(duration);
        return this;
    }

    public CacheBuilder withTimeToIdleExpirationByMinutes(long duration) {
        return withTimeToIdleExpiration(Duration.of(duration, ChronoUnit.MINUTES));
    }

    public CacheBuilder withNoExpiration() {
        expiryPolicy = ExpiryPolicyBuilder.noExpiration();
        return this;
    }

    public Cache<K, V> build() {
        CacheConfigurationBuilder cacheConfigurationBuilder =  CacheConfigurationBuilder.newCacheConfigurationBuilder(keyType, valueType, resourcePoolsBuilder);
        cacheConfigurationBuilder.withExpiry(expiryPolicy);
        Cache<K, V> cache = cacheManager.createCache(name, cacheConfigurationBuilder.build());
        return cache;

    }

}
