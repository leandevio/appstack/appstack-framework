package io.leandev.appstack.scheduling;

public class JobExecutionException extends Exception {
    public JobExecutionException(String message) {
        super(message);
    }
    public JobExecutionException(Throwable cause) {
        super(cause);
    }
    public JobExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
