package io.leandev.appstack.security.domain;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class UserInfo {
    /** 用戶名稱 */
    private String username;
    /** 密碼 */
    private String password;
    /** 啟用日期 */
    private Date activationDate;
    /** 失效日期 */
    private Date expiryDate;
    /** 停權日期 */
    private Date suspensionDate;
    /** 授權 */
    private List<String> authorities = Collections.emptyList();

    public UserInfo(String username) {
        this.username = username;
    }

    public UserInfo(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getSuspensionDate() {
        return suspensionDate;
    }

    public void setSuspensionDate(Date suspensionDate) {
        this.suspensionDate = suspensionDate;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

    public boolean isActivated() {
        Date now = new Date();
        return (this.activationDate!=null && this.activationDate.compareTo(now)<=0);
    }

    public boolean isExpired() {
        Date now = new Date();
        return (this.expiryDate!=null && this.expiryDate.compareTo(now)<=0);
    }

    public boolean isSuspended() {
        Date now = new Date();
        return (this.suspensionDate!=null && this.suspensionDate.compareTo(now)<=0);
    }
}