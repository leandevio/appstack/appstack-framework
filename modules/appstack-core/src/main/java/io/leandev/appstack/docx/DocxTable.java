package io.leandev.appstack.docx;

import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;

import java.util.List;
import java.util.stream.Collectors;

public class DocxTable {
    private XWPFTable xwpfTable;

    protected DocxTable(XWPFTable xwpfTable) {
        this.xwpfTable = xwpfTable;
    }

    protected XWPFTable getXwpfTable() {
        return this.xwpfTable;
    }

    public DocxTableRow createRow() {
        return new DocxTableRow(this.xwpfTable.createRow());
    }

    public List<DocxTableRow> getRows() {
        return this.xwpfTable.getRows().stream()
                .map(DocxTableRow::new)
                .collect(Collectors.toList());
    }

    public DocxTableRow getRow(int position) {
        return new DocxTableRow(this.xwpfTable.getRow(position));
    }

    public int getNumberOfRows() {
        return this.xwpfTable.getNumberOfRows();
    }


    public void removeRow(int position) {
        xwpfTable.removeRow(position);
    }

    public void copyTo(DocxTable table) {
        XWPFTable xwpfSource = this.getXwpfTable();
        XWPFTable xwpfTarget = table.getXwpfTable();
        xwpfTarget.getCTTbl().setTblPr(xwpfSource.getCTTbl().getTblPr());
        xwpfTarget.getCTTbl().setTblGrid(xwpfSource.getCTTbl().getTblGrid());
        for (int r = 0; r<xwpfSource.getRows().size(); r++) {
            XWPFTableRow targetRow = xwpfTarget.createRow();
            XWPFTableRow row = xwpfSource.getRows().get(r);
            targetRow.getCtRow().setTrPr(row.getCtRow().getTrPr());
            for (int c=0; c<row.getTableCells().size(); c++) {
                //newly created row has 1 cell
                XWPFTableCell targetCell = c==0 ? targetRow.getTableCells().get(0) : targetRow.createCell();
                XWPFTableCell cell = row.getTableCells().get(c);
                targetCell.getCTTc().setTcPr(cell.getCTTc().getTcPr());
                XmlCursor cursor = targetCell.getParagraphArray(0).getCTP().newCursor();
                for (int p = 0; p < cell.getParagraphs().size(); p++) {
                    XWPFParagraph par = (XWPFParagraph) cell.getParagraphs().get(p);
                    XWPFParagraph targetPar = targetCell.insertNewParagraph(cursor);
                    cursor.toNextToken();
                    new DocxParagraph(par).copyTo(new DocxParagraph(targetPar));
                }
                //newly created cell has one default paragraph we need to remove
                targetCell.removeParagraph(targetCell.getParagraphs().size()-1);
            }
        }
        //newly created table has one row by default. we need to remove the default row.
        xwpfTarget.removeRow(0);
    }
}
