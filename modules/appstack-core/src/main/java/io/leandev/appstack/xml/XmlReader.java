package io.leandev.appstack.xml;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class XmlReader implements AutoCloseable {
    private InputStream inputStream;

    public XmlReader(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public XmlReader(String text) {
        this.inputStream = new ByteArrayInputStream(text.getBytes());
    }

    public Document readAll() {
        Document document;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(inputStream);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new XmlException(e);
        }
        return document;
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }
}
