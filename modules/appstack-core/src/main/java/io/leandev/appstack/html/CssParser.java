package io.leandev.appstack.html;

import com.steadystate.css.parser.CSSOMParser;
import com.steadystate.css.parser.SACParserCSS3;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSStyleSheet;

import java.io.IOException;
import java.io.StringReader;

public class CssParser {
    public CSSStyleSheet parse(String css) throws IOException {
        InputSource source = new InputSource(new StringReader(css));
        CSSOMParser parser = new CSSOMParser(new SACParserCSS3());
        CSSStyleSheet sheet = parser.parseStyleSheet(source, null, null);

        return sheet;
    }
}
