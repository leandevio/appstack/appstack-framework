package io.leandev.appstack.security.domain;

import java.util.List;

public class AccessToken {
    private Object principal;
    private List<String> authorities;

    public AccessToken() {}

    public AccessToken(Object principal) {
        this.principal = principal;
    }

    public AccessToken(Object principal, List<String> authorities) {
        this.principal = principal;
        this.authorities = authorities;
    }

    public Object getPrincipal() {
        return principal;
    }

    public void setPrincipal(Object principal) {
        this.principal = principal;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }
}
