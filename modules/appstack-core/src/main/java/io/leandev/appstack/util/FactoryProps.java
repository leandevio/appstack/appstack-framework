package io.leandev.appstack.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FactoryProps {

    private Properties props = new Properties();

    private static class SingletonHolder {
        private static final FactoryProps INSTANCE = new FactoryProps();
    }

    public static FactoryProps getDefaultInstance() {
        return FactoryProps.SingletonHolder.INSTANCE;
    }

    public FactoryProps() {
        initialize();
    }

    private void initialize() {
        String propFileName = "META-INF/appstack.factories";
        try (InputStream input = FactoryProps.class.getClassLoader().getResourceAsStream(propFileName)) {
            if(input!=null) {
                props.load(input);
            } else {
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String property(Class<?> type, String defaultValue) {
        String key = type.getCanonicalName();

        return props.containsKey(key) ? props.getProperty(key) : defaultValue;
    }
}
