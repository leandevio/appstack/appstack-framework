package io.leandev.appstack.util;

import io.leandev.appstack.converter.Converters;
import org.apache.commons.beanutils.BeanMap;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

public class Item<T> implements Map<String, Object> {
    private Converters converters = new Converters();

    private BeanMap bm = new BeanMap();

    public Item(T bean) {
        this.setBean(bean);
    }

    public Item() {}

    public T getBean() {
        return (T) bm.getBean();
    }

    public void setBean(T bean) {
        this.bm.setBean(bean);
    }

    public T bean() {
        return getBean();
    }

    @Override
    public int size() {
        return bm.size();
    }

    @Override
    public boolean isEmpty() {
        return bm.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return bm.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return bm.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return bm.get(key);
    }

    public String getAsString(Object key) {
        return (String) get(key);
    }

    public int getAsInt(Object key) {
        return (int) get(key);
    }

    @Override
    public Object put(String key, Object value) {
        if(bm.getWriteMethod(key)==null) {
            throw new IllegalArgumentException(String.format("Failed to put: %s is not writable.", key));
        }
        Type targetType = bm.getWriteMethod(key).getGenericParameterTypes()[0];
        Class<?> targetRawType = getRawType(targetType);
        if(java.util.Collection.class.isAssignableFrom(targetRawType) && bm.get(key)!=null
                && (value instanceof java.util.Collection || value.getClass().isArray())) {
            java.util.Collection collection;
            if(value.getClass().isArray()) {
                collection = Arrays.asList(value);
            } else {
                collection = (java.util.Collection) value;
            }
            java.util.Collection target = (java.util.Collection) bm.get(key);
            target.clear();
            Type[] paraTypes = getTypeArguments(targetType);
            if(paraTypes==null) {
                target.addAll(collection);
            } else {
                Type paraType = paraTypes[0];
                List data = new ArrayList<>();
                for(Object datum : collection) {
                    data.add(convert(datum, getRawType(paraType)));
                }
                target.addAll(data);
            }
        } else {
            try {
                bm.put(key, convert(value, targetRawType));
            } catch (Exception e) {
                String message = String.format("%s. Failed to set the property(%s). The value is %s(%s).", e.getMessage(), key, value, targetRawType);
                throw new IllegalArgumentException(message, e.getCause());
            }
        }
        
        return bm.get(key);
    }

    @Override
    public Object remove(Object key) {
        return bm.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ?> source) {
        this.putAll(source, new String[0]);
    }

    public void copyProperties(Object source, String... excludes) {
        List<String> list = Arrays.asList(excludes);
        Map bean = source instanceof Map ? (Map) source : new BeanMap(source);
        for(Object key : bean.keySet()) {
            if(list.contains(key)) continue;
            if(bm.getWriteMethod((String) key)==null) continue;
            Object value = bean.get(key);
            this.put((String) key, value);
        }
    }

    public static Class<?> getRawType(Type type) {
        if(type instanceof Class) return (Class<?>) type;
        if(type instanceof ParameterizedType) return (Class<?>) ((ParameterizedType) type).getRawType();
        return null;
    }

    public static Type[] getTypeArguments(Type type) {
        Type[] paraTypes;
        if(type instanceof ParameterizedType) {
            paraTypes = ((ParameterizedType) type).getActualTypeArguments();
        } else {
            paraTypes = null;
        }
        return paraTypes;
    }

    private <V> V convert(Object value, Class<V> targetType) {
        V retval;
        if(value instanceof Map) {
            try {
                retval = targetType.newInstance();
                Item<V> item = new Item<>(retval);
                item.copyProperties(value);
            } catch (InstantiationException | IllegalAccessException e) {
                retval = converters.convert(value, targetType);
            }
        } else {
            retval = converters.convert(value, targetType);
        }

        return retval;
    }

    public void putAll(Object source, String... excludes) {
        this.putAllWriteable(source, excludes);
    }

    public void putAllWriteable(Object source, String... excludes) {
        List<String> list = Arrays.asList(excludes);
        Map bean = source instanceof Map ? (Map) source : new BeanMap(source);
        for(Object key : bean.keySet()) {
            if(list.contains(key)) continue;
            if(bm.getWriteMethod((String) key)==null) continue;
            bm.put(key, bean.get(key));
        }
    }

    @Override
    public void clear() {
        bm.clear();
    }

    @Override
    public Set<String> keySet() {
        return bm.keySet()
                .stream()
                .filter(key-> bm.getWriteMethod((String) key)!=null)
                .map(key -> (String) key)
                .collect(Collectors.toSet());
    }

    @Override
    public java.util.Collection<Object> values() {
        return bm.values();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        Set<Map.Entry<String, Object>> entrySet = bm.entrySet()
                .stream()
                .filter(entry-> bm.getWriteMethod((String) entry.getKey())!=null)
                .map(entry -> new AbstractMap.SimpleEntry<>((String) entry.getKey(), entry.getValue()))
                .collect(Collectors.toSet());
        return entrySet;
    }
}
