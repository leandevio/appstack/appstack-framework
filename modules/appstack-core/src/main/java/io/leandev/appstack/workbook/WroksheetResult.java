package io.leandev.appstack.workbook;

import io.leandev.appstack.parser.Result;

public class WroksheetResult implements Result<WorksheetRecord> {
    private Worksheet data;
    private int cursor = 0;

    protected WroksheetResult(Worksheet worksheet) {
        this.data = worksheet;
    }

    @Override
    public boolean hasNext() {
        return (cursor < data.lastRowNum());
    }

    @Override
    public WorksheetRecord next() {
        return toRecord(data.getRow(++cursor));
    }

    protected WorksheetRecord toRecord(Row row) {
        if(row==null) return null;
        WorksheetRecord record = new WorksheetRecord(row);
        return record;
    }

}
