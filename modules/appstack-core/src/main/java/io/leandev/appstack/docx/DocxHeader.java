package io.leandev.appstack.docx;

import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DocxHeader {
    XWPFHeader xwpfHeader;
    public DocxHeader(XWPFHeader xwpfHeader) {
        this.xwpfHeader = xwpfHeader;
    }

    public Optional<DocxParagraph> getFirstParagraph(boolean autoCreate) {
        Optional<XWPFParagraph> xwpfParagraph = xwpfHeader.getParagraphs().stream().findFirst();
        DocxParagraph docxParagraph;
        if(xwpfParagraph.isPresent()) {
            docxParagraph = new DocxParagraph(xwpfParagraph.get());
        } else {
            docxParagraph = (autoCreate) ? new DocxParagraph(xwpfHeader.createParagraph()) : null;
        }
        return Optional.of(docxParagraph);
    }

    public Optional<DocxParagraph> getParagraphByBookmark(String bookmark) {
        DocxParagraph docxParagraph = null;
        List<IBodyElement> elements = xwpfHeader.getBodyElements();
        for(IBodyElement element : elements) {
            if(element instanceof XWPFTable) {
                for(XWPFTableRow row : ((XWPFTable) element).getRows()) {
                    for(XWPFTableCell cell : row.getTableCells()) {
                        for(XWPFParagraph xwpfParagraph : cell.getParagraphs()) {
                            if(isParagraphBookmarkBy(xwpfParagraph, bookmark)) {
                                docxParagraph = new DocxParagraph(xwpfParagraph);
                                break;
                            }
                        }
                        if(docxParagraph!=null) break;
                    }
                    if(docxParagraph!=null) break;
                }
            } else if(element instanceof XWPFParagraph) {
                if(isParagraphBookmarkBy((XWPFParagraph) element, bookmark)) {
                    docxParagraph = new DocxParagraph((XWPFParagraph) element);
                }
            }
            if(docxParagraph!=null) break;
        }
        return Optional.of(docxParagraph);
    }

    private boolean isParagraphBookmarkBy(XWPFParagraph xwpfParagraph, String bookmark) {
        boolean test = false;
        CTP ctp = xwpfParagraph.getCTP();
        // Get all bookmarks and loop through them
        List<CTBookmark> ctBookmarks = ctp.getBookmarkStartList();
        for (CTBookmark ctBookmark : ctBookmarks) {
            if (ctBookmark.getName().equals(bookmark)) {
                test = true;
                break;
            }
        }
        return test;
    }

    public List<DocxParagraph> getAllParagraphsByText(String text) {
        List<DocxParagraph> docxParagraphs = new ArrayList<>();
        List<IBodyElement> elements = xwpfHeader.getBodyElements();
        for(IBodyElement element : elements) {
            if(element instanceof XWPFTable) {
                for(XWPFTableRow row : ((XWPFTable) element).getRows()) {
                    for(XWPFTableCell cell : row.getTableCells()) {
                        for(XWPFParagraph xwpfParagraph : cell.getParagraphs()) {
                            if(isParagraphContainsText(xwpfParagraph, text)) {
                                docxParagraphs.add(new DocxParagraph(xwpfParagraph));
                            }
                        }
                    }
                }
            } else if(element instanceof XWPFParagraph) {
                if(isParagraphContainsText((XWPFParagraph) element, text)) {
                    docxParagraphs.add(new DocxParagraph((XWPFParagraph) element));
                }
            }
        }
        return docxParagraphs;
    }

    public Optional<DocxParagraph> getParagraphByText(String text) {
        DocxParagraph docxParagraph = null;
        List<IBodyElement> elements = xwpfHeader.getBodyElements();
        for(IBodyElement element : elements) {
            if(element instanceof XWPFTable) {
                for(XWPFTableRow row : ((XWPFTable) element).getRows()) {
                    for(XWPFTableCell cell : row.getTableCells()) {
                        for(XWPFParagraph xwpfParagraph : cell.getParagraphs()) {
                            if(isParagraphContainsText(xwpfParagraph, text)) {
                                docxParagraph = new DocxParagraph(xwpfParagraph);
                                break;
                            }
                        }
                        if(docxParagraph!=null) break;
                    }
                    if(docxParagraph!=null) break;
                }
            } else if(element instanceof XWPFParagraph) {
                if(isParagraphContainsText((XWPFParagraph) element, text)) {
                    docxParagraph = new DocxParagraph((XWPFParagraph) element);
                }
            }
            if(docxParagraph!=null) break;
        }
        return Optional.of(docxParagraph);
    }

    private boolean isParagraphContainsText(XWPFParagraph xwpfParagraph, String text) {
        return xwpfParagraph.getText().contains(text);
    }

    public DocxParagraph createParagraph() {
        XWPFParagraph xwpfParagraph = xwpfHeader.createParagraph();
        return new DocxParagraph(xwpfParagraph);
    }
}
