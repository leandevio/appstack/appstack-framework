package io.leandev.appstack.converter;


import io.leandev.appstack.exception.ConversionException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class DateConverter implements Converter<Date> {
    private static final String DEFAULT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSX";

    private final SimpleDateFormat formatter;

    public static DateConverter of(String pattern) {
        return new DateConverter(pattern);
    }

    public DateConverter(String pattern) {
        this.formatter = new SimpleDateFormat(pattern);
    }

    public DateConverter() {
        this.formatter = new SimpleDateFormat(DEFAULT_PATTERN);
    }

    @Override
    public Date convert(Object value) {
        Date date;

        if(value instanceof Date) {
            date = (Date) value;
        } else if(value==null) {
            date = null;
        } else if(value instanceof String) {
            date = toDate((String) value);
        } else if(value instanceof Long) {
            date = toDate((Long) value);
        } else if(value instanceof Integer) {
            date = toDate((Integer) value);
        } else {
            throw new ConversionException("Only support convert String, Long and Integer to Date.");
        }
        return date;

    }

    public String format(Date value) {
        return this.formatter.format(value);
    }

    private Date toDate(String value) {
        Date date;
        if(value.trim().equals("")) return null;

        try{
            date = formatter.parse(value);
        } catch(ParseException e) {
            throw new RuntimeException(e);
        }

        return date;
    }

    private Date toDate(Long value) {
        Instant instant = Instant.ofEpochMilli(value);

        return Date.from(instant);
    }

    private Date toDate(Integer value) {
        Instant instant = Instant.ofEpochSecond(value);

        return Date.from(instant);
    }
}
