package io.leandev.appstack.text;

public class TxtParserBuilder {
    TxtFormat txtFormat = new TxtFormat();

    public static TxtParserBuilder newTxtParser() {
        return new TxtParserBuilder();
    }

    public TxtParserBuilder withColumnWidths(int... columnWidths) {
        txtFormat = new TxtFormat(columnWidths);
        return this;
    }

    public TxtParserBuilder withPadding(Character padding) {
        txtFormat.setPadding(padding);
        return this;
    }

    public TxtParserBuilder withLineSeparator(String separator) {
        txtFormat.setLineSeparator(separator);
        return this;
    }

    public TxtParserBuilder enableHeaderExtraction(boolean enableHeaderExtraction) {
        txtFormat.setHeaderExtractionEnabled(enableHeaderExtraction);
        return this;
    }

    public TxtParserBuilder skipEmptyLines(boolean skipEmptyLines) {
        txtFormat.setSkipEmptyLines(skipEmptyLines);
        return this;
    }

    public TxtParser build() {
        return new TxtParser(txtFormat);
    }

    public TxtParserBuilder keepPadding() {
        txtFormat.setKeepPadding(true);
        return this;
    }

    public TxtParserBuilder keepPadding(boolean keep) {
        txtFormat.setKeepPadding(keep);
        return this;
    }
}
