package io.leandev.appstack.mail;

import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.Session;
import java.security.GeneralSecurityException;
import java.util.Properties;

public class IMAPSessionBuilder {
    private Properties props = new Properties();
    private boolean debug = false;
    public static IMAPSessionBuilder newSession(String host, int port) {
        return new IMAPSessionBuilder(host, port);
    }

    public static IMAPSessionBuilder newOffice365Session() {
        String host = "outlook.office365.com";
        int port = 993;
        Properties props = new Properties();
        props.put("mail.imap.ssl.enable", "true");
        props.put("mail.imap.starttls.enable", "true");
        props.put("mail.imap.auth", "true");
        props.put("mail.imap.auth.mechanisms", "XOAUTH2");
        return new IMAPSessionBuilder(host, port, props);
    }

    public static IMAPSessionBuilder newGMailMailbox() {
        final String host = "imap.gmail.com";
        final int port = 993;
        Properties props = new Properties();
        props.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.imap.socketFactory.fallback", "false");
        props.setProperty("mail.imap.socketFactory.port", String.valueOf(port));
        return new IMAPSessionBuilder(host, port, props);
    }

    public IMAPSessionBuilder(String host, int port) {
        props.put("mail.store.protocol", "imap");
        props.put("mail.imap.host", host);
        props.put("mail.imap.port", String.valueOf(port));
    }

    public IMAPSessionBuilder(String host, int port, Properties props) {
        props.put("mail.store.protocol", "imap");
        props.put("mail.imap.host", host);
        props.put("mail.imap.port", String.valueOf(port));
        this.props = props;
    }

    public Session build() {
        if(this.debug) {
            props.put("mail.debug", "true");
            props.put("mail.debug.auth", "true");
        }
        Session session = Session.getInstance(props);
        if(this.debug) {
            session.setDebug(true);
        }
        return session;
    }

    public IMAPSessionBuilder withDebug(boolean debug) {
        this.debug = debug;
        return this;
    }

    public IMAPSessionBuilder withSSL() {
        // SSL setting
        props.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.imap.socketFactory.fallback", "false");
        props.setProperty("mail.imap.socketFactory.port", props.getProperty("mail.imap.port"));

        try {
            MailSSLSocketFactory sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
            props.put("mail.imap.ssl.trust", "*");
            props.put("mail.imap.ssl.socketFactory", sf);
        } catch (GeneralSecurityException e) {
            throw new EmailException(e);
        }

        return this;
    }

    public IMAPSessionBuilder withProperty(String key, String value) {
        props.put(key, value);
        return this;
    }
}
