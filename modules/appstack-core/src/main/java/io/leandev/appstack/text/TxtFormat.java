package io.leandev.appstack.text;

public class TxtFormat {
    private int[] columnLengths;
    private Character padding = ' ';
    private boolean headerExtractionEnabled = true;
    private String lineSeparator = System.lineSeparator();
    private boolean keepPadding = false;
    private boolean skipEmptyLines = true;

    public TxtFormat(int... columnLengths) {
        this.columnLengths = columnLengths;
    }

    public int[] columnLengths() {
        return this.columnLengths;
    }

    public void setPadding(Character padding) {
        this.padding = padding;
    }

    public Character padding() {
        return this.padding;
    }

    public void setHeaderExtractionEnabled(boolean headerExtractionEnabled) {
        this.headerExtractionEnabled = headerExtractionEnabled;
    }

    public boolean isHeaderExtractionEnabled() {
        return this.headerExtractionEnabled;
    }

    /**
     * the line separator sequence is defined here to ensure systems such as MacOS and Windows
     * are able to process this file correctly
     * - Windows: '\r\n'
     * - Mac (OS 9-): '\r'
     * - Mac (OS 10+): '\n'
     * - Unix/Linux: '\n'
     *
     * @param lineSeparator
     */
    public void setLineSeparator(String lineSeparator) {
        this.lineSeparator = lineSeparator;
    }

    public String lineSeparator() {
        return this.lineSeparator;
    }

    public void setKeepPadding(boolean keepPadding) {
        this.keepPadding = keepPadding;
    }

    public boolean isKeepPadding() {
        return this.keepPadding;
    }

    public void setSkipEmptyLines(boolean skipEmptyLines) {
        this.skipEmptyLines = skipEmptyLines;
    }

    public boolean isSkipEmptyLines() {
        return this.skipEmptyLines;
    }
}
