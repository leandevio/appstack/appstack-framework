package io.leandev.appstack.doc;

import java.util.Arrays;
import java.util.List;

public interface Block extends Element {
    void setAlign(Align align);

    List<? extends Chunk> chunks();

    Chunk appendChunk(Chunk chunk);

    default List<? extends Chunk> appendChunks(Chunk... chunks) {
        return appendChunks(Arrays.asList(chunks));
    }

    default List<? extends Chunk> appendChunks(List<Chunk> chunks) {
        for(Chunk chunk : chunks) {
            appendChunk(chunk);
        }
        return chunks();
    }

    default int size() {
        return chunks().size();
    }

    default Chunk getChunk(int index) {
        return chunks().get(index);
    }
}
