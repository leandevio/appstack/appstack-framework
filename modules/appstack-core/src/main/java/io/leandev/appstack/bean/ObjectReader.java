package io.leandev.appstack.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ObjectReader {
    public <T> T readObject(byte[] bytes, Class<T> type) throws ObjectReadWriteException {
        T object;
        try(ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)) {
            object = (T) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new ObjectReadWriteException(e);
        }
        return object;
    }
}
