package io.leandev.appstack.doc;

public enum VerticalAlignment {
    TOP, BOTTOM, MIDDLE, JUSTIFY, DISTRIBUTED;
}
