package io.leandev.appstack.mail;

public enum AttendeeType {
    REQUIRED, OPTIONAL
}
