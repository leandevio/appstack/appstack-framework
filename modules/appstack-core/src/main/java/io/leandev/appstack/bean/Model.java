package io.leandev.appstack.bean;


import io.leandev.appstack.converter.Converter;
import io.leandev.appstack.converter.Converters;
import io.leandev.appstack.json.JsonParser;
import org.apache.commons.beanutils.BeanMap;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class Model<T> implements Map<String, Object> {
    private Converters converters = new Converters();
    private JsonParser jsonParser = new JsonParser();
    private T bean;
    private Map map;

    public Model(T bean) {
        this.setBean(bean);
    }

    public Model() {
        this.setBean((T) new HashMap<String, Object>());
    }

    public <V> void register(Converter<V> converter, Class<V> targetType) {
        converters.register(converter, targetType);
    }

    public T getBean() {
        return this.bean;
    }

    public void setBean(T bean) {
        this.bean = bean;
        if(bean instanceof Map) {
            this.map = (Map) bean;
        } else {
            this.map = new BeanMap(bean);
        }
    }

    private <V> V convert(Object value, Class<V> targetType) {
        V retval;
        if(value instanceof Map) {
            try {
                retval = targetType.newInstance();
                Model<V> model = new Model<>(retval);
                model.copyProperties(value);
            } catch (InstantiationException | IllegalAccessException e) {
                retval = converters.convert(value, targetType);
            }
        } else {
            retval = converters.convert(value, targetType);
        }

        return retval;
    }

    public String getString(String key) {
        return (String) get(key);
    }

    public String getAsString(String key) {
        return convert(get(key), String.class);
    }

    public Date getDate(String key) {
        return (Date) get(key);
    }

    public Date getAsDate(String key) {
        return convert(get(key), Date.class);
    }

    public Integer getInteger(String key) {
        return (Integer) get(key);
    }

    public Integer getAsInteger(String key) {
        return convert(get(key), Integer.class);
    }

    public Long getLong(String key) {
        return (Long) get(key);
    }

    public Long getAsLong(String key) {
        return convert(get(key), Long.class);
    }

    public Double getDouble(String key) {
        return (Double) get(key);
    }

    public Double getAsDouble(String key) {
        return convert(get(key), Double.class);
    }

    public Float getFloat(String key) {
        return (Float) get(key);
    }

    public Float getAsFloat(String key) {
        return convert(get(key), Float.class);
    }

    public BigDecimal getBigDecimal(String key) {
        return (BigDecimal) get(key);
    }

    public BigDecimal getAsBigDecimal(String key) {
        return convert(get(key), BigDecimal.class);
    }

    public Boolean getBoolean(String key) {
        return (Boolean) get(key);
    }

    public Boolean getAsBoolean(String key) {
        return convert(get(key), Boolean.class);
    }

    public Map<String, ?> getMap(String key) {
        return (Map<String, ?>) get(key);
    }

    public List<?> getList(String key, List<?> defaultValue) {
        return (this.get(key) == null) ? defaultValue : (List<?>) get(key);
    }

    public List<?> getList(String key) {
        return this.getList(key, (List<?>) null);
    }

    public <V> List<V> getList(String key, Class<V> type, List<V> defaultValue) {
        return (this.get(key) == null) ? defaultValue : (List<V>) get(key);
    }

    public <V> List<V> getList(String key, Class<V> type) {
        return getList(key, type, null);
    }

    public <V> List<V> getAsList(String key, Class<V> type, List<V> defaultValue) {
        List<?> values = (List<?>) this.get(key);
        if(values == null) {
            return defaultValue;
        }
        List<V> data = new ArrayList<>();
        for(Object value : values) {
            V item;
            if(type.equals(Model.class)) {
                item = (V) new Model(value);
            } else {
                item = converters.convert(value, type);
            }
            data.add(item);
        }
        return data;
    }

    public <V> List<V> getAsList(String key, Class<V> type) {
        return this.getAsList(key, type, null);
    }

    public void copyProperties(Object source, String... excludes) {
        List<String> list = Arrays.asList(excludes);
        Model model = new Model(source);
        Set<String> keySet = this.map instanceof Map ? model.keySet() : this.keySet();
        for(String key : keySet) {
            if(list.contains(key) || !model.containsKey(key)) continue;
            if(!this.isWritable(key)) continue;
            Object value = model.get(key);
            this.put(key, value);
        }
    }

    public boolean isWritable(String key) {
        return this.bean instanceof Map || this.getWriteMethod(key) != null;
    }

    public Method getWriteMethod(String key) {
        return this.map instanceof BeanMap ? ((BeanMap) this.map).getWriteMethod(key) : null;
    }

    public Method getReadMethod(String key) {
        return this.map instanceof BeanMap ? ((BeanMap) this.map).getReadMethod(key) : null;
    }

    private Class<?> getRawType(Type type) {
        if(type instanceof Class) return (Class<?>) type;
        if(type instanceof ParameterizedType) return (Class<?>) ((ParameterizedType) type).getRawType();
        return null;
    }

    private Type[] getTypeArguments(Type type) {
        Type[] paraTypes;
        if(type instanceof ParameterizedType) {
            paraTypes = ((ParameterizedType) type).getActualTypeArguments();
        } else {
            paraTypes = null;
        }
        return paraTypes;
    }

    @Override
    public int size() {
        return this.map.size();
    }

    @Override
    public boolean isEmpty() {
        return this.map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return this.map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return this.map.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return this.map.get(key);
    }

    @Override
    public Object put(String key, Object value) {
        if(this.bean instanceof Map) {
            return this.map.put(key, value);
        }
        if(this.getWriteMethod(key)==null) {
            throw new IllegalArgumentException(String.format("Failed to put: %s is not writable.", key));
        }
        Type targetType = this.getWriteMethod(key).getGenericParameterTypes()[0];
        Class<?> targetRawType = getRawType(targetType);
        if(Collection.class.isAssignableFrom(targetRawType) && this.map.get(key)!=null) {
            Collection collection;
            if(value==null) {
                collection = new ArrayList();
            } else if(value.getClass().isArray()) {
                collection = Arrays.asList(value);
            } else {
                collection = (Collection) value;
            }
           Collection target = (Collection) this.get(key);
            target.clear();
            Type[] paraTypes = getTypeArguments(targetType);
            if(paraTypes==null) {
                target.addAll(collection);
            } else {
                Type paraType = paraTypes[0];
                List data = new ArrayList<>();
                for(Object datum : collection) {
                    data.add(convert(datum, getRawType(paraType)));
                }
                target.addAll(data);
            }
        } else {
            try {
                if(targetRawType.isAssignableFrom(Object.class)) {
                    this.map.put(key, value);
                } else if(targetRawType.isAssignableFrom(List.class) && value instanceof String) {
                    this.map.put(key, jsonParser.readValueAsList((String) value, Map.class));
                } else if(targetRawType.isAssignableFrom(Map.class) && value instanceof String) {
                    this.map.put(key, jsonParser.readValue((String) value, Map.class));
                } else if(targetRawType.isAssignableFrom(String.class) && (value instanceof List || value instanceof Map)) {
                    this.map.put(key, jsonParser.writeValueAsString(value));
                } else {
                    this.map.put(key, convert(value, targetRawType));
                }
            } catch (Exception e) {
                String message = String.format("%s. Failed to set the property(%s). The value is %s(%s).", e.getMessage(), key, value, targetRawType);
                throw new IllegalArgumentException(message, e.getCause());
            }
        }

        return this.get(key);
    }

    @Override
    public Object remove(Object key) {
        return this.map.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ?> source) {
        this.copyProperties(source);
    }

    @Override
    public void clear() {
        this.map.clear();
    }

    @Override
    public Set<String> keySet() {
        return this.entrySet()
                .stream()
                .map(entry -> entry.getKey())
                .collect(Collectors.toSet());
    }

    @Override
    public Collection<Object> values() {
        return this.entrySet()
                .stream()
                .map(entry -> entry.getValue())
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        Set<Entry<String, Object>> entrySet = this.map.entrySet();
        return entrySet.stream()
                .map(entry -> new AbstractMap.SimpleEntry<>(entry.getKey(), entry.getValue()))
                .filter(entry -> !entry.getKey().equals("class"))
                .collect(Collectors.toSet());
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Model)) {
            return false;
        }
        Model other = (Model)o;
        Set<String> keys = this.keySet();
        boolean allEquals = true;
        for(String key : keys) {
            Object value = this.get(key);
            Object otherValue = other.get(key);
            boolean valueEquals = (value == null && otherValue == null)
                    || (value != null && value.equals(otherValue));
            if(!valueEquals) {
                allEquals = false;
                break;
            }
        }
        return allEquals;
    }

    @Override
    public final int hashCode() {
        int result = 17;
        Set<String> keys = this.keySet();
        for(String key : keys) {
            Object value = this.get(key);
            if(value != null) {
                result = 31 * result + value.hashCode();
            }
        }
        return result;
    }
}
