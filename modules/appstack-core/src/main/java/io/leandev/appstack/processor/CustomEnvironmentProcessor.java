package io.leandev.appstack.processor;

import io.leandev.appstack.reflect.Reflection;
import io.leandev.appstack.util.Environment;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import java.util.*;

public class CustomEnvironmentProcessor implements EnvironmentPostProcessor {
    private static final String PACKAGETOSCAN = "io.leandev.appstack";
    private static final Environment environment = Environment.getDefaultInstance();
    @Override
    public void postProcessEnvironment(ConfigurableEnvironment springEnvironment, SpringApplication application) {
        Set<Class<? extends EnvironmentProcessor>> processors = scan();

        Map<String, Object> props = new HashMap<>();
        try {
            for(Class<? extends EnvironmentProcessor> processor : processors) {
                EnvironmentProcessor instance = processor.newInstance();
                instance.postProcess(environment, props);
            }
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException(e);
        }
        MutablePropertySources propertySources = springEnvironment.getPropertySources();
        propertySources.addFirst(new MapPropertySource("appstack", props));
    }

    public Set<Class<? extends EnvironmentProcessor>> scan() {
        Reflection reflection = new Reflection(PACKAGETOSCAN);
        return reflection.getSubTypesOf(EnvironmentProcessor.class);
    }
}
