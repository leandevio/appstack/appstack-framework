package io.leandev.appstack.html;

import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class HtmlElement implements io.leandev.appstack.doc.Element {
    private static Logger LOGGER = LogManager.getLogger(HtmlElement.class);
    private Element element;

    protected HtmlElement(Element element) {
        this.element = element;
    }

    protected Element element() {
        return this.element;
    }

    public HtmlElement(String tagName) {
        this.element = new Element(tagName);
    }

    public HtmlElement append(HtmlElement htmlElement) {
        this.element.appendChild(htmlElement.element());
        return this;
    }

    public HtmlElement appendText(String text) {
        this.element.appendText(text==null ? "" : text);
        return this;
    }

    public HtmlElement attr(String key, String value) {
        this.element.attr(key, value);
        return this;
    }

    public String attr(String key) {
        return this.element.attr(key);
    }

    public HtmlElement child(int index) {
        return new HtmlElement(this.element.child(index));
    }

    public HtmlElement html(String html) {
        Element element = this.element.html(html);
        return new HtmlElement(element);
    }

    public String html() {
        return this.element.html();
    }

    public void css(String name, String value) {
        Map<String, String> rules = this.cssRules();
        if(value==null) {
            rules.remove(name);
        } else {
            rules.put(name, value);
        }
        String cssText = toCssText(rules);
        this.attr("style", cssText);
    }

    private String toCssText(Map<String, String> rules) {
        StringBuilder cssText = new StringBuilder();
        for(String name : rules.keySet()) {
            String value = rules.get(name);
            cssText.append(name).append(":").append(value).append(";");
        }
        return cssText.toString();
    }

    private Map<String, String> cssRules() {
        Map<String, String> rules = new HashMap<>();
        String cssText = this.attr("style");
        String[] pairs = cssText.split(";");
        for(String pair : pairs) {
            if(pair.contains(":")) {
                String[] tokens = pair.split(":");
                rules.put(tokens[0].trim(), tokens[1].trim());
            }
        }
        return rules;
    }

}
