package io.leandev.appstack.workbook;

import java.util.Collection;
import java.util.List;

public class WorkbookWriter {
    private WorksheetWriter writer;

    public WorkbookWriter(Workbook workbook) {
        Worksheet worksheet = workbook.numberOfWorksheets()==0 ? workbook.createWorksheet() : workbook.getWorksheetAt(0);
        this.writer = new WorksheetWriter(worksheet);
    }

    public void writeRow(Object bean) {
        writer.writeRow(bean);
    }

    private void writeRow(Collection<?> values) {
        writer.writeRow(values);
    }
}
