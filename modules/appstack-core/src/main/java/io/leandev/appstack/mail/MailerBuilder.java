package io.leandev.appstack.mail;

import io.leandev.appstack.constant.CorePropsDefault;
import io.leandev.appstack.constant.CorePropsKey;
import io.leandev.appstack.util.Environment;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MailerBuilder {
    private static Environment environment = Environment.getDefaultInstance();
    private final static Pattern REGEX = Pattern.compile("[\\.\\*\\+\\?]+");
    private Mailer mailer = new Mailer();

    public static MailerBuilder newMailer() {
        return new MailerBuilder();
    }

    public MailerBuilder withSMTPServer() {
        String host = environment.property(CorePropsKey.MAILSMTPHOST);
        int port = environment.propertyAsInteger(CorePropsKey.MAILSMTPPORT, CorePropsDefault.MAILSMTPPORT);
        boolean starttls = environment.propertyAsBoolean(CorePropsKey.MAILSMTPSTARTTLSENABLE, CorePropsDefault.MAILSMTPSTARTTLSENABLE);
        boolean auth = environment.propertyAsBoolean(CorePropsKey.MAILSMTPAUTH, CorePropsDefault.MAILSMTPAUTH);
        boolean debug = environment.propertyAsBoolean(CorePropsKey.MAILDEBUG, CorePropsDefault.MAILDEBUG);

        String username = environment.property(CorePropsKey.MAILSMTPUSERNAME);
        String password = environment.property(CorePropsKey.MAILSMTPPASSWORD);

        Properties props = new Properties();
        props.put(CorePropsKey.MAILSMTPAUTH, auth);
        props.put(CorePropsKey.MAILSMTPSTARTTLSENABLE, starttls);
        props.put(CorePropsKey.MAILDEBUG, debug);

        this.withSMTPServer(host, port, username, password, props);

        if(environment.propertyAsBoolean(CorePropsKey.MAILFIREWALLENABLE, CorePropsDefault.MAILFIREWALLENABLE)) {
            mailer.startFirewall();
        } else {
            mailer.stopFirewall();
        }
        String whitelist = environment.property(CorePropsKey.MAILWHITELIST, CorePropsDefault.MAILWHITELIST);
        this.withWhitelist(whitelist.split(","));

        return this;
    }

    public MailerBuilder withSMTPServer(String host, int port, String username, String password, Properties properties) {
        this.mailer.setHost(host);
        this.mailer.setPort(port);
        this.mailer.setUsername(username);
        this.mailer.setPassword(password);
        this.mailer.setProperties(properties);
        return this;
    }

    public MailerBuilder withSMTPServer(String host, int port, String username, String password) {
        Properties props = new Properties();
        props.put(CorePropsKey.MAILSMTPAUTH, true);

        return this.withSMTPServer(host, port, username, password, props);
    }

    public MailerBuilder withTransportStrategy(TransportStrategy strategy) {
        return this;
    }

    public MailerBuilder withSessionTimeout(int timeout) {
        this.mailer.setTimeout(timeout);
        return this;
    }

    public Mailer build() {
        return this.mailer;
    }

    public MailerBuilder withProperty(String key, String value) {
        this.mailer.setProperty(key, value);
        return this;
    }

    public MailerBuilder withGmailServer(String username, String password) {
        final String HOSTNAME = "smtp.gmail.com";
        final int PORT = 587;
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        return withSMTPServer(HOSTNAME, PORT, username, password, properties);
    }

    public MailerBuilder withOffice365Server(String username, String password) {
        final String HOSTNAME = "smtp.office365.com";
        final int PORT = 587;
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        return withSMTPServer(HOSTNAME, PORT, username, password, properties);
    }

    public MailerBuilder withDebug(boolean debug) {
        this.mailer.setDebug(debug);
        return this;
    }

    public MailerBuilder stopFirewall() {
        this.mailer.stopFirewall();
        return this;
    }

    public MailerBuilder withWhitelist(String... whitelist) {
        return this.withWhitelist(Arrays.asList(whitelist));
    }

    public MailerBuilder withWhitelist(List<String> whitelist) {
        List<Pattern> patterns = whitelist.stream()
                .map(regex -> isRegex(regex) ? Pattern.compile(regex) : Pattern.compile(String.format("^.+@%s$", regex)))
                .collect(Collectors.toList());
        this.mailer.setWhitelist(patterns);
        return this;
    }

    private boolean isRegex(String regex) {
        return REGEX.matcher(regex).matches();
    }
}
