package io.leandev.appstack.workbook;

import io.leandev.appstack.parser.AbstractRecord;

import java.util.Arrays;
import java.util.List;


public class WorksheetRecord extends AbstractRecord {
    private Row data;

    public WorksheetRecord(Row row) {
        this.data = row;
    }

    @Override
    public List getValues() {
        return Arrays.asList(data.getValues());
    }

    @Override
    public Object getValue(int index) {
        return data.getValue(index);
    }

    @Override
    public <T> T getValue(int index, Class<T> type) {
        return (T) this.data.getValue(index);
    }

    @Override
    public int size() {
        return this.data.size();
    }

}
