package io.leandev.appstack.mail;

import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import io.leandev.appstack.text.TextFileReader;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 對大多數郵件伺服器而言（例如 Office 365 與 Gmail），Mail from 必須是屬於連接使用者帳號的電子郵件信箱。
 * 行事曆邀請函的 Organizer 收到行事曆邀請時，
 */
public class Mailer {
    private static final Logger logger = LogManager.getLogger(Mailer.class);
    private final Path RESOURCE = Paths.get(Mailer.class.getPackage().getName().replaceAll("\\.", "/"));
    private String template;
    private String host = "localhost";
    private String port = "25";
    private String username;
    private String password;
    private int timeout = 0;
    private boolean firewall = true;
    private List<Pattern> whitelist = new ArrayList<>();
    private Properties properties = new Properties();

    public Mailer() {
        ClassLoader loader = getClass().getClassLoader();
        try(InputStream input = loader.getResourceAsStream(RESOURCE.resolve("Invitation.html").toString());
            TextFileReader reader = new TextFileReader(input)) {
            template = reader.getText();
        } catch (IOException e) {
            logger.warn("Failed to retrieve the invitation template.", e);
        }
    }
    protected Session connect() {
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.transport.protocol", "smtp");
        Session session;
        if(username == null || password == null) {
            session = Session.getInstance(properties, null);
        } else {
            session = Session.getInstance(properties,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });
        }
        return session;
    }

    public void test() throws MessagingException {
        Session session = this.connect();
        session.setDebug(true);
        //Get transport for session
        Transport transport = session.getTransport("smtp");
        //Connect
        transport.connect(this.username, this.password);
        transport.close();
    }

    public void send(Email email) throws MessagingException {
        Message.RecipientType[] recipientTypes = {Message.RecipientType.TO, Message.RecipientType.CC, Message.RecipientType.BCC};
        Message message;
        if(email instanceof Invitation) {
            message = toMessage((Invitation) email, "REQUEST");
        } else {
            message = toMessage(email);
        }

        if(message.getFrom() == null) {
            message.setFrom(new InternetAddress(this.getUsername()));
        }

        Address[] originalRecipients = message.getAllRecipients();

        if(this.isFirewallRunning()) {
            if(message.getFrom() == null || message.getFrom().length == 0) {
                throw new MailException("From is required when firewall is on");
            }
            Address from = message.getFrom()[0];
            String domain = null;
            if(from instanceof InternetAddress) {
                domain = this.parseInternetAddressDomain((InternetAddress) from);
            }

            for(Message.RecipientType recipientType : recipientTypes) {
                Address[] recipients = message.getRecipients(recipientType);
                if(recipients != null) {
                    message.setRecipients(recipientType, this.filterRecipients(recipients, domain, whitelist));
                }
            }
        }

        Address[] finalRecipients = message.getAllRecipients();

        if(this.isFirewallRunning() && logger.isDebugEnabled()) {
            if(count(originalRecipients) != count(finalRecipients)) {
                List<InternetAddress> blockedAddress = difference(originalRecipients, finalRecipients);
                String addresses = blockedAddress.stream().map(address -> address.getAddress()).collect(Collectors.joining(", "));
                String content = "The message to the following recipients has been blocked by firewall.\nSubject: %s\nRecipients: %s";
                logger.debug(String.format(content, message.getSubject(), addresses));
            }
        }

        if(this.isFirewallRunning() && count(finalRecipients) == 0 && count(originalRecipients) > 0) {
            if(logger.isDebugEnabled()) {
                logger.debug(String.format("The message has been completely blocked by firewall.\nSubject: %s", message.getSubject()));
            }
        } else {
            Transport.send(message);
        }
    }

    private int count(Address[] addresses) {
        return addresses == null ? 0 : addresses.length;
    }

    private List<InternetAddress> difference(Address[] a, Address[] b) {
        Address[] minuend = (a == null) ? new Address[0] : a;
        Address[] subtrahend = (b == null) ? new Address[0] : b;
        String dictionary = Arrays.stream(subtrahend).map(address -> ((InternetAddress) address).getAddress().toLowerCase()).collect(Collectors.joining(","));
        List<InternetAddress> results = new ArrayList<>();
        for(Address address : minuend) {
            InternetAddress internetAddress = (InternetAddress) address;
            if(dictionary.indexOf(internetAddress.getAddress().toLowerCase()) == -1) {
                results.add(internetAddress);
            }
        }
        return results;
    }

    public void cancel(Invitation invitation) throws MessagingException {
        Message message;
        message = toMessage(invitation, "CANCEL");
        Transport.send(message);
    }

    private Address[] filterRecipients(Address[] addresses, String domain, List<Pattern> whitelist) {
        return Arrays.stream(addresses)
                .filter(address -> {
                    boolean passed = false;
                    String text = ((InternetAddress) address).getAddress();
                    for(Pattern pattern : whitelist) {
                        passed = pattern.matcher(text).matches();
                        logger.debug(String.format("Mailer match the address('%s') against the pattern('%s'): %s", text, pattern, passed));
                        if(passed) break;
                    }
                    if(!passed) {
                        passed = domain.equalsIgnoreCase(this.parseInternetAddressDomain((InternetAddress) address));
                        logger.debug(String.format("Mailer match the address('%s') against the domain('%s'): %s", text, domain, passed));
                    }
                    return passed;
                }).toArray(Address[]::new);
    }

    private String parseInternetAddressDomain(InternetAddress internetAddress) {
        String address = internetAddress.getAddress();
        return this.parseInternetAddressDomain(address);
    }

    private String parseInternetAddressDomain(String internetAddress) {
        int index = internetAddress.indexOf("@");
        String domain;
        if(index > 0) {
            domain = internetAddress.substring(index + 1);
        } else {
            domain = null;
        }
        return domain;
    }

    private String getDomain() {
        return this.parseInternetAddressDomain(this.getUsername());
    }

    protected Message toMessage(Invitation invitation, String method) throws MessagingException {
        Message.RecipientType[] recipientTypes = {Message.RecipientType.TO, Message.RecipientType.CC, Message.RecipientType.BCC};
        Session session = this.connect();

        MimeMessage message = new MimeMessage(session);
        message.setFrom(invitation.getFrom());

        for(Message.RecipientType recipientType : recipientTypes) {
            List<InternetAddress> recipients = invitation.getRecipients(recipientType);
            if(recipients == null) continue;
            message.setRecipients(recipientType, toArray(recipients));
        }

        message.setReplyTo(toArray(invitation.getReplyTo()));

        String subject = invitation.getSubject();
        if(method.equals("CANCEL")) {
            subject = "Cancelled: " + subject;
        }
        message.setSubject(subject, invitation.getCharset().name());

        Multipart multipart = new MimeMultipart("mixed");
        BodyPart descriptionPart;
        // Note: even if the content is specified as being text/html, outlook won't read correctly tables at all
        // and only some properties from div:s. Thus, try to avoid too fancy content
        if(invitation.getContent().getArticles().size()>0) {
            descriptionPart = toBodyPart(invitation.getContent().getArticle());
        } else {
            descriptionPart = new MimeBodyPart();
            String content = fill(template, invitation.getActivity());
            descriptionPart.setContent(content, "text/html; charset=utf-8");
        }

        multipart.addBodyPart(descriptionPart);

        multipart.addBodyPart(toCalendarPart(invitation, method));

        for(Attachment attachment : invitation.getAttachments()) {
            BodyPart messageBodyPart = new MimeBodyPart();
            DataSource source = new ByteArrayDataSource(attachment.getBytes(), attachment.getMediaType().toString());
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(attachment.getName());
            multipart.addBodyPart(messageBodyPart);
        }

        message.setContent(multipart);

        return message;
    }

    private String fill(String template, Activity activity) {
        String organizer = String.format("%s(%s)", activity.getOrganizer().getPersonal(), activity.getOrganizer().getAddress());
        String location = activity.getLocation()==null ? "" : activity.getLocation();
        String description = activity.getDescription()==null ? "" : activity.getDescription();
        StringBuffer attendees = new StringBuffer();
        activity.getAttendees(AttendeeType.REQUIRED).stream().forEach(attendee -> {
            attendees.append(String.format("<p>&nbsp;-&nbsp;%s(%s)<p>", attendee.getPersonal(), attendee.getAddress()));
        });
        String time = String.format("%s - %s", activity.getStartDate(), activity.getEndDate());
        String text = template.replace("${subject}", activity.getSubject())
                .replace("${time}", time)
                .replace("${location}", location)
        .replace("${organizer}", organizer)
        .replace("${attendees}", attendees.toString())
        .replace("${description}", description);
        return text;
    }

    protected BodyPart toCalendarPart(Invitation invitation, String method) throws MessagingException {
        Activity activity = invitation.getActivity();
        BodyPart bodyPart = new MimeBodyPart();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date now = new Date();
        String timestamp = formatter.format(now);
        String startDate = formatter.format(activity.getStartDate());;
        String endDate = formatter.format(activity.getEndDate());
        StringBuffer content = new StringBuffer();
        content.append("BEGIN:VCALENDAR\n");
        content.append("METHOD:").append(method).append("\n");
        content.append("PRODID:LeanDev Application Framework\n");
        content.append("VERSION:2.0\n");
        content.append("BEGIN:VEVENT\n");
        content.append("DTSTAMP:").append(timestamp).append("\n");
        content.append("DTSTART:").append(startDate).append("\n");
        content.append("DTEND:").append(endDate).append("\n");
        content.append("SUMMARY:").append(activity.getSubject()).append("\n");
        content.append("UID:").append(activity.getUid()).append("\n");
        activity.getAttendees(AttendeeType.REQUIRED).stream().forEach(attendee -> {
            String tpl = "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=%s:MAILTO:%s\n";
            content.append(String.format(tpl, attendee.getPersonal(), attendee.getAddress()));
        });
        // organizer 必須與 from 相同, 否則 microsoft outlook 不會收到郵件。
        // InternetAddress organizer = activity.getOrganizer();
        InternetAddress organizer = invitation.getFrom();
        content.append(String.format("ORGANIZER;CN=%s:MAILTO:%s\n", organizer.getPersonal(), organizer.getAddress()));
        content.append("LOCATION:").append(activity.getLocation()).append("\n");
        content.append("DESCRIPTION").append(activity.getSubject()).append("\n");
        content.append("SEQUENCE:").append("0").append("\n");
        content.append("PRIORITY:").append("0").append("\n");
        content.append("CLASS:").append("PUBLIC").append("\n");
        content.append("STATUS:").append("CONFIRMED").append("\n");
        content.append("TRANSP:").append("OPAQUE").append("\n");
        content.append("BEGIN:").append("VALARM").append("\n");
        content.append("ACTION:").append("DISPLAY").append("\n");
        content.append("DESCRIPTION:").append("REMINDER").append("\n");
        content.append("TRIGGER;RELATED=START:-PT01H30M00S\n");
        content.append("END:VALARM\n");
        content.append("END:VEVENT\n");
        content.append("END:VCALENDAR\n");

        bodyPart.addHeader("Content-Class", "urn:content-classes:calendarmessage");
        bodyPart.setContent(content.toString(), "text/calendar;method=" + method);

        return bodyPart;
    }

    protected Message toMessage(Email email) throws MessagingException {
        Message.RecipientType[] recipientTypes = {Message.RecipientType.TO, Message.RecipientType.CC, Message.RecipientType.BCC};
        Session session = this.connect();

        MimeMessage message = new MimeMessage(session);
        message.setFrom(email.getFrom());
        for(Message.RecipientType recipientType : recipientTypes) {
            List<InternetAddress> recipients = email.getRecipients(recipientType);
            if(recipients == null) continue;
            message.setRecipients(recipientType, toArray(recipients));
        }

        message.setReplyTo(toArray(email.getReplyTo()));
        message.setSubject(email.getSubject(), email.getCharset().name());

        if(email.isMultipart()) {
            Multipart multipart = new MimeMultipart("mixed");
            for(Article article : email.getArticles()) {
                multipart.addBodyPart(toBodyPart(article));
            }
            for(Attachment attachment : email.getAttachments()) {
                BodyPart messageBodyPart = new MimeBodyPart();
                DataSource source = new ByteArrayDataSource(attachment.getBytes(), attachment.getMediaType().toString());
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(attachment.getName());
                multipart.addBodyPart(messageBodyPart);
            }
            message.setContent(multipart);
        } else {
            Article article = email.getArticle();
            String contentType = String.format("%s; charset=%s", article.getMediaType().toString(), article.getCharset().name());
            message.setContent(article.getData(), contentType);
        }
        return message;
    }

    private InternetAddress[] toArray(List<InternetAddress> addresses) {
        if(addresses == null) return null;
        InternetAddress[] array = new InternetAddress[addresses.size()];
        return addresses.toArray(array);
    }

    protected BodyPart toBodyPart(Article article) throws MessagingException {
        BodyPart bodyPart = new MimeBodyPart();
        if(article.hasText()) {
            Multipart multipart = new MimeMultipart("alternative");
            BodyPart textPart = new MimeBodyPart();
            textPart.setContent(article.getText(), String.format("text/plain; charset=%s", article.getCharset().name()));
            multipart.addBodyPart(textPart);
            BodyPart dataPart = new MimeBodyPart();
            String contentType = String.format("%s; charset=%s", article.getMediaType().toString(), article.getCharset().name());
            dataPart.setContent(article.getData(), contentType);
            multipart.addBodyPart(dataPart);
            bodyPart.setContent(multipart);
        } else {
            String contentType = String.format("%s; charset=%s", article.getMediaType().toString(), article.getCharset().name());
            bodyPart.setContent(article.getData(), contentType);
        }
        return bodyPart;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return Integer.parseInt(port);
    }

    public void setPort(int port) {
        this.port = Integer.toString(port);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getProperty(String name) {
        return this.properties.getProperty(name);
    }

    public void setProperty(String name, String value) {
        this.properties.setProperty(name, value);
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties.putAll(properties);
    }

    public boolean getDebug() {
        return Boolean.parseBoolean(this.getProperty("mail.debug"));
    }

    public void setDebug(Boolean debug) {
        this.setProperty("mail.debug", debug.toString());
    }

    public void startFirewall() {
        this.firewall = true;
    }

    public void stopFirewall() {
        this.firewall = false;
    }

    public boolean isFirewallRunning() {
        return this.firewall;
    }

    public List<Pattern> getWhitelist() {
        return this.whitelist;
    }

    public void setWhitelist(List<Pattern> whitelist) {
        this.whitelist = whitelist;
    }
}
