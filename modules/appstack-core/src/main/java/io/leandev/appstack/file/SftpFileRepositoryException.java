package io.leandev.appstack.file;

public class SftpFileRepositoryException extends FileRepositoryException {
    private int status;
    public SftpFileRepositoryException(int status, String message) {
        super(message);
    }
    public SftpFileRepositoryException(String message) {
        super(message);
    }

    public SftpFileRepositoryException(Throwable cause) {
        super(cause);
    }

    public SftpFileRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getStatus() {
        return this.status;
    }
}
