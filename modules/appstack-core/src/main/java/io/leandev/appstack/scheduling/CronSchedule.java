package io.leandev.appstack.scheduling;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class CronSchedule implements Schedule {
    private Class<? extends Job> jobType;
    private String name;
    private String group;
    private String description;
    private Date startTime = new Date();
    private Date endTime;
    private int priority = 5;
    private CronExpression cronExpression;
    private Map<String, Object> data = new HashMap<>();

    private Date nextFireTime;
    private Date previousFireTime;
    private int timesTriggered;


    public CronSchedule(String cronExpression) throws ParseException {
        this.cronExpression = new CronExpression(cronExpression);
    }

    public Class<? extends Job> getJobType() {
        return jobType;
    }

    public void setJobType(Class<? extends Job> jobType) {
        this.jobType = jobType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public CronExpression getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(CronExpression cronExpression) {
        this.cronExpression = cronExpression;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public Map<String, Object> getData() {
        return this.data;
    }

    public Date getNextFireTime() {
        return nextFireTime;
    }

    public void setNextFireTime(Date nextFireTime) {
        this.nextFireTime = nextFireTime;
    }

    public Date getPreviousFireTime() {
        return previousFireTime;
    }

    public void setPreviousFireTime(Date previousFireTime) {
        this.previousFireTime = previousFireTime;
    }

    public int getTimesTriggered() {
        return timesTriggered;
    }

    public void setTimesTriggered(int timesTriggered) {
        this.timesTriggered = timesTriggered;
    }

    @Override
    public Class<? extends Job> jobType() {
        return getJobType();
    }

    @Override
    public String name() {
        return getName();
    }

    @Override
    public String group() {
        return getGroup();
    }

    @Override
    public String description() {
        return getDescription();
    }

    @Override
    public Date startTime() {
        return getStartTime();
    }

    @Override
    public Date endTime() {
        return getEndTime();
    }

    @Override
    public int priority() {
        return getPriority();
    }

    public CronExpression cronExpression() {
        return getCronExpression();
    }

    public void setTimeZone(TimeZone timeZone) {
        cronExpression.setTimeZone(timeZone);
    }

    public TimeZone timeZone() {
        return cronExpression().timeZone();
    }

    @Override
    public Map<String, Object> data() {
        return getData();
    }

    @Override
    public int timesTriggered() {
        return getTimesTriggered();
    }

    @Override
    public Date previousFireTime() {
        return getPreviousFireTime();
    }

    @Override
    public Date nextFireTime() {
        return getNextFireTime();
    }
}
