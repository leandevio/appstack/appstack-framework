package io.leandev.appstack.mail;

import javax.mail.internet.InternetAddress;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Date;

public class InvitationBuilder {
    private Charset charset = StandardCharsets.UTF_8;
    private MediaType mediaType = MediaType.TEXT_HTML_TYPE;
    private Invitation invitation ;

    public static InvitationBuilder newInvitation() {
        return new InvitationBuilder();
    }

    private InvitationBuilder() {
        this.invitation = new Invitation();
    }

    public InvitationBuilder withUid(String uid) {
        invitation.getActivity().setUid(uid);
        return this;
    }

    public InvitationBuilder from(String address, String name) throws UnsupportedEncodingException {
        InternetAddress internetAddress = new InternetAddress(address, name);
        invitation.setFrom(internetAddress);
        return this;
    }

    public InvitationBuilder withOrganizer(String address, String name) throws UnsupportedEncodingException {
        InternetAddress internetAddress = new InternetAddress(address, name);
        invitation.getActivity().setOrganizer(internetAddress);
        return this;
    }

    public InvitationBuilder withAttendee(String address, String name) throws UnsupportedEncodingException {
        InternetAddress internetAddress = new InternetAddress(address, name);
        invitation.getActivity().addAttendee(internetAddress);
        return this;
    }

    public InvitationBuilder withSubject(String subject) {
        invitation.setSubject(subject);
        return this;
    }

    public InvitationBuilder withDescription(String description) {
        invitation.getActivity().setDescription(description);
        return this;
    }

    public InvitationBuilder withContent(Object data, String text) {
        invitation.addArticle(data, this.mediaType, this.charset, text);
        return this;
    }

    public InvitationBuilder withContent(Object data) {
        return withContent(data, null);
    }

    public InvitationBuilder withAttachment(byte[] bytes, String name) {
        invitation.addAttachment(bytes, name);
        return this;
    }

    public InvitationBuilder withAttachment(InputStream inputStream, String name) throws IOException {
        invitation.addAttachment(inputStream, name);
        return this;
    }

    public InvitationBuilder withAttachment(File file) throws IOException {
        invitation.addAttachment(file);
        return this;
    }

    public InvitationBuilder startAt(Date start) {
        invitation.getActivity().setStartDate(start);
        return this;
    }

    public InvitationBuilder endAt(Date end) {
        invitation.getActivity().setEndDate(end);
        return this;
    }

    public InvitationBuilder atLocation(String location) {
        invitation.getActivity().setLocation(location);
        return this;
    }

    public Invitation build() {
        return this.invitation;
    }

}
