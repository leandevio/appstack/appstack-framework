package io.leandev.appstack.microsoft;

import io.leandev.appstack.client.ClientAccessToken;
import io.leandev.appstack.json.JsonParser;
import io.leandev.appstack.util.Model;
import lombok.*;

import java.util.Base64;
import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MicrosoftMailAccessToken extends ClientAccessToken {
    private static JsonParser jsonParser = new JsonParser();
    private String ext_expires_in;
    private Date expiryDate;

    public Date getExpiryDate() {
        String token = this.getAccessToken();
        if(token == null) return null;
        String[] chunks = token.split("\\.");
        Model payload = jsonParser.readValue(new String(Base64.getDecoder().decode(chunks[1])), Model.class);
        Long exp = payload.getAsLong("exp");

        return (exp == null) ? null :new Date(exp * 1000);
    }

    @Override
    public boolean isExpired() {
        return (expiryDate == null) || this.expiryDate.before(new Date());
    }
}
