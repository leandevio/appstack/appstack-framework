package io.leandev.appstack.converter;

public class DoubleConverter implements Converter<Double>{
    @Override
    public Double convert(Object value) {
        Double number;
        if(value instanceof Number) {
            number = toDouble((Number) value);
        } else if(value==null) {
            number = null;
        } else if(value instanceof String) {
            number = toDouble((String) value);
        } else {
            throw new ConversionException("Only support convert String and Number to Double.");
        }
        return number;
    }

    private Double toDouble(Number value) {
        Double n = value.doubleValue();
        return n;
    }

    private Double toDouble(String value) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new ConversionException(String.format("Failed to convert the value: %s", value), e);
        }
    }
}
