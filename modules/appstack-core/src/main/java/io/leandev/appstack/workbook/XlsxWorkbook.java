package io.leandev.appstack.workbook;

import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperty;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

public class XlsxWorkbook implements Workbook {
    private XSSFWorkbook workbook;

    public XlsxWorkbook() {
        this.workbook = new XSSFWorkbook();
    }

    public XlsxWorkbook(InputStream in) throws IOException {
        this.workbook = new XSSFWorkbook(in);
    }

    public void autoSizeColumns() {
        for(int i=0; i<this.numberOfWorksheets(); i++) {
            this.getWorksheetAt(i).autoSizeColumns();
        }
    }

    public void setLocale(Locale locale) {
        this.workbook.getProperties().getCustomProperties().addProperty("locale", locale.toLanguageTag());
    }

    public Locale locale() {
        CTProperty ctProperty = this.workbook.getProperties().getCustomProperties().getProperty("locale");
        return (ctProperty==null) ? null : Locale.forLanguageTag(ctProperty.getLpwstr());
    }

    public XlsxCellStyle createCellStyle() {
        XSSFCellStyle xssfCellStyle = workbook.createCellStyle();
        XSSFFont xssfFont = workbook.createFont();
        xssfCellStyle.setFont(xssfFont);
        XlsxCellStyle cellStyle = new XlsxCellStyle(xssfCellStyle, this);
        return cellStyle;
    }

    protected XlsxCellStyle createCellStyle(XSSFCellStyle style) {
        XSSFCellStyle xssfCellStyle = workbook.createCellStyle();
        xssfCellStyle.cloneStyleFrom(style);
        XSSFFont xssfFont = workbook.createFont();
        xssfCellStyle.setFont(xssfFont);
        XlsxCellStyle cellStyle = new XlsxCellStyle(xssfCellStyle, this);
        return cellStyle;
    }

    @Override
    public int numberOfWorksheets() {
        return this.workbook.getNumberOfSheets();
    }

    @Override
    public XlsxWorksheet createWorksheet(String name) {
        return toWorksheet(workbook.createSheet(name));
    }

    @Override
    public XlsxWorksheet createWorksheet() {
        return toWorksheet(workbook.createSheet());
    }

    @Override
    public XlsxWorksheet getWorksheetAt(int i) {
        XSSFSheet sheet;
        try {
            sheet = workbook.getSheetAt(i);
        } catch(IllegalArgumentException ex) {
            sheet = null;
        }
        return toWorksheet(sheet);
    }

    @Override
    public XlsxWorksheet getWorksheet(String name) {
        return toWorksheet(workbook.getSheet(name));
    }

    @Override
    public void close() throws IOException {
        workbook.close();
    }

    @Override
    public void write(OutputStream outputStream) throws IOException {
        workbook.write(outputStream);
    }

    private XlsxWorksheet toWorksheet(XSSFSheet sheet) {
        if(sheet==null) return null;

        XlsxWorksheet worksheet = new XlsxWorksheet(sheet, this);
        return worksheet;
    }

    protected XSSFWorkbook data() {
        return this.workbook;
    }
}
