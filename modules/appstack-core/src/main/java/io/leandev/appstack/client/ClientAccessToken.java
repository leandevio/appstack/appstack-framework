package io.leandev.appstack.client;
import lombok.*;

@Getter
@Setter
public class ClientAccessToken {
    private String access_token;
    private Long expires_in;
    private String token_type;

    public String getAccessToken() {
        return this.access_token;
    }

    public String getTokenType() {
        return this.token_type;
    }

    public boolean isExpired() {
        return false;
    }
}
