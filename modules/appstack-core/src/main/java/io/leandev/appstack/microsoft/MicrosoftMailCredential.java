package io.leandev.appstack.microsoft;

import io.leandev.appstack.client.ClientCredential;
import lombok.Builder;

public class MicrosoftMailCredential extends ClientCredential {
    private String SCOPES = "https://outlook.office365.com/.default";
    @Builder
    public MicrosoftMailCredential(String tenantId, String clientId, String clientSecret) {
        super(tenantId, clientId, clientSecret, null);
        this.setScopes(SCOPES);
    }
}
