package io.leandev.appstack.scheduling;

import org.quartz.JobExecutionException;

public class JobWrapper implements org.quartz.Job {
    @Override
    public void execute(org.quartz.JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            JobExecutionContext executionContext = createJobExecutionContext(jobExecutionContext);
            Job job = executionContext.getSchedule().jobType().newInstance();
            job.execute(executionContext);
        } catch (io.leandev.appstack.scheduling.JobExecutionException | SchedulerException | InstantiationException | IllegalAccessException e) {
            throw new JobExecutionException(e);
        }
    }

    private JobExecutionContext createJobExecutionContext(org.quartz.JobExecutionContext jobExecutionContext) throws SchedulerException {
        JobExecutionContext executionContext = new JobExecutionContext();
        Scheduler scheduler = new Scheduler(jobExecutionContext.getScheduler());
        executionContext.setScheduler(scheduler);
        executionContext.setFireTime(jobExecutionContext.getFireTime());
        executionContext.setSchedule(scheduler.createSchedule(jobExecutionContext.getTrigger()));
        return executionContext;
    }

}
