package io.leandev.appstack.pdf;

import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

public class PdfDocument {
    protected PDDocument pdDocument;

    protected PdfDocument(PDDocument pdDocument) {
        this.pdDocument = pdDocument;
    }

    public PdfDocument() {
        pdDocument = new PDDocument();
    }

    public PdfDocument(InputStream inputStream) throws IOException {
        this.pdDocument = PDDocument.load(inputStream);
    }

    public PdfDocument(InputStream inputStream, String password) throws IOException {
        this.pdDocument = PDDocument.load(inputStream, password);
    }

    public PdfDocument(byte[] bytes) throws IOException {
        this.pdDocument = PDDocument.load(bytes);
    }

    public PdfDocument(byte[] bytes, String password) throws IOException {
        this.pdDocument = PDDocument.load(bytes, password);
    }

    public PdfDocument(File file) throws IOException {
        this.pdDocument = PDDocument.load(file);
    }

    public PdfDocument(File file, String password) throws IOException {
        this.pdDocument = PDDocument.load(file, password);
    }

    public void save(OutputStream outputStream) throws IOException {
        pdDocument.save(outputStream);
    }

    public void close() throws IOException {
        pdDocument.close();
    }

    /**
     *
     * 指定開啟檔案的使用者密碼，並且加上擁有者密碼與存取限制．
     *
     * @param ownerPassword
     * @param userPassword
     * @param accessPermission
     * @throws IOException
     */
    public void protect(String ownerPassword, String userPassword, PdfAccessPermission accessPermission) throws IOException{
        // Define the length of the encryption key.
        // Possible values are 40 or 128 (256 will be available in PDFBox 2.0).
        int keyLength = 128;

        StandardProtectionPolicy protectionPolicy = new StandardProtectionPolicy(ownerPassword, userPassword, accessPermission.getAccessPermission());
        protectionPolicy.setEncryptionKeyLength(keyLength);
        pdDocument.protect(protectionPolicy);
    }

    /**
     *
     * 指定開啟檔案的使用者密碼，但是沒有存取限制．
     *
     * @param userPassword
     * @throws IOException
     */
    public void protect(String userPassword) throws IOException{
        PdfAccessPermission accessPermission = new PdfAccessPermission();
        this.protect(null, userPassword, accessPermission);
    }

    /**
     * 指定開啟檔案的使用者密碼，並且使用預設的存取限制．
     *
     * @param ownerPassword
     * @param userPassword
     * @throws IOException
     */
    public void protect(String ownerPassword, String userPassword) throws IOException {
        PdfAccessPermission accessPermission = new PdfAccessPermission();
        accessPermission.setAllowPrinting(true);
        accessPermission.setAllowModification(false);
        accessPermission.setAllowExtraction(true);

        this.protect(ownerPassword, userPassword, accessPermission);
    }

    public void protect(String userPassword, PdfAccessPermission accessPermission) throws IOException{
        String ownerPassword = UUID.randomUUID().toString();
        this.protect(ownerPassword, userPassword, accessPermission);
    }

    public void protect(PdfAccessPermission accessPermission) throws IOException{
        this.protect(null, accessPermission);
    }

    protected PDDocument getPDDocument() {
        return this.pdDocument;
    }

    public List<PdfDocument> split() throws IOException {
        Splitter splitter = new Splitter();
        List<PdfDocument> pages = splitter.split(pdDocument).stream().map((PDDocument page) -> new PdfDocument(page))
                .collect(toList());
        return pages;
    }

    public PdfPage getPage(int index) {
        PDPage pdPage = pdDocument.getPage(index);
        return new PdfPage(pdPage);
    }

    public void addPage(PdfPage pdfPage) {
        pdDocument.addPage(pdfPage.pdPage);
    }
}
