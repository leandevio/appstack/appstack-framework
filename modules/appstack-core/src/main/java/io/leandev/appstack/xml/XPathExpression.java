package io.leandev.appstack.xml;

public class XPathExpression {
    private String expression;
    public XPathExpression(String expression) {
        this.expression = expression;
    }

    public static XPathExpression root() {
        return new XPathExpression("/");
    }

    public XPathExpression resolve(String expression) {
        StringBuffer sb = new StringBuffer(this.expression);
        if(!this.expression.endsWith("/")) {
            sb.append("/");
        }
        if(expression.startsWith("/")) {
            sb.append(expression.substring(1));
        } else {
            sb.append(expression);
        }
        return new XPathExpression(sb.toString());
    }

    public XPathExpression resolveByAttribute(String name, Object value) {
        return new XPathExpression(String.format("%s[@%s='%s']", this.expression, name, value.toString()));
    }

    public String toString() {
        return this.expression;
    }
}
