package io.leandev.appstack.bean;

public class ObjectReadWriteException extends RuntimeException {
    public ObjectReadWriteException(String message) {
        super(message);
    }
    public ObjectReadWriteException(Throwable cause) {
        super(cause);
    }
    public ObjectReadWriteException(String message, Throwable cause) {
        super(message, cause);
    }
}