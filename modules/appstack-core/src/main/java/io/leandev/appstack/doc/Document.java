package io.leandev.appstack.doc;

import io.leandev.appstack.doc.delta.Content;
import io.leandev.appstack.doc.delta.Delta;
import io.leandev.appstack.doc.delta.InsertOperation;
import io.leandev.appstack.doc.delta.Operation;
import io.leandev.appstack.util.Model;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface Document {
    List<? extends Block> blocks();

    Block appendBlock(Block block);

    default List<? extends Block> appendBlocks(Block... blocks) {
        return appendBlocks(Arrays.asList(blocks));
    }

    default List<? extends Block> appendBlocks(List<Block> blocks) {
        for(Block block : blocks) {
            appendBlock(block);
        }
        return blocks();
    }
    default int size() {
        return blocks().size();
    }

    default Block getBlock(int index) {
        return blocks().get(index);
    }

    default List<? extends Block> appendBlocks(Delta delta) {
        List<Block> blocks = this.compose(delta);
        return this.appendBlocks(blocks);
    }

    default Chunk appendChunk(Chunk chunk) {
        Block block;

        if(this.size()>0) {
            block = this.getBlock(this.size()-1);
        } else {
            block = this.appendBlock(this.createParagraph());
        }
        return block.appendChunk(chunk);
    }

    Paragraph createParagraph();
    Heading createHeading(int level);
    Listing createList(boolean ordered);

    TextRun createTextRun(String text);
    Image createImage(String source);
    Link createLink(String href, String text);

    default List<Block> compose(Delta delta) {
        Composer composer = new Composer(this);
        return composer.compose(delta);
    }

    default TextRun createTextRun(String text, TextDecoration textDecoration) {
        TextRun textRun = createTextRun(text);
        textRun.setTextDecoration(textDecoration);
        return textRun;
    }

    void write(OutputStream outputStream) throws IOException;

    class Composer {
        private Document document;

        private Composer(Document document) {
            this.document = document;
        }

        private List<Block> compose(Delta delta) {
            List<Block> blocks = new ArrayList<>();
            List<InsertOperation> ops = new ArrayList<>();
            for(Operation operation : delta.getOperations()) {
                if(operation instanceof InsertOperation) {
                    if(isBlockDescriptor((InsertOperation) operation)) {
                        Block block;
                        if(isListingDescriptor((InsertOperation) operation)) {
                            block = composeList(ops, (InsertOperation) operation);
                        } else if(isHeadingDescriptor((InsertOperation) operation)) {
                            block = composeHeader(ops, (InsertOperation) operation);
                        } else {
                            block = composeParagraph(ops, (InsertOperation) operation);
                        }
                        blocks.add(block);
                        ops.clear();
                    } else {
                        ops.add((InsertOperation) operation);
                    }
                }
            }
            if(ops.size()>0) {
                Paragraph paragraph = composeParagraph(ops, null);
                blocks.add(paragraph);
            }
            return blocks;
        }


        private boolean isBlockDescriptor(InsertOperation operation) {
            Content content = operation.content();
            return content.type().equals("text") && content.source().equals("\n");
        }

        private boolean isListingDescriptor(InsertOperation operation) {
            Content content = operation.content();
            Model attributes = operation.attributes();
            return content.type().equals("text") && content.source().equals("\n") && attributes.containsKey("list");
        }

        private boolean isHeadingDescriptor(InsertOperation operation) {
            Content content = operation.content();
            Model attributes = operation.attributes();
            return content.type().equals("text") && content.source().equals("\n") && attributes.containsKey("header");
        }

        private Paragraph composeParagraph(List<InsertOperation> operations, InsertOperation descriptor) {
            Paragraph paragraph = document.createParagraph();
            if(descriptor!=null) {
                Align align = toAlign(descriptor.attributes());
                paragraph.setAlign(align);
            }
            List<Chunk> chunks = composeChunks(operations);
            paragraph.appendChunks(chunks);
            return paragraph;
        }

        private List<Chunk> composeChunks(List<InsertOperation> operations) {
            List<Chunk> chunks = new ArrayList<>();
            for(InsertOperation operation : operations) {
                String type = operation.content().type();
                Chunk chunk;
                if(isLink(operation)) {
                    chunk = composeLink(operation);
                } else if(type.equals("text")) {
                    chunk = composeTextRun(operation);
                } else if(type.equals("image")) {
                    chunk = composeImage(operation);
                } else {
                    chunk = composeTextRun(operation);
                }
                chunks.add(chunk);
            }
            return chunks;
        }

        private boolean isLink(InsertOperation operation) {
            return operation.attributes().containsKey("link");
        }

        private Heading composeHeader(List<InsertOperation> operations, InsertOperation descriptor) {
            int level = descriptor.attributes().getAsInteger("header");
            Heading heading = document.createHeading(level);
            List<Chunk> chunks = composeChunks(operations);
            heading.appendChunks(chunks);
            return heading;
        }

        private Listing composeList(List<InsertOperation> operations, InsertOperation descriptor) {
            boolean ordered = descriptor.attributes().getAsString("list").equals("ordered");
            Listing listing = document.createList(ordered);
            List<Chunk> chunks = composeChunks(operations);
            listing.appendChunks(chunks);
            return listing;
        }

        private TextRun composeTextRun(InsertOperation operation) {
            TextDecoration textDecoration = toTextDecoration(operation.attributes());
            Color color = toColor(operation.attributes());
            Background background = toBackground(operation.attributes());
            TextRun textRun = document.createTextRun(operation.content().source());
            textRun.setTextDecoration(textDecoration);
            textRun.setColor(color);
            textRun.setBackground(background);
            return textRun;
        }

        private Image composeImage(InsertOperation operation) {
            Content content = operation.content();
            Image image  = document.createImage(content.source());
            return image;
        }

        private Link composeLink(InsertOperation operation) {
            String href = operation.attributes().getAsString("link");
            Link link = document.createLink(href, operation.content().source());
            return link;
        }

        private TextDecoration toTextDecoration(Model attributes) {
            TextDecoration textDecoration = new TextDecoration();
            attributes.keySet().stream().forEach(key -> {
                if(key.equalsIgnoreCase(TextDecoration.Weight.BOLD.name()) && attributes.getAsBoolean(key)) {
                    textDecoration.setWeight(TextDecoration.Weight.BOLD);
                } else if(key.equalsIgnoreCase(TextDecoration.Style.ITALIC.name()) && attributes.getAsBoolean(key)) {
                    textDecoration.setStyle(TextDecoration.Style.ITALIC);
                } else if(key.equalsIgnoreCase(TextDecoration.Line.UNDERLINE.name()) && attributes.getAsBoolean(key)) {
                    textDecoration.setLine(TextDecoration.Line.UNDERLINE);
                } else if(key.equalsIgnoreCase(TextDecoration.Line.STRIKE.name()) && attributes.getAsBoolean(key)) {
                    textDecoration.setLine(TextDecoration.Line.STRIKE);
                } else if(key.equalsIgnoreCase("script")) {
                    textDecoration.setVariant(TextDecoration.Variant.valueOf(attributes.getAsString("script").toUpperCase()));
                } else if(key.equalsIgnoreCase("size")) {
                    String value = attributes.getAsString("size");
                    TextDecoration.Size size;
                    if(value.equalsIgnoreCase("small")) {
                        size = TextDecoration.Size.SMALL;
                    } else if(value.equalsIgnoreCase("large")) {
                        size = TextDecoration.Size.LARGE;
                    } else if(value.equalsIgnoreCase("huge")) {
                        size = TextDecoration.Size.XXLARGE;
                    } else {
                        size = TextDecoration.Size.MEDIUM;
                    }
                    textDecoration.setSize(size);
                }
            });
            return textDecoration;
        }

        private Color toColor(Model attributes) {
            String hex = attributes.getAsString("color");
            if(hex==null) return null;
            Color color = new Color(hex);
            return color;
        }

        private Background toBackground(Model attributes) {
            String hex = attributes.getAsString("background");
            if(hex==null) return null;
            Background background = new Background();
            Color color = new Color(hex);
            background.setColor(color);
            return background;
        }

        private Align toAlign(Model attributes) {
            String value = attributes.getAsString("align");
            if(value==null) return null;
            Align align = Align.valueOf(value.toUpperCase());
            return align;
        }

    }
}
