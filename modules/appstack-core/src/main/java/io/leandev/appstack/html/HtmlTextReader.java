package io.leandev.appstack.html;

import io.leandev.appstack.text.ITextReader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.internal.StringUtil;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;

import java.io.*;

public class HtmlTextReader implements ITextReader {
    private final Document document;
    private BufferedReader reader;
    private int length;
    private int maxWidth = 0;

    public HtmlTextReader(String html, int maxWidth) {
        this.maxWidth = maxWidth;
        this.document = Jsoup.parse(html);
        String text = getPlainText(document.body());
        Reader inputString = new StringReader(text);
        this.length = text.length();
        this.reader = new BufferedReader(inputString);
    }

    public HtmlTextReader(String html) {
        this(html, 0);
    }

    @Override
    public String readLine() throws IOException {
        this.reader.mark(length);
        return this.reader.readLine();
    }

    @Override
    public String peekLine() throws IOException {
        this.mark();
        String line = this.readLine();
        this.reset();
        return line;
    }

    @Override
    public void mark() throws IOException {
        this.reader.mark(length);
    }

    @Override
    public void reset() throws IOException {
        this.reader.reset();
    }

    @Override
    public void close() throws IOException {
        this.reader.close();
    }

    /**
     * Format an Element to plain-text
     * @param element the root element to format
     * @return formatted text
     */
    private String getPlainText(Element element) {
        FormattingVisitor formatter = new FormattingVisitor(this.maxWidth);
        NodeTraversor.traverse(formatter, element); // walk the DOM, and call .head() and .tail() for each node

        return formatter.toString();
    }

    // the formatting rules, implemented in a breadth-first DOM traverse
    private static class FormattingVisitor implements NodeVisitor {
        private int maxWidth = 0;
        private int width = 0;
        private StringBuilder accum = new StringBuilder(); // holds the accumulated text

        public FormattingVisitor(int maxWidth) {
            this.maxWidth = maxWidth;
        }
        // hit when the node is first seen
        public void head(Node node, int depth) {
            String name = node.nodeName();
            if (node instanceof TextNode) {
                // TextNodes carry all user-readable text in the DOM.
                Node parent = node.parentNode();
                if (parent.nodeName().equals("a")&&parent.childNodeSize()==1) {
                    append(String.format("[%s](%s)", ((TextNode) node).text(), parent.absUrl("href")));
                } else {
                    append(((TextNode) node).text());
                }
            } else if (name.equals("li")) {
                append("\n * ");
            } else if (name.equals("dt")) {
                append("  ");
            } else if (StringUtil.in(name, "p", "h1", "h2", "h3", "h4", "h5", "tr", "div")) {
                append("\n");
            }
        }

        // hit when all of the node's children (if any) have been visited
        public void tail(Node node, int depth) {
            String name = node.nodeName();
            if (StringUtil.in(name, "br", "dd", "dt", "p", "h1", "h2", "h3", "h4", "h5")) {
                append("\n");
            }
        }

        // appends text to the string builder with a simple word wrap method
        private void append(String text) {
            text = removeLSEP(text);
            if (text.startsWith("\n"))
                // reset counter if starts with a newline. only from formats above, not in natural text
                width = 0;
            if (text.equals(" ") &&
                    (accum.length() == 0 || StringUtil.in(accum.substring(accum.length() - 1), " ", "\n")))
                return; // don't accumulate long runs of empty spaces

            if (maxWidth>0 && (text.length() + width > maxWidth)) {
                // won't fit, needs to wrap
                String[] words = text.split("\\s+");
                for (int i = 0; i < words.length; i++) {
                    String word = words[i];
                    boolean last = i == words.length - 1;
                    if (!last) {
                        // insert a space if not the last word
                        word = word + " ";
                    }
                    if (word.length() + width > maxWidth) {
                        // wrap and reset counter
                        accum.append("\n").append(word);
                        width = word.length();
                    } else {
                        accum.append(word);
                        width += word.length();
                    }
                }
            } else { // fits as is, without need to wrap text
                accum.append(text);
                width += text.length();
            }
        }

        private String removeLSEP(String text) {
            for(int i; (i = text.indexOf('\u2028'))!=-1;) {
                text = text.substring(0, i) + text.substring(i+1, text.length()-1);
            }
            return text;
        }

        @Override
        public String toString() {
            return accum.toString();
        }
    }
}
