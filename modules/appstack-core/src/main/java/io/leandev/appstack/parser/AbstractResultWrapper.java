package io.leandev.appstack.parser;

import java.util.Iterator;

public abstract class AbstractResultWrapper<T extends Record, E> implements Result<T> {
    protected Iterator<E> iterator;

    public AbstractResultWrapper(Iterator<E> iterator) {
        this.iterator = iterator;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public T next() {
        E record = iterator.next();
        return toRecord(record);
    }

    protected abstract T toRecord(E data);

}
