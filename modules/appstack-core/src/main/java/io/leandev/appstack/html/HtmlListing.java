package io.leandev.appstack.html;

import io.leandev.appstack.doc.*;

public class HtmlListing extends HtmlBlock implements Listing {
    private boolean ordered;

    public static HtmlListing of(Listing listing) {
        HtmlListing htmlListItem = new HtmlListing(listing.ordered());
        listing.chunks().stream().forEach(htmlListItem::appendChunk);
        return htmlListItem;
    }

    public HtmlListing() {
        this(false);
    }

    public HtmlListing(boolean ordered) {
        super(ordered? "ol" : "ul");
        this.ordered = ordered;
    }

    @Override
    public boolean ordered() {
        return this.ordered;
    }

    @Override
    public HtmlChunk appendChunk(Chunk chunk) {
        HtmlChunk htmlChunk;
        if(chunk instanceof HtmlChunk) {
            htmlChunk = (HtmlChunk) chunk;
        } else if(chunk instanceof TextRun) {
            htmlChunk = HtmlTextRun.of((TextRun) chunk);
        } else if(chunk instanceof Image) {
            htmlChunk = HtmlImage.of((HtmlImage) chunk);
        } else {
            htmlChunk = HtmlTextRun.of(chunk.content().toString());
        }

        HtmlElement container = new HtmlElement("li");
        container.append(htmlChunk);
        this.append(container);

        this.chunks.add(htmlChunk);

        return htmlChunk;
    }
}
