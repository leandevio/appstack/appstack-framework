package io.leandev.appstack.converter;

public class IntegerConverter implements Converter<Integer>{
    @Override
    public Integer convert(Object value) {
        Integer number;
        if(value instanceof Number) {
            number = toInteger((Number) value);
        } else if(value==null) {
            number = null;
        } else if(value instanceof String) {
            number = toInteger((String) value);
        } else {
            throw new ConversionException("Only support convert String and Number to Integer.");
        }
        return number;
    }

    private Integer toInteger(Number value) {
        Integer n = value.intValue();
        return n;
    }

    private Integer toInteger(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new ConversionException(String.format("Failed to convert the value: %s", value), e);
        }
    }
}
