package io.leandev.appstack.image;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import java.awt.image.BufferedImage;

public class QRCodeReader {
    private MultiFormatReader reader = new MultiFormatReader();

    public String decode(BufferedImage qrcode) throws ImageException {
        LuminanceSource source = new BufferedImageLuminanceSource(qrcode);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        try {
            Result result = reader.decode(bitmap);
            return result.getText();
        } catch (NotFoundException e) {
            throw new ImageException(e);
        }
    }

}
