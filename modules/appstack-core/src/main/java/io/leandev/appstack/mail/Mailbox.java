package io.leandev.appstack.mail;


import lombok.extern.slf4j.Slf4j;

import javax.mail.*;

@Slf4j
public class Mailbox {
    private Session session;

    private Store store;

    public Mailbox (Session session) {
        this.session = session;
        try {
            this.store = session.getStore("imap");
        } catch (NoSuchProviderException e) {
            throw new EmailException(e);
        }
    }

    public void connect(String username, String password) throws EmailException {
        try {
            this.store.connect(username, password);
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public boolean isConnected() {
        return this.store.isConnected();
    }

    public void close() throws MessagingException {
        this.store.close();
    }

    public Folder getInbox() throws EmailException {
        try {
            javax.mail.Folder inbox = this.store.getFolder("INBOX");

            return Folder.of(inbox);
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public Folder getFolder(String path) throws EmailException {
        try {
            javax.mail.Folder folder = this.store.getFolder(path);
            return Folder.of(folder);
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public Folder createFolder(String name) throws EmailException {
        try {
            javax.mail.Folder folder = this.store.getFolder(name);
            return folder.create(javax.mail.Folder.HOLDS_MESSAGES) ? Folder.of(folder) : null;
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }

    public boolean deleteFolder(String name) throws EmailException {
        try {
            javax.mail.Folder folder = this.store.getFolder(name);
            return folder.delete(false);
        } catch (MessagingException e) {
            throw new EmailException(e);
        }
    }
}
