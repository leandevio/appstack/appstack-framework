package io.leandev.appstack.file;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.stream.Stream;

public interface FileRepository extends AutoCloseable {

    OutputStream newOutputStream(Path path, OpenOption... openOptions) throws IOException;
    default OutputStream newOutputStream(Path path, boolean override) throws IOException {
        if(override) {
            return newOutputStream(path, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } else {
            return newOutputStream(path, StandardOpenOption.CREATE_NEW);
        }
    }

    void write(Path path, byte[] bytes, OpenOption... openOptions) throws IOException;
    default void write(Path path, byte[] bytes, boolean override) throws IOException {
        if(override) {
            write(path, bytes, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } else {
            write(path, bytes, StandardOpenOption.CREATE_NEW);
        }
    }

    void write(Path path, String text, OpenOption... openOptions) throws IOException;
    default void write(Path path, String text, boolean override) throws IOException {
        if(override) {
            write(path, text, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } else {
            write(path, text, StandardOpenOption.CREATE_NEW);
        }
    }

    InputStream newInputStream(Path path) throws IOException;

    boolean exists(Path uri);

    boolean isDirectory(Path uri);

    boolean isRegularFile(Path uri);

    long size(Path uri) throws IOException;

    boolean deleteIfExists(Path uri) throws IOException;

    void delete(Path uri) throws IOException;

    Path move(Path source, Path target) throws IOException;

    Stream<Path> list(Path uri, boolean hidden) throws IOException;

    default Stream<Path> list(Path uri) throws IOException {
        return list(uri, false);
    }

    void close();
}
