package io.leandev.appstack.csv;

import com.univocity.parsers.csv.CsvParserSettings;

public class CsvFormat {
    CsvParserSettings data = new CsvParserSettings();

    public CsvFormat() {
        data.setHeaderExtractionEnabled(true);
    }

    protected CsvParserSettings data() {
        return data;
    }
}
