package io.leandev.appstack.workbook;

import java.io.IOException;
import java.io.OutputStream;

public interface Workbook {

    int numberOfWorksheets();

    Worksheet createWorksheet(String name);

    Worksheet createWorksheet();

    Worksheet getWorksheetAt(int i);

    Worksheet getWorksheet(String name);

    void close() throws IOException;

    void write(OutputStream outputStream) throws IOException;
}
