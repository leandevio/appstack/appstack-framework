package io.leandev.appstack.geo;

/**
 *  A more complex representation that accounts for the existing structure of the transport network.
 */
public class TransportDistance implements GeoDistance {
    private GeoLocation origin;
    private GeoLocation destination;
    private Double distance;
//    private Long duration;
//    private BigDecimal fare;
//    private String currency;
//    private String unit;

    public TransportDistance(GeoLocation origin, GeoLocation destination, Double distance) {
        this.origin = origin;
        this.destination = destination;
        this.distance = distance;
    }

    @Override
    public GeoLocation getOrigin() {
        return origin;
    }

    @Override
    public void setOrigin(GeoLocation origin) {
        this.origin = origin;
    }

    @Override
    public GeoLocation getDestination() {
        return destination;
    }

    @Override
    public void setDestination(GeoLocation destination) {
        this.destination = destination;
    }

    @Override
    public Double getDistance() {
        return distance;
    }

    @Override
    public void setDistance(Double distance) {
        this.distance = distance;
    }
}
