package io.leandev.appstack.doc.delta;

public class Content {
    private String type;
    private String source;

    public Content(String type, String source) {
        this.type = type;
        this.source = source;
    }

    public Content(String text) {
        this.type = "text";
        this.source = text;
    }

    public String getType() {
        return type;
    }

    public String type() {
        return getType();
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSource() {
        return source;
    }

    public String source() {
        return getSource();
    }

    public void setSource(String source) {
        this.source = source;
    }
}
