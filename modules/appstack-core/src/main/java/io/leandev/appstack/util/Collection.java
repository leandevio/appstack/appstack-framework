package io.leandev.appstack.util;

import java.util.ArrayList;
import java.util.List;

public class Collection extends ArrayList<Model> implements List<Model> {
    public Collection() {
        super();
    }

    public Collection(int size) {
        super(size);
    }
}
