package io.leandev.appstack.converter;

public class ConversionException extends RuntimeException {
    public ConversionException(String message) {
        super(message);
    }
    public ConversionException(Throwable cause) {
        super(cause);
    }
    public ConversionException(String message, Throwable cause) {
        super(message, cause);
    }
}
