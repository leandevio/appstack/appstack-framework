package io.leandev.appstack.mail;

import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email {
    private static final Logger LOGGER = LogManager.getLogger(Email.class);
    private static final Pattern CHARSETPATTERN = Pattern.compile("charset=([a-zA-Z0-9\\-]*)(;|$| )+");

    private Envelope envelope;
    private Content content;
    private Message _message;

    public Email(Charset charset) {
        this.envelope = new Envelope(charset);
        this.content = new Content();
    }

    public Email() {
        this(StandardCharsets.UTF_8);
    }

    public static Email of(Message message) {
        Email email = new Email(message);
        return email;
    }

    protected Email(Message message) {
        this._message = message;
    }

    protected Envelope getEnvelope() throws EmailException {
        if(envelope==null) {
            try {
                this.envelope = toEnvelope(this._message);
            } catch (MessagingException e) {
                throw new EmailException(e);
            }
        }
        return this.envelope;
    }

    private Envelope toEnvelope(Message message) throws MessagingException {
        Charset charset = getCharset(message.getContentType());
        Envelope envelope = new Envelope(charset);
        javax.mail.UIDFolder uidFolder = (UIDFolder) message.getFolder();
        long uid = uidFolder.getUID(message);
        envelope.setUID(uid);
        envelope.setSubject(message.getSubject());
        envelope.setSentDate(message.getSentDate());
        envelope.setReceivedDate(message.getReceivedDate());
        envelope.setFrom((InternetAddress) message.getFrom()[0]);
        Address[] to = message.getRecipients(Message.RecipientType.TO);
        if(to!=null) {
            envelope.addRecipients(Message.RecipientType.TO, Arrays.copyOf(to, to.length, InternetAddress[].class));
        }
        Address[] cc = message.getRecipients(Message.RecipientType.CC);
        if(cc!=null) {
            envelope.addRecipients(Message.RecipientType.CC, Arrays.copyOf(cc, cc.length, InternetAddress[].class));
        }
        Address[] bcc = message.getRecipients(Message.RecipientType.BCC);
        if(bcc!=null) {
            envelope.addRecipients(Message.RecipientType.BCC, Arrays.copyOf(bcc, bcc.length, InternetAddress[].class));
        }
        Address[] replyTo =message.getReplyTo();
        if(replyTo!=null) {
            envelope.setReplyTo(Arrays.copyOf(replyTo, replyTo.length, InternetAddress[].class));
        }

        return envelope;
    }

    /**
     * mixed
     * -- alternative
     * -- -- text
     * -- -- related
     * -- -- -- html
     * -- -- -- inline image
     * -- -- -- inline image
     * -- attachment
     * -- attachment
     *
     *
     * mixed
     * -- related
     * -- -- alternative
     * -- -- -- text
     * -- -- -- html
     * -- -- inline image
     * -- -- inline image
     * -- attachment
     * -- attachment
     * @param message
     * @return
     * @throws MessagingException
     * @throws IOException
     */
    private Content toContent(Part message) throws MessagingException, IOException {
        Content content = new Content();
        if(message.isMimeType("text/*")) {
            content.addArticle(toArticle(message));
        } else if(message.isMimeType("multipart/alternative")||message.isMimeType("multipart/related")) {
            content.addArticle(toArticle(message));
        } else if(message.isMimeType("multipart/*")) {
            Multipart multipart = (Multipart) message.getContent();
            int count = multipart.getCount();
            for (int i = 0; i < count; i++) {
                Part part = multipart.getBodyPart(i);
                if (part.isMimeType("text/*")) {
                    content.addArticle(toArticle(part));
                } else if (part.isMimeType("multipart/alternative")||part.isMimeType("multipart/related")) {
                    content.addArticle(toArticle(part));
                } else if (part.isMimeType("multipart/*")) {
                    content.addContent(toContent(part));
                } else if(part.isMimeType("message/*") && part.getContent() instanceof Message) {
                    content.addEmail(Email.of((Message) part.getContent()));
                } else {
                    content.addAttachment(toAttachment(part));
                }
            }
        } else {
            content.addAttachment(toAttachment(message));
        }
        return content;
    }

    /**
     * alternative
     * -- text
     * -- related
     * -- -- html
     * -- -- inline image
     * -- -- inline image
     *
     *
     * related
     * -- alternative
     * -- -- text
     * -- -- html
     * -- inline image
     * -- inline image
     *
     * @param part
     * @return
     * @throws IOException
     * @throws MessagingException
     */
    private Article toArticle(Part part) throws IOException, MessagingException {
        Part main;
        List<Attachment> resources = new ArrayList<>();
        String text = null;
        if(part.isMimeType("multipart/alternative")) {
            Multipart multipart = (Multipart) part.getContent();
            main = multipart.getBodyPart(0);
            int count = multipart.getCount();
            for(int i=0; i< count; i++) {
                Part bodyPart = multipart.getBodyPart(i);
                if(bodyPart.isMimeType("text/plain")) {
                    text = bodyPart.getContent().toString();
                } else {
                    main = bodyPart;
                }
            }
        } else {
            main = part;
        }

        if(main.isMimeType("multipart/related")) {
            Multipart multipart = (Multipart) main.getContent();
            int count = multipart.getCount();
            for (int i = 0; i < count; i++) {
                Part bodyPart = multipart.getBodyPart(i);
                if (bodyPart.isMimeType("text/*")||bodyPart.isMimeType("multipart/alternative")) {
                    main = bodyPart;
                } else if (bodyPart.isMimeType("multipart/*")) {
                    LOGGER.debug(String.format("Could not recognize the content type inside the multipart/related: %s", bodyPart.getContentType()));
                } else {
                    resources.add(toAttachment(bodyPart));
                }
            }
        }

        if(main.isMimeType("multipart/alternative")) {
            Multipart multipart = (Multipart) main.getContent();
            int count = multipart.getCount();
            for(int i=0; i< count; i++) {
                Part bodyPart = multipart.getBodyPart(i);
                if(bodyPart.isMimeType("text/plain")) {
                    text = bodyPart.getContent().toString();
                } else {
                    main = bodyPart;
                }
            }
        }

        MediaType mediaType = getMediaType(main.getContentType());
        Charset charset = getCharset(main.getContentType());
        Object data;
        if(main.isMimeType("text/html")||main.isMimeType("text/plain")) {
            data = main.getContent().toString();
        } else if(main.isMimeType("text/calendar")) {
            data = main.getContent().toString();
        } else {
            data = main.getContent().toString();
        }

        return new Article(data, mediaType, charset, text, resources);
    }

    private Attachment toAttachment(Part part) throws IOException, MessagingException {
        InputStream input = (InputStream) part.getContent();
        int size = 0;
        byte[] bytes = new byte[input.available()];
        while ((size = input.available()) > 0) {
            int result = input.read(bytes);
            if (result == -1)
                break;
        }
        String filename = part.getFileName();
        MediaType mediaType = MediaType.valueOf(part.getContentType());
        input.close();
        return new Attachment(bytes, filename, mediaType);
    }

    private MediaType getMediaType(String contentType) {
        String[] tokens = contentType.split(";");
        return MediaType.valueOf(tokens[0]);
    }

    private Charset getCharset(String contentType) {
        Matcher m = CHARSETPATTERN.matcher(contentType);
        return m.find() ? Charset.forName(m.group(1)) : StandardCharsets.UTF_8;
    }

    public Long getUID() {
        return this.getEnvelope().getUID();
    }

    public Date getSentDate() {
        return this.getEnvelope().getSentDate();
    }

    public void setSentDate(Date sentDate) {
        this.getEnvelope().setSentDate(sentDate);
    }

    public Date getReceivedDate() throws EmailException {
        return this.getEnvelope().getReceivedDate();
    }

    public void setReceivedDate(Date receivedDate) {
        this.getEnvelope().setReceivedDate(receivedDate);
    }

    public Charset getCharset() {
        return this.getEnvelope().getCharset();
    }

    public void setFrom(InternetAddress address) {
        this.getEnvelope().setFrom(address);
    }

    public InternetAddress getFrom() {
        return this.getEnvelope().getFrom();
    }

    public void addRecipient(Message.RecipientType type, InternetAddress address) {
        this.getEnvelope().addRecipient(type, address);
    }

    public void addRecipients(Message.RecipientType type, List<InternetAddress> addresses) {
        this.getEnvelope().addRecipients(type, addresses);
    }

    public void addRecipients(Message.RecipientType type, InternetAddress[] addresses) {
        this.getEnvelope().addRecipients(type, addresses);
    }

    public void addReplyTo(InternetAddress address) {
        this.getEnvelope().addReplyTo(address);
    }

    public List<InternetAddress> getReplyTo() {
        return this.getEnvelope().getReplyTo();
    }

    public void setReplyTo(List<InternetAddress> addresses) {
        this.getEnvelope().setReplyTo(addresses);
    }

    public void setReplyTo(InternetAddress[] addresses) {
        this.getEnvelope().setReplyTo(addresses);
    }

    public void setSubject(String subject) {
        this.getEnvelope().setSubject(subject);
    }

    public String getSubject() {
        return this.getEnvelope().getSubject();
    }

    public List<InternetAddress> getRecipients(Message.RecipientType type) {
        return this.getEnvelope().getRecipients(type);
    }

    public List<InternetAddress> getAllRecipients() {
        return this.getEnvelope().getAllRecipients();
    }

    protected Content getContent() {
        if(this.content==null) {
            try {
                this.content = toContent(this._message);
            } catch (MessagingException | IOException e) {
                throw new EmailException(e);
            }
        }
        return this.content;
    }

    public void addArticle(Article article) {
        this.getContent().addArticle(article);
    }

    public void addArticle(Object data, MediaType mediaType, Charset charset, String text) {
        this.getContent().addArticle(data, mediaType, charset, text);
    }

    public void addArticle(Object data, MediaType mediaType, String text) {
        addArticle(data, mediaType, this.getCharset(), text);
    }

    public void setContent(Object data, MediaType mediaType, Charset charset, String text) {
        this.getContent().clear();
        this.getContent().addArticle(data, mediaType, charset, text);
    }

    public void setContent(Object data, MediaType mediaType, String text) {
        setContent(data, mediaType, this.getCharset(), text);
    }

    public List<Article> getArticles() {
        return this.getContent().getArticles();
    }

    public Article getArticle() {
        return this.getContent().getArticle();
    }

    public void addAttachment(Attachment attachment) {
        this.content.addAttachment(attachment);
    }

    public void addAttachment(byte[] bytes, String name) {
        this.content.addAttachment(bytes, name);
    }

    public void addAttachment(InputStream inputStream, String name) throws IOException {
        this.content.addAttachment(inputStream, name);
    }

    public void addAttachment(File file) throws IOException {
        this.content.addAttachment(file);
    }

    public List<Attachment> getAttachments() {
        return this.content.getAttachments();
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public boolean isMultipart() {
        return this.content.isMultipart();
    }
}
