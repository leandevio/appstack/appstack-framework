package io.leandev.appstack.image;

/**
 *
 * scaling can be set with one of these five values:
 * - fill: this is the default value which stretches the image to fit the content box, regardless of its aspect-ratio.
 * - contain: increases or decreases the size of the image to fill the box whilst preserving its aspect-ratio.
 * - cover: the image will fill the height and width of its box, once again maintaining its aspect ratio but often cropping the image in the process.
 * - none: image will ignore the height and width of the parent and retain its original size.
 * - scale-down: the image will compare the difference between none and contain in order to find the smallest concrete object size.
 *
 */
public enum Scaling {
    FIT("fit"),
    CONTAIN("contain"),
    COVER("cover"),
    NONE("none"),
    SCALE_DOWN("scale-down");

    private final String name;

    Scaling(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }
}
