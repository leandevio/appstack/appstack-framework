package io.leandev.appstack.pdf;

import org.apache.pdfbox.pdmodel.encryption.AccessPermission;

public class PdfAccessPermission {
    private AccessPermission accessPermission = new AccessPermission();

    public void setAllowPrinting(boolean allowPrinting) {
        accessPermission.setCanPrint(allowPrinting);
        accessPermission.setCanPrintDegraded(allowPrinting);
    }

    public void setAllowModification(boolean allowModification) {
        accessPermission.setCanModify(allowModification);
        accessPermission.setCanModifyAnnotations(allowModification);
        accessPermission.setCanFillInForm(allowModification);
        accessPermission.setCanAssembleDocument(allowModification);
    }

    public void setAllowExtraction(boolean allowExtraction) {
        accessPermission.setCanExtractContent(allowExtraction);
        accessPermission.setCanExtractForAccessibility(allowExtraction);
    }

    protected AccessPermission getAccessPermission() {
        return this.accessPermission;
    }
}
