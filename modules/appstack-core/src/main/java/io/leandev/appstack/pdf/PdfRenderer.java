package io.leandev.appstack.pdf;

import org.apache.pdfbox.rendering.PDFRenderer;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class PdfRenderer {
    private PDFRenderer renderer;

    public PdfRenderer(PdfDocument pdfDocument) {
        this.renderer = new PDFRenderer(pdfDocument.getPDDocument());
    }

    public BufferedImage renderImage(int pageIndex) throws IOException {
        BufferedImage bufferedImage = this.renderer.renderImage(pageIndex);
        return bufferedImage;
    }
}
