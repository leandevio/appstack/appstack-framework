package io.leandev.appstack.security.domain;

public class RememberMeSecurityToken  extends SecurityToken {
    public RememberMeSecurityToken(Object principal) {
        super(principal);
    }
}
