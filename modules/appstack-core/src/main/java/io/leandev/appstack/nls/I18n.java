package io.leandev.appstack.nls;

import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import io.leandev.appstack.util.Environment;
import io.leandev.appstack.json.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class I18n {
    private static final Logger LOGGER = LogManager.getLogger(I18n.class);
    private static final String DATETIMEPATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final String DATEPATTERN = "yyyy-MM-dd";
    private static final String TIMEPATTERN = "HH:mm";
    private static final String FONTFAMILY = "Arial";
    private static final String NUMBERPATTERN = "#,##0.##";
    private static final String INTEGERPATTERN = "#,##0";
    private static final String FLOATPATTERN = "#,##0.##";
    private static final String MONEYPATTERN = "$#,##0.##";

    private static final String DATETIMEPATTERNKEY = "datetimePattern";
    private static final String DATEPATTERNKEY = "datePattern";
    private static final String TIMEPATTERNKEY = "timePattern";
    private static final String NUMBERPATTERNKEY = "numberPattern";
    private static final String INTEGERPATTERNKEY = "integerPattern";
    private static final String FLOATPATTERNKEY = "floatPattern";
    private static final String MONEYPATTERNKEY = "moneyPattern";
    private static final String FONTFAMILYKEY = "fontFamily";
    private Locale locale;

    private Map<String, Dictionary> dictionaries = new HashMap<>();

    private static class SingletonHolder {
        private static final I18n INSTANCE = new I18n();
    }

    public static I18n getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    public I18n() {
        this(null);
    }

    public I18n(Locale locale) {
        this.locale = (locale == null) ? Environment.getDefaultInstance().locale() : locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Locale locale() {
        return this.locale;
    }

    public String translate(String text) {
        return translate(text, this.locale);
    }

    public String translate(String text, Locale locale) {
        String[] namespaces = {"term", "message"};

        String translation = null;
        for(String namespace : namespaces) {
            translation = interpret(text, namespace, locale);
            if(translation!=null) break;
        }

        return (translation==null) ? text : translation;
    }

    public String translate(String text, String namespace) {
        return translate(text, namespace, this.locale);
    }

    public String translate(String text, String namespace, Locale locale) {
        String translation = interpret(text, namespace, locale);
        return (translation==null) ? text : translation;
    }


    public String translate(String text, String namespace, String language) {
        Locale locale = Locale.forLanguageTag(language);
        return translate(text, namespace, locale);
    }

    public String interpret(String text, String namespace, Locale locale) {
        String translation = get(text, namespace, locale);
        if(translation==null&&locale.getCountry().length()>0) {
            String language = locale.getLanguage();
            translation = get(text, namespace, language);
        }
        return translation;
    }

    public String term(String text, Locale locale) {
        return this.translate(text, "term", locale);
    }

    public String term(String text) {
        return this.translate(text, "term", this.locale);
    }

    public String message(String text, Locale locale) {
        return this.translate(text, "message", locale);
    }

    public String message(String text) {
        return this.translate(text, "message", this.locale);
    }

    public String datetimePattern() {
        return this.datetimePattern(DATETIMEPATTERN);
    }

    public String datetimePattern(String defaultDatetimePattern) {
        return this.datetimePattern(locale, defaultDatetimePattern);
    }

    public String datetimePattern(Locale locale) {
        return this.datetimePattern(locale, DATETIMEPATTERN);
    }

    public String datetimePattern(Locale locale, String defaultDatetimePattern) {
        String datetimePattern = this.interpret(DATETIMEPATTERNKEY, "app", locale);
        if(datetimePattern==null||datetimePattern.length()==0) {
            datetimePattern = defaultDatetimePattern;
        }
        return datetimePattern;
    }

    public String datePattern() {
        return this.datePattern(DATEPATTERN);
    }

    public String datePattern(String defaultDatePattern) {
        return this.datePattern(locale, defaultDatePattern);
    }

    public String datePattern(Locale locale) {
        return this.datePattern(locale, DATEPATTERN);
    }

    public String datePattern(Locale locale, String defaultDatePattern) {
        String datePattern = this.interpret(DATEPATTERNKEY, "app", locale);
        if(datePattern==null||datePattern.length()==0) {
            datePattern = defaultDatePattern;
        }
        return datePattern;
    }

    public String timePattern() {
        return this.datePattern(TIMEPATTERN);
    }

    public String timePattern(String defaultDatePattern) {
        return this.timePattern(locale, defaultDatePattern);
    }

    public String timePattern(Locale locale) {
        return this.timePattern(locale, TIMEPATTERN);
    }

    public String timePattern(Locale locale, String defaultDatePattern) {
        String datePattern = this.interpret(TIMEPATTERNKEY, "app", locale);
        if(datePattern==null||datePattern.length()==0) {
            datePattern = defaultDatePattern;
        }
        return datePattern;
    }

    public String numberPattern() {
        return this.floatPattern(NUMBERPATTERN);
    }

    public String numberPattern(String defaultPattern) {
        return this.numberPattern(locale, defaultPattern);
    }

    public String numberPattern(Locale locale) {
        return this.numberPattern(locale, NUMBERPATTERN);
    }

    public String numberPattern(Locale locale, String defaultPattern) {
        String pattern = this.interpret(NUMBERPATTERNKEY, "app", locale);
        if(pattern==null||pattern.length()==0) {
            pattern = defaultPattern;
        }
        return pattern;
    }

    public String moneyPattern() {
        return this.moneyPattern(MONEYPATTERN);
    }

    public String moneyPattern(String defaultPattern) {
        return this.moneyPattern(locale, defaultPattern);
    }

    public String moneyPattern(Locale locale) {
        return this.moneyPattern(locale, MONEYPATTERN);
    }

    public String moneyPattern(Locale locale, String defaultPattern) {
        String pattern = this.interpret(MONEYPATTERNKEY, "app", locale);
        if(pattern==null||pattern.length()==0) {
            pattern = defaultPattern;
        }
        return pattern;
    }

    public String integerPattern() {
        return this.integerPattern(INTEGERPATTERN);
    }

    public String integerPattern(String defaultPattern) {
        return this.integerPattern(locale, defaultPattern);
    }

    public String integerPattern(Locale locale) {
        return this.integerPattern(locale, INTEGERPATTERN);
    }

    public String integerPattern(Locale locale, String defaultPattern) {
        String pattern = this.interpret(INTEGERPATTERNKEY, "app", locale);
        if(pattern==null||pattern.length()==0) {
            pattern = defaultPattern;
        }
        return pattern;
    }

    public String floatPattern() {
        return this.floatPattern(FLOATPATTERN);
    }

    public String floatPattern(String defaultPattern) {
        return this.floatPattern(locale, defaultPattern);
    }

    public String floatPattern(Locale locale) {
        return this.floatPattern(locale, FLOATPATTERN);
    }

    public String floatPattern(Locale locale, String defaultPattern) {
        String pattern = this.interpret(FLOATPATTERNKEY, "app", locale);
        if(pattern==null||pattern.length()==0) {
            pattern = defaultPattern;
        }
        return pattern;
    }

    public String fontFamily() {
        return this.fontFamily(locale, FONTFAMILY);
    }

    public String fontFamily(String defaultFontFamily) {
        return this.fontFamily(locale, defaultFontFamily);
    }

    public String fontFamily(Locale locale) {
        return this.fontFamily(locale, FONTFAMILY);
    }

    public String fontFamily(Locale locale, String defaultFontFamily) {
        String fontFamily = this.interpret(FONTFAMILYKEY, "app", locale);
        if(fontFamily==null||fontFamily.length()==0) {
            fontFamily = defaultFontFamily;
        }
        return fontFamily;
    }

    public String get(String key, String namespace, String language) {
        Book book = book(namespace, language);
        String translation = book.get(key);
        return translation;
    }

    public String get(String key, String namespace, Locale locale) {
        Book book = book(namespace, locale);
        String translation = book.get(key);
        return translation;
    }

    private Book book(String namespace, Locale locale) {
        Dictionary dictionary = dictionary(locale);
        Book book = (dictionary==null) ? null : dictionary.get(namespace);
        if(book==null) {
            book = fetch(namespace, locale);
        }
        return book;
    }

    private Book book(String namespace, String language) {
        Locale locale = Locale.forLanguageTag(language);
        return book(namespace, locale);
    }

    private Dictionary dictionary(Locale locale) {
        String language = locale.toLanguageTag();
        return this.dictionaries.get(language);
    }

    private Dictionary dictionary(String language) {
        return this.dictionaries.get(language);
    }

    private Book fetch(String namespace, Locale locale) {
        String language = locale.toLanguageTag();
        String resource = String.format("nls/%s/%s.json", language, namespace);
        ClassLoader loader = I18n.class.getClassLoader();
        Book book = new Book();
        if(loader.getResource(resource)!=null) {
            try (InputStream in = loader.getResourceAsStream(resource);
                 InputStreamReader reader = new InputStreamReader(in,  StandardCharsets.UTF_8)) {
                JsonParser parser = new JsonParser();
                book = parser.readValue(reader, Book.class);
            } catch (IOException e) {
                LOGGER.warn(String.format("Failed to load the resource - %s, Reason:", resource), e);
            }
        }

        Dictionary dictionary = dictionary(locale);

        if(dictionary==null) {
            dictionary = new Dictionary(locale);
            this.dictionaries.put(language, dictionary);
        }
        dictionary.put(namespace, book);

        return book;
    }
}
