package io.leandev.appstack.docx;

import io.leandev.appstack.image.ImageReader;
import io.leandev.appstack.image.ImageWriter;
import io.leandev.appstack.measure.Length;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFPicture;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGraphicalObject;
import org.openxmlformats.schemas.drawingml.x2006.wordprocessingDrawing.CTAnchor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDrawing;
import org.springframework.http.MediaType;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class DocxImage {
    // 使用與 MS Word 相同設定
    private static final int PPI = 150;
    private static final int DPI = 72;
    private static final double PT2PX = (double) PPI / DPI;

    private XWPFRun xwpfRun;
    private XWPFPicture xwpfPicture;
    private String filename;
    private double width;
    private double height;

    protected DocxImage(XWPFRun xwpfRun, InputStream image, double width, double height) throws IOException, InvalidFormatException {
        this.xwpfRun = xwpfRun;
        this.width = width;
        this.height = height;
        this.addPicture(image);
    }

    protected DocxImage(XWPFRun xwpfRun, InputStream image, double width) throws IOException, InvalidFormatException {
        this.xwpfRun = xwpfRun;
        ImageReader imageReader = new ImageReader(image);
        BufferedImage bufferedImage = imageReader.read();
        imageReader.close();
        double ratio =(double) bufferedImage.getHeight() / bufferedImage.getWidth();
        this.width = width;
        this.height = width * ratio;
        this.addPicture(bufferedImage);
    }

    protected DocxImage(XWPFRun xwpfRun, InputStream image) throws IOException, InvalidFormatException {
        this.xwpfRun = xwpfRun;
        ImageReader imageReader = new ImageReader(image);
        BufferedImage bufferedImage = imageReader.read();
        imageReader.close();
        this.width = (double) bufferedImage.getWidth() / PT2PX;
        this.height = (double) bufferedImage.getHeight() / PT2PX;
        this.addPicture(bufferedImage);
    }

    protected DocxImage(XWPFRun xwpfRun, BufferedImage image, double width, double height) throws IOException, InvalidFormatException {
        this.xwpfRun = xwpfRun;
        this.width = width;
        this.height = height;
        this.addPicture(image);
    }

    protected DocxImage(XWPFRun xwpfRun, BufferedImage image, double width) throws IOException, InvalidFormatException {
        this.xwpfRun = xwpfRun;
        double ratio = (double) image.getHeight() / image.getWidth();
        this.width = width;
        this.height = width * ratio;
        this.addPicture(image);
    }

    protected DocxImage(XWPFRun xwpfRun, BufferedImage image, Length width, Length height) throws IOException, InvalidFormatException {
        this(xwpfRun, image, width.toPoint(), height.toPoint());
    }

    protected DocxImage(XWPFRun xwpfRun, BufferedImage image, Length width) throws IOException, InvalidFormatException {
        this(xwpfRun, image, width.toPoint());
    }

    protected DocxImage(XWPFRun xwpfRun, BufferedImage image) throws IOException, InvalidFormatException {
        this.xwpfRun = xwpfRun;
        this.width = (double) image.getWidth() / PT2PX;
        this.height = (double) image.getHeight() / PT2PX;
        this.addPicture(image);
    }

    private void addPicture(BufferedImage picture) throws IOException, InvalidFormatException {
        int emuWidth = Units.toEMU(width), emuHeight = Units.toEMU(height);

        this.filename = UUID.randomUUID().toString();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ImageWriter imageWriter = new ImageWriter(output);
        imageWriter.write(picture);
        imageWriter.close();

        InputStream imageStream = new ByteArrayInputStream(output.toByteArray());

        XWPFPicture xwpfPicture = xwpfRun.addPicture(imageStream, XWPFDocument.PICTURE_TYPE_PNG, this.filename, emuWidth, emuHeight);
        this.xwpfPicture = xwpfPicture;

    }

    private void addPicture(InputStream picture) throws IOException, InvalidFormatException {
        ImageReader imageReader = new ImageReader(picture);
        BufferedImage image = imageReader.read();
        imageReader.close();

        this.addPicture(image);

    }

    public void setPosition(int left, int top, boolean behind) throws DocxException {
        int emuWidth = Units.toEMU(width), emuHeight = Units.toEMU(height);
        CTDrawing drawing = xwpfRun.getCTR().getDrawingArray(0);
        CTGraphicalObject graphicalobject = drawing.getInlineArray(0).getGraphic();

        //Replace the newly inserted picture, add CTAnchor, set floating attribute, delete inline attribute
        CTAnchor anchor = null;//The left offset of the existing content of the paragraph needs to be calculated relative to the current paragraph position
        try {
            anchor = this.getAnchorWithGraphic(graphicalobject, filename,
                    emuWidth, emuHeight,//picture size
                    Units.toEMU(left), Units.toEMU(top), behind);
        } catch (XmlException e) {
            throw new DocxException("Failed to get the anchor to set the position.", e);
        }
        drawing.setAnchorArray(new CTAnchor[]{anchor});//Add floating attribute
        drawing.removeInline(0);//Remove inline attributes

    }

    public void setPosition(Length left, Length top, boolean behind) throws DocxException {
        this.setPosition((int) left.toPoint(), (int) top.toPoint(), behind);
    }

    public void setPosition(Length left, Length top) throws DocxException {
        this.setPosition(left, top, false);
    }

    public void setPosition(int left, int top) throws DocxException {
        this.setPosition(left, top, false);
    }

    private int toPictureType(MediaType mediaType) throws DocxException {
        if(mediaType.isCompatibleWith(MediaType.IMAGE_PNG)) {
            return XWPFDocument.PICTURE_TYPE_PNG;
        } else if(mediaType.isCompatibleWith(MediaType.IMAGE_JPEG)) {
            return XWPFDocument.PICTURE_TYPE_JPEG;
        } else if(mediaType.isCompatibleWith(MediaType.IMAGE_GIF)) {
            return XWPFDocument.PICTURE_TYPE_GIF;
        } else {
            String message = String.format("Unsupported Media Type: %s", mediaType.toString());
            throw new DocxException(message);
        }
    }

    private CTAnchor getAnchorWithGraphic(CTGraphicalObject ctGraphicalObject,
                                          String deskFileName, int width, int height,
                                          int leftOffset, int topOffset, boolean behind) throws XmlException {
        String anchorXML =
                "<wp:anchor xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\" "
                        + "simplePos=\"0\" relativeHeight=\"0\" behindDoc=\"" + ((behind) ? 1 : 0) + "\" locked=\"0\" layoutInCell=\"1\" allowOverlap=\"1\">"
                        + "<wp:simplePos x=\"0\" y=\"0\"/>"
                        + "<wp:positionH relativeFrom=\"page\">"
                        + "<wp:posOffset>" + leftOffset + "</wp:posOffset>"
                        + "</wp:positionH>"
                        + "<wp:positionV relativeFrom=\"page\">"
                        + "<wp:posOffset>" + topOffset + "</wp:posOffset>" +
                        "</wp:positionV>"
                        + "<wp:extent cx=\"" + width + "\" cy=\"" + height + "\"/>"
                        + "<wp:effectExtent l=\"0\" t=\"0\" r=\"0\" b=\"0\"/>"
                        + "<wp:wrapNone/>"
                        + "<wp:docPr id=\"1\" name=\"Drawing 0\" descr=\"" + deskFileName + "\"/><wp:cNvGraphicFramePr/>"
                        + "</wp:anchor>";

        CTDrawing drawing = CTDrawing.Factory.parse(anchorXML);
        CTAnchor anchor = drawing.getAnchorArray(0);
        anchor.setGraphic(ctGraphicalObject);
        return anchor;
    }
}
