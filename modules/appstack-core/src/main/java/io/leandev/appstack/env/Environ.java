package io.leandev.appstack.env;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;

/**
 * The class represents the application runtime environment. The implementation provides:
 * - read the properties of the runtime environment
 * - modify the properties of the runtime environment
 *
 * Environment look for the system property file & the default property file in the class path and use them as
 * the defaults to set up the environment first, then the customized property file in the application base directory
 * to customize the environment. The properties in the system property file are loaded first and can not be overridden by
 * the default property file or the custom property file. The properties in the default property file are defaults and
 * can be overridden by those specified in the customized property file.
 *
 * The name of the system property file is appstack.properties. It is in the class path.
 *
 * The file name of the property file is the application name with the "properties" extension. Environment read the
 * value of ${app.name} from the system property file to determine the application name. The default is "app"
 * if not specified. Therefore, the default application property file is app.properties.
 *
 * The application home is the location for the application binary distribution and default configurations. It can be
 * specify by the environment variable (or the system property). The name of the variable is the uppercase of the
 * application name with "_HOME" suffix. The default is "APP_HOME".
 *
 * The application base is the location for the customizations to application configurations and the data repository.
 * It can be specified by the environment variable (or the system property). The name of the variable is the uppercase of the
 * application name with "_BASE" suffix. The default is "APP_BASE".
 *
 * The application base is the same as the application home by default.
 *
 * Usage:
 *
 * ```java
 * Environment environment = Environment.getDefaultInstance();
 *
 * assertEquals(environment.getProperty("session.timeout"), "30");
 * assertEquals(environment.getPropertyAsInteger("batch.timeout"), 180);
 * assertEquals(environment.getPropertyAsInteger("helpdesk.email"), "alice@leandev.com.tw");
 * ```
 *
 * app.properties
 *
 * ```
 * # File Storage
 * data.folder=var/data
 * index.folder=var/index
 * log.folder=var/log
 * cache.folder=var/cache
 * tmp.folder=var/tmp
 *
 * # Locale
 * locale.language=en
 * locale.country=US
 *
 * #Time
 * time.timeZone=America/Los_Angeles
 * time.dateTimePattern=yyyy-MM-ddTHH:mm:ss.SSSZ
 * #Resource
 * resource.encoding=UTF-8
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Environ {
    private static final String PROPFILEEXTENSION = "properties";
    private static final String SYSPROPERTYFILE = "appstack.properties";

    private static final Path APPHOME = Paths.get(System.getProperty("user.home"));

    private static final String CONFFOLDER = "conf";

    // system property
    // keys
    private static final String APPNAMEKEY = "app.name";
    private static final String APPVERSIONKEY = "app.version";
    // default values
    private static final String APPNAME = "app";
    private static final String APPVERSION = "1.0.0";

    private static final String[] SYSTEMPROPERTIES = {APPNAMEKEY, APPVERSIONKEY};

    // application property
    // keys
    private static final String APPSTAGEKEY = "appstack.app.stage";

    private static final String LOGFOLDERKEY = "appstack.folder.log";
    private static final String DATAFOLDERKEY = "appstack.folder.data";
    private static final String INDEXFOLDERKEY = "appstack.folder.index";
    private static final String TPLFOLDERKEY = "appstack.folder.tpl";
    private static final String CACHEFOLDERKEY = "appstack.folder.cache";
    private static final String TMPFOLDERKEY = "appstack.folder.tmp";
    private static final String BACKUPFOLDERKEY = "appstack.folder.backup";

    private static final String LOCALELANGUAGEKEY = "appstack.locale.language";
    private static final String LOCALECOUNTRYKEY = "appstack.locale.country";

    private static final String TIMEZONEKEY = "appstack.time.timezone";
    private static final String TIMEFORMATKEY = "appstack.time.format";

    // default values
    private static final Stage APPSTAGE = Stage.PRODUCTION;

    private static final String DATAFOLDER = "var/data";
    private static final String INDEXFOLDER = "var/index";
    private static final String TPLFOLDER = "var/tpl";
    private static final String LOGFOLDER = "var/log";
    private static final String CACHEFOLDER = "var/cache";
    private static final String TMPFOLDER = "var/tmp";
    private static final String BACKUPFOLDER = "var/backup";

    private static final Locale LOCALE = Locale.getDefault();

    private static final TimeZone TIMEZONE = TimeZone.getDefault();
    private static final String TIMEFORMAT = "yyyy-MM-ddTHH:mm:ss.SSSZ";

    private static final Pattern WINABSOLUTEFILEPATHPATTERN = Pattern.compile("^[a-zA-Z]{1}:\\\\");

    private Set<String> sysProps = new HashSet<>();
    private Properties props = new Properties();

    private String appName;
    private Stage stage;
    private Path appHome, appBase;
    private String appVersion;

    private Locale locale;
    private TimeZone timezone;
    private String timeFormat;
    private Path dataFolder, indexFolder, tplFolder, logFolder, cacheFolder, tmpFolder, backupFolder;

    private static class SingletonHolder {
        private static final Environ INSTANCE = new Environ();
    }

    public static Environ getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public Environ() {
        // read the system property file.
        readSystemProperties();

        // setup application home location
        setupAppHome();

        // read the default property file.
        readDefaultProperties();

        // read the custom property file.
        readCustomProperties();

        // use the properties to set up the context variables
        setupContext();

        // print the effective properties
        print();
    }

    public Path appHome() {
        return appHome;
    }

    public Path appBase() {
        return appBase;
    }

    public String appName() {
        return appName;
    }

    protected void setAppName(String appName) {
        this.appName = (appName==null) ? APPNAME : appName;
    }

    public String appVersion() {
        return this.appVersion;
    }

    public Stage stage() {
        return stage;
    }

    public boolean isNamedApp() {
        return !appName().equals(APPNAME);
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> properties() {
        Map props = this.props;
        Map<String, String> map = (Map<String, String>) props;
        return new HashMap<>(map);
    }

    public String property(String key) {
        return property(key, null);
    }

    public String property(String key, String defaultValue) {
        return props.getProperty(key, defaultValue);
    }

    public Integer propertyAsInteger(String key, Integer defaultValue) {
        return props.containsKey(key) ? Integer.parseInt(props.getProperty(key)) : defaultValue;
    }

    public Long propertyAsLong(String key) {
        return propertyAsLong(key, null);
    }

    public Long propertyAsLong(String key, Long defaultValue) {
        return props.containsKey(key) ? Long.parseLong(props.getProperty(key)) : defaultValue;
    }

    public Integer propertyAsInteger(String key) {
        return propertyAsInteger(key, null);
    }


    public boolean propertyAsBoolean(String key, Boolean defaultValue) {
        return props.containsKey(key) ? Boolean.parseBoolean(props.getProperty(key)) : defaultValue;
    }

    public boolean propertyAsBoolean(String key) {
        return propertyAsBoolean(key, false);
    }

    public String[] propertyAsArray(String key, String[] defaultValue) {
        if(!props.containsKey(key)) return defaultValue;
        String s = property(key, "").trim();
        if(s.isEmpty()) return new String[0];
        String[] value = s.split(",");
        for(int i=0; i<value.length; i++) {
            value[i] = value[i].trim();
        }
        return value;
    }

    public String[] propertyAsArray(String key) {
        return this.propertyAsArray(key, new String[0]);
    }

    public Path propertyAsPath(String key, String defaultValue, Boolean create) {
        String value = property(key, defaultValue);
        Path path;

        if(value.startsWith("~")) {
            path = Paths.get(value.replaceFirst("~", System.getProperty("user.home")));
        } else if(value.startsWith("\\.")) {
            path = appBase().resolve(value);
        } else if(!value.startsWith("/") && !WINABSOLUTEFILEPATHPATTERN.matcher(value).find()) {
            path = appBase().resolve(value);
        } else {
            path = Paths.get(value);
        }

        if(create) createFolder(path);

        return path;
    }

    public Path propertyAsPath(String key, String defaultValue) {
        return propertyAsPath(key, defaultValue, false);
    }

    public Path propertyAsNamedPath(String key, String defaultValue, Boolean create) {
        Path path = propertyAsPath(key, defaultValue, create);

        if(isNamedApp()) {
            path = path.resolve(appName());
        }

        if(create) createFolder(path);

        return path;
    }


    public Path propertyAsNamedPath(String key, String defaultValue) {
        return propertyAsNamedPath(key, defaultValue, false);
    }

    public URL propertyAsURL(String key, URL defaultValue) {
        String value = property(key);
        if(value==null) return defaultValue;

        URL url;

        try {
            url = new URL(value);
        } catch (MalformedURLException e) {
//            this.warn(e);
            url = null;
        }

        return url;

    }

    public boolean containsProperty(String key) {
        return props.containsKey(key);
    }

    public TimeZone propertyAsTimeZone(String key, TimeZone defaultValue) {
        return containsProperty(key) ? TimeZone.getTimeZone(property(TIMEZONEKEY)) : defaultValue;
    }

    public synchronized void setProperty(String key, String value) {
        setProperty(key, value, true);
    }

    public synchronized void setProperty(String key, String value, boolean overwrite) {
        if(!overwrite && containsProperty(key)) return;
        props.put(key, value);
    }

    public synchronized void setProperty(String key, Number value) {
        setProperty(key, value.toString());
    }


    public synchronized void setProperty(String key, Number value, boolean overwrite) {
        setProperty(key, value.toString(), overwrite);
    }

    public Locale locale() {
        return locale;
    }

    public synchronized void setLocale(Locale locale) {
        this.locale = locale;
        setProperty(LOCALELANGUAGEKEY, locale.getLanguage());
        setProperty(LOCALECOUNTRYKEY, locale.getCountry());
    }

    public TimeZone timezone() {
        return timezone;
    }

    public synchronized void setTimezone(TimeZone timezone) {
        this.timezone = timezone;
    }

    public Path dataFolder() {
        return dataFolder;
    }

    public Path indexFolder() {
        return indexFolder;
    }

    public Path tplFolder() {
        return tplFolder;
    }

    public Path logFolder() {
        return logFolder;
    }

    public Path cacheFolder() {
        return cacheFolder;
    }

    public Path tmpFolder() {
        return tmpFolder;
    }

    public Path backupFolder() {
        return backupFolder;
    }

    public Path conf(String filename) {
        return appHome().resolve(CONFFOLDER).resolve(filename);
    }

    private void print() {
        Set<String> keys = props.stringPropertyNames();
        List<String> names = new ArrayList<>(keys);
        Collections.sort(names);

        info("Effective application properties:");
        for(String name : names) {
            String value = props.getProperty(name);
            info(name + " => " + value);
        }
    }

    private void setupContext() {
        this.stage = property(APPSTAGEKEY)==null ? APPSTAGE : Stage.valueOf(property(APPSTAGEKEY));
        this.locale = (property(LOCALELANGUAGEKEY)==null||property(LOCALECOUNTRYKEY)==null) ? LOCALE : new Locale(property(LOCALELANGUAGEKEY), property(LOCALECOUNTRYKEY));
        this.timezone = propertyAsTimeZone(TIMEZONEKEY, TIMEZONE);
        this.dataFolder = propertyAsNamedPath(DATAFOLDERKEY, DATAFOLDER, true);
        this.indexFolder = propertyAsNamedPath(INDEXFOLDERKEY, INDEXFOLDER, true);
        this.tplFolder = propertyAsNamedPath(TPLFOLDERKEY, TPLFOLDER, true);
        this.logFolder = propertyAsNamedPath(LOGFOLDERKEY, LOGFOLDER, true);
        this.cacheFolder = propertyAsNamedPath(CACHEFOLDERKEY, CACHEFOLDER, true);
        this.tmpFolder = propertyAsNamedPath(TMPFOLDERKEY, TMPFOLDER, true);
        this.backupFolder = propertyAsNamedPath(BACKUPFOLDERKEY, BACKUPFOLDER, true);
        this.appVersion = property(APPVERSIONKEY, APPVERSION);

        Properties props = System.getProperties();
        props.setProperty("APP_HOME", appHome().toString());
        props.setProperty("APP_NAME", appName());
    }

    private void readSystemProperties() {
        info("Looking for the system property file (" + SYSPROPERTYFILE + ") in the class path.");
        try (InputStream input = Environ.class.getClassLoader().getResourceAsStream(SYSPROPERTYFILE)) {
            if(input!=null) {
                info("Use the the system property file (" + SYSPROPERTYFILE + ") in the class path.");
                props.load(input);
            } else {
                info("The system property file (" + SYSPROPERTYFILE + ") is not found.");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        this.appName = props.getProperty(APPNAMEKEY, APPNAME);
        this.appVersion = props.getProperty(APPVERSIONKEY, APPVERSION);

        sysProps.addAll(Arrays.asList(SYSTEMPROPERTIES));

        for(Object key : props.keySet()) {
            sysProps.add((String) key);
        }
    }

    private void readDefaultProperties() {
        String propFileName = this.appName + "." + PROPFILEEXTENSION;
        Properties appProps = new Properties();

        info("Looking for the default property file (" + propFileName + ") in the class path.");
        try (InputStream input = Environ.class.getClassLoader().getResourceAsStream(propFileName)) {
            if(input!=null) {
                info("Use the default property file (" + propFileName + ") in the class path.");
                appProps.load(input);
            } else {
                info("The default property file (" + propFileName + ") is not found.");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Enumeration<?> e = appProps.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            if(sysProps.contains(key)) continue;
            String value = appProps.getProperty(key);
            props.setProperty(key, value);
        }
    }

    private void readCustomProperties() {
        String propFileName = this.appName + "." + PROPFILEEXTENSION;
        Properties appProps = new Properties();

        Path conf = appBase.resolve("conf");
        info("Looking for the custom property file (" + propFileName + ") in the " + conf + ".");
        Path path = conf.resolve(propFileName);
        if(Files.exists(path)) {
            info("Use the custom property file (" + propFileName + ") in the " + conf + ".");
            try (InputStream input = new FileInputStream(path.toFile())) {
                appProps.load(input);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            info("The custom property file (" + propFileName + ") is not found");
        }

        Enumeration<?> e = appProps.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            if(sysProps.contains(key)) continue;
            String value = appProps.getProperty(key);
            props.setProperty(key, value);
        }
    }

    private void setupAppHome() {
        String app_home = appName.toUpperCase() + "_HOME";
        String app_base = appName.toUpperCase() + "_BASE";

        String property = System.getProperty(app_home, System.getenv(app_home));

        if(property==null) property = System.getProperty("APP_HOME", System.getenv("APP_HOME"));

        appHome = toPath(property, APPHOME);
        appBase = toPath(System.getProperty(app_base, System.getenv(app_base)), appHome);
    }

    private Path toPath(String s, Path defaultValue) {
        if(s==null) return defaultValue;

        if(s.startsWith("~")) {
            s = System.getProperty("user.home") + s.substring(1);
        }

        Path path = Paths.get(s);
        return Files.exists(path) ? path : defaultValue;
    }

    private void createFolder(Path dir) {
        try {
            Files.createDirectories(dir);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void info(String text) {
        System.out.println(text);
    }


    public enum Stage {
        DEVELOPMENT("DEVELOPMENT"), TEST("TEST"), PRODUCTION("PRODUCTION");

        private final String name;

        Stage(String name) {
            this.name = name;
        }

        public String toString(){
            return name;
        }
    }
}
