package io.leandev.appstack.http;

import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.client5.http.socket.ConnectionSocketFactory;
import org.apache.hc.client5.http.socket.PlainConnectionSocketFactory;
import org.apache.hc.client5.http.ssl.NoopHostnameVerifier;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactory;
import org.apache.hc.core5.http.config.Registry;
import org.apache.hc.core5.http.config.RegistryBuilder;
import org.apache.hc.core5.ssl.SSLContexts;
import org.apache.hc.core5.ssl.TrustStrategy;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class HttpClientConnectionManagerBuilder {
    private int maxTotal = 100;
    private int defaultMaxPerRoute = 20;
    private boolean sslVerification = true;

    public static HttpClientConnectionManagerBuilder create() {
        return new HttpClientConnectionManagerBuilder();
    }

    public HttpClientConnectionManagerBuilder setMaxTotal(int max) {
        this.maxTotal = max;
        return this;
    }

    public HttpClientConnectionManagerBuilder setDefaultMaxPerRoute(int max) {
        this.defaultMaxPerRoute = max;
        return this;
    }

    public HttpClientConnectionManagerBuilder disableSSLVerification() {
        this.sslVerification = false;
        return this;
    }

    public PoolingHttpClientConnectionManager build() {
        // 建立一個 connection manager
        PoolingHttpClientConnectionManager connectionManager;
        if(sslVerification) {
            connectionManager = new PoolingHttpClientConnectionManager();
        } else {
            Registry<ConnectionSocketFactory> socketFactoryRegistry = createSocketFactoryRegistry();
            connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        }
        // 初始化 connection manager
        connectionManager.setMaxTotal(maxTotal);
        connectionManager.setDefaultMaxPerRoute(defaultMaxPerRoute);
        return connectionManager;
    }


    private Registry<ConnectionSocketFactory> createSocketFactoryRegistry() {
        TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
        SSLContext sslContext;
        try {
            sslContext = SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            throw new HttpClientException(e);
        }
        SSLConnectionSocketFactory sslsf =
                new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

        return RegistryBuilder.<ConnectionSocketFactory> create()
                .register("https", sslsf)
                .register("http", new PlainConnectionSocketFactory())
                .build();
    }
}
