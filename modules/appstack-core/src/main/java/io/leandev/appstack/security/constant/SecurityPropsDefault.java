package io.leandev.appstack.security.constant;

import java.util.UUID;

public class SecurityPropsDefault {
    public static final String SECURITYENABLED = "true";
    public static final long SECURITYTOKENTTL = 86400_000; // in milliseconds
    public static final String SECURITYTOKENKEY = UUID.randomUUID().toString();
    public static final String SECURITYANONYMOUSENABLED = "false";
    public static final String SECURITYANONYMOUSPRINCIPAL = "anonymous";
}
