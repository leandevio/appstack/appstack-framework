package io.leandev.appstack.parser;

public abstract class AbstractRecordWrapper<T> implements Record {
    protected T data;

    protected AbstractRecordWrapper(T data) {
        this.data = data;
    }

    protected T data() {
        return this.data;
    }
}
