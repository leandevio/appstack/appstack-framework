package io.leandev.appstack.doc;

import javax.ws.rs.core.MediaType;
import java.awt.*;

public interface Chunk<T> extends Element {
    T content();
    void setContent(T content);
    MediaType mediaType();
    void render();
}
