package io.leandev.appstack.html;

import io.leandev.appstack.doc.Chunk;
import io.leandev.appstack.doc.Paragraph;

import java.util.List;

public class HtmlParagraph extends HtmlBlock implements Paragraph {
    public static HtmlParagraph of(Paragraph paragraph) {
        HtmlParagraph htmlParagraph = new HtmlParagraph();
        paragraph.chunks().stream().forEach(htmlParagraph::appendChunk);
        return htmlParagraph;
    }
    public static HtmlParagraph of(List<Chunk> chunks) {
        HtmlParagraph htmlParagraph = new HtmlParagraph();
        chunks.stream().forEach(htmlParagraph::appendChunk);
        return htmlParagraph;
    }

    public HtmlParagraph() {
        super("p");
    }
}
