package io.leandev.appstack.crypto;

import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

/**
 * This class will encrypt and decrypt a string using 256-bit AES in Galois Counter Mode (GCM).
 *
 * The Advanced Encryption Standard (AES, Rijndael) is a block cipher encryption and decryption algorithm, the most used encryption algorithm in the worldwide.
 * The AES processes block of 128 bits using a secret key of 128, 192, or 256 bits.
 *
 * The AES-GCM inputs:
 *
 * AES Secret key (256 bits)
 * IV – 96 bits (12 bytes)
 * Length (in bits) of authentication tag – 128 bits (16 bytes)
 *
 * For the encrypted output, we prefix the 16 bytes IV to the encrypted text (ciphertext), because we need the same IV for decryption.
 */
public class AESGCMEncryptor implements Encryptor {
    private static final Logger LOGGER = LogManager.getLogger(AESGCMEncryptor.class);

    private static final String ALGORITHM = "AES/GCM/NoPadding";
    private static final int TAG_LENGTH_BIT = 128;
    private static final int IV_LENGTH_BYTE = 16;
    private static final int KEY_LENGTH_BIT = 256;
    private static final int SALT_LENGTH_BYTE = 16;

    private SecretKey secretKey;
    private String password;
    private byte[] salt = new byte[0];

    private Base64.Encoder base64Encoder = Base64.getEncoder();
    private Base64.Decoder base64Decoder = Base64.getDecoder();

    public AESGCMEncryptor(String password) throws CryptoException {
        try {
            this.salt = this.getRandomNonce(SALT_LENGTH_BYTE);
            this.secretKey = this.getSecretKeyFromPassword(password, this.salt);
            this.password = password;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new CryptoException(String.format("Failed to generate the secret key from the password: %s", password), e);
        }
    }

    public AESGCMEncryptor() throws CryptoException {
        try {
            this.secretKey = this.getSecretKey(KEY_LENGTH_BIT);
        } catch (NoSuchAlgorithmException e) {
            throw new CryptoException("Failed to generate the secret key.", e);
        }
    }

    private byte[] getRandomNonce(int numBytes) {
        byte[] nonce = new byte[numBytes];
        new SecureRandom().nextBytes(nonce);
        return nonce;
    }

    /**
     * the AES secret key can be derived from a given password using a password-based key derivation function like PBKDF2. We also need a salt value for turning a password into a secret key. The salt is also a random value.
     *
     * We can use the SecretKeyFactory class with the PBKDF2WithHmacSHA256 algorithm for generating a key from a given password.
     *
     * Let’s define a method for generating the AES key from a given password with 65,536 iterations and a key length of 256 bits:
     *
     * @param password
     * @param salt
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    private SecretKey getSecretKeyFromPassword(String password, byte[] salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        // iterationCount = 65536
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, KEY_LENGTH_BIT);
        SecretKey secret = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
        return secret;
    }

    private SecretKey getSecretKey(int size) throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(size, SecureRandom.getInstanceStrong());
        return keyGenerator.generateKey();
    }

    @Override
    public byte[] encrypt(byte[] data) throws CryptoException  {
        byte[] iv = this.getRandomNonce(IV_LENGTH_BYTE);

        if(salt.length>0) {
            LOGGER.debug(String.format("Salt: %s", this.hex(salt)));
        }
        LOGGER.debug(String.format("IV: %s", this.hex(iv)));

        Cipher cipher = getEncryptCipher(this.secretKey, iv);

        byte[] cipherDataWithIvSalt;
        try {
            byte[] cipherData = cipher.doFinal(data);
            cipherDataWithIvSalt = ByteBuffer.allocate(iv.length + salt.length + cipherData.length)
                    .put(iv)
                    .put(salt)
                    .put(cipherData)
                    .array();
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new CryptoException(String.format("Failed to encrypt the data: \n%s", hex(data)), e);
        }
        return cipherDataWithIvSalt;
    }

    @Override
    public byte[] decrypt(byte[] cipherDataWithIvSalt) throws CryptoException {

        ByteBuffer buffer = ByteBuffer.wrap(cipherDataWithIvSalt);

        byte[] iv = new byte[IV_LENGTH_BYTE];
        buffer.get(iv);

        SecretKey secretKey = this.secretKey;
        if(this.password!=null) {
            byte[] salt = new byte[SALT_LENGTH_BYTE];
            buffer.get(salt);
            LOGGER.debug(String.format("Salt: %s", this.hex(salt)));
            try {
                secretKey = this.getSecretKeyFromPassword(password, salt);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                throw new CryptoException(e);
            }
        }

        LOGGER.debug(String.format("IV: %s", this.hex(iv)));

        byte[] cipherData = new byte[buffer.remaining()];
        buffer.get(cipherData);


        Cipher cipher = this.getDecryptCipher(secretKey, iv);

        byte[] data;
        try {
            data = cipher.doFinal(cipherData);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new CryptoException(String.format("Failed to decrypt the cipher: \n%s", hex(cipherData)), e);
        }
        return data;
    }

    @Override
    public String encrypt(String text) throws CryptoException  {
        byte[] cipherData = this.encrypt(text.getBytes());
        return base64Encoder.encodeToString(cipherData);
    }

    @Override
    public String decrypt(String cipherTextWithIv) throws CryptoException {
        byte[] data = this.decrypt(base64Decoder.decode(cipherTextWithIv));
        return new String(data);
    }

    @Override
    public void encrypt(InputStream inputStream, OutputStream outputStream) throws CryptoException, IOException {
        byte[] iv = this.getRandomNonce(IV_LENGTH_BYTE);

        if(salt.length>0) {
            LOGGER.debug(String.format("Salt: %s", this.hex(salt)));
        }
        LOGGER.debug(String.format("IV: %s", this.hex(iv)));

        Cipher cipher = getEncryptCipher(this.secretKey, iv);

        outputStream.write(iv);
        outputStream.write(salt);

        byte[] buffer = new byte[64];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            byte[] output = cipher.update(buffer, 0, bytesRead);
            if (output != null) {
                outputStream.write(output);
            }
        }
        try {
            byte[] outputBytes = cipher.doFinal();
            if (outputBytes != null) {
                outputStream.write(outputBytes);
            }
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new CryptoException("Failed to encrypt the data.", e);
        }
    }

    @Override
    public void decrypt(InputStream inputStream, OutputStream outputStream) throws CryptoException, IOException {
        byte[] iv = new byte[IV_LENGTH_BYTE];
        inputStream.read(iv);

        SecretKey secretKey = this.secretKey;
        if(this.password!=null) {
            byte[] salt = new byte[SALT_LENGTH_BYTE];
            inputStream.read(salt);
            LOGGER.debug(String.format("Salt: %s", this.hex(salt)));
            try {
                secretKey = this.getSecretKeyFromPassword(password, salt);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                throw new CryptoException(e);
            }
        }

        LOGGER.debug(String.format("IV: %s", this.hex(iv)));

        Cipher cipher = this.getDecryptCipher(secretKey, iv);

        byte[] buffer = new byte[64];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            byte[] output = cipher.update(buffer, 0, bytesRead);
            if (output != null) {
                outputStream.write(output);
            }
        }
        try {
            byte[] outputBytes = cipher.doFinal();
            if (outputBytes != null) {
                outputStream.write(outputBytes);
            }
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new CryptoException("Failed to decrypt the cipher.", e);
        }
    }

    private Cipher getEncryptCipher(SecretKey secretKey, byte[] iv) throws CryptoException {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, this.secretKey, new GCMParameterSpec(TAG_LENGTH_BIT, iv));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            throw new CryptoException("Failed to get the Cipher instance in encryption mode.", e);
        }
        return cipher;
    }

    private Cipher getDecryptCipher(SecretKey secretKey, byte[] iv) throws CryptoException {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, new GCMParameterSpec(TAG_LENGTH_BIT, iv));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            throw new CryptoException("Failed to get the Cipher instance in decryption mode.", e);
        }
        return cipher;
    }

}
