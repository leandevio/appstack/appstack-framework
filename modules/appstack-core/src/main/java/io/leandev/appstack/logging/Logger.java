package io.leandev.appstack.logging;

public class Logger {
    private org.slf4j.Logger logger;

    protected Logger(org.slf4j.Logger logger) {
        this.logger = logger;
    }

    public void error(String s) {
        logger.error(s);
    }

    public void error(String s, Throwable t) {
        logger.error(s, t);
    }

    public void warn(String s) {
        logger.warn(s);
    }

    public void warn(String s, Throwable t) {
        logger.warn(s, t);
    }

    public void info(String s) {
        logger.info(s);
    }

    public void info(String s, Throwable t) {
        logger.info(s, t);
    }

    public void debug(String s) {
        logger.debug(s);
    }

    public void debug(String s, Throwable t) {
        logger.debug(s, t);
    }

    public void trace(String s) {
        logger.trace(s);
    }

    public void trace(String s, Throwable t) {
        logger.trace(s, t);
    }

    public boolean isErrorEnabled() {
        return logger.isErrorEnabled();
    }

    public boolean isWarnEnabled() {
        return logger.isWarnEnabled();
    }

    public boolean isInfoEnabled() {
        return logger.isInfoEnabled();
    }

    public boolean isDebugEnabled() {
        return logger.isDebugEnabled();
    }

    public boolean isTraceEnabled() {
        return logger.isTraceEnabled();
    }
}
