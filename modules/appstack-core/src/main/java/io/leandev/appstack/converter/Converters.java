package io.leandev.appstack.converter;

import org.apache.commons.beanutils.ConvertUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Converters {
    private static final Logger LOGGER = LoggerFactory.getLogger(Converters.class);
    private ConvertUtils convertUtils = new ConvertUtils();
    private DateConverter dateConverter = new DateConverter();
    private ZonedDateTimeConverter zonedDateTimeConverter = new ZonedDateTimeConverter();
    private LocalDateTimeConverter localDateTimeConverter = new LocalDateTimeConverter();
    private BytesConverter bytesConverter = new BytesConverter();
    private IntegerConverter integerConverter = new IntegerConverter();
    private LongConverter longConverter = new LongConverter();
    private BigIntegerConverter bigIntegerConverter = new BigIntegerConverter();
    private FloatConverter floatConverter = new FloatConverter();
    private DoubleConverter doubleConverter = new DoubleConverter();
    private BigDecimalConverter bigDecimalConverter = new BigDecimalConverter();

    private Map<Class<?>, Converter<?>> converters = new HashMap<>();

    private static class SingletonHolder {
        private static final Converters INSTANCE = new Converters();
    }

    public static Converters getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    public Converters() {
        this.register(dateConverter, Date.class);
        this.register(zonedDateTimeConverter, ZonedDateTime.class);
        this.register(bytesConverter, byte[].class);
        this.register(integerConverter, Integer.class);
        this.register(longConverter, Long.class);
        this.register(bigIntegerConverter, BigInteger.class);
        this.register(floatConverter, Float.class);
        this.register(doubleConverter, Double.class);
        this.register(bigDecimalConverter, BigDecimal.class);
    }

    public <T> T convert(Object value, Class<T> targetType) {
        if(value==null) return null;
        if(targetType.isAssignableFrom(value.getClass())) return (T) value;
        if(value instanceof String && ((String) value).length()==0) return null;
        Converter<T> converter = lookup(targetType);
        return (converter==null) ? (T) convertUtils.convert(value, targetType) : converter.convert(value);
    }

    public <T> void register(Converter<T> converter, Class<T> targetType) {
        converters.put(targetType, converter);
    }

    public <T> Converter<T> lookup(Class<T> targetType) {
        return (Converter<T>) converters.get(targetType);
    }
}
