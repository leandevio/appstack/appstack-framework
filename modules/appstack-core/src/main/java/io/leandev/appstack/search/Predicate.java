package io.leandev.appstack.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Predicate {
    private final Node node;

    public static Predicate empty() {
        return new Predicate();
    }

    public static Predicate of(String selector, ComparisonOperator operator, Object expectation) {
        if(!(ComparisonOperator.IS.equals(operator) || ComparisonOperator.IS_NOT.equals(operator))) {
            if(expectation==null || (expectation instanceof String && ((String) expectation).length()==0)
                    || (expectation.getClass().isArray() && Arrays.asList(expectation).size()==0)
                    || (expectation instanceof Collection && ((Collection) expectation).size()==0)) {
                return Predicate.empty();
            }
        }
        return new Predicate(selector, operator, expectation);
    }

    public static Predicate eq(String selector, Object expectation) {
        return Predicate.of(selector, ComparisonOperator.EQUAL, expectation);
    }

    public static Predicate neq(String selector, Object expectation) {
        return Predicate.of(selector, ComparisonOperator.NOT_EQUAL, expectation);
    }

    public static Predicate like(String selector, CharSequence expectation) {
        return Predicate.of(selector, ComparisonOperator.EQUAL, expectation==null ? null : String.format("*%s*", expectation));
    }

    public static Predicate startsWith(String selector, CharSequence expectation) {
        return Predicate.of(selector, ComparisonOperator.EQUAL, expectation==null ? null : String.format("%s*", expectation));
    }

    public static Predicate ge(String selector, Object expectation) {
        return Predicate.of(selector, ComparisonOperator.GREATER_THAN_OR_EQUAL, expectation);
    }

    public static Predicate gt(String selector, Object expectation) {
        return Predicate.of(selector, ComparisonOperator.GREATER_THAN, expectation);
    }

    public static Predicate lt(String selector, Object expectation) {
        return Predicate.of(selector, ComparisonOperator.LESS_THAN, expectation);
    }

    public static Predicate le(String selector, Object expectation) {
        return Predicate.of(selector, ComparisonOperator.LESS_THAN_OR_EQUAL, expectation);
    }

    public static Predicate in(String selector, Object expectation) {
        return Predicate.of(selector, ComparisonOperator.IN, expectation);
    }

    public static Predicate notIn(String selector, Object expectation) {
        return Predicate.of(selector, ComparisonOperator.NOT_IN, expectation);
    }

    public static Predicate has(String selector, Object expectation) {
        return Predicate.of(selector, ComparisonOperator.HAS, expectation);
    }

    public static Predicate between(String selector, Object[] expectation) {
        return Predicate.of(selector, ComparisonOperator.BETWEEN, expectation);
    }

    public static Predicate between(String selector, Object start, Object end) {
        Object[] expectation = new Object[2];
        expectation[0] = start;
        expectation[1] = end;
        return Predicate.between(selector, expectation);
    }

    public static Predicate is(String selector, Object expectation) {
        return Predicate.of(selector, ComparisonOperator.IS, expectation);
    }

    public static Predicate isNot(String selector, Object expectation) {
        return Predicate.of(selector, ComparisonOperator.IS_NOT, expectation);
    }

    public static Predicate isNull(String selector) {
        return Predicate.of(selector, ComparisonOperator.IS, null );
    }

    public static Predicate isNotNull(String selector) {
        return Predicate.of(selector, ComparisonOperator.IS_NOT, null);
    }

    public boolean isEmpty() {
        return (this.node instanceof EmptyNode);
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }

    public Predicate(String selector, ComparisonOperator operator, Object expectation) {
        Node node;
        if(operator.equals(ComparisonOperator.BETWEEN)) {
            Object[] args = (Object[]) expectation;
            Predicate start = Predicate.empty(), end = Predicate.empty();
            if(args[0] != null) {
                start = Predicate.gt(selector, args[0]);
            }
            if(args.length>1 && args[1] != null) {
                end = Predicate.lt(selector, args[1]);
            }
            node = Predicate.empty().and(start, end).node();
        } else {
            node = new ComparisonNode(operator, selector, expectation);
        }
        this.node = node;
    }

    public Predicate() {
        this.node = new EmptyNode();
    }

    public Predicate(Node node) {
        this.node = node;
    }

    public Predicate and(Predicate... predicates) {
        return and(Arrays.asList(predicates));
    }

    public Predicate and(List<Predicate> predicates) {
        Node node;

        List<Node> nodes = predicates.stream()
                .filter(predicate -> predicate.isNotEmpty())
                .map(predicate -> predicate.node())
                .collect(Collectors.toList());

        if(this.node() instanceof EmptyNode) {
            node = (nodes.size() == 0) ? new EmptyNode() : new AndNode(nodes);
        } else if(this.node() instanceof AndNode) {
            node = new AndNode(((AndNode) this.node()).children(), nodes);
        } else {
            node = new AndNode(this.node(), nodes);
        }

        return new Predicate(node);
    }

    public Predicate and(String selector, ComparisonOperator operator, Object expectation) {
        return and(new Predicate(selector, operator, expectation));
    }

    public Predicate or(Predicate... predicates) {
        return or(Arrays.asList(predicates));
    }

    public Predicate or(List<Predicate> predicates) {
        Node node;

        List<Node> nodes = predicates.stream()
                .filter(predicate -> predicate.isNotEmpty())
                .map(predicate -> predicate.node())
                .collect(Collectors.toList());

        if(this.node() instanceof EmptyNode) {
            node = (nodes.size() == 0) ? new EmptyNode() : new OrNode(nodes);
        } else if(this.node() instanceof OrNode) {
            node = new OrNode(((OrNode) this.node()).children(), nodes);
        } else {
            node = new OrNode(this.node(), nodes);
        }

        return new Predicate(node);
    }

    public Predicate or(String selector, ComparisonOperator operator, Object expectation) {
        return or(new Predicate(selector, operator, expectation));
    }

    public Node node() {
        return this.node;
    }

    public Predicate map(Function<ComparisonNode, Node> fn) {
        Node node = this.map(this.node, fn);
        return new Predicate(node);
    }

    private Node map(Node node, Function<ComparisonNode, Node> fn) {
        if(node instanceof LogicalNode) {
            return this.map((LogicalNode) node, fn);
        } else if(node instanceof ComparisonNode) {
            return this.map((ComparisonNode) node, fn);
        } else {
            return node;
        }
    }

    private Node map(ComparisonNode comparisonNode, Function<ComparisonNode, Node> fn) {
        return fn.apply(comparisonNode);
    }

    private Node map(LogicalNode logicalNode, Function<ComparisonNode, Node> fn) {
        List<Node> nodes = logicalNode.children().stream()
                .map(node -> this.map(node, fn))
                .collect(Collectors.toList());
        if(logicalNode instanceof AndNode) {
            return new AndNode(nodes);
        } else if(logicalNode instanceof OrNode) {
            return new OrNode(nodes);
        } else {
            return logicalNode;
        }
    }

    public String selector() {
        if(this.node instanceof ComparisonNode) {
            return ((ComparisonNode) node).selector();
        } else if(node instanceof ComparisonNode) {
            return null;
        } else {
            return null;
        }
    }

    public Object expectation() {
        if(this.node instanceof ComparisonNode) {
            return ((ComparisonNode) node).expectation();
        } else if(node instanceof ComparisonNode) {
            return null;
        } else {
            return null;
        }
    }

    public Object operator() {
        if(this.node instanceof LogicalNode) {
            return ((LogicalNode) node).operator();
        } else if(node instanceof ComparisonNode) {
            return ((ComparisonNode) node).operator();
        } else {
            return null;
        }
    }

    public List<Predicate> children() {
        if(this.node instanceof LogicalNode) {
            return ((LogicalNode) node).children().stream()
                    .map(node -> new Predicate(node))
                    .collect(Collectors.toList());
        } else if(node instanceof ComparisonNode) {
            return new ArrayList<>();
        } else {
            return new ArrayList<>();
        }
    }
}
