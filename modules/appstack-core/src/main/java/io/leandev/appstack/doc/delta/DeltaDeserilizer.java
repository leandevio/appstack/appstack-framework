package io.leandev.appstack.doc.delta;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import io.leandev.appstack.util.Model;

import java.io.IOException;
import java.util.Iterator;

public class DeltaDeserilizer extends StdDeserializer<Delta> {
    public DeltaDeserilizer() {
        this(null);
    }

    public DeltaDeserilizer(Class<?> valueType) {
        super(valueType);
    }

    @Override
    public Delta deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        JsonNode ops = node.get("ops");

        Delta delta = new Delta();
        for(JsonNode op : ops) {
            JsonNode any = op.get("insert");
            Content content;
            if(any.isTextual()) {
                content = new Content(any.asText());
            } else if(any.isObject()) {
                String type = any.fieldNames().next();
                String source = any.get(type).asText();
                content = new Content(type, source);
            } else {
                continue;
            }

            Model attributes = new Model();
            if(op.has("attributes")) {
                JsonNode attrs = op.get("attributes");
                for(Iterator<String> iter = attrs.fieldNames(); iter.hasNext();) {
                    String name = iter.next();
                    JsonNode valueNode = attrs.get(name);
                    Object value;
                    if(valueNode.isTextual()) {
                        value = valueNode.asText();
                    } else if(valueNode.isNumber()) {
                        value = valueNode.isDouble() ? valueNode.asDouble() : valueNode.asLong();
                    } else if(valueNode.isBoolean()) {
                        value = valueNode.asBoolean();
                    } else {
                        value = valueNode.asText();
                    }
                    attributes.put(name, value);
                }
            }

            delta.insert(content, attributes);
        }
        return delta;
    }
}