package io.leandev.appstack.text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class StringTextReader implements ITextReader {
    private BufferedReader reader;
    private int length;
    private String buffer;
    private String line;

    public StringTextReader(String text) {
        this.length = text.length();
        Reader inputString = new StringReader(text);
        this.reader = new BufferedReader(inputString);
    }

    @Override
    public String readLine() throws IOException {
        if(this.buffer==null) {
            this.line = this.reader.readLine();
        } else {
            this.line = this.buffer;
            this.buffer = null;
        }

        return this.line;
    }

    @Override
    public String peekLine() throws IOException {
        this.mark();
        String line = this.readLine();
        this.reset();
        return line;
    }

    @Override
    public void mark() throws IOException {
        this.reader.mark(length);
    }

    @Override
    public void reset() throws IOException {
        this.reader.reset();
    }

    @Override
    public void close() throws IOException {
        this.reader.close();
    }
}
