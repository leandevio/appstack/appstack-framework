package io.leandev.appstack.cache;

import org.ehcache.CacheManager;

import java.io.File;
import java.nio.file.Path;

public class CacheManagerBuilder {
    private org.ehcache.config.builders.CacheManagerBuilder builder;

    public CacheManagerBuilder() {
        this.builder = org.ehcache.config.builders.CacheManagerBuilder.newCacheManagerBuilder();
    }

    public static CacheManagerBuilder newCacheManager() {
        return new CacheManagerBuilder();
    }

    public CacheManagerBuilder usingPersistence(Path folder) {
        return this.usingPersistence(folder.toFile());
    }

    public CacheManagerBuilder usingPersistence(File folder) {
        builder.with(org.ehcache.config.builders.CacheManagerBuilder.persistence(folder));
        return this;
    }

    public CacheManager build() {
        CacheManager cacheManager = builder.build(true);

        return cacheManager;
    }


}
