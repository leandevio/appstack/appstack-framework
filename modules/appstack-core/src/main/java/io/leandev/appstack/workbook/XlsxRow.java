package io.leandev.appstack.workbook;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;

import java.util.ArrayList;
import java.util.List;

public class XlsxRow implements Row {
    private XSSFRow data;
    private XlsxWorksheet worksheet;

    protected XlsxRow(XSSFRow row, XlsxWorksheet worksheet) {
        this.data = row;
        this.worksheet = worksheet;
    }

    @Override
    public XlsxCell getCell(int i) {
        XSSFCell cell = data.getCell(i);
        return toCell(cell);
    }

    @Override
    public Object getValue(int index) {
        XlsxCell cell = getCell(index);
        return cell == null ? null : cell.getValue();
    }

    @Override
    public XlsxCell createCell() {
        int lastCellNum = this.lastCellNum();
        int index = (lastCellNum==-1) ? 0 : lastCellNum + 1;
        return toCell(data.createCell(index));
    }

    @Override
    public Object[] getValues() {
        List<Object> values = new ArrayList<>();
        if(this.size()>0) {
            for(int i=firstCellNum(); i<=this.lastCellNum(); i++) {
                values.add(this.getValue(i));
            }
        }
        return values.toArray();
    }

    @Override
    public int lastCellNum() {
        int num = data.getLastCellNum();
        return num==-1 ? num : data.getLastCellNum() - 1;
    }

    @Override
    public int firstCellNum() {
        return data.getFirstCellNum();
    }

    @Override
    public int size() {
        return lastCellNum()==-1 ? 0 : lastCellNum() - firstCellNum() + 1;
    }

    @Override
    public XlsxWorksheet worksheet() {
        return this.worksheet;
    }

    private XlsxCell toCell(XSSFCell cell) {
        if(cell==null) return null;
        return new XlsxCell(cell, this);
    }
}
