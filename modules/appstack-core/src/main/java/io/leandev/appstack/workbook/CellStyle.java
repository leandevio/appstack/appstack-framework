package io.leandev.appstack.workbook;

import io.leandev.appstack.doc.Align;
import io.leandev.appstack.doc.BorderStyle;
import io.leandev.appstack.doc.Color;
import io.leandev.appstack.doc.VerticalAlignment;

public interface CellStyle {
    void setColor(Color color);

    void setBackground(Color color);

    void setDataFormat(String dataFormat);

    String dataFormat();

    void setAlign(Align align);

    void setVerticalAlignment(VerticalAlignment verticalAlignment);

    default void setBorderColor(Color color) {
        this.setBorderTopColor(color);
        this.setBorderRightColor(color);
        this.setBorderBottomColor(color);
        this.setBorderLeftColor(color);
    }

    void setBorderTopColor(Color color);

    void setBorderBottomColor(Color color);

    void setBorderLeftColor(Color color);

    void setBorderRightColor(Color color);

    default void setBorderStyle(BorderStyle borderStyle) {
        this.setBorderTopStyle(borderStyle);
        this.setBorderRightStyle(borderStyle);
        this.setBorderBottomStyle(borderStyle);
        this.setBorderLeftStyle(borderStyle);
    }

    void setBorderTopStyle(BorderStyle borderStyle);

    void setBorderBottomStyle(BorderStyle borderStyle);

    void setBorderLeftStyle(BorderStyle borderStyle);

    void setBorderRightStyle(BorderStyle borderStyle);

    void setFontFamily(String name);

    void setWrapText(boolean wrapText);
}
