package io.leandev.appstack.image;

import io.leandev.appstack.measure.Length;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageProcessor {
    private static final int PPI = 300;
    private int ppi = PPI;

    public ImageProcessor() {}

    public ImageProcessor(int ppi) {
        this.ppi = ppi;
    }

    public BufferedImage scale(BufferedImage image, int width, int height) {
        Image tmp = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage scaledImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics = scaledImage.createGraphics();
        graphics.drawImage(tmp, 0, 0, null);
        graphics.dispose();
        return scaledImage;
    }

    public BufferedImage scale(BufferedImage image, Length width, Length height) {
        return this.scale(image, width.toPixel(this.ppi), height.toPixel(this.ppi));
    }
    public BufferedImage scaleByWidth(BufferedImage image, int width) {
        double ratio = (double) width / image.getWidth();
        int height = (int) (image.getHeight() * ratio);
        return this.scale(image, width, height);
    }

    public BufferedImage scaleByWidth(BufferedImage image, Length width) {
        return this.scaleByWidth(image, width.toPixel(this.ppi));
    }

    public BufferedImage scaleByHeight(BufferedImage image, int height) {
        double ratio = (double) height / image.getWidth() ;
        int width = (int) (image.getWidth() * ratio);
        return this.scale(image, width, height);
    }

    public BufferedImage scaleByHeight(BufferedImage image, Length height) {
        return this.scaleByHeight(image, height.toPixel(this.ppi));
    }
}
