package io.leandev.appstack.ftp;

import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

import java.io.*;
import java.nio.file.Path;

public class FtpClient {
    private static Logger LOGGER = LogManager.getLogger(FtpClient.class);
    private FTPClient client;

    public FtpClient() {
        this(false);
    }

    public FtpClient(boolean ftps) {
        if (ftps) {
            client = new FTPSClient(true);
        } else {
            client = new FTPClient();
        }
    }

    public boolean changeDirectory(Path remotePath) throws Exception {
        return client.changeWorkingDirectory(remotePath.toString());
    }

    public boolean connect(String host, int port) throws IOException {
        LOGGER.debug("FTP request connect to " + host + ":" + port);
        client.connect(host, port);
        int reply = client.getReplyCode();
        return FTPReply.isPositiveCompletion(reply);
    }

    public void disconnect() throws IOException {
        LOGGER.debug("FTP request disconnect");
        client.disconnect();
    }

    public boolean isConnected() {
        return client.isConnected();
    }

    public boolean login(String username, String password) throws IOException {
        LOGGER.debug("FTP request login as " + username);
        if (client.login(username, password)) {
            client.enterLocalPassiveMode();
            return true;
        }
        return false;
    }

    public boolean logout() throws IOException {
        return client.logout();
    }

    public InputStream download(Path remote) throws IOException {
        return client.retrieveFileStream(remote.toString());
    }

    public boolean download(Path file, OutputStream outputStream) throws IOException {
        return client.retrieveFile(file.toString(), outputStream);
    }

    public boolean download(Path file, Path localFile) throws IOException {
        boolean result;
        try (FileOutputStream fileOutputStream = new FileOutputStream(localFile.toFile())) {
            result = download(file, fileOutputStream);
        }
        return result;
    }

    public boolean upload(Path file, byte[] bytes) throws IOException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        return client.storeFile(file.toString(), inputStream);
    }

    public boolean upload(Path file, InputStream inputStream) throws IOException {
        return client.storeFile(file.toString(), inputStream);
    }

    public boolean upload(Path file, Path localFile) throws IOException {
        boolean result;
        try (FileInputStream fileInputStream = new FileInputStream(localFile.toFile())) {
            result = upload(file, fileInputStream);
        }
        return result;
    }

    public FtpFile[] listFiles(Path path) throws IOException {
        FTPFile[] files = client.listFiles(path.toString());
        FtpFile[] ftpFiles = new FtpFile[files.length];
        for(int i=0; i<files.length; i++) {
            ftpFiles[i] = toFtpFile(files[i]);
        }
        return ftpFiles;
    }

    private FtpFile toFtpFile(FTPFile file) {
        FtpFile ftpFile = new FtpFile();

        ftpFile.setName(file.getName());
        ftpFile.setType(file.getType());
        ftpFile.setSize(file.getSize());
        ftpFile.setModifiedDate(file.getTimestamp().getTime());
        ftpFile.setUser(file.getUser());
        ftpFile.setGroup(file.getGroup());
        ftpFile.setLink(file.getLink());

        return ftpFile;
    }

    public FtpFile[] listDirectories(Path path) throws IOException {
        FTPFile[] files = client.listDirectories(path.toString());
        FtpFile[] ftpFiles = new FtpFile[files.length];
        for(int i=0; i<files.length; i++) {
            ftpFiles[i] = toFtpFile(files[i]);
        }
        return ftpFiles;
    }

    public boolean makeDirectory(Path path) throws IOException {
        return client.makeDirectory(path.toString());
    }

    public boolean deleteFile(Path path) throws IOException {
        return client.deleteFile(path.toString());
    }

    public boolean removeDirectory(Path path) throws IOException {
        return client.removeDirectory(path.toString());
    }

    public String currentDirectory() throws IOException {
        return client.printWorkingDirectory();
    }
}
