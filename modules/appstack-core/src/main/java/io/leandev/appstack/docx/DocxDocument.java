package io.leandev.appstack.docx;

import org.apache.poi.wp.usermodel.HeaderFooterType;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DocxDocument {
    private XWPFDocument xwpfDocument;
    private List<DocxBlock> blocks = new ArrayList<>();

//    public static DocxDocument of(Document document) {
//        DocxDocument docxDocument = new DocxDocument();
//
//        document.blocks().stream().forEach(docxDocument::appendBlock);
//
//        return docxDocument;
//    }

    public DocxDocument() {
        this.xwpfDocument = new XWPFDocument();
    }

    public DocxDocument(InputStream inputStream) throws IOException {
        this.xwpfDocument = new XWPFDocument(inputStream);
    }

    protected DocxDocument(XWPFDocument xwpfDocument) {
        this.xwpfDocument = xwpfDocument;
    }

    public XWPFDocument getXwpfDocument() {
        return this.xwpfDocument;
    }

    public List<DocxParagraph> getParagraphs() {
        return xwpfDocument.getParagraphs().stream().map(DocxParagraph::new).collect(Collectors.toList());
    }

    public DocxParagraph createParagraph() {
        XWPFParagraph xwpfParagraph = xwpfDocument.createParagraph();
        return new DocxParagraph(xwpfParagraph);
    }

    public DocxParagraph createParagraphAfter(DocxParagraph paragraph) {
        XWPFParagraph xwpfParagraph = paragraph.getXwpfParagraph();
        XmlCursor cursor = xwpfParagraph.getCTP().newCursor();
        cursor.toNextSibling();
        XWPFParagraph newXwpfParagraph = xwpfDocument.insertNewParagraph(cursor);
        return new DocxParagraph(newXwpfParagraph);
    }

    public DocxParagraph createParagraphAfter(DocxTable table) {
        XWPFTable xwpfTable = table.getXwpfTable();
        XmlCursor cursor = xwpfTable.getCTTbl().newCursor();
        cursor.toNextSibling();
        XWPFParagraph newXwpfParagraph = xwpfDocument.insertNewParagraph(cursor);
        return new DocxParagraph(newXwpfParagraph);
    }

    public DocxParagraph createParagraphBefore(DocxParagraph paragraph) {
        XWPFParagraph xwpfParagraph = paragraph.getXwpfParagraph();
        XmlCursor cursor = xwpfParagraph.getCTP().newCursor();
        cursor.toPrevSibling();
        XWPFParagraph newXwpfParagraph = xwpfDocument.insertNewParagraph(cursor);
        return new DocxParagraph(newXwpfParagraph);
    }

    public void removeParagraph(DocxParagraph paragraph) {
        int position = getPositionOfParagraph(paragraph);
        xwpfDocument.removeBodyElement(position);
    }

    public int getPositionOfParagraph(DocxParagraph paragraph) {
        return xwpfDocument.getPosOfParagraph(paragraph.getXwpfParagraph());
    }

    public Optional<DocxParagraph> getFirstParagraph(boolean autoCreate) {
        Optional<XWPFParagraph> xwpfParagraph = xwpfDocument.getParagraphs().stream().findFirst();
        DocxParagraph docxParagraph;
        if(xwpfParagraph.isPresent()) {
            docxParagraph = new DocxParagraph(xwpfParagraph.get());
        } else {
            docxParagraph = (autoCreate) ? new DocxParagraph(xwpfDocument.createParagraph()) : null;
        }
        return Optional.of(docxParagraph);
    }

    public Optional<DocxParagraph> getFirstParagraph() {
        return this.getFirstParagraph(false);
    }

    public DocxParagraph getParagraphAfter(DocxParagraph paragraph) {
        int position = getPositionOfParagraph(paragraph);
        XWPFParagraph xwpfParagraph = (XWPFParagraph) xwpfDocument.getBodyElements().get(position+1);
        return new DocxParagraph(xwpfParagraph);
    }

    public DocxParagraph getParagraphBefore(DocxParagraph paragraph) {
        int position = getPositionOfParagraph(paragraph);
        XWPFParagraph xwpfParagraph = (XWPFParagraph) xwpfDocument.getBodyElements().get(position-1);
        return new DocxParagraph(xwpfParagraph);
    }

    public Optional<DocxParagraph> getParagraphByBookmark(String bookmark) {
        DocxParagraph docxParagraph = null;
        List<IBodyElement> elements = xwpfDocument.getBodyElements();
        for(IBodyElement element : elements) {
            if(element instanceof XWPFTable) {
                for(XWPFTableRow row : ((XWPFTable) element).getRows()) {
                    for(XWPFTableCell cell : row.getTableCells()) {
                        for(XWPFParagraph xwpfParagraph : cell.getParagraphs()) {
                            if(isParagraphBookmarkBy(xwpfParagraph, bookmark)) {
                                docxParagraph = new DocxParagraph(xwpfParagraph);
                                break;
                            }
                        }
                        if(docxParagraph!=null) break;
                    }
                    if(docxParagraph!=null) break;
                }
            } else if(element instanceof XWPFParagraph) {
                if(isParagraphBookmarkBy((XWPFParagraph) element, bookmark)) {
                    docxParagraph = new DocxParagraph((XWPFParagraph) element);
                }
            }
            if(docxParagraph!=null) break;
        }
        return Optional.of(docxParagraph);
    }

    private boolean isParagraphBookmarkBy(XWPFParagraph xwpfParagraph, String bookmark) {
        boolean test = false;
        CTP ctp = xwpfParagraph.getCTP();
        // Get all bookmarks and loop through them
        List<CTBookmark> ctBookmarks = ctp.getBookmarkStartList();
        for (CTBookmark ctBookmark : ctBookmarks) {
            if (ctBookmark.getName().equals(bookmark)) {
                test = true;
                break;
            }
        }
        return test;
    }

    public List<DocxParagraph> getAllParagraphsByText(String text) {
        List<DocxParagraph> docxParagraphs = new ArrayList<>();
        List<IBodyElement> elements = xwpfDocument.getBodyElements();
        for(IBodyElement element : elements) {
            if(element instanceof XWPFTable) {
                for(XWPFTableRow row : ((XWPFTable) element).getRows()) {
                    for(XWPFTableCell cell : row.getTableCells()) {
                        for(XWPFParagraph xwpfParagraph : cell.getParagraphs()) {
                            if(isParagraphContainsText(xwpfParagraph, text)) {
                                docxParagraphs.add(new DocxParagraph(xwpfParagraph));
                            }
                        }
                    }
                }
            } else if(element instanceof XWPFParagraph) {
                if(isParagraphContainsText((XWPFParagraph) element, text)) {
                    docxParagraphs.add(new DocxParagraph((XWPFParagraph) element));
                }
            }
        }
        return docxParagraphs;
    }

    public Optional<DocxParagraph> getParagraphByText(String text) {
        DocxParagraph docxParagraph = null;
        List<IBodyElement> elements = xwpfDocument.getBodyElements();
        for(IBodyElement element : elements) {
            if(element instanceof XWPFTable) {
                for(XWPFTableRow row : ((XWPFTable) element).getRows()) {
                    for(XWPFTableCell cell : row.getTableCells()) {
                        for(XWPFParagraph xwpfParagraph : cell.getParagraphs()) {
                            if(isParagraphContainsText(xwpfParagraph, text)) {
                                docxParagraph = new DocxParagraph(xwpfParagraph);
                                break;
                            }
                        }
                        if(docxParagraph!=null) break;
                    }
                    if(docxParagraph!=null) break;
                }
            } else if(element instanceof XWPFParagraph) {
                if(isParagraphContainsText((XWPFParagraph) element, text)) {
                    docxParagraph = new DocxParagraph((XWPFParagraph) element);
                }
            }
            if(docxParagraph!=null) break;
        }
        return Optional.of(docxParagraph);
    }

    private boolean isParagraphContainsText(XWPFParagraph xwpfParagraph, String text) {
        return xwpfParagraph.getText().contains(text);
    }

    public DocxTable createTable() {
        XWPFTable xwpfTable = xwpfDocument.createTable();

        return new DocxTable(xwpfTable);
    }

    public DocxTable createTable(int rows, int cols) {
        XWPFTable xwpfTable = xwpfDocument.createTable(rows, cols);

        return new DocxTable(xwpfTable);
    }

    public DocxTable createTableAfter(int rows, int cols, DocxParagraph paragraph) {
        XWPFParagraph xwpfParagraph = paragraph.getXwpfParagraph();
        XmlCursor cursor = xwpfParagraph.getCTP().newCursor();
        cursor.toNextSibling();
        DocxTable docxTable = new DocxTable(xwpfDocument.insertNewTbl(cursor));
        DocxTableRow docxTableRow = docxTable.getRow(0);
        for(int col=0; col<cols-1; col++) {
            docxTableRow.createCell();
        }
        for(int row=0; row<rows-1; row++) {
            docxTable.createRow();
        }
        return docxTable;
    }

    public DocxTable createTableAfter(DocxParagraph paragraph) {
        XWPFParagraph xwpfParagraph = paragraph.getXwpfParagraph();
        XmlCursor cursor = xwpfParagraph.getCTP().newCursor();
        cursor.toNextSibling();
        DocxTable docxTable = new DocxTable(xwpfDocument.insertNewTbl(cursor));
        return docxTable;
    }

    public DocxTable createTableAfter(int rows, int cols, DocxTable table) {
        XmlCursor cursor = table.getXwpfTable().getCTTbl().newCursor();
        cursor.toNextSibling();
        DocxTable docxTable = new DocxTable(xwpfDocument.insertNewTbl(cursor));
        DocxTableRow docxTableRow = docxTable.getRow(0);
        for(int col=0; col<cols-1; col++) {
            docxTableRow.createCell();
        }
        for(int row=0; row<rows-1; row++) {
            docxTable.createRow();
        }
        return docxTable;
    }

    public DocxTable createTableAfter(DocxTable table) {
        XmlCursor cursor = table.getXwpfTable().getCTTbl().newCursor();
        cursor.toNextSibling();
        DocxTable docxTable = new DocxTable(xwpfDocument.insertNewTbl(cursor));
        return docxTable;
    }

    public Optional<DocxTable> getTableByBookmark(String bookmark) {
        DocxTable docxTable = null;
        List<XWPFTable> xwpfTables = xwpfDocument.getTables();
        for(XWPFTable xwpfTable : xwpfTables) {
            if(isTableBookmarkBy(xwpfTable, bookmark)) {
                docxTable = new DocxTable(xwpfTable);
                break;
            }
        }
        return Optional.of(docxTable);
    }

    private boolean isTableBookmarkBy(XWPFTable xwpfTable, String bookmark) {
        boolean test = false;
        for(XWPFTableRow row : xwpfTable.getRows()) {
            for(XWPFTableCell cell : row.getTableCells()) {
                for(XWPFParagraph xwpfParagraph : cell.getParagraphs()) {
                    if(isParagraphBookmarkBy(xwpfParagraph, bookmark)) {
                        test = true;
                        break;
                    }
                }
                if(test) break;
            }
            if(test) break;
        }
        return test;
    }

    public void close() throws IOException {
        this.xwpfDocument.close();
    }

    public DocxHeader getHeader() {
        XWPFHeader xwpfHeader = xwpfDocument.createHeader(HeaderFooterType.DEFAULT);
        DocxHeader docxHeader = new DocxHeader(xwpfHeader);
        return docxHeader;
    }

    public DocxHeader getFirstHeader() {
        XWPFHeader xwpfHeader = xwpfDocument.createHeader(HeaderFooterType.FIRST);
        DocxHeader docxHeader = new DocxHeader(xwpfHeader);
        return docxHeader;
    }

    public DocxHeader getEvenHeader() {
        XWPFHeader xwpfHeader = xwpfDocument.createHeader(HeaderFooterType.EVEN);
        DocxHeader docxHeader = new DocxHeader(xwpfHeader);
        return docxHeader;
    }

    public DocxFooter getFooter() {
        XWPFFooter xwpfFooter = xwpfDocument.createFooter(HeaderFooterType.DEFAULT);
        DocxFooter docxFooter = new DocxFooter(xwpfFooter);
        return docxFooter;
    }

    public DocxFooter getFirstFooter() {
        XWPFFooter xwpfFooter = xwpfDocument.createFooter(HeaderFooterType.FIRST);
        DocxFooter docxFooter = new DocxFooter(xwpfFooter);
        return docxFooter;
    }

    public DocxFooter getEvenFooter() {
        XWPFFooter xwpfFooter = xwpfDocument.createFooter(HeaderFooterType.EVEN);
        DocxFooter docxFooter = new DocxFooter(xwpfFooter);
        return docxFooter;
    }

//    @Override
//    public List<? extends Block> blocks() {
//        return null;
//    }
//
//    @Override
//    public Block appendBlock(Block block) {
//        return null;
//    }
//
//    @Override
//    public Paragraph createParagraph() {
//        return null;
//    }
//
//    @Override
//    public Heading createHeading(int level) {
//        return new DocxHeading(level);
//    }
//
//    @Override
//    public Listing createList(boolean ordered) {
//        return null;
//    }
//
//    @Override
//    public TextRun createTextRun(String text) {
//        return null;
//    }
//
//    @Override
//    public Image createImage(String source) {
//        return null;
//    }
//
//    @Override
//    public Link createLink(String href, String text) {
//        return null;
//    }
//
//    @Override
    public void write(OutputStream outputStream) throws IOException {
        this.xwpfDocument.write(outputStream);
    }

    public Optional<DocxStyle> getStyleByName(String name) {
        XWPFStyle xwfpStyle = this.xwpfDocument.getStyles().getStyleWithName(name);
        return Optional.of(new DocxStyle(xwfpStyle));
    }

    public Optional<DocxStyle> getStyle(String id) {
        XWPFStyle xwfpStyle = this.xwpfDocument.getStyles().getStyle(id);
        return Optional.of(new DocxStyle(xwfpStyle));
    }

    public DocxStyle getH1Style() {
        return this.getStyle(DocxStyle.H1).get();
    }

    public DocxStyle getH2Style() {
        return this.getStyle(DocxStyle.H2).get();
    }

    public DocxStyle getH3Style() {
        return this.getStyle(DocxStyle.H3).get();
    }

    public DocxStyle getH4Style() {
        return this.getStyle(DocxStyle.H4).get();
    }


}
