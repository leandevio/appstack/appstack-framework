package io.leandev.appstack.workbook;

public interface Worksheet {
    Row getRow(int i);

    Row createRow();

    Object data();

    int lastRowNum();

    int firstRowNum();

    int size();

    Workbook workbook();
}
