package io.leandev.appstack.geo;

/**
 * the distance measured along the surface of the earth.
 */
public class EuclideanDistance implements GeoDistance {
    private GeoLocation origin;
    private GeoLocation destination;
    private Double distance;
//    private String unit;

    public EuclideanDistance(GeoLocation origin, GeoLocation destination, Double distance) {
        this.origin = origin;
        this.destination = destination;
        this.distance = distance;
    }

    @Override
    public GeoLocation getOrigin() {
        return origin;
    }

    @Override
    public void setOrigin(GeoLocation origin) {
        this.origin = origin;
    }

    @Override
    public GeoLocation getDestination() {
        return destination;
    }

    @Override
    public void setDestination(GeoLocation destination) {
        this.destination = destination;
    }

    @Override
    public Double getDistance() {
        return distance;
    }

    @Override
    public void setDistance(Double distance) {
        this.distance = distance;
    }
}
