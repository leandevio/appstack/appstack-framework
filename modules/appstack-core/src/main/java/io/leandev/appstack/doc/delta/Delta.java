package io.leandev.appstack.doc.delta;

import io.leandev.appstack.util.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Delta {
    private List<Operation> operations = new ArrayList<>();

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    public void insert(Content content, Model attributes) {
        InsertOperation insertOperation = new InsertOperation(content, attributes);
        operations.add(insertOperation);
    }


    public void normalize() {
        List<InsertOperation> ops = merge(this.operations);
        ops = split(ops);
        ops = clean(ops);

        this.operations.clear();
        this.operations.addAll(ops);
    }

    private List<InsertOperation> merge(List<Operation> operations) {
        List<InsertOperation> ops = new ArrayList<>();
        for(Operation operation : operations) {
            if(operation instanceof InsertOperation) {
                ops.add((InsertOperation) operation);
            }
        }
        return ops;
    }

    private List<InsertOperation> split(List<InsertOperation> operations) {
        List<InsertOperation> ops = new ArrayList<>();
        for(InsertOperation operation : operations) {
            if(hasBlockBreak(operation)) {
                List<InsertOperation> splitOps = split(operation);
                ops.addAll(splitOps);
            } else {
                ops.add(operation);
            }
        }
        return ops;
    }

    private boolean hasBlockBreak(InsertOperation operation) {
        Content content = operation.content();
        String type = content.type();
        String text = content.source();
        return type.equals("text") && text.length()>1 && text.contains("\n");
    }

    private List<InsertOperation> split(InsertOperation operation) {
        List<InsertOperation> ops = new ArrayList<>();
        String source = operation.content().source();
        Model attributes = operation.attributes();
        String[] texts = source.split("\n", -1);
        for(String text : texts) {
            if(text.length()>0) {
                Content content = new Content("text" , text);
                Model attribs = new Model();
                attribs.putAll(attributes);
                InsertOperation op = new InsertOperation(content, attribs);
                ops.add(op);
            }
            Content content = new Content("text" , "\n");
            InsertOperation op = new InsertOperation(content);
            ops.add(op);
        }
        ops.remove(ops.size()-1);
        return ops;
    }

    private List<InsertOperation> clean(List<InsertOperation> operations) {
        List<InsertOperation> descriptors = operations.stream()
                .filter(operation -> isBlockDescriptor(operation))
                .collect(Collectors.toList());

        List<InsertOperation> redundant = new ArrayList<>();
        for(int i=0; i<descriptors.size()-2; i++) {
            InsertOperation descriptor = descriptors.get(i);
            InsertOperation nextDescriptor = descriptors.get(i+1);
            if(isListingDescriptor(descriptor) && isListingDescriptor(nextDescriptor)
            && descriptor.attributes().getAsString("list").equals(nextDescriptor.attributes().getAsString("list"))) {
                redundant.add(descriptor);
            }
        }

        List<InsertOperation> ops = new ArrayList<>(operations);
        ops.removeAll(redundant);
        return ops;
    }

    private boolean isBlockDescriptor(InsertOperation operation) {
        Content content = operation.content();
        return content.type().equals("text") && content.source().equals("\n");
    }

    private boolean isListingDescriptor(InsertOperation operation) {
        Content content = operation.content();
        Model attributes = operation.attributes();
        return content.type().equals("text") && content.source().equals("\n") && attributes.containsKey("list");
    }
}
