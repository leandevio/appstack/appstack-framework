package io.leandev.appstack.measure;

public class Length {

    private static final int DPI = 72;
    private static final double CM2MM = 10d;
    private static final double INCH2MM = 25.4d;
    private static final double PT2MM = 1d / DPI * INCH2MM;

    private Unit unit;
    private double value;

    public static Length ofCentimeter(double value) {
        return new Length(value, Unit.Centimeter);
    }

    public static Length ofMillimeter(double value) {
        return new Length(value, Unit.Millimeter);
    }

    public static Length ofPoint(double value) {
        return new Length(value, Unit.Point);
    }

    public Length(double value, Unit unit) {
        this.unit = unit;
        this.value = value;
    }

    public double getValue() {
        return this.value;
    }

    public Unit getUnit() {
        return this.unit;
    }

    public double toPoint() {
        double rate = this.unit.rate / Unit.Point.rate;
        return this.value * rate;
    }

    public double toMillimeter() {
        double rate = this.unit.rate / Unit.Millimeter.rate;
        return this.value * rate;
    }

    public double toInch() {
        double rate = this.unit.rate / Unit.Inch.rate;
        return this.value * rate;
    }

    public double toCentimeter() {
        double rate = this.unit.rate / Unit.Centimeter.rate;
        return this.value * rate;
    }

    public int toPixel(int ppi) {
        double inch = this.toInch();
        return (int) (inch * ppi);
    }

    public Length toLength(Unit unit) {
        double rate = this.unit.rate / unit.rate;
        double value = this.value * rate;

        return new Length(value, unit);
    }

    public Length add(Length augend) {
        double rate = augend.unit.rate / this.unit.rate;
        double value = augend.getValue() * rate;
        return new Length(this.value + value, this.getUnit());
    }

    public Length subtract(Length subtrahend) {
        double rate = subtrahend.unit.rate / this.unit.rate;
        double value = subtrahend.getValue() * rate;
        return new Length(this.value - value, this.getUnit());
    }

    public Length multiply(double multiplicand) {
        return new Length(this.value * multiplicand, this.getUnit());
    }

    public Length divide(double divisor) {
        return new Length(this.value / divisor, this.getUnit());
    }

    public enum Unit {
        Centimeter(CM2MM), Inch(INCH2MM), Point(PT2MM), Millimeter(1);

        public final double rate;

        Unit(double rate) {
            this.rate = rate;
        }
    }
}
