package io.leandev.appstack.workbook;

import io.leandev.appstack.converter.Converters;
import io.leandev.appstack.doc.Align;
import io.leandev.appstack.doc.BorderStyle;
import io.leandev.appstack.doc.Color;
import io.leandev.appstack.formatter.DataFormat;
import io.leandev.appstack.nls.I18n;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFCell;

import java.util.Date;
import java.util.Locale;

public class XlsxCell implements Cell {
    private static final String FONTFAMILY = "Calibri";
    private static final I18n i18n = I18n.getDefaultInstance();
    private Converters convert = new Converters();
    protected XSSFCell data;
    private XlsxRow row;
    private XlsxCellStyle cellStyle = null;

    protected XlsxCell(XSSFCell cell, XlsxRow row) {
        this.data = cell;
        this.row = row;
        Locale locale = this.workbook().locale();
        if(locale!=null) {
            this.setFontFamily(i18n.fontFamily(locale, FONTFAMILY));
        }
    }

    public XlsxWorkbook workbook() {
        return this.worksheet().workbook();
    }

    public XlsxWorksheet worksheet() {
        return this.row().worksheet();
    }

    @Override
    public void setValue(Object value) {
        if(value instanceof String) {
            data.setCellValue((String) value);
        } else if(value instanceof Number) {
            data.setCellValue(convert.convert(value, Double.class));
        } else if(value instanceof Date) {
            data.setCellValue((Date) value);
            if(this.dataFormat()==null) {
                Locale locale = this.workbook().locale();
                String dataFormat;
                if(locale==null) {
                    dataFormat = i18n.datetimePattern();
                } else {
                    dataFormat = i18n.datetimePattern(this.workbook().locale());
                }
                this.setDataFormat(dataFormat);
            }
        } else if (value instanceof Boolean) {
            data.setCellValue((Boolean) value);
        } else {
            data.setCellValue(convert.convert(value, String.class));
        }
    }

    public void setDataFormat(String dataFormat) {
        this.cellStyle().setDataFormat(dataFormat);
    }

    public void setDataFormat(DataFormat dataFormat) {
        String format;
        Locale locale = this.workbook().locale();
        if(dataFormat==DataFormat.DATETIME) {
            format = (locale==null) ? i18n.datetimePattern() : i18n.datetimePattern(locale);
        } else if(dataFormat==DataFormat.DATE) {
            format = (locale==null) ? i18n.datePattern() : i18n.datePattern(locale);
        } else if(dataFormat==DataFormat.MONEY) {
            format = (locale==null) ? i18n.moneyPattern() : i18n.moneyPattern(locale);
        } else if(dataFormat==DataFormat.INTEGER) {
            format = (locale==null) ? i18n.integerPattern() : i18n.integerPattern(locale);
        } else if(dataFormat==DataFormat.FLOAT) {
            format = (locale==null) ? i18n.floatPattern() : i18n.floatPattern(locale);
        } else {
            format = null;
        }

        this.setDataFormat(format);
    }


    public String dataFormat() {
        return this.cellStyle().dataFormat();
    }

    public XlsxCellStyle cellStyle() {
        if(this.cellStyle!=null) {
            return this.cellStyle;
        }
        XlsxWorkbook workbook = this.workbook();
        this.cellStyle = workbook.createCellStyle(this.data.getCellStyle());
        this.data.setCellStyle(this.cellStyle.data());
        return this.cellStyle;
    }

    public void setStyle(XlsxCellStyle cellStyle) {
        this.cellStyle = cellStyle;
        this.data.setCellStyle(this.cellStyle.data());
    }

    public void setColor(Color color) {
        this.cellStyle().setColor(color);
    }

    public void setBackground(Color color) {
        this.cellStyle().setBackground(color);
    }

    public void setBorderColor(Color color) {
        this.cellStyle().setBorderColor(color);
    }

    public void setAlign(Align align) {
        this.cellStyle().setAlign(align);
    }

    public void setBorderStyle(BorderStyle borderStyle) {
        this.cellStyle().setBorderStyle(borderStyle);
    }

    public void setBorderTopStyle(BorderStyle borderStyle) {
        this.cellStyle().setBorderTopStyle(borderStyle);
    }

    public void setBorderRightStyle(BorderStyle borderStyle) {
        this.cellStyle().setBorderRightStyle(borderStyle);
    }

    public void setBorderBottomStyle(BorderStyle borderStyle) {
        this.cellStyle().setBorderBottomStyle(borderStyle);
    }

    public void setBorderLeftStyle(BorderStyle borderStyle) {
        this.cellStyle().setBorderLeftStyle(borderStyle);
    }

    public void setBorderTopColor(Color color) {
        this.cellStyle().setBorderTopColor(color);
    }

    public void setBorderRightColor(Color color) {
        this.cellStyle().setBorderRightColor(color);
    }

    public void setBorderBottomColor(Color color) {
        this.cellStyle().setBorderBottomColor(color);
    }

    public void setBorderLeftColor(Color color) {
        this.cellStyle().setBorderLeftColor(color);
    }

    public void setFontFamily(String name) {
        this.cellStyle().setFontFamily(name);
    }

    @Override
    public Object getValue() {
        if(data==null) return null;
        DataFormatter formatter = new DataFormatter();
        Object value;
        switch (data.getCellType()) {
            case STRING:
                String text = data.getStringCellValue();
                if (text == null || text.equalsIgnoreCase("NULL") || text.length() == 0) {
                    value = null;
                } else {
                    value = text;
                }
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(data)) {
                    value = data.getDateCellValue();
                } else {
                    value = data.getNumericCellValue();
                }
                break;
            case BOOLEAN:
                value = data.getBooleanCellValue();
                break;
            case FORMULA:
                value = this.evaluate();
                break;
            case BLANK:
                value = null;
                break;
            default:
                value = formatter.formatCellValue(data);
        }
        return value;
    }

    private Object evaluate() {
        FormulaEvaluator evaluator = this.workbook().data().getCreationHelper().createFormulaEvaluator();
        CellValue cellValue = evaluator.evaluate(this.data);
        Object value;
        switch (cellValue.getCellType()) {
            case STRING:
                String text = data.getStringCellValue();
                if (text == null || text.equalsIgnoreCase("NULL") || text.length() == 0) {
                    value = null;
                } else {
                    value = text;
                }
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(data)) {
                    value = data.getDateCellValue();
                } else {
                    value = data.getNumericCellValue();
                }
                break;
            case BOOLEAN:
                value = data.getBooleanCellValue();
                break;
            case BLANK:
                value = null;
                break;
            default:
                value = null;
        }

        return value;
    }

    @Override
    public XlsxRow row() {
        return row;
    }
}
