package io.leandev.appstack.ftp;


public class FtpServerContext {
    FtpUserManager ftpUserManager;
    int port = 21;

    public FtpUserManager ftpUserManager() {
        return ftpUserManager;
    }

    public void setFtpUserManager(FtpUserManager ftpUserManager) {
        this.ftpUserManager = ftpUserManager;
    }

    public int port() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
