package io.leandev.appstack.doc.delta;

import io.leandev.appstack.json.JsonParser;

import java.io.IOException;
import java.io.InputStream;

public class DeltaParser {
    private final JsonParser jsonParser = new JsonParser();

    public DeltaParser() {
        jsonParser.register(Delta.class, new DeltaDeserilizer());
    }

    public Delta parse(InputStream inputStream) throws IOException {
        Delta delta = jsonParser.readValue(inputStream, Delta.class);
        delta.normalize();
        return delta;
    }

    public Delta parse(String text) throws IOException {
        Delta delta = jsonParser.readValue(text, Delta.class);
        delta.normalize();
        return delta;
    }
}
