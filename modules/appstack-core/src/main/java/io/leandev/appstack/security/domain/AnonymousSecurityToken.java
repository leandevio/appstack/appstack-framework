package io.leandev.appstack.security.domain;

public class AnonymousSecurityToken extends SecurityToken {
    public AnonymousSecurityToken(Object principal) {
        super(principal);
    }
}
