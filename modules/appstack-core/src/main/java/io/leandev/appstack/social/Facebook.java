package io.leandev.appstack.social;

import io.leandev.appstack.image.ImageReader;
import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import io.leandev.appstack.security.exception.AuthenticationException;
import io.leandev.appstack.json.JsonParser;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Facebook {
    private static final Logger LOGGER = LogManager.getLogger(Facebook.class);
    private String accessToken;
    private String id;
    private String name;
    private String email;
    private String gender;
    private Date birthday;
    private String familyName;
    private String givenName;

    private static JsonParser parser = new JsonParser();

    private static final String USER_AGENT = "Mozilla/5.0";

    public static Facebook connect(String accessToken) throws IOException {

        String url = String.format("https://graph.facebook.com/v3.2/me?access_token=%s&fields=id,name,email,gender,birthday,first_name,last_name", accessToken);

        URL obj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

        //add request header
        conn.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = conn.getResponseCode();
        LOGGER.info("Sending 'GET' request to URL : " + url);
        LOGGER.info("Response Code : " + responseCode);
        if(responseCode!=200) {
            Map<String, Object> response = parser.readValue(conn.getErrorStream(), HashMap.class);
            Map<String, Object> error = (Map<String, Object>) response.get("error");
            String error_message = (String) error.get("message");
            String error_type = (String) error.get("type");
            Integer error_code = (Integer) error.get("code");
            Integer error_subcode = (Integer) error.get("error_subcode");
            String code = (error_code==null) ? "" : error_code.toString();
            code = (error_subcode==null) ? code : code + ", " + error_subcode;
            String message = String.format("%s(%s): %s", error_type, code, error_message);
            LOGGER.debug(message);
            throw new AuthenticationException(message);
        }

        Map<String, String> props = parser.readValue(conn.getInputStream(), HashMap.class);

        Facebook facebook = new Facebook(accessToken);
        facebook.id = props.get("id");
        facebook.name = props.get("name");
        facebook.email = props.get("email");
        facebook.gender = props.get("gender");
        facebook.givenName = props.get("first_name");
        facebook.familyName = props.get("last_name");

        return facebook;
    }

    private Facebook(String accessToken) {
        this.accessToken = accessToken;
    }

    public String id() {
        return id;
    }

    public String name() {
        return name;
    }

    public String email() {
        return email;
    }

    public String gender() {
        return gender;
    }

    public Date birthday() {
        return birthday;
    }

    public String givenName() {
        return givenName;
    }

    public String familyName() {
        return familyName;
    }

    public BufferedImage picture(int width, int height) throws IOException {
        String url = String.format("https://graph.facebook.com/v3.2/me/picture?access_token=%s&type=normal&redirect=1&width=%s&height=%s", accessToken, width, height);
        URL obj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

        //add request header
        conn.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = conn.getResponseCode();
        LOGGER.info("Sending 'GET' request to URL : " + url);
        LOGGER.info("Response Code : " + responseCode);
        if(responseCode!=200) {
            Map<String, Object> response = parser.readValue(conn.getErrorStream(), HashMap.class);
            Map<String, Object> error = (Map<String, Object>) response.get("error");
            String error_message = (String) error.get("message");
            String error_type = (String) error.get("type");
            Integer error_code = (Integer) error.get("code");
            Integer error_subcode = (Integer) error.get("error_subcode");
            String code = (error_code==null) ? "" : error_code.toString();
            code = (error_subcode==null) ? code : code + ", " + error_subcode;
            String message = String.format("%s(%s): %s", error_type, code, error_message);
            LOGGER.debug(message);
            throw new FacebookException(message);
        }

        ImageReader reader = new ImageReader(conn.getInputStream());

        BufferedImage picture = reader.read();

        reader.close();

        return picture;
    }

    public BufferedImage picture() throws IOException {
        String url = String.format("https://graph.facebook.com/v3.2/me/picture?access_token=%s&type=normal&redirect=1", accessToken);
        URL obj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

        //add request header
        conn.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = conn.getResponseCode();
        LOGGER.info("Sending 'GET' request to URL : " + url);
        LOGGER.info("Response Code : " + responseCode);
        if(responseCode!=200) {
            Map<String, Object> response = parser.readValue(conn.getErrorStream(), HashMap.class);
            Map<String, Object> error = (Map<String, Object>) response.get("error");
            String error_message = (String) error.get("message");
            String error_type = (String) error.get("type");
            Integer error_code = (Integer) error.get("code");
            Integer error_subcode = (Integer) error.get("error_subcode");
            String code = (error_code==null) ? "" : error_code.toString();
            code = (error_subcode==null) ? code : code + ", " + error_subcode;
            String message = String.format("%s(%s): %s", error_type, code, error_message);
            LOGGER.debug(message);
            throw new FacebookException(message);
        }

        ImageReader reader = new ImageReader(conn.getInputStream());

        BufferedImage picture = reader.read();

        reader.close();

        return picture;
    }


}
