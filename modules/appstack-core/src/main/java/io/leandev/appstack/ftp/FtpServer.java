package io.leandev.appstack.ftp;

import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.*;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.WritePermission;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FtpServer {
    FtpServerContext ftpServerContext;
    FtpServerFactory serverFactory = new FtpServerFactory();
    ListenerFactory listenerFactory = new ListenerFactory();
    PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
    org.apache.ftpserver.ftplet.UserManager userManager = userManagerFactory.createUserManager();
    org.apache.ftpserver.FtpServer ftpServer;

    protected FtpServer(FtpServerContext ftpServerContext) throws FtpException {
        this.ftpServerContext = ftpServerContext;
        FtpUserManager ftpUserManager = ftpServerContext.ftpUserManager();
        listenerFactory.setPort(ftpServerContext.port());
        userManagerFactory.setPasswordEncryptor(new PasswordEncryptorImpl(ftpUserManager.passwordEncoder()));

        try {
            for(FtpUser ftpUser : ftpUserManager.users()) {
                BaseUser user = new BaseUser();
                user.setName(ftpUser.getUsername());
                user.setPassword(ftpUser.getPassword());
                Files.createDirectories(Paths.get(ftpUser.getHome()));
                user.setHomeDirectory(ftpUser.getHome());
                List<Authority> authorities = new ArrayList<>();
                authorities.add(new WritePermission());
                user.setAuthorities(authorities);
                userManager.save(user);
            }
        } catch (org.apache.ftpserver.ftplet.FtpException | IOException e) {
            throw new FtpException(e);
        }

        serverFactory.addListener("default", listenerFactory.createListener());
        serverFactory.setUserManager(userManager);
        Map<String, Ftplet> ftplets = new HashMap<>();
        ftplets.put("mainFtplet", new FtpletImpl());
        serverFactory.setFtplets(ftplets);
        ftpServer = serverFactory.createServer();
    }

    public void startup() throws FtpException {
        try {
            ftpServer.start();
        } catch (org.apache.ftpserver.ftplet.FtpException e) {
            throw new FtpException(e);
        }
    }

    public void shutdown() {
        ftpServer.stop();
    }
}
