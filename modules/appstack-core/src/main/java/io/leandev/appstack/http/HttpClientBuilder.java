package io.leandev.appstack.http;

import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.io.HttpClientConnectionManager;

import java.util.concurrent.TimeUnit;

public class HttpClientBuilder {
    private static final int TIMEOUT = 60;
    private HttpClientConnectionManagerBuilder httpClientConnectionManagerBuilder;
    private final RequestConfig.Builder requestConfigBuilder = RequestConfig.custom()
            .setConnectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .setConnectionRequestTimeout(TIMEOUT, TimeUnit.SECONDS)
            .setResponseTimeout(TIMEOUT, TimeUnit.SECONDS);

    public static HttpClientBuilder create() {
        return new HttpClientBuilder();
    }

    public HttpClientBuilder() {
        httpClientConnectionManagerBuilder = HttpClientConnectionManagerBuilder.create();
    }

    public HttpClientBuilder disableSSLVerification() {
        httpClientConnectionManagerBuilder = httpClientConnectionManagerBuilder.disableSSLVerification();
        return this;
    }

    public HttpClientBuilder setMaxTotal(int max) {
        httpClientConnectionManagerBuilder = httpClientConnectionManagerBuilder.setMaxTotal(max);
        return this;
    }

    public HttpClientBuilder setDefaultMaxPerRoute(int max) {
        httpClientConnectionManagerBuilder = httpClientConnectionManagerBuilder.setDefaultMaxPerRoute(max);
        return this;
    }

    public HttpClientBuilder setTimeout(int timeout, TimeUnit timeUnit) {
        requestConfigBuilder.setConnectTimeout(timeout, timeUnit)
                .setConnectionRequestTimeout(timeout, timeUnit)
                .setResponseTimeout(timeout, timeUnit);
        return this;
    }

    public CloseableHttpClient build() {
        RequestConfig config = requestConfigBuilder.build();

        HttpClientConnectionManager httpClientConnectionManager = httpClientConnectionManagerBuilder.build();
        return HttpClients.custom()
                .setDefaultRequestConfig(config)
                .setConnectionManager(httpClientConnectionManager)
                .build();
    }
}
