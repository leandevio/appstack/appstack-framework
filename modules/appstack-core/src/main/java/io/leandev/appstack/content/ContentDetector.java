package io.leandev.appstack.content;

import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

public class ContentDetector {
    TikaConfig tika;

    public ContentDetector(){
        try {
            tika = new TikaConfig();
        } catch (TikaException | IOException e) {
            // TODO: 2021/12/1 handle exception
            tika = null;
        }
    }

    public MediaType detect(Path path) throws IOException {
        MediaType mediaType;
        Metadata metadata = new Metadata();
        metadata.set(TikaCoreProperties.RESOURCE_NAME_KEY, path.toFile().toString());

        org.apache.tika.mime.MediaType tikaMediaType = tika.getDetector().detect(TikaInputStream.get(path), metadata);
        mediaType = new MediaType(tikaMediaType.getType(), tikaMediaType.getSubtype());

        return mediaType;
    }

    public MediaType detect(InputStream input) throws IOException {
        MediaType mediaType;
        Metadata metadata = new Metadata();
        org.apache.tika.mime.MediaType tikaMediaType = tika.getDetector().detect(TikaInputStream.get(input), metadata);
        mediaType = new MediaType(tikaMediaType.getType(), tikaMediaType.getSubtype());

        return mediaType;
    }

    public MediaType detect(byte[] bytes) throws IOException {
        MediaType mediaType;
        Metadata metadata = new Metadata();
        org.apache.tika.mime.MediaType tikaMediaType = tika.getDetector().detect(TikaInputStream.get(bytes), metadata);
        mediaType = new MediaType(tikaMediaType.getType(), tikaMediaType.getSubtype());

        return mediaType;
    }
}

