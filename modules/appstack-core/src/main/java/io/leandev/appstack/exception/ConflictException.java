package io.leandev.appstack.exception;

public class ConflictException extends ApplicationException {
    private static final String MESSAGE = "There was a conflict detected.";

    public ConflictException() {
        this(MESSAGE);
    }
    public ConflictException(String message) {
        super(message);
    }

    public ConflictException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConflictException(Throwable cause) {
        this(MESSAGE, cause);
    }

    public ConflictException(String message, Object... params) {
        super(message, params);
    }

    public ConflictException(String message, Throwable cause, Object... params) {
        super(message, cause, params);
    }

    public String toString() {
        return this.getMessage();
    }

    @Override
    public String defaultMessage() {
        return MESSAGE;
    }
}
