package io.leandev.appstack.exception;

public class DuplicateException extends ApplicationException {
    private static final String MESSAGE = "Duplicate values are not allowed.";

    public DuplicateException() {
        this(MESSAGE);
    }
    public DuplicateException(String message) {
        super(message);
    }

    public DuplicateException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateException(Throwable cause) {
        this(MESSAGE, cause);
    }

    public DuplicateException(String message, Object... params) {
        super(message, params);
    }

    public DuplicateException(String message, Throwable cause, Object... params) {
        super(message, cause, params);
    }

    public String toString() {
        return this.getMessage();
    }

    @Override
    public String defaultMessage() {
        return MESSAGE;
    }
}
