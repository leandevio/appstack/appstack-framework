package io.leandev.appstack.docx;

import io.leandev.appstack.measure.Length;
import io.leandev.appstack.measure.Paper;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

public class DocxParagraph {
    XWPFParagraph xwpfParagraph;

    protected DocxParagraph(XWPFParagraph xwpfParagraph) {
        this.xwpfParagraph = xwpfParagraph;
    }

    public XWPFParagraph getXwpfParagraph() {
        return this.xwpfParagraph;
    }

    public List<DocxTextRun> getTextRuns() {
        return xwpfParagraph.getRuns().stream().map(DocxTextRun::new).collect(Collectors.toList());
    }

    public DocxTextRun createTextRun() {
        XWPFRun xwpfRun = xwpfParagraph.createRun();
        return new DocxTextRun(xwpfRun);
    }

    public DocxTextRun createTextRun(int position) {
        XWPFRun xwpfRun = xwpfParagraph.insertNewRun(position);
        return new DocxTextRun(xwpfRun);
    }

    public boolean removeTextRun(int position) {
        return xwpfParagraph.removeRun(position);
    }

    public DocxImage createImage(InputStream image, double width, double height) throws IOException, InvalidFormatException {
        XWPFRun xwpfRun = xwpfParagraph.createRun();
        return new DocxImage(xwpfRun, image, width, height);
    }

    public DocxImage createImage(InputStream image, double width) throws IOException, InvalidFormatException {
        XWPFRun xwpfRun = xwpfParagraph.createRun();
        return new DocxImage(xwpfRun, image, width);
    }

    public DocxImage createImage(InputStream image, double width, double height, int position) throws IOException, InvalidFormatException {
        XWPFRun xwpfRun = xwpfParagraph.insertNewRun(position);
        return new DocxImage(xwpfRun, image, width, height);
    }

    public DocxImage createImage(InputStream image, Length width, Length height) throws IOException, InvalidFormatException {
        return this.createImage(image, width.toPoint(), height.toPoint());
    }

    public DocxImage createImage(InputStream image, Length width) throws IOException, InvalidFormatException {
        return this.createImage(image, width.toPoint());
    }

    public DocxImage createImage(InputStream image, Length width, Length height, int position) throws IOException, InvalidFormatException {
        return this.createImage(image, width.toPoint(), height.toPoint(), position);
    }

    public DocxImage createImage(InputStream image, Paper paper) throws IOException, InvalidFormatException {
        return this.createImage(image, paper.width, paper.height);
    }

    public DocxImage createImage(InputStream image, Paper paper, int position) throws IOException, InvalidFormatException {
        return this.createImage(image, paper.width, paper.height, position);
    }

    public DocxImage createImage(InputStream image) throws IOException, InvalidFormatException {
        XWPFRun xwpfRun = xwpfParagraph.createRun();
        return new DocxImage(xwpfRun, image);
    }

    public DocxImage createImage(BufferedImage image, double width, double height, int position) throws IOException, InvalidFormatException {
        XWPFRun xwpfRun = xwpfParagraph.insertNewRun(position);
        return new DocxImage(xwpfRun, image, width, height);
    }

    public DocxImage createImage(BufferedImage image, double width, double height) throws IOException, InvalidFormatException {
        XWPFRun xwpfRun = xwpfParagraph.createRun();
        return new DocxImage(xwpfRun, image, width, height);
    }

    public DocxImage createImage(BufferedImage image, double width) throws IOException, InvalidFormatException {
        XWPFRun xwpfRun = xwpfParagraph.createRun();
        return new DocxImage(xwpfRun, image, width);
    }

    public DocxImage createImage(BufferedImage image, Length width, Length height, int position) throws IOException, InvalidFormatException {
        return this.createImage(image, width.toPoint(), height.toPoint(), position);
    }

    public DocxImage createImage(BufferedImage image, Length width, Length height) throws IOException, InvalidFormatException {
        return this.createImage(image, width.toPoint(), height.toPoint());
    }

    public DocxImage createImage(BufferedImage image, Length width) throws IOException, InvalidFormatException {
        return this.createImage(image, width.toPoint());
    }

    public DocxImage createImage(BufferedImage image) throws IOException, InvalidFormatException {
        XWPFRun xwpfRun = xwpfParagraph.createRun();
        return new DocxImage(xwpfRun, image);
    }

    public void setText(String text) {
        this.clear();

        XWPFRun run;
        if(!xwpfParagraph.getRuns().isEmpty()) {
            run = xwpfParagraph.getRuns().get(0);
        } else {
            run = xwpfParagraph.createRun();
        }
        // The text of XWPFRun is a collection. setText(text) will add the text to the collection.
        // To replace the text, use the setText(text, position) with position = 0.
        run.setText(text, 0);
    }

    public void clear() {
        int size = xwpfParagraph.getRuns().size();
        for (int i = size-1; i > 0; i--) {
            xwpfParagraph.removeRun(i);
        }
        if(!xwpfParagraph.getRuns().isEmpty()) {
            xwpfParagraph.getRuns().get(0).setText("",0);
        }
    }

    public void setStyle(DocxStyle style) {
        xwpfParagraph.setStyle(style.getId());
    }

    public DocxParagraph withText(String text) {
        this.setText(text);
        return this;
    }

    public DocxParagraph withStyle(DocxStyle style) {
        this.setStyle(style);
        return this;
    }

    public String getStyleId() {
        return xwpfParagraph.getStyleID();
    }

    public boolean isH1() {
        String styleId = this.getStyleId();
        return (styleId!=null && styleId.equals(DocxStyle.H1));
    }

    public boolean isH2() {
        String styleId = this.getStyleId();
        return (styleId!=null && styleId.equals(DocxStyle.H2));
    }

    public boolean isH3() {
        String styleId = this.getStyleId();
        return (styleId!=null && styleId.equals(DocxStyle.H3));
    }

    public boolean isH4() {
        String styleId = this.getStyleId();
        return (styleId!=null && styleId.equals(DocxStyle.H4));
    }

    public void copyTo(DocxParagraph paragraph) {
        XWPFParagraph xwpfSource = this.getXwpfParagraph();
        XWPFParagraph xwpfTarget = paragraph.getXwpfParagraph();

        xwpfTarget.getCTP().setPPr(xwpfSource.getCTP().getPPr());
        for (int i=0; i<xwpfSource.getRuns().size(); i++ ) {
            XWPFRun run = xwpfSource.getRuns().get(i);
            XWPFRun targetRun = xwpfTarget.createRun();
            //copy formatting
            targetRun.getCTR().setRPr(run.getCTR().getRPr());
            //no images just copy text
            targetRun.setText(run.getText(0), 0);
        }
    }
}
