package io.leandev.appstack.logging;

public class LogManager {
    public static Logger getLogger(Class<?> clazz) {
        Logger logger = new Logger(org.slf4j.LoggerFactory.getLogger(clazz));
        return logger;
    }
}
