package io.leandev.appstack.text;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class TextFileWriter implements AutoCloseable {
    OutputStream out;
    BufferedWriter writer;

    public TextFileWriter(OutputStream out) {
        this(out, StandardCharsets.UTF_8);
    }

    public TextFileWriter(OutputStream out, Charset charset) {
        this.out = out;
        writer = new BufferedWriter(new OutputStreamWriter(out, charset));
    }

    public void write(String text) throws IOException {
        writer.write(text);
    }

    @Override
    public void close() throws Exception {
        writer.close();
    }
}
