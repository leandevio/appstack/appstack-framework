package io.leandev.appstack.text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.stream.Stream;

public class TextFileReader implements AutoCloseable {
    InputStream in;
    BufferedReader reader;

    public TextFileReader(InputStream in) {
        this(in, StandardCharsets.UTF_8);
    }

    public TextFileReader(InputStream in, Charset charset) {
        this.in = in;
        reader = new BufferedReader(new InputStreamReader(in, charset));
    }

    public Iterable<String> iterate() {
        return reader.lines()::iterator;
    }

    public Stream<String> stream() {
        return reader.lines();
    }

    public Iterator<String> lines() {
        return reader.lines().iterator();
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();
        stream().forEach(line -> sb.append(line).append('\n'));
        return sb.toString();
    }

    public String readLine() throws IOException {
        return reader.readLine();
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }
}
