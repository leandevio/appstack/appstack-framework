package io.leandev.appstack.html;

import io.leandev.appstack.doc.Background;
import io.leandev.appstack.doc.Color;
import io.leandev.appstack.doc.TextDecoration;
import io.leandev.appstack.doc.TextRun;

public class HtmlTextRun extends HtmlChunk<String> implements TextRun {
    private TextDecoration textDecoration = new TextDecoration();
    private String text;
    private Color color = null;
    private Background background = null;

    public static HtmlTextRun of(TextRun textRun) {
        HtmlTextRun htmlTextRun = new HtmlTextRun(textRun.content());
        return htmlTextRun;
    }

    public static HtmlTextRun of(String text) {
        HtmlTextRun htmlTextRun = new HtmlTextRun(text);
        return htmlTextRun;
    }

    public HtmlTextRun() {
        super("span");
    }

    public HtmlTextRun(String text) {
        this();
        this.setContent(text);
    }

    @Override
    public String content() {
        return this.text;
    }

    @Override
    public void setContent(String text) {
        this.text = text;
        this.render();
    }

    @Override
    public TextDecoration textDecoration() {
        return this.textDecoration;
    }

    @Override
    public void setTextDecoration(TextDecoration textDecoration) {
        this.textDecoration = textDecoration;
        this.render();
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
        String value = (color==null) ? null : color.hex();
        this.css("color", value);
    }

    @Override
    public void setBackground(Background background) {
        this.background = background;
        Color color = background==null ? null : background.color();
        String value = (color==null) ? null : color.hex();
        this.css("background-color", value);
    }

    @Override
    public void render() {
        HtmlElement htmlElement = this;
        this.html("");
        if(textDecoration!=null) {
            if(textDecoration.getStyle().equals(TextDecoration.Style.ITALIC)) {
                HtmlElement child = new HtmlElement("i");
                htmlElement.append(child);
                htmlElement = child;
            }

            if(textDecoration.getWeight().equals(TextDecoration.Weight.BOLD)) {
                HtmlElement child = new HtmlElement("b");
                htmlElement.append(child);
                htmlElement = child;
            }

            if(textDecoration.getLine().equals(TextDecoration.Line.UNDERLINE)) {
                HtmlElement child = new HtmlElement("u");
                htmlElement.append(child);
                htmlElement = child;
            } else if(textDecoration.getLine().equals(TextDecoration.Line.STRIKE)) {
                HtmlElement child = new HtmlElement("s");
                htmlElement.append(child);
                htmlElement = child;
            }

            if(textDecoration.getVariant().equals(TextDecoration.Variant.SUPER)) {
                HtmlElement child = new HtmlElement("sup");
                htmlElement.append(child);
                htmlElement = child;
            } else if(textDecoration.getVariant().equals(TextDecoration.Variant.SUB)) {
                HtmlElement child = new HtmlElement("sub");
                htmlElement.append(child);
                htmlElement = child;
            }

            if(!textDecoration.getSize().equals(TextDecoration.Size.MEDIUM)) {
                this.css("font-size", textDecoration.getSize().value());
            }
        }
        htmlElement.appendText(text);
    }
}
