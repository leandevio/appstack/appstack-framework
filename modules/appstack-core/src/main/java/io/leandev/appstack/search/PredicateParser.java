package io.leandev.appstack.search;

import cz.jirutka.rsql.parser.ast.AndNode;
import cz.jirutka.rsql.parser.ast.ComparisonNode;
import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import cz.jirutka.rsql.parser.ast.LogicalNode;
import cz.jirutka.rsql.parser.ast.Node;
import cz.jirutka.rsql.parser.ast.OrNode;
import cz.jirutka.rsql.parser.ast.RSQLOperators;
import io.leandev.appstack.converter.Converters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class PredicateParser {
    private Pattern regexpInteger = Pattern.compile("^(\\+|-)?([1-9](\\d+)?|0)$");
    private Pattern regexpFloat = Pattern.compile("^(\\+|-)?\\d*\\.\\d+$");
    // +0800; +08:00; +08;
    private Pattern regexpISODate = Pattern.compile("^\\d{4}-([0]\\d|1[0-2])-([0-2]\\d|3[01])T([0-1]\\d|2[0-3]):([0-5]\\d):([0-5]\\d)\\.\\d{3}(Z|(\\+|-)\\d{2}(:?\\d{2}))$");
    private Pattern regexpBoolean = Pattern.compile("^(true|false)$");
    private Converters converters = new Converters();

    private cz.jirutka.rsql.parser.RSQLParser parser;

    public PredicateParser() {
        Set<ComparisonOperator> operators = RSQLOperators.defaultOperators();
        operators.add(RSQLComparator.HAS.getOperator());
        operators.add(RSQLComparator.BETWEEN.getOperator());
        operators.add(RSQLComparator.IS.getOperator());
        operators.add(RSQLComparator.IS_NOT.getOperator());
        this.parser = new cz.jirutka.rsql.parser.RSQLParser(operators);
    }

    public Predicate parse(String text) {
        Node node = this.parser.parse(text);

        Predicate predicate = createPredicate(node);

        return predicate;
    }

    private Predicate createPredicate(cz.jirutka.rsql.parser.ast.LogicalNode logicalNodeNode) {
        List<Predicate> predicates = logicalNodeNode.getChildren().stream()
                .map(node -> createPredicate(node))
                .filter(predicate -> predicate.isNotEmpty())
                .collect(Collectors.toList());

        Predicate predicate = Predicate.empty();
        if(logicalNodeNode instanceof AndNode) {
            predicate = predicate.and(predicates);
        } else if(logicalNodeNode instanceof OrNode) {
            predicate = predicate.or(predicates);
        }

        return predicate;
    }

    private Predicate createPredicate(Node node) {
        Predicate predicate;
        if(node instanceof cz.jirutka.rsql.parser.ast.LogicalNode) {
            predicate = createPredicate((LogicalNode) node);
        } else if(node instanceof cz.jirutka.rsql.parser.ast.ComparisonNode) {
            predicate = createPredicate((cz.jirutka.rsql.parser.ast.ComparisonNode) node);
        } else {
            predicate = Predicate.empty();
        }
        return predicate;
    }

    private Predicate createPredicate(ComparisonNode comparisonNode) {
        String selector = comparisonNode.getSelector();
        List<Object> args = calibrate(comparisonNode.getArguments());
        ComparisonOperator comparisonOperator = comparisonNode.getOperator();

        Predicate predicate;
        RSQLComparator comparator = RSQLComparator.get(comparisonOperator);
        if(comparator.equals(RSQLComparator.IN) || comparator.equals(RSQLComparator.NOT_IN)) {
            predicate = Predicate.of(selector, io.leandev.appstack.search.ComparisonOperator.valueOf(comparator.name()), args);
        } else {
            predicate = Predicate.of(selector, io.leandev.appstack.search.ComparisonOperator.valueOf(comparator.name()), args.get(0));
        }

        return predicate;
    }

    private List<Object> calibrate(List<String> args) {
        List<Object> values = new ArrayList<>();
        for(String arg : args) {
            Object value = arg;
            if(arg.equals("null")) {
                value = null;
            } else if(regexpInteger.matcher(arg).find()) {
                value = converters.convert(arg, Long.class);
            } else if(regexpFloat.matcher(arg).find()) {
                value = converters.convert(arg, BigDecimal.class);
            } else if(regexpBoolean.matcher(arg).find()) {
                value = converters.convert(arg, Boolean.class);
            } else if(regexpISODate.matcher(arg).find()) {
                value = converters.convert(arg, Date.class);
            }
            values.add(value);
        }
        return values;
    }

}
