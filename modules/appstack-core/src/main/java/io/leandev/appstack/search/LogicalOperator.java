package io.leandev.appstack.search;

public enum LogicalOperator {
    AND(";"), OR(",");

    private String operator;

    LogicalOperator(final String operator) {
        this.operator = operator;
    }
}
