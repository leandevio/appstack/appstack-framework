package io.leandev.appstack.util;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * Orika is a Java Bean mapping framework that recursively copies data from one object to another. It can be very useful
 * when developing multi-layered applications.
 *
 * While moving data objects back and forth between these layers it is common to find that we need to convert objects
 * from one instance into another to accommodate different APIs.
 *
 * Some ways to achieve this are: hard coding the copying logic or to implement bean mappers like Dozer. However, it can
 * be used to simplify the process of mapping between one object layer and another.
 *
 * Orika uses byte code generation to create fast mappers with minimal overhead, making it much faster than other
 * reflection based mappers like Dozer.
 */
public class ObjectMapper {
    private MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
    private MapperFacade mapper;

    public ObjectMapper() {
        this.mapper = mapperFactory.getMapperFacade();
    }

    public <S, T> ObjectMapper(Class<S> sourceType, Class<T> destType) {
        mapperFactory.classMap(sourceType, destType);
        this.mapper = mapperFactory.getMapperFacade();
    }

    public <T> T map(Object source, Class<T> destType) {
        return mapper.map(source, destType);
    }

}
