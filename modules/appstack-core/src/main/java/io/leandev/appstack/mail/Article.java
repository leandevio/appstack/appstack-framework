package io.leandev.appstack.mail;

import javax.ws.rs.core.MediaType;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Article {
    private Object data;
    private MediaType mediaType = MediaType.TEXT_HTML_TYPE;
    private Charset charset = StandardCharsets.UTF_8;
    private List<Attachment> resources = new ArrayList<>();
    private String text = null;

    public Article(Object data, MediaType mediaType, Charset charset, String text, List<Attachment> resources) {
        this.data = data;
        this.charset = charset;
        this.mediaType = mediaType;
        this.text = text;
        this.resources = resources;
    }

    public Article(Object data, MediaType mediaType, Charset charset, String text) {
        this.data = data;
        this.charset = charset;
        this.mediaType = mediaType;
        this.text = text;
    }

    public Article(Object data, MediaType mediaType, Charset charset) {
        this.data = data;
        this.charset = charset;
        this.mediaType = mediaType;
    }

    public Article(Object data) {
        this.data = data;
    }

    public Object getData() {
        return this.data;
    }

    public MediaType getMediaType() {
        return this.mediaType;
    }

    public Charset getCharset() {
        return this.charset;
    }

    public String getText() {
        return this.text;
    }

    public List<Attachment> getResources() {
        return this.resources;
    }

    public boolean hasText() {
        return this.text != null;
    }

    public String toString() {
        return this.data ==null ? this.text : this.data.toString();
    }
}
