package io.leandev.appstack.measure;

public enum Paper {
    A4(Length.ofMillimeter(210), Length.ofMillimeter(297));

    public final Length width;
    public final Length height;

    Paper(Length width, Length height) {
        this.width = width;
        this.height = height;
    }
}
