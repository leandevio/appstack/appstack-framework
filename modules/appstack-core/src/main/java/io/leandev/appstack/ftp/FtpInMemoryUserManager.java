package io.leandev.appstack.ftp;

import io.leandev.appstack.crypto.PasswordEncoder;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class FtpInMemoryUserManager implements FtpUserManager {
    private Map<String, FtpUser> users = new HashMap<>();
    private PasswordEncoder passwordEncoder;

    protected FtpInMemoryUserManager(PasswordEncoder passwordEncoder, Collection<FtpUser> users) {
        this.passwordEncoder = passwordEncoder;
        save(users);
    }

    protected FtpInMemoryUserManager(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public PasswordEncoder passwordEncoder() {
        return this.passwordEncoder;
    }

    @Override
    public void save(FtpUser user) {
        users.put(user.getUsername(), user);
    }

    @Override
    public FtpUser get(String username) {
        return users.get(username);
    }

    @Override
    public Collection<FtpUser> users() {
        return Collections.unmodifiableCollection(users.values());
    }
}
