package io.leandev.appstack.nls;

import java.util.HashMap;
import java.util.Map;

public class ChineseTranslator {
    private final Map<Integer, String> DIGIT = new HashMap<>();

    public ChineseTranslator() {
        DIGIT.put(0, "〇");
        DIGIT.put(1, "一");
        DIGIT.put(2, "二");
        DIGIT.put(3, "三");
        DIGIT.put(4, "四");
        DIGIT.put(5, "五");
        DIGIT.put(6, "六");
        DIGIT.put(7, "七");
        DIGIT.put(8, "八");
        DIGIT.put(9, "九");
        DIGIT.put(10, "十");
        DIGIT.put(100, "百");
        DIGIT.put(1000, "千");
    }

    public String translate(int n) {
        return this.translate(n, true);
    }

    public String translate(int n, boolean verbose) {
        StringBuffer text = new StringBuffer();
        if(verbose) {
            for(int i=1; i<5; i++) {
                int r = n % 10;
                n = (n - r) / 10;
                if(r>0) {
                    if(r!=1||i!=2||n!=0) {
                        text.insert(0,DIGIT.get(r));
                    }
                } else {
                    if(n==0 || (n>0 && i>1 && !text.substring(0, 1).equals("零"))) {
                        text.insert(0, "零");
                    }
                }
                if(n==0) break;
                if(n % 10>0) {
                    text.insert(0, DIGIT.get((int)Math.pow(10, i)));
                }
            }
        } else {
            for(int i=1; i<5; i++) {
                int r = n % 10;
                n = (n - r) / 10;
                text.insert(0,DIGIT.get(r));
                if(n==0) break;
            }
        }

        return text.toString();

    }
}
