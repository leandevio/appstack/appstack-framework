package io.leandev.appstack.csv;
import com.univocity.parsers.csv.CsvParserSettings;
import io.leandev.appstack.parser.Record;
import io.leandev.appstack.parser.Result;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CsvParser {
    com.univocity.parsers.csv.CsvParser parser;
    CsvFormat csvFormat = new CsvFormat();

    public CsvParser() {
        CsvParserSettings settings = toCsvParserSettings(csvFormat);
        this.parser = new com.univocity.parsers.csv.CsvParser(settings);
    }

    public Iterable<CsvRecord> iterate(InputStream in) {
        CsvResult csvResult = parse(in);
        return () -> csvResult;
    }

    public Stream<CsvRecord> stream(InputStream in) {
        Iterable<CsvRecord> result = this.iterate(in);
        Stream<CsvRecord> stream  = StreamSupport.stream(result.spliterator(), false);
        return stream;
    }

    public CsvResult parse(InputStream in) {
        CsvResult csvResult = new CsvResult(parser.iterateRecords(in).iterator());
        return csvResult;
    }

    // Function to get the Stream
    private <T> Stream<T> getStreamFromIterator(Iterator<T> iterator) {

        // Convert the iterator to Spliterator
        Spliterator<T> spliterator = Spliterators.spliteratorUnknownSize(iterator, 0);

        // Get a Sequential Stream from spliterator
        return StreamSupport.stream(spliterator, false);
    }

    public CsvFormat csvFormat() {
        return csvFormat;
    }

    private CsvParserSettings toCsvParserSettings(CsvFormat csvFormat) {
        return csvFormat.data();
    }
}
