package io.leandev.appstack.mail;

import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import java.util.*;

public class Invitation extends Email {
    private static final Logger LOGGER = LogManager.getLogger(Invitation.class);
    private Activity activity = new Activity();

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public String getSubject() {
        return super.getSubject()==null ? activity.getSubject() : super.getSubject();
    }

    @Override
    public void setSubject(String subject) {
        if(activity.getSubject()==null) {
            activity.setSubject(subject);
        }
    }

    public List<InternetAddress> getRecipients(Message.RecipientType recipientType) {
        List<InternetAddress> recipients = new ArrayList<>();

        if(recipientType.equals(Message.RecipientType.TO)) {
            recipients.addAll(super.getRecipients(Message.RecipientType.TO));
            InternetAddress organizer = this.getActivity().getOrganizer();
            List<InternetAddress> attendees = this.getActivity().getAttendees(AttendeeType.REQUIRED);
            recipients.addAll(attendees);
            if(!attendees.stream().anyMatch(attendee -> attendee.getAddress().equalsIgnoreCase(organizer.getAddress()))) {
                recipients.add(organizer);
            }
        } else if(recipientType.equals(Message.RecipientType.CC)) {
            recipients.addAll(super.getRecipients(Message.RecipientType.CC));
            recipients.addAll(this.getActivity().getAttendees(AttendeeType.OPTIONAL));
        } else {
            recipients.addAll(super.getRecipients(Message.RecipientType.BCC));
        }

        return recipients;
    }
}
