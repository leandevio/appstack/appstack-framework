package io.leandev.appstack.scheduling;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SimpleScheduleBuilder {
    private Class<? extends Job> jobType;
    private String name;
    private String group;
    private String description;
    private Date startTime = new Date();
    private Date endTime;
    private int priority = 5;
    private long interval = 0L;
    private int repeatCount = 0;
    private Map<String, Object> data = new HashMap<>();

    public static SimpleScheduleBuilder newSchedule() {
        return new SimpleScheduleBuilder();
    }

    public SimpleScheduleBuilder forJob(Class<? extends Job> jobType) {
        this.jobType = jobType;
        return this;
    }

    public SimpleScheduleBuilder withIdentity(String name, String group) {
        this.name = name;
        this.group = group;
        return this;
    }

    public SimpleScheduleBuilder withIdentity(String name) {
        return withIdentity(name, null);
    }

    public SimpleScheduleBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public SimpleScheduleBuilder withIntervalInSeconds(int seconds) {
        this.interval = seconds * 1000;
        return this;
    }

    public SimpleScheduleBuilder repeatForever() {
        this.repeatCount = -1;
        return this;
    }

    public SimpleScheduleBuilder startNow() {
        return startAt(new Date());
    }

    public SimpleScheduleBuilder endAt(Date endTime) {
        this.endTime = endTime;
        return this;
    }

    public SimpleScheduleBuilder startAt(Date startTime) {
        this.startTime = startTime;
        return this;
    }

    public SimpleScheduleBuilder withPriority(int priority) {
        this.priority = priority;
        return this;
    }

    public Schedule build() {
        SimpleSchedule schedule = new SimpleSchedule();
        schedule.setJobType(jobType);
        schedule.setName(name);
        schedule.setGroup(group);
        schedule.setDescription(description);
        schedule.setStartTime(startTime);
        schedule.setEndTime(endTime);
        schedule.setPriority(priority);
        schedule.setInterval(interval);
        schedule.setRepeatCount(repeatCount);
        schedule.setData(data);
        return schedule;
    }

    public SimpleScheduleBuilder usingData(String key, Object value) {
        data.put(key, value);
        return this;
    }
}
