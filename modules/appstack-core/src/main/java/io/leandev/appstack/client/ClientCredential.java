package io.leandev.appstack.client;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ClientCredential {
    private String tenantId;
    private String clientId;
    private String clientSecret;
    private String scopes;
}
