package io.leandev.appstack.ftp;

public class FtpPropsKey {
    public static final String FTPPORT = "ftp.port";
    public static final String FTPHOME = "ftp.home";
}
