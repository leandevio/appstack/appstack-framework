package io.leandev.appstack.image;

import io.leandev.appstack.font.FontBuilder;
import io.leandev.appstack.font.FontFamily;
import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import io.leandev.appstack.measure.Length;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Canvas {
    private static final Logger LOGGER = LogManager.getLogger(Canvas.class);
    private static final int PPI = 300;

    private BufferedImage image;
    private Graphics2D graphics;
    private int ppi = PPI;

    public Canvas(int width, int height) {
        this.image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        this.graphics = image.createGraphics();
        this.setBackground(new Color(255, 255, 255, 0));
        this.setColor(Color.BLACK);
    }

    public Canvas(Length width, Length height, int ppi) {
        this(width.toPixel(ppi), height.toPixel(ppi));
        this.ppi = ppi;
    }

    public Canvas(Length width, Length height) {
        this(width, height, PPI);
    }

    public Canvas(BufferedImage image) {
        this.image = image;
        this.graphics = image.createGraphics();
    }

    public int getHeight() {
        return this.image.getHeight();
    }

    public int getWidth() {
        return this.image.getWidth();
    }

    public void setBackground(Color color) {
        Rectangle rect = new Rectangle(image.getWidth(), image.getHeight());
        Color currentColor = this.getColor();
        this.setColor(color);
        this.fillRect(rect);
        this.setColor(currentColor);
    }

    public void setBorder(Color color) {
        Rectangle rect = new Rectangle(image.getWidth(), image.getHeight());
        Color currentColor = this.getColor();
        this.setColor(color);
        this.drawRect(rect);
        this.setColor(currentColor);
    }

    public void drawRect(int x, int y, int width, int height) {
        graphics.drawRect(x, y, width, height);
    }

    public void drawRect(Rectangle rect) {
        drawRect((int) rect.getX(), (int) rect.getY(), (int) rect.getWidth(), (int) rect.getHeight());
    }

    public void fillRect(int x, int y, int width, int height) {
        graphics.fillRect(x, y, width, height);
    }

    public void fillRect(Rectangle rect) {
        fillRect((int) rect.getX(), (int) rect.getY(), (int) rect.getWidth(), (int) rect.getHeight());
    }

    /**
     *
     * scaling can be set with one of these five values:
     * - fill: this is the default value which stretches the image to fit the content box, regardless of its aspect-ratio.
     * - contain: increases or decreases the size of the image to fill the box whilst preserving its aspect-ratio.
     * - cover: the image will fill the height and width of its box, once again maintaining its aspect ratio but often cropping the image in the process.
     * - none: image will ignore the height and width of the parent and retain its original size.
     * - scale-down: the image will compare the difference between none and contain in order to find the smallest concrete object size.
     *
     * @param image
     * @param rect
     * @param position
     * @param scaling ref css object-fit property
     */
    public void drawImage(BufferedImage image, Rectangle rect, Scaling scaling, Position position) {
        BufferedImage scaledImage = (scaling == Scaling.NONE) ? image : this.scale(image, rect.getSize(), scaling);
        double x = rect.getX();
        double y = rect.getY();

        double deltaX;
        if(position.getName().contains("left")) {
            deltaX = 0;
        } else if(position.getName().contains("right")) {
            deltaX = rect.getWidth() - scaledImage.getWidth();
        } else {
            deltaX = (rect.getWidth() - scaledImage.getWidth()) / 2;
        }

        double deltaY;
        if(position.getName().contains("top")) {
            deltaY = 0;
        } else if(position.getName().contains("bottom")) {
            deltaY = rect.getHeight() - scaledImage.getHeight();
        } else {
            deltaY = (rect.getHeight() - scaledImage.getHeight()) / 2;
        }

        x += deltaX;
        y += deltaY;

        graphics.drawImage(scaledImage, null, (int) x, (int) y);
    }

    public void drawImage(BufferedImage image, Rectangle rect, Position position) {
        drawImage(image, rect, Scaling.NONE, position);
    }

    public void drawImage(BufferedImage image, Rectangle rect, Scaling scaling) {
        drawImage(image, rect, scaling, Position.CENTER);
    }

    public void drawImage(BufferedImage image, Rectangle rect) {
        drawImage(image, rect, Scaling.NONE, Position.CENTER);
    }

    public void drawImage(BufferedImage image, int left, int top) {
        Rectangle rect = new Rectangle(left, top, image.getWidth(), image.getHeight());
        drawImage(image, rect);
    }

    public void drawText(String text, Rectangle rect, Position position) {
        if(text==null) return;

        double x = rect.getX();
        double y = rect.getY();
        Dimension size = this.measureText(text);
        Font font = graphics.getFont();
        // get metrics from the graphics
        FontMetrics metrics = graphics.getFontMetrics(font);
        y += (size.getHeight() - metrics.getMaxDescent());

        double deltaX;
        if(position.getName().contains("left")) {
            deltaX = 0;
        } else if(position.getName().contains("right")) {
            deltaX = rect.getWidth() - size.getWidth();
        } else {
            deltaX = (rect.getWidth() - size.getWidth()) / 2;
        }

        double deltaY;
        if(position.getName().contains("top")) {
            deltaY = 0;
        } else if(position.getName().contains("bottom")) {
            deltaY = rect.getHeight() - size.getHeight();
        } else {
            deltaY = (rect.getHeight() - size.getHeight()) / 2;
        }

        x += deltaX;
        y += deltaY;

        graphics.drawString(text, (int) x, (int) y);
    }

    public void drawText(String text, Rectangle rect) {
        this.drawText(text, rect, Position.CENTER);
    }

    private Dimension measureText(String text) {
        Font font = graphics.getFont();
        // get metrics from the graphics
        FontMetrics metrics = graphics.getFontMetrics(font);
        // get the height of a line of text in this
        // font and render context
        int hgt = metrics.getHeight();
        // get the advance of my text in this font
        // and render context
        int adv = metrics.stringWidth(text);
        // calculate the size of a box to hold the
        // text with some padding.
        Dimension size = new Dimension(adv, hgt);

        return size;
    }

    public void setColor(Color color) {
        graphics.setColor(color);
    }

    public Color getColor() {
        return graphics.getColor();
    }

    public void setFont(Font font) {
        graphics.setFont(font);
    }

    public Font getFont() {
        return graphics.getFont();
    }

    public BufferedImage getImage() {
        return this.image;
    }

    public void dispose() {
        this.graphics.dispose();
    }

    public BufferedImage getBufferedImage() {
        return this.image;
    }

    private BufferedImage scale(BufferedImage image, Dimension size, Scaling scaling) {
        double height, width;
        double wRatio = (double) image.getWidth() / size.getWidth();
        double hRatio = (double) image.getHeight() / size.getHeight();
        if(scaling.equals(Scaling.CONTAIN)) {
            if(wRatio > hRatio) {
                width = size.getWidth();
                height =  image.getHeight() / wRatio;
            } else {
                height = size.getHeight();
                width = image.getWidth() / hRatio;
            }
        } else {
            height = size.getHeight();
            width = size.getWidth();
        }

        Image tmp = image.getScaledInstance((int) width, (int) height, Image.SCALE_SMOOTH);
        BufferedImage scaledImage = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics = scaledImage.createGraphics();
        graphics.drawImage(tmp, 0, 0, null);
        graphics.dispose();
        return scaledImage;
    }

    public Font createFont(String fontFamily, int style, int size) {
        int fontSize = size * this.ppi / 72;
        FontBuilder fontBuilder = new FontBuilder();
        return fontBuilder.withFontFamily(fontFamily)
                .withStyle(style)
                .ofSize(fontSize)
                .build();
    }

    public Font createFont(FontFamily fontFamily, int style, int size) {
        int fontSize = size * this.ppi / 72;
        FontBuilder fontBuilder = new FontBuilder();
        return fontBuilder.withFontFamily(fontFamily)
                .withStyle(style)
                .ofSize(fontSize)
                .build();
    }

    public Font createFont(String fontFamily, int style, Length size) {
        return createFont(fontFamily, style, (int) size.toPoint());
    }

    public Font createFont(FontFamily fontFamily, int style, Length size) {
        return createFont(fontFamily, style, (int) size.toPoint());
    }

    public Font createFont(Locale locale, int style, int size) {
        int fontSize = size * this.ppi / 72;
        FontBuilder fontBuilder = new FontBuilder();
        return fontBuilder.withLocale(locale)
                .withStyle(style)
                .ofSize(fontSize)
                .build();
    }

    public Font createFont(Locale locale, int style, Length size) {
        return createFont(locale, style, (int) size.toPoint());
    }

    public Rectangle createRectangle(Length x, Length y, Length width, Length height) {
        Rectangle rectangle = new Rectangle(x.toPixel(this.ppi), y.toPixel(this.ppi), width.toPixel(this.ppi), height.toPixel(this.ppi));
        return rectangle;
    }

    public Rectangle createRectangle(Length width, Length height) {
        Rectangle rectangle = new Rectangle(width.toPixel(this.ppi), height.toPixel(this.ppi));
        return rectangle;
    }

    public int getPpi() {
        return ppi;
    }

    public void setPpi(int ppi) {
        this.ppi = ppi;
    }
}
