package io.leandev.appstack.search;

import java.util.List;

public class OrNode extends LogicalNode {
    public OrNode(List<Node> nodes, Node... more) {
        super(LogicalOperator.OR, nodes, more);
    }

    public OrNode(List<Node> nodes, List<Node> more) {
        super(LogicalOperator.OR, nodes, more);
    }

    public OrNode(Node node, List<Node> more) {
        super(LogicalOperator.OR, node, more);
    }

    public OrNode(Node... nodes) {
        super(LogicalOperator.OR, nodes);
    }
}
