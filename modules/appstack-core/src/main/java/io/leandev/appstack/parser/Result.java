package io.leandev.appstack.parser;

import java.util.Iterator;

public interface Result<E extends Record> extends Iterator<E> {
}
