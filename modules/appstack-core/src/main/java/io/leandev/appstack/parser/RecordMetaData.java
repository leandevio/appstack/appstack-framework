package io.leandev.appstack.parser;

public interface RecordMetaData {
    String columnName(int i);
}
