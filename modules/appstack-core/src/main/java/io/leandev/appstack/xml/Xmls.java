package io.leandev.appstack.xml;

import org.w3c.dom.Document;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Xmls {
    public static InputStream newInputStream(Document document) throws IOException {
        InputStream inputStream;
        try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            XmlWriter xmlWriter = new XmlWriter(byteArrayOutputStream)) {
            xmlWriter.write(document);
            inputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        }
        return inputStream;
    }
}
