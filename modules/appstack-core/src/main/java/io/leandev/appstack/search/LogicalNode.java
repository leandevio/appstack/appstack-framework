package io.leandev.appstack.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LogicalNode implements Node {
    private LogicalOperator operator;
    private List<Node> children;

    public LogicalNode(LogicalOperator operator, List<Node> nodes, Node... more) {
        this.operator = operator;
        this.children = new ArrayList<>(nodes);
        this.children.addAll(Arrays.asList(more));
    }

    public LogicalNode(LogicalOperator operator, List<Node> nodes, List<Node> more) {
        this.operator = operator;
        this.children = new ArrayList<>(nodes);
        this.children.addAll(more);
    }

    public LogicalNode(LogicalOperator operator, Node node, List<Node> more) {
        this.operator = operator;
        this.children = new ArrayList<>(more);
        this.children.add(node);
    }

    public LogicalNode(LogicalOperator operator, Node... nodes) {
        this.operator = operator;
        this.children = Arrays.asList(nodes);
    }

    public LogicalOperator operator() {
        return this.operator;
    }

    public List<Node> children() {
        return this.children;
    }
}
