package io.leandev.appstack.workbook;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

import java.util.ArrayList;
import java.util.List;

public class XlsRow implements Row {
    private HSSFRow data;
    private XlsWorksheet worksheet;

    protected XlsRow(HSSFRow row, XlsWorksheet worksheet) {
        this.data = row;
        this.worksheet = worksheet;
    }

    @Override
    public XlsCell getCell(int i) {
        HSSFCell cell = data.getCell(i);
        return toCell(cell);
    }

    @Override
    public Object getValue(int index) {
        XlsCell cell = getCell(index);
        return cell == null ? null : cell.getValue();
    }

    @Override
    public XlsCell createCell() {
        int lastCellNum = this.lastCellNum();
        int index = (lastCellNum==-1) ? 0 : lastCellNum + 1;
        return toCell((HSSFCell) data.createCell(index));
    }

    @Override
    public Object[] getValues() {
        List<Object> values = new ArrayList<>();
        if(this.size()>0) {
            for(int i=firstCellNum(); i<=this.lastCellNum(); i++) {
                values.add(this.getValue(i));
            }
        }
        return values.toArray();
    }

    @Override
    public int lastCellNum() {
        int num = data.getLastCellNum();
        return num==-1 ? num : data.getLastCellNum() - 1;
    }

    @Override
    public int firstCellNum() {
        return data.getFirstCellNum();
    }

    @Override
    public int size() {
        return lastCellNum()==-1 ? 0 : lastCellNum() - firstCellNum() + 1;
    }

    public XlsWorksheet worksheet() {
        return this.worksheet;
    }

    private XlsCell toCell(HSSFCell cell) {
        if(cell==null) return null;
        return new XlsCell(cell, this);
    }
}
