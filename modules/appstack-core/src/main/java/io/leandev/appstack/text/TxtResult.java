package io.leandev.appstack.text;

import io.leandev.appstack.parser.Result;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class TxtResult implements Result<TxtRecord> {
    private Iterator<String> lines;
    private TxtFormat txtFormat;
    private TxtRecordMetaData metaData;
    private String buffer = null;

    public TxtResult(Iterator<String> lines, TxtFormat txtFormat) {
        this.lines = lines;
        this.txtFormat = txtFormat;
        extractMetaData();
    }

    private void extractMetaData() {
        if(txtFormat.isHeaderExtractionEnabled() && lines.hasNext()) {
            lines.next();
        }
        this.metaData = new TxtRecordMetaData();
    }

    @Override
    public boolean hasNext() {
        boolean test = false;
        if(txtFormat.isSkipEmptyLines()) {
            if(buffer==null) {
                while(lines.hasNext()) {
                    buffer = lines.next();
                    if(buffer.length()>0) {
                        test = true;
                        break;
                    }
                }
            } else {
                test = true;
            }
        } else {
            test = lines.hasNext();
        }

        return test;
    }

    @Override
    public TxtRecord next() {
        String line = null;

        if(buffer==null) {
            if(txtFormat.isSkipEmptyLines()) {
                while(lines.hasNext()) {
                    line = lines.next();
                    if(line.length()>0) {
                        break;
                    }
                }
                if(line==null || line.length()==0) {
                    throw new NoSuchElementException();
                }
            } else {
                line = lines.next();
            }
        } else {
            line = buffer;
            buffer = null;
        }

        TxtRecord txtRecord = new TxtRecord();
        int beginIndex = 0;
        int[] columnLengths = txtFormat.columnLengths();
        for(int i=0; i<columnLengths.length; i++) {
            int length = columnLengths[i];
            String name = metaData.columnName(i);
            int endIndex = Math.min(beginIndex + length, line.length());
            String value = line.substring(beginIndex, endIndex);
            if(!txtFormat.isKeepPadding()) {
                value = value.trim();
            }
            if(value.length()==0) {
                value = null;
            }
            txtRecord.put(name, value);
            beginIndex = endIndex;
        }
        return txtRecord;
    }
}
