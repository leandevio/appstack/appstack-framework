package io.leandev.appstack.text;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class TextWriter implements AutoCloseable {
    private OutputStream outputStream;
    private Charset charset = StandardCharsets.UTF_8;

    public TextWriter(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public TextWriter(OutputStream outputStream, Charset charset) {
        this.outputStream = outputStream;
        this.charset = charset;
    }

    public void write(String text) throws IOException {
        this.write(text, charset);
    }

    public void write(String text, Charset charset) throws IOException {
        byte[] bytes = text.getBytes(charset);
        outputStream.write(bytes);
    }

    @Override
    public void close() throws IOException {
        this.outputStream.close();
    }
}
