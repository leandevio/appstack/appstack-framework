package io.leandev.appstack.doc;

public class Background {
    private Color color;

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return this.color;
    }

    public Color color() {
        return getColor();
    }
}
