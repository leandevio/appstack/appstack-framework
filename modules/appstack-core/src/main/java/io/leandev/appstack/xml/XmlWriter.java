package io.leandev.appstack.xml;

import org.w3c.dom.Document;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class XmlWriter implements AutoCloseable {
    private OutputStream outputStream;

    public XmlWriter(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public XmlWriter() {
        this.outputStream = new ByteArrayOutputStream();
    }

    public void write(Document document) throws IOException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(outputStream);
        try {
            Transformer transformer = transformerFactory.newTransformer();
            // pretty print
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);
        } catch (TransformerException e) {
            throw new XmlException(e);
        }
    }

    @Override
    public void close() throws IOException {
        this.outputStream.close();
    }
}