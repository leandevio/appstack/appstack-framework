package io.leandev.appstack.text;

import java.io.IOException;

/**
 * 使用讀取一般文字檔案的方式讀取資料．例如可以使用一次讀取一列文字的方式讀取資料．
 */
public interface ITextReader extends AutoCloseable {
    /**
     * 讀取下一列文字，並且將讀取指標移到下一列．
     *
     * @return 下一列文字
     * @throws IOException
     */
    String readLine() throws IOException;

    /**
     * 讀取下一列文字，但是讀取的指標不會移動到下一列．
     * @return 下一列文字
     * @throws IOException
     */
    String peekLine() throws IOException;

    /**
     * 標記現在的位置．搭配 reset() 使用．
     *
     * @throws IOException
     * @see #mark()
     */
    void mark() throws IOException;

    /**
     * 倒轉回到上一個標記的位置．
     *
     * @throws IOException
     */
    void reset() throws IOException;

    void close() throws IOException;
}
