package io.leandev.appstack.xml;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class XmlFinder {
    XPath xPath = XPathFactory.newInstance().newXPath();
    Node node;
    public XmlFinder(Node node) {
        this.node = node;
    }

    public Optional<Element> findElement(XPathExpression xPathExpression) {
        String expression = xPathExpression.toString();
        return this.findElement(expression);
    }

    public Optional<Element> findElement(String xPathExpression) {
        NodeList nodeList = null;
        try {
            nodeList = (NodeList) xPath.compile(xPathExpression).evaluate(
                    this.node, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            throw new XmlException(e);
        }
        if(nodeList.getLength() == 0) {
            return Optional.empty();
        }
        Node node = nodeList.item(0);
        if(node.getNodeType() != Node.ELEMENT_NODE) {
            throw new XmlException(String.format("Failed to cast the node to an Element. %s", node));
        }
        return Optional.of((Element) node);
    }

    public Optional<Element> findLastElement(String xPathExpression) {
        List<Element> elements = findAllElements(xPathExpression);

        return (elements.size() > 0) ? Optional.of(elements.get(elements.size() - 1)) : Optional.empty();
    }

    public List<Element> findAllElements(XPathExpression xPathExpression) {
        String expression = xPathExpression.toString();
        return this.findAllElements(expression);
    }

    public List<Element> findAllElements(String xPathExpression) {
        NodeList nodeList;
        try {
            nodeList = (NodeList) xPath.compile(xPathExpression).evaluate(
                    this.node, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            throw new XmlException(e);
        }
        List<Element> elements = new ArrayList<>();
        for(int i=0; i<nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if(node.getNodeType() != Node.ELEMENT_NODE) {
                throw new XmlException(String.format("Failed to cast the node to an Element. %s", node));
            }
            elements.add((Element) node);
        }
        return elements;
    }

    public List<Node> findAllNodes(String xPathExpression) {
        NodeList nodeList;
        try {
            nodeList = (NodeList) xPath.compile(xPathExpression).evaluate(
                    this.node, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            throw new XmlException(e);
        }
        List<Node> nodes = new ArrayList<>();
        for(int i=0; i< nodeList.getLength(); i++) {
            nodes.add(nodeList.item(i));
        }
        return nodes;
    }

    public Optional<Node> findNode(String xPathExpression) {
        return this.findAllNodes(xPathExpression).stream().findFirst();
    }

    public List<Element> findAllChildElements(String xPathExpression) {
        Optional<Node> node = this.findNode(xPathExpression);
        if(!node.isPresent()) {
            throw new XmlException(String.format("Failed to find the node. %s", xPathExpression));
        }
        NodeList nodeList = node.get().getChildNodes();
        List<Element> elements = new ArrayList<>();
        for(int i=0; i<nodeList.getLength(); i++) {
            Node _node = nodeList.item(i);
            if(_node.getNodeType() == Node.ELEMENT_NODE) {
                elements.add((Element) _node);
            }
        }
        return elements;
    }

    public Optional<Element> findFirstChildElement(String xPathExpression) {
        Optional<Node> node = this.findNode(xPathExpression);
        if(!node.isPresent()) {
            throw new XmlException(String.format("Failed to find the node. %s", xPathExpression));
        }
        NodeList nodeList = node.get().getChildNodes();
        Element element = null;
        for(int i=0; i<nodeList.getLength(); i++) {
            Node _node = nodeList.item(i);
            if(_node.getNodeType() == Node.ELEMENT_NODE) {
                element = (Element) _node;
                break;
            }
        }
        return Optional.ofNullable(element);
    }

    public List<Element> findAllChildElements() {
        return this.findAllChildElements("/");
    }

    public Optional<Element> findFirstChildElement() {
        return this.findFirstChildElement("/");
    }

    public Optional<CDATASection> findFirstChildCDATASection(String xPathExpression) {
        Optional<Node> node = this.findNode(xPathExpression);
        if(!node.isPresent()) {
            throw new XmlException(String.format("Failed to find the node. %s", xPathExpression));
        }
        NodeList nodeList = this.node.getChildNodes();
        CDATASection cdataSection = null;
        for(int i=0; i<nodeList.getLength(); i++) {
            Node _node = nodeList.item(i);
            if(_node.getNodeType() == Node.CDATA_SECTION_NODE) {
                cdataSection = (CDATASection) _node;
                break;
            }
        }
        return Optional.ofNullable(cdataSection);
    }

    public Optional<CDATASection> findFirstChildCDATASection() {
        return this.findFirstChildCDATASection("/");
    }
}
