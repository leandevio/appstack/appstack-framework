package io.leandev.appstack.converter;

public class LongConverter implements Converter<Long>{
    @Override
    public Long convert(Object value) {
        Long number;
        if(value instanceof Number) {
            number = toLong((Number) value);
        } else if(value==null) {
            number = null;
        } else if(value instanceof String) {
            number = toLong((String) value);
        } else {
            throw new ConversionException("Only support convert String and Number to Long.");
        }
        return number;
    }

    private Long toLong(Number value) {
        Long n = value.longValue();
        return n;
    }

    private Long toLong(String value) {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            throw new ConversionException(String.format("Failed to convert the value: %s", value), e);
        }
    }
}
