package io.leandev.appstack.text;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class TextReader implements AutoCloseable {
    private InputStream inputStream;

    public TextReader(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String readAll() throws IOException {
        Scanner scanner = new Scanner(inputStream).useDelimiter("\\A");
        StringBuilder sb = new StringBuilder();
        while(scanner.hasNext()) {
            sb.append(scanner.next());
        }
        scanner.close();
        return sb.toString();
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }
}
