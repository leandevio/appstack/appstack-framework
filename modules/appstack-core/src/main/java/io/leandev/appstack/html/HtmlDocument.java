package io.leandev.appstack.html;

import io.leandev.appstack.doc.Block;
import io.leandev.appstack.doc.Heading;
import io.leandev.appstack.doc.Listing;
import io.leandev.appstack.doc.Paragraph;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class HtmlDocument implements io.leandev.appstack.doc.Document {
    private Document document;
    private List<HtmlBlock> blocks = new ArrayList<>();

    public static HtmlDocument of(io.leandev.appstack.doc.Document document) {
        HtmlDocument htmlDocument = new HtmlDocument();

        document.blocks().stream().forEach(htmlDocument::appendBlock);

        return htmlDocument;
    }
    public HtmlDocument() {
        this.document = Jsoup.parse("<!DOCTYPE html><html></html>");
        this.document.charset(StandardCharsets.UTF_8);
    }

    public HtmlDocument(InputStream inputStream) throws IOException {
        StringBuilder textBuilder = new StringBuilder();
        try (Reader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            int c = 0;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
        }
        this.document = Jsoup.parse(textBuilder.toString());
    }

    protected HtmlDocument(Document document) {
        this.document = document;
    }

    public HtmlElement body() {
        return new HtmlElement(this.document.body());
    }

    public HtmlElement createHtmlElement(String tagName) {
        return new HtmlElement(this.document.createElement(tagName));
    }

    public String html() {
        return this.document.html();
    }

    public Document document() {
        return this.document;
    }
    @Override
    public List<HtmlBlock> blocks() {
        return this.blocks;
    }

    @Override
    public Block appendBlock(Block block) {
        HtmlElement body = this.body();

        HtmlBlock htmlBlock;
        if(block instanceof HtmlBlock) {
            htmlBlock = (HtmlBlock) block;
        } else if(block instanceof Paragraph) {
            htmlBlock = HtmlParagraph.of((Paragraph) block);
        } else if(block instanceof Listing) {
            htmlBlock = HtmlListing.of((Listing) block);
        } else if(block instanceof Heading) {
            htmlBlock = HtmlHeading.of((Heading) block);
        } else {
            htmlBlock = HtmlParagraph.of((Paragraph) block);
        }

        body.append(htmlBlock);
        this.blocks.add(htmlBlock);

        return htmlBlock;
    }

    @Override
    public HtmlParagraph createParagraph() {
        return new HtmlParagraph();
    }

    @Override
    public HtmlHeading createHeading(int level) {
        return new HtmlHeading(level);
    }

    @Override
    public HtmlListing createList(boolean ordered) {
        return new HtmlListing(ordered);
    }

    @Override
    public HtmlTextRun createTextRun(String text) {
        return new HtmlTextRun(text);
    }

    @Override
    public HtmlImage createImage(String source) {
        return new HtmlImage(source);
    }

    @Override
    public HtmlLink createLink(String href, String text) {
        return new HtmlLink(href, text);
    }

    @Override
    public void write(OutputStream outputStream) throws IOException {
        byte[] bytes = this.document.outerHtml().getBytes();
        outputStream.write(bytes);
    }

}
