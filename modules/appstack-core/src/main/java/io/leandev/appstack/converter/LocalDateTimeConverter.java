package io.leandev.appstack.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class LocalDateTimeConverter implements Converter<LocalDateTime> {
    private static final String DEFAULT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
    private final SimpleDateFormat formatter;
    private final ZoneId zoneId;

    public static LocalDateTimeConverter of(String pattern) {
        return new LocalDateTimeConverter(pattern, ZoneId.systemDefault());
    }
    public static LocalDateTimeConverter of(String pattern, ZoneId zoneId) {
        return new LocalDateTimeConverter(pattern, zoneId);
    }

    public LocalDateTimeConverter(String pattern, ZoneId zoneId) {
        this.formatter = new SimpleDateFormat(pattern);
        this.zoneId = zoneId;
    }

    public LocalDateTimeConverter(String pattern) {
        this(pattern, ZoneId.systemDefault());
    }

    public LocalDateTimeConverter() {
        this(DEFAULT_PATTERN, ZoneId.systemDefault());
    }

    @Override
    public LocalDateTime convert(Object value) {
        return this.convert(value, zoneId);
    }

    public LocalDateTime convert(Object value, ZoneId zoneId) {
        LocalDateTime localDateTime;

        if(value instanceof LocalDateTime) {
            localDateTime = (LocalDateTime) value;
        } else if(value==null) {
            localDateTime = null;
        } else if(value instanceof Date) {
            localDateTime = toLocalDateTime((Date) value, zoneId);
        } else if(value instanceof String) {
            try {
                localDateTime = toLocalDateTime((String) value, zoneId);
            } catch (ParseException e) {
                throw new ConversionException(e);
            }
        } else if(value instanceof Long) {
            localDateTime = toLocalDateTime((Long) value, zoneId);
        } else if(value instanceof Integer) {
            localDateTime = toLocalDateTime((Integer) value, zoneId);
        } else {
            throw new ConversionException("Only support convert String, Long and Integer to LocalDateTime.");
        }
        return localDateTime;
    }

    private LocalDateTime toLocalDateTime(Integer ms, ZoneId zoneId) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(ms), zoneId);
    }

    private LocalDateTime toLocalDateTime(Long ms, ZoneId zoneId) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(ms), zoneId);
    }

    private LocalDateTime toLocalDateTime(Date date, ZoneId zoneId) {
        return LocalDateTime.ofInstant(date.toInstant(), zoneId);
    }

    private LocalDateTime toLocalDateTime(String s, ZoneId zoneId) throws ParseException {
        return  toLocalDateTime(formatter.parse(s), zoneId);
    }
}
