package io.leandev.appstack.processor;

import io.leandev.appstack.constant.CorePropsDefault;
import io.leandev.appstack.constant.CorePropsKey;
import io.leandev.appstack.util.Environment;

import java.nio.file.Path;
import java.util.Map;

public class CoreEnvironmentProcessor implements EnvironmentProcessor {
    @Override
    public void postProcess(Environment environment) {
        Path log = environment.logFolder().resolve(environment.appName() + ".log");
        environment.setProperty(CorePropsKey.SERVERPORT, CorePropsDefault.SERVERPORT, false);
        environment.setProperty(CorePropsKey.LOGFILENAME, log.toString(), false);
        environment.setProperty(CorePropsKey.LOGFILEPATTERN, CorePropsDefault.LOGFILEPATTERN, false);
    }

    @Override
    public void postProcess(Environment environment, Map<String, Object> props) {
        configJmx(environment, props);
        configLogging(environment, props);
        configJackson(environment, props);
        configServer(environment, props);
        configSpringBoot(environment, props);
    }

    private void configSpringBoot(Environment environment, Map<String, Object> props) {
        environment.properties().forEach((k, v) -> {
            String key = (String) k;
            String value = (String) v;
            if(key.startsWith("spring.") || key.startsWith("logging.") || key.startsWith("info.") || key.equals("debug")) {
                props.put(key, value);
            }
        });
    }

    private void configLogging(Environment environment, Map<String, Object> props) {
        props.put("logging.level.root", environment.logLevel().name());
        environment.properties().forEach((k, v) -> {
            String key = (String) k;
            String value = (String) v;
            if(key.startsWith(CorePropsKey.LOGLEVEL + ".")) {
                String mappignKey = "logging.level." + key.substring(CorePropsKey.LOGLEVEL.length()+1);
                props.put(mappignKey, value);
            }
        });
    }

    /**
     *  Since Spring Boot 2.2 server.use-forward-headers is now deprecated. server.
     *  forward-headers-strategy has options native, framework or none. After test, only framework works with Nginx.
     *  FRAMEWORK uses Spring's support for handling forwarded headers.
     *  For example, Spring Boot auto creates an ForwardedHeaderFilter bean for Spring MVC when server.
     *  ForwardedHeaderFilter handles non-standard headers X-Forwarded-Host, X-Forwarded-Port, X-Forwarded-Proto, X-Forwarded-Ssl, and X-Forwarded-Prefix.
     *
     * @param environment
     * @param props
     */
    private void configServer(Environment environment, Map<String, Object> props) {
        props.put("server.port", environment.property(CorePropsKey.SERVERPORT));
        props.put("server.forward-headers-strategy", "framework");
    }

    private void configJackson(Environment environment, Map<String, Object> props) {
        props.put("spring.jackson.serialization.write-dates-as-timestamps", "false");
        props.put("spring.jackson.date-format", "yyyy-MM-dd'T'HH:mm:ss.SSSX");
    }

    private void configJmx(Environment environment, Map<String, Object> props) {
        props.put("spring.jmx.default-domain", environment.appName());
    }
}
