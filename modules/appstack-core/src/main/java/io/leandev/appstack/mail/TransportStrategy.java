package io.leandev.appstack.mail;

/**
 *
 * SMTP
 * Vanilla SMTP with an insecure STARTTLS upgrade (if supported).
 * SMTP_TLS
 * Plaintext SMTP with a mandatory, authenticated STARTTLS upgrade.
 * SMTPS
 * SMTP entirely encapsulated by TLS.
 *
 */
public enum TransportStrategy {
    SMTP("SMTP"), SMTP_TLS("SMTP_TLS"), SMTPS("SMTPS");

    private final String name;

    TransportStrategy(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }
}
