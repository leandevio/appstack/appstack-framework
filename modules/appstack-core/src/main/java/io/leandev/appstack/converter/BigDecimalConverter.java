package io.leandev.appstack.converter;

import io.leandev.appstack.exception.ConversionException;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

public class BigDecimalConverter implements Converter<BigDecimal>{
    private DecimalFormat decimalFormat;

    public BigDecimalConverter(String pattern) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        symbols.setDecimalSeparator('.');
        decimalFormat = new DecimalFormat(pattern, symbols);
        decimalFormat.setParseBigDecimal(true);
    }

    public BigDecimalConverter() {
        this( "#,##0.0#");
    }
    @Override
    public BigDecimal convert(Object value) {
        BigDecimal bigDecimal;
        if(value instanceof BigDecimal) {
            bigDecimal = (BigDecimal) value;
        } else if(value==null) {
            bigDecimal = null;
        } else if(value instanceof Number) {
            bigDecimal = toDecimal((Number) value);
        } else if(value instanceof String) {
            bigDecimal = toDecimal((String) value);
        } else {
            throw new ConversionException("Only support convert String and Number to BigDecimal.");
        }
        return bigDecimal;
    }

    private BigDecimal toDecimal(Number value) {
        return new BigDecimal(value.toString());
    }

    private BigDecimal toDecimal(String value) {
        try {
            return (BigDecimal) decimalFormat.parse(value);
        } catch (ParseException e) {
            throw new ConversionException(String.format("Failed to convert the value: %s", value), e);
        }
    }
}
