package io.leandev.appstack.html;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class HtmlParser {

    public HtmlDocument parse(String html) {
        return new HtmlDocument(Jsoup.parse(html));
    }

    public HtmlDocument parse(InputStream inputStream) throws IOException {
        org.jsoup.nodes.Document document = Jsoup.parse(inputStream, StandardCharsets.UTF_8.name(), "www.leandev.io");

        HtmlDocument htmlDocument = new HtmlDocument(document);

        return htmlDocument;
    }
}
