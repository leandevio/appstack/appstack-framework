package io.leandev.appstack.scheduling;

public interface Job {
    void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException;
}
