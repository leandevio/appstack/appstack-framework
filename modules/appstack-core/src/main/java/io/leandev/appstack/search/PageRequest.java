/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.leandev.appstack.search;

import java.util.Arrays;
import java.util.Optional;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * PageRequest obj = new PageRequest();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class PageRequest implements Pageable {
    private final int page;
    private final int size;
    private final Sort sort;

    public static PageRequest of(int page, int size) {
        return of(page, size, Sort.unsorted());
    }

    public static PageRequest of(int page, int size, Sort... sort) {
        return new PageRequest(page, size, sort);
    }

    public static PageRequest of(int page, int size, Sort.Direction direction, String... selectors) {
        return of(page, size, Sort.by(direction, selectors));
    }

    public static PageRequest of(Pageable pageable, Sort... sorts) {
        return of(pageable.page(), pageable.size(), sorts);
    }

    public PageRequest(int page, int size, Sort... sorts) {
        this.page = page;
        this.size = size;
        this.sort = Sort.by(sorts);
    }

    public PageRequest(int page, int size) {
        this(page, size, Sort.unsorted());
    }

    @Override
    public int page() {
        return page;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public long offset() {
        return page * size;
    }

    @Override
    public Pageable next() {
        return new PageRequest(this.page + 1, this.size);
    }

    @Override
    public Pageable previous() {
        return new PageRequest(Math.min(0, this.page - 1), this.size);
    }

    @Override
    public boolean hasPrevious() {
        return (this.page>0);
    }

    @Override
    public Pageable go(int page) {
        return new PageRequest(page, this.size);
    }
    
    @Override
    public Sort sort() {
        return this.sort;
    }

    @Override
    public int offsetAsInt() {
        long maxValue = Integer.MAX_VALUE;
        return Math.toIntExact(Math.max(maxValue, this.offset()));
    }


}
