package io.leandev.appstack.xml;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import java.util.List;
import java.util.stream.Collectors;

public class XmlDocument {
    private Document document;
    protected XmlDocument(Document document) {
        this.document = document;
    }

    public XmlElement getRootElement() {
        Element root = this.document.getRootElement();
        return new XmlElement(root);
    }

    public XmlElement select(String xpathExpression) {
        Node node = this.document.selectSingleNode(xpathExpression);
        XmlElement xmlElement;
        if(node!=null && node instanceof Element) {
            xmlElement = new XmlElement((Element) node);
        } else {
            xmlElement = null;
        }
        return xmlElement;
    }

    public List<XmlElement> selectAll(String xpathExpression) {
        List<Node> nodes = this.document.selectNodes(xpathExpression);
        List<XmlElement> xmlElements = nodes.stream()
                .filter(node -> node!=null && node instanceof Element)
                .map(node -> new XmlElement((Element) node))
                .collect(Collectors.toList());
        return xmlElements;
    }
}
