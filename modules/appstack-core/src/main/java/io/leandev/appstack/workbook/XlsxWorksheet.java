package io.leandev.appstack.workbook;

import io.leandev.appstack.doc.BorderStyle;
import io.leandev.appstack.doc.Color;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;


public class XlsxWorksheet implements Worksheet {
    private XSSFSheet data;
    private XlsxWorkbook workbook;

    protected XlsxWorksheet(XSSFSheet sheet, XlsxWorkbook workbook) {
        this.data = sheet;
        this.workbook = workbook;
    }

    public void autoSizeColumns() {
        for(int i = firstRowNum(); i<= lastRowNum(); i++) {
            data.autoSizeColumn(i);
//            int width = data.getColumnWidth(i);
//            float pixels = data.getColumnWidthInPixels(i);
            // 每個欄位的寬度最大不能超過 255 個 characters
//            data.setColumnWidth(i, width + Math.round(width/pixels * 12));
        }
    }

    public void setGridlines(boolean value) {
        setDisplayGridlines(value);
        setPrintGridlines(value);
    }

    public void setDisplayGridlines(boolean value) {
        data.setDisplayGridlines(value);
    }

    public void setPrintGridlines(boolean value) {
        data.setPrintGridlines(value);
    }

    public void setBorderColor(Color color) {
        if(this.size()==0) return;
        for(int i = firstRowNum(); i<= lastRowNum(); i++) {
            XlsxRow row = this.getRow(i);
            for(int j=row.firstCellNum(); j<=row.lastCellNum(); j++) {
                XlsxCell cell = row.getCell(j);
                if(i==firstRowNum()) {
                    cell.setBorderTopColor(color);
                } else if(i==lastRowNum()) {
                    cell.setBorderBottomColor(color);
                }
                if(j==row.firstCellNum()) {
                    cell.setBorderLeftColor(color);
                } else if(j==row.lastCellNum()) {
                    cell.setBorderRightColor(color);
                }
            }
        }
    }

    public void setBorderStyle(BorderStyle borderStyle) {
        if(this.size()==0) return;
        for(int i = firstRowNum(); i<= lastRowNum(); i++) {
            XlsxRow row = this.getRow(i);
            for(int j=row.firstCellNum(); j<=row.lastCellNum(); j++) {
                XlsxCell cell = row.getCell(j);
                if(i==firstRowNum()) {
                    cell.setBorderTopStyle(borderStyle);
                } else if(i==lastRowNum()) {
                    cell.setBorderBottomStyle(borderStyle);
                }
                if(j==row.firstCellNum()) {
                    cell.setBorderLeftStyle(borderStyle);
                } else if(j==row.lastCellNum()) {
                    cell.setBorderRightStyle(borderStyle);
                }
            }
        }
    }

    @Override
    public XlsxRow getRow(int i) {
        XSSFRow row = data.getRow(i);
        return toRow(row);
    }

    @Override
    public XlsxRow createRow() {
        return toRow(data.createRow(lastRowNum()+1));
    }

    @Override
    public XSSFSheet data() {
        return this.data;
    }

    @Override
    public int lastRowNum() {
        int lastRowNum = data.getLastRowNum();
        if(lastRowNum==0 && data.getRow(0)==null) {
            lastRowNum = -1;
        }
        return lastRowNum;
    }

    @Override
    public int firstRowNum() {
        int firstRowNum = data.getFirstRowNum();
        if(firstRowNum==0 && data.getRow(0)==null) {
            firstRowNum = -1;
        }
        return firstRowNum;
    }

    @Override
    public int size() {
        return data.getPhysicalNumberOfRows();
    }

    @Override
    public XlsxWorkbook workbook() {
        return this.workbook;
    }

    private XlsxRow toRow(XSSFRow row) {
        if(row==null) return null;
        return new XlsxRow(row, this);
    }
}
