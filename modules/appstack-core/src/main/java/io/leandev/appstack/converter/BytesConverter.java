package io.leandev.appstack.converter;

import io.leandev.appstack.exception.ConversionException;

import java.util.Base64;

public class BytesConverter implements Converter<byte[]> {
    @Override
    public byte[] convert(Object value) {
        if(value==null) return null;

        if(!(value instanceof String)) {
            throw new ConversionException("Only support convert String to byte[].");
        }
        String separator = ",";
        String data = (String) value;
        if (data.contains(separator)) {
            data = data.split(separator)[1];
        }
        byte[] decoded = Base64.getDecoder().decode(data);
        return decoded;
    }
}