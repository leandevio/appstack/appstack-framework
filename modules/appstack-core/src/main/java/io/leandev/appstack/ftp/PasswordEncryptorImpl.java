package io.leandev.appstack.ftp;

import io.leandev.appstack.crypto.PasswordEncoder;
import org.apache.ftpserver.usermanager.PasswordEncryptor;

public class PasswordEncryptorImpl implements PasswordEncryptor {
    private PasswordEncoder passwordEncoder;

    public PasswordEncryptorImpl(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
    @Override
    public String encrypt(String password) {
        return passwordEncoder.encode(password);
    }

    @Override
    public boolean matches(String passwordToCheck, String storedPassword) {
        return passwordEncoder.matches(passwordToCheck, storedPassword);
    }
}
