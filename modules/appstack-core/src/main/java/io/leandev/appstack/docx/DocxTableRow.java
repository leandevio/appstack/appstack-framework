package io.leandev.appstack.docx;

import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import java.util.List;
import java.util.stream.Collectors;

public class DocxTableRow {
    private XWPFTableRow xwpfTableRow;

    protected DocxTableRow(XWPFTableRow xwpfTableRow) {
        this.xwpfTableRow = xwpfTableRow;
    }

    public DocxTableCell getCell(int position) {
        return new DocxTableCell(xwpfTableRow.getCell(position));
    }

    public List<DocxTableCell> getCells() {
        return xwpfTableRow.getTableCells().stream()
                .map(DocxTableCell::new)
                .collect(Collectors.toList());
    }

    public DocxTableCell createCell() {
        return new DocxTableCell(xwpfTableRow.createCell());
    }
}
