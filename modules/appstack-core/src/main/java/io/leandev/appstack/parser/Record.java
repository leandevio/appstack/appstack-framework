package io.leandev.appstack.parser;

import io.leandev.appstack.converter.Converters;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public interface Record {
    Converters converters = new Converters();

    int size();

    List getValues();

    Object getValue(int index);

//    <S> S getValue(int index, Class<S> type);
//
//    <S> S getValue(String name, Class<S> type);

    default  String getString(int index) {
        return  converters.convert(getValue(index), String.class);
    }
    default Integer getInt(int index) {
        return converters.convert(getValue(index), Integer.class);
    }

    default Long getLong(int index) {
        return converters.convert(getValue(index), Long.class);
    }
    
    default Float getFloat(int index) {
        return converters.convert(getValue(index), Float.class);
    }
    
    default Double getDouble(int index) {
        return converters.convert(getValue(index), Double.class);
    }
    
    default Boolean getBoolean(int index) {
        return converters.convert(getValue(index), Boolean.class);
    }

    default BigInteger getBigInteger(int index) {
        return converters.convert(getValue(index), BigInteger.class);
    }
    
    default BigDecimal getBigDecimal(int index) {
        return converters.convert(getValue(index), BigDecimal.class);
    }
    
    default Date getDate(int index) {
        return converters.convert(getValue(index), Date.class);
    }
}
