package io.leandev.appstack.html;

import io.leandev.appstack.doc.Link;

public class HtmlLink extends HtmlChunk<String> implements Link {
    private String href;
    private String text;

    public static HtmlLink of(Link link) {
        HtmlLink htmlLink = new HtmlLink(link.href(), link.content());
        return htmlLink;
    }

    public static HtmlLink of(String href) {
        HtmlLink htmlLink = new HtmlLink(href);
        return htmlLink;
    }

    public HtmlLink(String href, String text) {
        super("a");
        this.href = href;
        this.attr("href", href);
        this.setContent(text);
    }

    public HtmlLink(String href) {
        this(href, "");
    }

    @Override
    public String href() {
        return this.href;
    }

    @Override
    public String content() {
        return this.text;
    }

    @Override
    public void setContent(String text) {
        this.text = text;
        this.render();
    }

    @Override
    public void render() {
        HtmlElement htmlElement = this;
        this.html("");
        htmlElement.appendText(text);
    }
}
