package io.leandev.appstack.formatter;

public enum DataFormat {
    STRING, TEXT, DATE, DATETIME, TIME, NUMBER, INTEGER, FLOAT, MONEY
}
