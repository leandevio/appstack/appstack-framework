package io.leandev.appstack.net;

import org.apache.commons.net.util.SubnetUtils;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAddressRange {
    public static final String ENTIRE_NETWORK = "0.0.0.0/0";
    private static final String LOCALHOST_NAME = "localhost";
    public static final String LOCALHOST_IPV4 = "127.0.0.1";
    public static final String LOCALHOST_IPV6 = "0:0:0:0:0:0:0:1";
    private String notation;
    public static InetAddressRange of(String ipAddressRange) {
        return new InetAddressRange(ipAddressRange);
    }

    public InetAddressRange(String ipAddressRange) {
        this.notation = ipAddressRange;
    }

    public boolean inRange(InetAddress inetAddress) {
        String ipAddress;

        if(inetAddress instanceof Inet6Address) {
            if(inetAddress.getHostAddress().equals(LOCALHOST_IPV6)) {
                ipAddress = LOCALHOST_IPV4;
            } else {
                throw new IllegalArgumentException("IPv6 is not supported yet.");
            }
        } else {
            ipAddress = inetAddress.getHostAddress();
        }
        if(this.isCidr()) {
            return new SubnetUtils(notation).getInfo().isInRange(ipAddress);
        } else {
            return notation.equalsIgnoreCase(ipAddress);
        }
    }

    public boolean inRange(String host) throws UnknownHostException {
        InetAddress inetAddress = InetAddress.getByName(host);
        return inRange(inetAddress);
    }

    private boolean isCidr() {
        return notation.contains("/");
    }

    private int ipv4AddressToInt(InetAddress addr) {
        if (addr instanceof Inet6Address)
            throw new IllegalArgumentException(
                    "IPv6 address used in IPv4 context");

        byte[] a = addr.getAddress();

        int res = ((a[0] & 0xFF) << 24) | ((a[1] & 0xFF) << 16)
                | ((a[2] & 0xFF) << 8) | (a[3] & 0xFF);

        return res;
    }

}
