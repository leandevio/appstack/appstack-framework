package io.leandev.appstack.converter;

import io.leandev.appstack.exception.ConversionException;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ZonedDateTimeConverter implements Converter<ZonedDateTime> {
    private static final String DEFAULT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
    private final DateTimeFormatter formatter;

    public static ZonedDateTimeConverter of(String pattern) {
        return new ZonedDateTimeConverter(pattern);
    }

    public ZonedDateTimeConverter(String pattern) {
        this.formatter = DateTimeFormatter.ofPattern(pattern);
    }

    public ZonedDateTimeConverter() {
        this.formatter = DateTimeFormatter.ofPattern(DEFAULT_PATTERN);
    }

    @Override
    public ZonedDateTime convert(Object value) {
        ZonedDateTime zonedDateTime;

        if(value instanceof ZonedDateTime) {
            zonedDateTime = (ZonedDateTime) value;
        } else if(value==null) {
            zonedDateTime = null;
        } else if(value instanceof Date) {
            zonedDateTime = toZonedDateTime((Date) value);
        } else if(value instanceof String) {
            zonedDateTime = toZonedDateTime((String) value);
        } else if(value instanceof Long) {
            zonedDateTime = toZonedDateTime((Long) value);
        } else if(value instanceof Integer) {
            zonedDateTime = toZonedDateTime((Integer) value);
        } else {
            throw new ConversionException("Only support convert String, Long and Integer to Date.");
        }
        return zonedDateTime;
    }

    private ZonedDateTime toZonedDateTime(Integer ms) {
        return ZonedDateTime.ofInstant(Instant.ofEpochMilli(ms), ZoneId.systemDefault());
    }

    private ZonedDateTime toZonedDateTime(Long ms) {
        return ZonedDateTime.ofInstant(Instant.ofEpochMilli(ms), ZoneId.systemDefault());
    }

    private ZonedDateTime toZonedDateTime(Date date) {
        return ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    private ZonedDateTime toZonedDateTime(String s) {
        return  ZonedDateTime.parse(s, formatter);
    }
}
