package io.leandev.appstack.compress;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipOutputStreamBuilder {
    private ZipOutputStream zipOutputStream;

    public static ZipOutputStreamBuilder of(OutputStream outputStream) {
        return new ZipOutputStreamBuilder(outputStream);
    }

    public ZipOutputStreamBuilder(OutputStream outputStream) {
        this.zipOutputStream = new ZipOutputStream(outputStream);
    }

    public ZipOutputStreamBuilder finish() throws IOException {
        this.zipOutputStream.finish();
        return this;
    }
    public ZipOutputStream build() {
        return this.zipOutputStream;
    }

    public ZipOutputStream build(boolean close) throws IOException {
        if(close) {
            this.zipOutputStream.close();
        }
        return this.zipOutputStream;
    }

    public ZipOutputStreamBuilder putNextEntry(String name, InputStream data) throws IOException {
        ZipEntry zipEntry = new ZipEntry(name);
        zipOutputStream.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = data.read(bytes)) >= 0) {
            zipOutputStream.write(bytes, 0, length);
        }
        zipOutputStream.closeEntry();
        return this;
    }

    public ZipOutputStreamBuilder putNextEntry(String name, byte[] data) throws IOException {
        ZipEntry zipEntry = new ZipEntry(name);
        zipOutputStream.putNextEntry(zipEntry);
        int length = data.length;
        zipOutputStream.write(data, 0, length);
        zipOutputStream.closeEntry();
        return this;
    }
}
