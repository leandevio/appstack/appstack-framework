package io.leandev.appstack.geo;

public class GeoDistanceService {

    /**
     *
     * Calculate distances between points which are defined by geographical coordinates in terms of latitude and longitude
     *
     * @param origin
     * @param destination
     * @return
     */
    public EuclideanDistance computeEuclideanDistance(GeoLocation origin, GeoLocation destination) {
        double distance;
        if ((origin.latitude() == destination.latitude()) && (origin.longitude() == destination.longitude())) {
            distance = 0;
        } else {
            double theta = origin.longitude() - destination.longitude();
            distance = Math.sin(Math.toRadians(origin.latitude())) * Math.sin(Math.toRadians(destination.latitude()))
                    + Math.cos(Math.toRadians(origin.latitude())) * Math.cos(Math.toRadians(destination.latitude())) * Math.cos(Math.toRadians(theta));
            distance = Math.acos(distance);
            distance = Math.toDegrees(distance);
            distance = distance * 60 * 1.1515 * 1.609344 * 1000;
        }

        EuclideanDistance euclideanDistance = new EuclideanDistance(origin, destination, distance);
        return euclideanDistance;
    }
}
