package io.leandev.appstack.workbook;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class WorksheetParser {
    public WroksheetResult parse(Worksheet worksheet) {
        WroksheetResult wroksheetResult = new WroksheetResult(worksheet);
        return wroksheetResult;
    }

    public WroksheetResult parse(Workbook workbook) {
        Worksheet worksheet = workbook.getWorksheetAt(0);
        return parse(worksheet);
    }

    public Iterable<WorksheetRecord> iterate(Workbook workbook) {
        Worksheet worksheet = workbook.getWorksheetAt(0);
        WroksheetResult wroksheetResult = parse(worksheet);
        return () -> wroksheetResult;
    }

    public Iterable<WorksheetRecord> iterate(Worksheet worksheet) {
        WroksheetResult wroksheetResult = parse(worksheet);
        return () -> wroksheetResult;
    }

    public Stream<WorksheetRecord> stream(Workbook workbook) {
        Iterable<WorksheetRecord> result = this.iterate(workbook);
        Stream<WorksheetRecord> stream  = StreamSupport.stream(result.spliterator(), false);
        return stream;
    }

    public Stream<WorksheetRecord> stream(Worksheet worksheet) {
        Iterable<WorksheetRecord> result = this.iterate(worksheet);
        Stream<WorksheetRecord> stream  = StreamSupport.stream(result.spliterator(), false);
        return stream;
    }
}
