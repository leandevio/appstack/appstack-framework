package io.leandev.appstack.file;

import io.leandev.appstack.env.Environ;
import org.apache.commons.vfs2.*;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class VFSTest {
    private static Environ environ = Environ.getInstance();
    private static final Class CLASS = VFSTest.class;
    private static final ClassLoader CLASSLOADER = VFSTest.class.getClassLoader();
    private final Path RESOURCE = Paths.get(CLASS.getPackage().getName().replaceAll("\\.", "/"));
    private static final Path OUTPUT = environ.tmpFolder().resolve(CLASS.getSimpleName());


    @BeforeAll
    static void setup() throws IOException {
        if(!Files.exists(OUTPUT)) {
            Files.createDirectory(OUTPUT);
        }
    }


    @Test
    public void testSFTP() throws Exception {
        String username = "appfusemgr";
        String password = "appfuse1943";
        String remoteHost = "sitapp.leandev.io";
        String remoteDir = ".";
        String filename = "products.json";
        FileSystemOptions opts = new FileSystemOptions();
        SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(
                opts, "no");
        SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);

        FileSystemManager manager = VFS.getManager();
        password =  URLEncoder.encode(password, StandardCharsets.UTF_8.toString());
        // sftp put
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCE.resolve(filename).toString());
            FileObject remote = manager.resolveFile("sftp://" + username + ":" + password + "@" + remoteHost + "/" + remoteDir + "/" + filename);
            OutputStream outputStream = remote.getContent().getOutputStream()) {
            byte[] buffer = new byte[8192];
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }
        }

        // sftp list
        try(FileObject remote = manager.resolveFile("sftp://" + username + ":" + password + "@" + remoteHost + "/" + remoteDir)) {
            for(FileObject fileObject : remote.getChildren()) {
                FileName fileName = fileObject.getName();
                if(fileObject.isFolder()) {
                    System.out.println(String.format("%s(%s): %s", fileName.getBaseName(), fileName.getType().toString(), fileName.getPath()));
                } else if(fileObject.isFile()) {
                    System.out.println(String.format("%s(%s): %s", fileName.getBaseName(), fileName.getType().toString(), fileName.getPath()));
                }

            }
        }

        // sftp get
        try(OutputStream outputStream = Files.newOutputStream(OUTPUT.resolve(filename));
            FileObject remote = manager.resolveFile("sftp://" + username + ":" + password + "@" + remoteHost + "/" + remoteDir + "/" + filename);
            InputStream inputStream = remote.getContent().getInputStream()) {
            byte[] buffer = new byte[8192];
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }
        }
    }

    @Test
    public void testOpenCloseFrequently() throws Exception {
        String username = "appfusemgr";
        String password = "appfuse1943";
        String remoteHost = "sitapp.leandev.io";
        String remoteDir = "tmp";
        FileSystemOptions opts = new FileSystemOptions();
        SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(
                opts, "no");
        SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);

        password =  URLEncoder.encode(password, StandardCharsets.UTF_8.toString());
        for(int i=0; i<10; i++) {
            String filename = UUID.randomUUID().toString();
            VFS.reset();
            try(FileSystemManager manager = VFS.getManager();
                FileObject remote = manager.resolveFile("sftp://" + username + ":" + password + "@" + remoteHost + "/" + remoteDir + "/" + filename);) {
                assertThat(remote.exists()).isFalse();
            }
        }
    }
}
