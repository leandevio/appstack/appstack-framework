package io.leandev.appstack.pdf;

import io.leandev.appstack.crypto.CryptoException;
import io.leandev.appstack.crypto.KeyStoreBuilder;
import io.leandev.appstack.image.ImageWriter;
import io.leandev.appstack.util.Environment;
import org.testng.annotations.Test;

import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;

public class PdfDocumentTest {
    private final Path CONF = Environment.getDefaultInstance().appHome().resolve("conf");
    private static Path SOURCEFOLDER = Paths.get("/").resolve(PdfDocumentTest.class.getPackage().getName().replaceAll("\\.", "/"));
    private static Path TARGETFOLDER = Environment.getDefaultInstance().tmpFolder();
    private static String SOURCEFILE = "report.pdf";
    private static Path SOURCEPATH = SOURCEFOLDER.resolve(SOURCEFILE);
    private static String TARGETFILE = "report.pdf";
    private final Path TARGETPATH = TARGETFOLDER.resolve(TARGETFILE);

    @Test
    public void testSave() throws IOException {

    }

    @Test
    public void testPdfGetFirstPage() throws Exception {
        Path source = SOURCEFOLDER.resolve("certificate.pdf");
        InputStream inputStream = PdfDocumentTest.class.getResourceAsStream(source.toString());
        PdfDocument pdfDocument = new PdfDocument(inputStream);
        PdfPage pdfPage = pdfDocument.getPage(0);
        File output = TARGETFOLDER.resolve("firstPage.pdf").toFile();
        OutputStream outputStream = new FileOutputStream(output);
        PdfDocument firstPagePdfDocument = new PdfDocument();
        firstPagePdfDocument.addPage(pdfPage);
        firstPagePdfDocument.save(outputStream);
        outputStream.close();

    }

    @Test
    public void testPdfToImage() throws Exception {
        Path source = SOURCEFOLDER.resolve("certificate.pdf");
        InputStream inputStream = PdfDocumentTest.class.getResourceAsStream(source.toString());
        PdfDocument pdfDocument = new PdfDocument(inputStream);
        PdfRenderer pdfRenderer = new PdfRenderer(pdfDocument);
        BufferedImage bufferedImage = pdfRenderer.renderImage(0);
        Path target = TARGETFOLDER.resolve("certificate.png");
        OutputStream outputStream = new FileOutputStream(target.toFile());
        ImageWriter writer = new ImageWriter(outputStream);
        writer.write(bufferedImage);
        writer.close();
        pdfDocument.close();
        inputStream.close();
        outputStream.close();
    }

    @Test
    public void testPasswordAccessProtection() throws Exception {
        String ownerPassword = "1234";
        String userPassword = "0000";
        InputStream inputStream = PdfDocumentTest.class.getResourceAsStream(SOURCEPATH.toString());
        OutputStream outputStream = new FileOutputStream(TARGETPATH.toFile());
        PdfDocument pdfDocument = new PdfDocument(inputStream);
        PdfAccessPermission accessPermission = new PdfAccessPermission();
        accessPermission.setAllowModification(false);
        accessPermission.setAllowExtraction(false);
        accessPermission.setAllowPrinting(false);
        // 開啟需要密碼，並且有存取限制．
        pdfDocument.protect(ownerPassword, userPassword, accessPermission);
        pdfDocument.save(outputStream);
        pdfDocument.close();
        inputStream.close();
        outputStream.close();
    }

    @Test
    public void testPasswordDefaultAccessProtection() throws Exception {
        String ownerPassword = "1234";
        String userPassword = "0000";
        InputStream inputStream = PdfDocumentTest.class.getResourceAsStream(SOURCEPATH.toString());
        OutputStream outputStream = new FileOutputStream(TARGETPATH.toFile());
        PdfDocument pdfDocument = new PdfDocument(inputStream);
        // 開啟需要密碼，並且使用預設的存取限制．
        pdfDocument.protect(ownerPassword, userPassword);
        pdfDocument.save(outputStream);
        pdfDocument.close();
        inputStream.close();
        outputStream.close();
    }

    @Test
    public void testPasswordProtection() throws Exception {
        String userPassword = "0000";
        InputStream inputStream = PdfDocumentTest.class.getResourceAsStream(SOURCEPATH.toString());
        OutputStream outputStream = new FileOutputStream(TARGETPATH.toFile());
        PdfDocument pdfDocument = new PdfDocument(inputStream);
        // 開啟需要密碼，但是無存取限制．
        pdfDocument.protect(userPassword);
        pdfDocument.save(outputStream);
        pdfDocument.close();
        inputStream.close();
        outputStream.close();
    }

    @Test
    public void testAccessProtection() throws IOException {
        String ownerPassword = "1234567";
        InputStream inputStream = PdfDocumentTest.class.getResourceAsStream(SOURCEPATH.toString());
        OutputStream outputStream = new FileOutputStream(TARGETPATH.toFile());
        PdfDocument pdfDocument = new PdfDocument(inputStream);
        PdfAccessPermission accessPermission = new PdfAccessPermission();
        accessPermission.setAllowModification(false);
        accessPermission.setAllowExtraction(false);
        accessPermission.setAllowPrinting(false);
        // 開啟不需要密碼，但是有存取限制．
        pdfDocument.protect(accessPermission);
        pdfDocument.save(outputStream);
        pdfDocument.close();
        inputStream.close();
        outputStream.close();
    }

    /**
     *  keytool -genkeypair -storepass 24485314 -storetype pkcs12 -alias myKey -validity 365 -v -keyalg RSA -keystore keystore.p12
     *
     * @throws Exception
     * @throws PdfException
     */
    @Test
    public void testDigitalSignature() throws Exception, PdfException, CryptoException {
        String storePass = "24485314";

        KeyStore keystore = KeyStoreBuilder.newPKCS12KeyStore()
                .from(CONF.resolve("keystore.p12"), storePass)
                .build();

        String keyPass = "123456";
        PdfSignature pdfSignature = new PdfSignature(keystore, keyPass);

        InputStream inputStream = PdfDocumentTest.class.getResourceAsStream(SOURCEPATH.toString());
        OutputStream outputStream = new FileOutputStream(TARGETPATH.toFile());

        pdfSignature.sign(inputStream, outputStream);
    }
}