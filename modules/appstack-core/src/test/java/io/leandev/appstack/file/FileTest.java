package io.leandev.appstack.file;

import io.leandev.appstack.env.Environ;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class FileTest {
    private static Environ environ = Environ.getInstance();
    private static Path folder = environ.tmpFolder().resolve("FileTest");

    @BeforeAll
    static void setup() throws IOException {
        deleteDirectory(folder);
        Files.createDirectory(folder);
        Path txt = folder.resolve("file1.txt");
        Files.write(txt, "sample text".getBytes(StandardCharsets.UTF_8));
        Path md = folder.resolve("file2.md");
        Files.write(md, "sample markdown".getBytes(StandardCharsets.UTF_8));
        Path html = folder.resolve("file3.html");
        Files.write(html, "sample html".getBytes(StandardCharsets.UTF_8));
        Path tmp = folder.resolve("tmp");
        Files.createDirectory(tmp);
    }

    @Test
    public void testListFile() throws IOException {
        Optional<Path> file;
        try (Stream stream = Files.list(folder)) {
            file = stream.filter(value -> {
                Path path = (Path) value;
                boolean test = false;
                if(Files.isRegularFile(path)) {
                    Optional<String> extension = getExtension(path.getFileName().toString());
                    test = extension.isPresent() && extension.get().equals("md");
                }
                return test;
            }).findFirst();
        }
        assertThat(file.get().getFileName().toString()).isEqualTo("file2.md");
    }

    private String getBasename(String filename, boolean removeAllExtensions) {
        if (filename == null || filename.isEmpty()) {
            return filename;
        }

        String extPattern = "(?<!^)[.]" + (removeAllExtensions ? ".*" : "[^.]*$");
        return filename.replaceAll(extPattern, "");
    }

    private Optional<String> getExtension(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    private static void deleteDirectory(Path pathToBeDeleted) throws IOException {
        Files.walk(pathToBeDeleted)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }
}
