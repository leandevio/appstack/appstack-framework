package io.leandev.appstack.file;

import io.leandev.appstack.env.Environ;
import io.leandev.appstack.json.JsonParser;
import io.leandev.appstack.text.TextReader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class HttpFileRepositoryTest {
    private static final Class CLASS = HttpFileRepositoryTest.class;
    private static final ClassLoader CLASSLOADER = HttpFileRepositoryTest.class.getClassLoader();
    private final Path RESOURCE = Paths.get(CLASS.getPackage().getName().replaceAll("\\.", "/"));
    private static final Path OUTPUT = Environ.getInstance().tmpFolder().resolve(CLASS.getSimpleName());
    private static Environ environ = Environ.getInstance();
    private static Path tmp = environ.tmpFolder();
    private static final String REPO_URL = "http://localhost:4000/repository";
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    private static final JsonParser jsonParser = new JsonParser();

    @BeforeAll
    static void setup() throws IOException {
        if(!Files.exists(OUTPUT)) {
            Files.createDirectory(OUTPUT);
        }
    }

    /**
     * 將 binary 的資料輸出到指定路徑的檔案(http://localhost:4000/repository/data/product.json)。
     */
    @Test
    public void testWriteBytesToFile() throws Exception {
        String filename = "Yellow Green Christmas Gift Box.jpg";
        Path path = Paths.get("data").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new HttpFileRepository(REPO_URL, USERNAME, PASSWORD);
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCE.resolve(filename).toString());
            ByteArrayOutputStream buffer = new ByteArrayOutputStream()) {
            this.transferTo(inputStream, buffer);
            byte[] bytes = buffer.toByteArray();
            // 輸出到檔案
            fileRepository.write(path, bytes, true);
        }
    }

    /**
     * 將文字內容輸出到指定路徑的檔案(http://localhost:4000/repository/data/product.json)。
     */
    @Test
    public void testWriteTextToFile() throws Exception {
        String filename = "products.json";
        Path path = Paths.get("data").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new HttpFileRepository(REPO_URL, USERNAME, PASSWORD);
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCE.resolve(filename).toString());
            TextReader textReader = new TextReader(inputStream)) {
            String text = textReader.readAll();
            // 輸出到檔案
            fileRepository.write(path, text, true);
        }
    }

    /**
     * 從指定路徑的檔案(http://localhost:4000/repository/data/product.json)讀取檔案內容。
     */
    @Test
    public void testGetFileContent() throws Exception {
        String filename = "products.json";
        Path path = Paths.get("data").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new HttpFileRepository(REPO_URL, USERNAME, PASSWORD);
        // 讀取檔案
        try(InputStream inputStream = fileRepository.newInputStream(path);
            TextReader textReader = new TextReader(inputStream)) {
            String text = textReader.readAll();
            System.out.println(text);
        }
    }

    /**
     * 檢查指定檔案路徑(http://localhost:4000/repository/data/products.json)的檔案是否存在。
     */
    @Test
    public void testExists() throws Exception {
        String filename = "products.json";
        Path path = Paths.get("data").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new HttpFileRepository(REPO_URL, USERNAME, PASSWORD);
        // 列出資料夾下的檔案路徑
        assertThat(fileRepository.exists(path)).isTrue();
    }

    /**
     * 檢查指定的檔案路徑(http://localhost:4000/repository/data)是否為目錄。
     */
    @Test
    public void testIsDirectory() throws Exception {
        Path path = Paths.get("data");
        // 初始化 FileRepository
        FileRepository fileRepository = new HttpFileRepository(REPO_URL, USERNAME, PASSWORD);
        // 列出資料夾下的檔案路徑
        assertThat(fileRepository.isDirectory(path)).isTrue();
    }

    /**
     * 查詢指定檔案路徑(http://localhost:4000/repository/data/product.json)的檔案大小。
     */
    @Test
    public void testSize() throws Exception {
        String filename = "products.json";
        Path path = Paths.get("data").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new HttpFileRepository(REPO_URL, USERNAME, PASSWORD);
        // 列出資料夾下的檔案路徑
        assertThat(fileRepository.size(path)).isGreaterThan(0);
    }

    /**
     * 列出資料夾(http://localhost:4000/repository/data)下面的檔案路徑。
     */
    @Test
    public void testListFolder() throws Exception {
        Path path = Paths.get("data");
        // 初始化 FileRepository
        FileRepository fileRepository = new HttpFileRepository(REPO_URL, USERNAME, PASSWORD);
        // 列出資料夾下的檔案路徑
        Stream<Path> files = fileRepository.list(path);
        files.forEach(file -> System.out.println(file.toString()));
    }

    /**
     * 刪除指定路徑(http://localhost:4000/repository/data/products.json)的檔案。
     */
    @Test
    public void testDeleteFile() throws Exception {
        String filename = "products.json";
        Path uri = Paths.get("data").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new HttpFileRepository(REPO_URL, USERNAME, PASSWORD);
        // 刪除檔案
        fileRepository.deleteIfExists(uri);
    }

    /**
     * 移動檔案(http://localhost:4000/repository/data/products.json)到指定路徑(http://localhost:4000/repository/archive/products.json)。
     */
    @Test
    public void testMoveFile() throws Exception {
        String filename = "products.json";
        Path source = Paths.get("data").resolve(filename);
        Path target = Paths.get("archive").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new HttpFileRepository(REPO_URL, USERNAME, PASSWORD);
        // 移動檔案
        fileRepository.move(source, target);
    }


    @Test
    // TODO: 2022/4/18
    public void testWriteToOutputStream() throws Exception {
        String filename = "products.json";
        Path path = Paths.get("data").resolve(filename);
        FileRepository fileRepository = new HttpFileRepository(REPO_URL, USERNAME, PASSWORD);
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCE.resolve(filename).toString());
            OutputStream outputStream = fileRepository.newOutputStream(path, true)) {
            transferTo(inputStream, outputStream);
        }
    }


    private void transferTo(InputStream source, OutputStream target) throws IOException {
        byte[] buf = new byte[8192];
        int length;
        while ((length = source.read(buf)) > 0) {
            target.write(buf, 0, length);
        }
    }
}