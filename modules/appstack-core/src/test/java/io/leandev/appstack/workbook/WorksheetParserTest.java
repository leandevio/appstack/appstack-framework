package io.leandev.appstack.workbook;

import io.leandev.appstack.parser.Record;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import static org.testng.Assert.assertNotNull;

public class WorksheetParserTest {

    @Test
    public void testIteratorWorkbook() throws IOException {
        InputStream in = WorksheetParserTest.class.getResourceAsStream("/data.xlsx");
        Workbook workbook = new XlsxWorkbook(in);
        WorksheetParser parser = new WorksheetParser();
        for(Record record : parser.iterate(workbook)) {
            System.out.println(Arrays.toString(record.getValues().toArray()));
        }
        in.close();
    }

    @Test
    public void testIterateWorksheet() throws IOException {
        InputStream in = WorksheetParserTest.class.getResourceAsStream("/data.xlsx");
        Workbook workbook = new XlsxWorkbook(in);
        Worksheet worksheet = workbook.getWorksheet("Product");
        WorksheetParser parser = new WorksheetParser();
        for(Record record : parser.iterate(worksheet)) {
            assertNotNull(record.getString(0));
            assertNotNull(record.getString(1));
            assertNotNull(record.getString(2));
            assertNotNull(record.getDouble(3));
            assertNotNull(record.getDate(5));
        }
        workbook.close();
        in.close();
    }

    @Test
    public void testStreamWorkbook() throws IOException {
        InputStream in = WorksheetParserTest.class.getResourceAsStream("/data.xlsx");
        Workbook workbook = new XlsxWorkbook(in);
        WorksheetParser parser = new WorksheetParser();
        parser.stream(workbook).forEach(record -> {
            System.out.println(Arrays.toString(record.getValues().toArray()));
        });
        in.close();
    }

    @Test
    public void testStreamWorksheet() throws IOException {
        InputStream in = WorksheetParserTest.class.getResourceAsStream("/data.xlsx");
        Workbook workbook = new XlsxWorkbook(in);
        Worksheet worksheet = workbook.getWorksheet("Product");
        WorksheetParser parser = new WorksheetParser();
        parser.stream(worksheet).forEach(record -> {
            System.out.println(Arrays.toString(record.getValues().toArray()));
        });
        in.close();
    }

    @Test
    public void testIteratorXlsWorkbook() throws IOException {
        InputStream in = WorksheetParserTest.class.getResourceAsStream("/data.xls");
        Workbook workbook = new XlsWorkbook(in);
        WorksheetParser parser = new WorksheetParser();
        for(Record record : parser.iterate(workbook)) {
            System.out.println(Arrays.toString(record.getValues().toArray()));
        }
        in.close();
        workbook.close();
    }

}