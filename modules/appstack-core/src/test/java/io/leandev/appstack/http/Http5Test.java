package io.leandev.appstack.http;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.core5.http.HttpEntity;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.DataInputStream;
import java.io.InputStream;

public class Http5Test {

    private final PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();

    @BeforeTest
    public void setUp() throws Exception {

    }
    
    @Test
    public void testGet() throws Exception {

        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();

        final HttpGet httpget = new HttpGet("http://httpbin.org/get");

        CloseableHttpResponse response = httpClient.execute(httpget);
        try {
            HttpEntity ent = response.getEntity();
            InputStream is = ent.getContent();
            DataInputStream dis = new DataInputStream(is);
            // Only stream the first 200 bytes
            for(int i = 0; i < 200; i++) {
                System.out.println(( (char)dis.readByte()));
            }

        } finally {
            response.close();
        }
    }
}
