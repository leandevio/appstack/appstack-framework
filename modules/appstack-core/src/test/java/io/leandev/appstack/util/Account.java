package io.leandev.appstack.util;

import java.util.Date;

public class Account {
    private String name;
    private int credits;
    private Date birthday;
    private Date expiryDate;

    public Account(String name, int credits, Date birthday) {
        this.name = name;
        this.credits = credits;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}
