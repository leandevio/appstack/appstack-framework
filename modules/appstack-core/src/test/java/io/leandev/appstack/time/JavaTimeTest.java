package io.leandev.appstack.time;

import org.testng.annotations.Test;

import java.io.IOException;
import java.time.*;

import static org.assertj.core.api.Assertions.*;
public class JavaTimeTest {
    @Test
    public void testPeriod() throws IOException {
        LocalDate startTime = LocalDate.of(2000, 1, 1);
        LocalDate endTime = LocalDate.of(2000, 2, 10);
        Period between = Period.between(startTime, endTime);
        assertThat(between.getMonths()).isEqualTo(1);
        assertThat(between.getDays()).isEqualTo(9);
        Period between2 = Period.between(LocalDate.of(2000, 2, 1), LocalDate.of(2000, 3, 10));
        assertThat(between).isEqualTo(between2);
        Period days = Period.ofDays(69);
        assertThat(days.getMonths()).isEqualTo(0);
        assertThat(days.getDays()).isEqualTo(69);
    }

    @Test
    public void testDuration() throws IOException {
        Duration between = Duration.between(LocalDateTime.of(2000, 1, 1, 8, 10), LocalDateTime.of(2000, 1, 1, 9, 40));
        assertThat(between.toHours()).isEqualTo(1);
        assertThat(between.toMinutes()).isEqualTo(90);
        Duration days = Duration.ofDays(1);
        assertThat(days.toHours()).isEqualTo(24);
    }
}

