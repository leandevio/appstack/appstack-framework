package io.leandev.appstack.html;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSRuleList;
import org.w3c.dom.css.CSSStyleSheet;

import static org.testng.Assert.*;

import static org.assertj.core.api.Assertions.*;

public class CssParserTest {
    @BeforeMethod
    public void setUp() {
    }

    @Test
    public void testParse() throws Exception {
        CssParser cssParser = new CssParser();
        CSSStyleSheet sheet = cssParser.parse("h1 { background: #ffcc44; }");
        CSSRuleList rules = sheet.getCssRules();
        for (int i = 0; i < rules.getLength(); i++) {
            final CSSRule rule = rules.item(i);

            System.out.println(rule.getCssText());
        }
    }
}