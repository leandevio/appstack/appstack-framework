package io.leandev.appstack.http;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class HttpTest {

    private final Path OUTPUT =  Paths.get(System.getProperty("user.home")).resolve("tmp");
    private final String USER_AGENT = "Mozilla/5.0";

    @BeforeTest
    public void setUp() throws Exception {

    }

    @Test
    public void testGet() throws Exception {

        URL url = new URL("https://www.baeldung.com/java-http-request");
        HttpURLConnection http = (HttpURLConnection) url.openConnection();

        http.setRequestMethod("GET");
        http.setRequestProperty("User-Agent", USER_AGENT);

        receiveText(http);

    }

    @Test
    public void testAuthGet() throws Exception {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("certificateNo", "3422");
        parameters.put("url", "http://www.leandev.io");
        parameters.put("president", "連錦漳");
        parameters.put("accreditDate", "2018-07-12T08:00:00.000+08");
        parameters.put("name", "測試領域_中.docx");

        String queryString = getParamsString(parameters);

        URL url = new URL("http://localhost:4000/report/template?" + queryString);
        HttpURLConnection http = (HttpURLConnection) url.openConnection();

        http.setRequestMethod("GET");
        http.setRequestProperty("User-Agent", USER_AGENT);
        String userCredentials = "admin:admin";
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));
        http.setRequestProperty ("Authorization", basicAuth);

        receiveFile(http);

    }

    private void receiveText(HttpURLConnection http) throws Exception {
        int responseCode = http.getResponseCode();

        System.out.println("\nSending 'GET' request to URL : " + http.getURL());
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(http.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());
    }

    private void receiveFile(HttpURLConnection http) throws Exception {
        int responseCode = http.getResponseCode();

        System.out.println("\nSending 'GET' request to URL : " + http.getURL());
        System.out.println("Response Code : " + responseCode);

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        InputStream input = http.getInputStream();

        byte[] buffer = new byte[1024];
        int n;
        while ((n = input.read(buffer)) != -1) {
            output.write(buffer, 0, n);
        }
        output.close();

        input.close();
        http.disconnect();

        OutputStream outStream = new FileOutputStream(OUTPUT.resolve("aaa.docx").toFile());
        outStream.write(output.toByteArray());

        outStream.close();
    }

    public String getParamsString(Map<String, String> params)
            throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            result.append("&");
        }

        String resultString = result.toString();
        return resultString.length() > 0
                ? resultString.substring(0, resultString.length() - 1)
                : resultString;
    }
}
