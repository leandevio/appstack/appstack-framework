package io.leandev.appstack.concurrent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

public class Worker implements Runnable {
    String name;
    ReentrantLock lock;

    public Worker(ReentrantLock lock, String name)
    {
        this.lock = lock;
        this.name = name;
    }

    public void run()
    {
        boolean done = false;
        while (!done) {
            // acquire a outer Lock
            this.log("Acquiring a outer lock....");
            if(lock.tryLock()) {
                try {
                    this.log("A outer lock acquired, doing outer work...");
                    Thread.sleep(3000);
                    // acquire a inner Lock
                    this.log("Acquiring a inner lock....");
                    lock.lock();
                    try {
                        this.log("A inner lock acquired, doing inner work...");
                        Thread.sleep(3000);
                    } catch(InterruptedException e) {
                        e.printStackTrace();
                    }
                    finally {
                        // release the inner lock.
                        this.log("Releasing the inner lock...");
                        lock.unlock();
                    }
                    this.log("work done.");
                    done = true;
                } catch(InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    // release outer lock
                    this.log("Releasing the outer lock...");
                    lock.unlock();
                }
            } else {
                this.log("waiting for lock...");
                try {
                    Thread.sleep(1000);
                } catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void log(String message) {
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
        System.out.println(String.format("Task name - %s @ %s: %s\n - Lock Hold Count: %d", this.name, formatter.format(new Date()), message, this.lock.getHoldCount()));
    }
}
