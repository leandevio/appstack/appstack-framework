package io.leandev.appstack.scheduling;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.TimeZone;

public class SchedulerTest {
    private Scheduler scheduler;

    @BeforeTest
    public void setUp() throws Exception {
        scheduler = new Scheduler();
        scheduler.start();
    }

    @Test
    public void testCronSchedule() throws SchedulerException, ParseException, InterruptedException {
        Schedule schedule = CronScheduleBuilder.newSchedule("0/10 * * * * ?").forJob(MyJob.class)
                .usingData("baseline", LocalDate.of(2019, 02, 12))
                .withIdentity("mySchedule", "cronGroup")
                .inTimeZone(TimeZone.getDefault())
                .build();

        scheduler.arrange(schedule);

        Thread.sleep(15*1000);
        schedule = scheduler.get("mySchedule", "cronGroup");
        System.out.println(String.format("previous fire time: %s", schedule.previousFireTime()));
        System.out.println(String.format("next fire time: %s", schedule.nextFireTime()));
        Thread.sleep(15*1000);
    }

    @Test
    public void testSimpleSchedule() throws SchedulerException, InterruptedException {
        Schedule schedule = SimpleScheduleBuilder.newSchedule().forJob(MyJob.class)
                .withIdentity("mySchedule", "simpleGroup")
                .usingData("baseline", LocalDate.of(2019, 02, 12))
                .withIntervalInSeconds(10)
                .repeatForever()
                .startNow()
                .build();
        scheduler.arrange(schedule);
        Thread.sleep(15*1000);
        schedule = scheduler.get("mySchedule", "simpleGroup");
        System.out.println(String.format("previous fire time: %s", schedule.previousFireTime()));
        System.out.println(String.format("next fire time: %s", schedule.nextFireTime()));
        Thread.sleep(15*1000);
    }

    @AfterTest
    public void tearDown() throws Exception {
        scheduler.shutdown();
    }

}