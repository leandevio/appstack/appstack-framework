package io.leandev.appstack.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.leandev.appstack.util.Model;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

import static org.assertj.core.api.Assertions.*;

public class JsonParserTest {
    private Order order;
    private Date now = new Date();
    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeMethod
    public void setUp() {
        order = new Order();

        order.setBuyer("Jason");
        order.setOrderDate(now);
        order.setAmount(new BigDecimal(5000));

        order.getItems().add(new OrderItem("orange", 3, new BigDecimal(50)));
        order.getItems().add(new OrderItem("apple", 4, new BigDecimal(100)));
        order.getItems().add(new OrderItem("banana", 5, new BigDecimal(10)));
    }

    @Test
    public void testSerializer() throws Exception {
        JsonParser jsonParser = new JsonParser();

        jsonParser.register(Order.class, new OrderSerializer());

        String json = jsonParser.writeValue(order);

        Model model = objectMapper.readValue(json, Model.class);

        assertThat(model).containsKeys("customer", "orderItems").doesNotContainKeys("buyer", "item");
        assertThat(model.getAsString("customer")).isEqualTo(order.getBuyer());
        assertThat(model.getAsBigDecimal("amount")).isEqualTo(order.getAmount());
        assertThat(model.getAsDate("orderDate")).isEqualTo(order.getOrderDate());
        List<Map<String, Object>> orderItems = (List<Map<String, Object>>) model.getAsList("orderItems");
        for(int i=0; i<orderItems.size(); i++) {
            Model orderItem = Model.of(orderItems.get(i));
            OrderItem item = order.getItems().get(i);

            assertThat(orderItem.getAsString("product")).isEqualTo(item.getProduct());
            assertThat(orderItem.getAsInteger("quantity")).isEqualTo(item.getQuantity());
            assertThat(orderItem.getAsBigDecimal("price")).isEqualTo(item.getPrice());
        }
    }

    @Test
    public void testDeserializer() throws Exception {
        JsonParser jsonParser = new JsonParser();

        jsonParser.register(Order.class, new OrderSerializer());

        String json = jsonParser.writeValue(order);

        jsonParser.register(Order.class, new OrderDeserializer());

        Order order = jsonParser.readValue(json, Order.class);

        assertThat(order.getBuyer()).isEqualTo(this.order.getBuyer());
        assertThat(order.getAmount()).isEqualTo(this.order.getAmount());
        assertThat(order.getOrderDate()).isEqualTo(this.order.getOrderDate());

        for(int i=0; i<order.getItems().size(); i++) {
            assertThat(order.getItems().get(i).getProduct()).isEqualTo(this.order.getItems().get(i).getProduct());
            assertThat(order.getItems().get(i).getQuantity()).isEqualTo(this.order.getItems().get(i).getQuantity());
            assertThat(order.getItems().get(i).getPrice()).isEqualTo(this.order.getItems().get(i).getPrice());
        }
    }
}