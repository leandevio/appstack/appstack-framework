package io.leandev.appstack.csv;

import com.univocity.parsers.csv.CsvParserSettings;
import io.leandev.appstack.csv.CsvParser;
import io.leandev.appstack.parser.Record;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import static org.testng.Assert.*;

public class CsvParserTest {
    @Test
    public void testUniVocityCsvParser() throws IOException {
        InputStream stream = CsvParserTest.class.getResourceAsStream("/Product.csv");
        CsvParserSettings settings = new CsvParserSettings();
        settings.setHeaderExtractionEnabled(true);
        // settings.getFormat().setLineSeparator("\n");
        // creates a CSV parser
        com.univocity.parsers.csv.CsvParser parser = new com.univocity.parsers.csv.CsvParser(settings);
        for(com.univocity.parsers.common.record.Record record : parser.iterateRecords(stream)){
            record.getValues();
        }
    }

    @Test
    public void testIterator() throws IOException {
        InputStream in = CsvParserTest.class.getResourceAsStream("/Product.csv");
        CsvParser parser = new CsvParser();
        for(Record record : parser.iterate(in)) {
            assertNotNull(record.getString(0));
            assertNotNull(record.getString(1));
            assertNotNull(record.getString(2));
            assertNotNull(record.getDouble(3));
            assertNotNull(record.getDate(4));
        }
        in.close();
    }

    @Test
    public void testStream() throws IOException {
        InputStream in = CsvParserTest.class.getResourceAsStream("/Product.csv");
        CsvParser parser = new CsvParser();
        parser.stream(in).forEach(record -> {
            System.out.println(Arrays.toString(record.getValues().toArray()));
        });
        in.close();
    }

}