package io.leandev.appstack.docx;

import io.leandev.appstack.util.Environment;
import org.apache.poi.util.Units;
import org.apache.poi.wp.usermodel.HeaderFooterType;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGraphicalObject;
import org.openxmlformats.schemas.drawingml.x2006.wordprocessingDrawing.CTAnchor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDrawing;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;

public class XWPFTest {
    private final Path RESOURCE = Paths.get(XWPFTest.class.getPackage().getName().replaceAll("\\.", "/"));
    private final Path OUTPUT = Environment.getDefaultInstance().tmpFolder();
    private final ClassLoader LOADER = XWPFTest.class.getClassLoader();

    @BeforeMethod
    public void setUp() {
    }

    @Test
    public void testCreateDocument() throws Exception {
        Path TARGETFILE = OUTPUT.resolve("simple.docx");
        //Blank Document
        XWPFDocument document = new XWPFDocument();

        //Write the Document in file system
        FileOutputStream out = new FileOutputStream(TARGETFILE.toFile());
        //create Paragraph
        XWPFParagraph paragraph = document.createParagraph();
        //Set bottom border to paragraph
        paragraph.setBorderBottom(Borders.BASIC_BLACK_DASHES);
        //Set left border to paragraph
        paragraph.setBorderLeft(Borders.BASIC_BLACK_DASHES);
        //Set right border to paragraph
        paragraph.setBorderRight(Borders.BASIC_BLACK_DASHES);
        //Set top border to paragraph
        paragraph.setBorderTop(Borders.BASIC_BLACK_DASHES);

        //Set Bold an Italic
        XWPFRun paragraphOneRunOne = paragraph.createRun();
        paragraphOneRunOne.setBold(true);
        paragraphOneRunOne.setItalic(true);
        paragraphOneRunOne.setText("Font Style");
        paragraphOneRunOne.addBreak();

        //Set text Position
        XWPFRun paragraphOneRunTwo = paragraph.createRun();
        paragraphOneRunTwo.setText("Font Style two");
        paragraphOneRunTwo.setTextPosition(100);

        //Set Strike through and Font Size and Subscript
        XWPFRun paragraphOneRunThree = paragraph.createRun();
        paragraphOneRunThree.setStrikeThrough(true);
        paragraphOneRunThree.setFontSize(20);
        paragraphOneRunThree.setSubscript(VerticalAlign.SUBSCRIPT);
        paragraphOneRunThree.setText(" Different Font Styles");

        //create table
        XWPFTable table = document.createTable();

        //create first row
        XWPFTableRow tableRowOne = table.getRow(0);
        tableRowOne.getCell(0).setText("col one, row one");
        tableRowOne.addNewTableCell().setText("col two, row one");
        tableRowOne.addNewTableCell().setText("col three, row one");

        //create second row
        XWPFTableRow tableRowTwo = table.createRow();
        tableRowTwo.getCell(0).setText("col one, row two");
        tableRowTwo.getCell(1).setText("col two, row two");
        tableRowTwo.getCell(2).setText("col three, row two");

        //create third row
        XWPFTableRow tableRowThree = table.createRow();
        tableRowThree.getCell(0).setText("col one, row three");
        tableRowThree.getCell(1).setText("col two, row three");
        tableRowThree.getCell(2).setText("col three, row three");

        document.write(out);

        out.close();
        System.out.println("simple.docx written successfully");
    }

    @Test
    public void testExport() throws Exception {
        Path TARGETFILE = OUTPUT.resolve("style.docx");
        InputStream template = XWPFTest.class.getResourceAsStream(RESOURCE.resolve("template.docx").toString());
        FileOutputStream out = new FileOutputStream( TARGETFILE.toFile());

        XWPFDocument document = new XWPFDocument(template);
//        PdfOptions options = PdfOptions.create();
//        PdfConverter converter = (PdfConverter)PdfConverter.getInstance();
//        converter.convert(document, new FileOutputStream("XWPFToPDFConverterSampleMin.pdf"), options);

        document.close();
    }

    @Test
    public void testStyle() throws Exception {
        Path TARGETFILE = OUTPUT.resolve("style.docx");
        InputStream template = LOADER.getResourceAsStream(RESOURCE.resolve("template.docx").toString());
        //Blank Document
        XWPFDocument document = new XWPFDocument(template);

        //Write the Document in file system
        FileOutputStream out = new FileOutputStream( TARGETFILE.toFile());
        //create Paragraph
        XWPFParagraph heading = document.createParagraph();
        heading.setStyle("Heading3");

        //Set Bold an Italic
        XWPFRun headingText = heading.createRun();
        headingText.setText("說明");

        XWPFParagraph body = document.createParagraph();
        XWPFRun bodyText = body.createRun();
        bodyText.setText("這是說明段落的本文 ....");


        document.write(out);
        out.close();
        System.out.println("style.docx written successfully");
    }

    @Test
    public void testBookmark() throws Exception {
        Path TARGETFILE = OUTPUT.resolve("bookmark.docx");
        InputStream template = LOADER.getResourceAsStream(RESOURCE.resolve("template.docx").toString());

        XWPFDocument document = new XWPFDocument(template);
        FileOutputStream out = new FileOutputStream( TARGETFILE.toFile());

        List<XWPFParagraph> paragraphs = document.getParagraphs();
        for (XWPFParagraph paragraph : paragraphs)
        {
            //Here you have your paragraph;
            CTP ctp = paragraph.getCTP();
            // Get all bookmarks and loop through them
            List<CTBookmark> bookmarks = ctp.getBookmarkStartList();
            for(CTBookmark bookmark : bookmarks)
            {
                if(bookmark.getName().equals("Introduction"))
                {
                    int size = paragraph.getRuns().size();
                    for (int i = 0; i < size; i++) {
                        paragraph.removeRun(0);
                    }
                    XWPFRun run = paragraph.createRun();
                    run.setText("這份測試文件將用來測試如何使用範本產生docx的文件．");
                    paragraph.addRun(run);

                }
            }
        }

        document.write(out);
        out.close();
        System.out.println("style.docx written successfully");
    }

    @Test
    public void testAddPicture() throws Exception {
        Path OUTPUTFILE = OUTPUT.resolve("stamp.docx");
        InputStream template = LOADER.getResourceAsStream(RESOURCE.resolve("template.docx").toString());
        FileOutputStream out = new FileOutputStream(OUTPUTFILE.toFile());
        InputStream stamp = LOADER.getResourceAsStream(RESOURCE.resolve("verified.png").toString());
        sealInWord(template, out, stamp,"測試文件", 3009/10,1184/10,0,0,false);
    }

    /**
     * <b> Add a stamp to Word</b>
     * <br><br>
     * <i>Description</i> :
     * String srcPath, source Word path
     * String storePath, the path after adding the stamp
     * String sealPath, stamp path (namely picture)
     * tString abText, the identification string for stamping in Word, such as: (signature/stamp)
     * int width, stamp width
     * int height, stamp height
     * int leftOffset, the offset of the stamp to the left in the editing paragraph
     * int topOffset, the offset of the stamp in the editing paragraph
     * boolean behind, whether the stamp is under the text
     * @return void
     * <br><br>Date: 2019/12/26 15:12     <br>Author : dxl
     */
    public void sealInWord(InputStream input, OutputStream output, InputStream stamp, String tabText,
                           int width, int height, int leftOffset, int topOffset, boolean behind) throws Exception {
        XWPFDocument document = new XWPFDocument(input);
        List<XWPFParagraph> paragraphs = document.getParagraphs();
        XWPFRun targetRun = null;
        for(XWPFParagraph  paragraph : paragraphs){
            if(!"".equals(paragraph.getText()) && paragraph.getText().contains(tabText)){
                List<XWPFRun> runs = paragraph.getRuns();
                targetRun = runs.get(runs.size()-1);
            }
        }
        if(targetRun != null){
            if(width <= 0){
                width = 100;
            }
            if(height <= 0){
                height = 100;
            }
            //Create Random class object
            Random random = new Random();
            //Generate random numbers
            int number = random.nextInt(999) + 1;
            targetRun.addPicture(stamp, Document.PICTURE_TYPE_PNG, "Seal"+number, Units.toEMU(width), Units.toEMU(height));
            // 2. Get image data
            CTDrawing drawing = targetRun.getCTR().getDrawingArray(0);
            CTGraphicalObject graphicalobject = drawing.getInlineArray(0).getGraphic();

            //Replace the newly inserted picture, add CTAnchor, set floating attribute, delete inline attribute
            CTAnchor anchor = this.getAnchorWithGraphic(graphicalobject, "Seal"+number,
                    Units.toEMU(width), Units.toEMU(height),//picture size
                    Units.toEMU(leftOffset), Units.toEMU(topOffset), behind);//The left offset of the existing content of the paragraph needs to be calculated relative to the current paragraph position
            drawing.setAnchorArray(new CTAnchor[]{anchor});//Add floating attribute
            drawing.removeInline(0);//Remove inline attributes
        }
        document.write(output);
        document.close();
    }


    public CTAnchor getAnchorWithGraphic(CTGraphicalObject ctGraphicalObject,
                                         String deskFileName, int width, int height,
                                         int leftOffset, int topOffset, boolean behind) {
        System.out.println(">>width>>"+width+"; >>height>>>>"+height);
        String anchorXML =
                "<wp:anchor xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\" "
                        + "simplePos=\"0\" relativeHeight=\"0\" behindDoc=\"" + ((behind) ? 1 : 0) + "\" locked=\"0\" layoutInCell=\"1\" allowOverlap=\"1\">"
                        + "<wp:simplePos x=\"0\" y=\"0\"/>"
                        + "<wp:positionH relativeFrom=\"column\">"
                        + "<wp:posOffset>" + leftOffset + "</wp:posOffset>"
                        + "</wp:positionH>"
                        + "<wp:positionV relativeFrom=\"paragraph\">"
                        + "<wp:posOffset>" + topOffset + "</wp:posOffset>" +
                        "</wp:positionV>"
                        + "<wp:extent cx=\"" + width + "\" cy=\"" + height + "\"/>"
                        + "<wp:effectExtent l=\"0\" t=\"0\" r=\"0\" b=\"0\"/>"
                        + "<wp:wrapNone/>"
                        + "<wp:docPr id=\"1\" name=\"Drawing 0\" descr=\"" + deskFileName + "\"/><wp:cNvGraphicFramePr/>"
                        + "</wp:anchor>";

        CTDrawing drawing = null;
        try {
            drawing = CTDrawing.Factory.parse(anchorXML);
        } catch (XmlException e) {
            e.printStackTrace();
        }
        CTAnchor anchor = drawing.getAnchorArray(0);
        anchor.setGraphic(ctGraphicalObject);
        return anchor;
    }

    @Test
    public void testAddHeaderFooter() throws Exception {
        Path OUTPUTFILE = OUTPUT.resolve("WordWithHeaderFooter.docx");
        InputStream template = LOADER.getResourceAsStream(RESOURCE.resolve("測試領域_中.docx").toString());
        FileOutputStream out = new FileOutputStream(OUTPUTFILE.toFile());
        InputStream logo = LOADER.getResourceAsStream(RESOURCE.resolve("verified.png").toString());

        XWPFDocument doc = new XWPFDocument(template);

        // the body content
        XWPFParagraph paragraph = doc.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setText("The Body...");

//        // create header
//        XWPFHeader header = doc.createHeader(HeaderFooterType.DEFAULT);
//
//        // header's first paragraph
//        XWPFParagraph headerParagraph = header.getParagraphArray(0);
//        if (headerParagraph == null) headerParagraph = header.createParagraph();
//        headerParagraph.setAlignment(ParagraphAlignment.CENTER);
//
//        XWPFRun headerText = headerParagraph.createRun();
//
//        headerText.setText("HEADER");

        // create footer
        XWPFFooter footer = doc.createFooter(HeaderFooterType.DEFAULT);
        XWPFParagraph footerParagraph = footer.getParagraphArray(0);
        if (footerParagraph == null) footerParagraph = footer.createParagraph();
        XWPFRun footerText = footerParagraph.createRun();

        int width = 100, height = 50;
        int leftOffset = -100, topOffset = -100;
        boolean behind = false;

        footerText.addPicture(logo, Document.PICTURE_TYPE_JPEG, "LeanDev Logo", Units.toEMU(width), Units.toEMU(height));
        logo.close();

        // 2. Get image data
        CTDrawing drawing = footerText.getCTR().getDrawingArray(0);
        CTGraphicalObject graphicalobject = drawing.getInlineArray(0).getGraphic();

        //Replace the newly inserted picture, add CTAnchor, set floating attribute, delete inline attribute
        CTAnchor anchor = this.getAnchorWithGraphic(graphicalobject, "Logo",
                Units.toEMU(width), Units.toEMU(height),//picture size
                Units.toEMU(leftOffset), Units.toEMU(topOffset), behind);//The left offset of the existing content of the paragraph needs to be calculated relative to the current paragraph position
        drawing.setAnchorArray(new CTAnchor[]{anchor});//Add floating attribute
        drawing.removeInline(0);//Remove inline attributes

        footerText.setText("Footer");

        doc.write(out);
        doc.close();
        out.close();
    }

}
