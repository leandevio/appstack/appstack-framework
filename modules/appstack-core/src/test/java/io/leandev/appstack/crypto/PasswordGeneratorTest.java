package io.leandev.appstack.crypto;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class PasswordGeneratorTest {
    @Test
    public void testGeneratorPassword() {
        PasswordGenerator passwordGenerator = new PasswordGenerator();
        passwordGenerator.setLength(6);
        passwordGenerator.setAllowSymbol(false);
        String password = passwordGenerator.generate();
        assertNotEquals(password.toLowerCase(), password.toUpperCase());
    }

}