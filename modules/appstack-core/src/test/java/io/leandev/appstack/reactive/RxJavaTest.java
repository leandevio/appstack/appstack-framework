package io.leandev.appstack.reactive;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class RxJavaTest {
    @Test
    public void testObservableUsingTestObserver() throws Exception {
        List<String> letters = Arrays.asList("A", "B", "C", "D", "E");
        TestObserver<String> subscriber = new TestObserver<>();

        Observable<String> observable = Observable
                .fromIterable(letters)
                .zipWith(
                        Observable.range(1, Integer.MAX_VALUE),
                        ((string, index) -> index + "-" + string));

        observable.subscribe(subscriber);

        subscriber.assertComplete();
        subscriber.assertNoErrors();
        subscriber.assertValueCount(5);
        subscriber.assertResult("1-A", "2-B", "3-C", "4-D", "5-E");

    }

    @Test
    public void testObservable() throws Exception {
        List<String> letters = Arrays.asList("A", "B", "C", "D", "E");
        List<String> results = new ArrayList<>();
        Observable<String> observable = Observable
                .fromIterable(letters)
                .zipWith(
                        Observable.range(1, Integer.MAX_VALUE),
                        (string, index) -> index + "-" + string);

        observable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(5));
        assertThat(results, hasItems("1-A", "2-B", "3-C", "4-D", "5-E"));
    }
}
