package io.leandev.appstack.image;

import io.leandev.appstack.font.FontBuilder;
import io.leandev.appstack.util.Environment;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

public class ImageTest {
    private static final String CERTIFICATE = "certificate.png";
    private static final String STAMP = "stamp.png";
    private static Environment environment = Environment.getDefaultInstance();
    private static final Class CLASS = ImageTest.class;
    private static final ClassLoader CLASSLOADER = ImageTest.class.getClassLoader();
    private static final Path RESOURCES = Paths.get(CLASS.getPackage().getName().replaceAll("\\.", "/"));
    private static final Path OUTPUT = environment.tmpFolder().resolve(CLASS.getSimpleName());

    @BeforeTest
    static void setup() throws IOException {
        if(!Files.exists(OUTPUT)) {
            Files.createDirectory(OUTPUT);
        }
    }


    @Test
    public void testCanvas() throws Exception {
        BufferedImage certificate = ImageReader.getBufferedImage(ImageTest.class.getResourceAsStream("/" + CERTIFICATE));
        BufferedImage stamp = ImageReader.getBufferedImage(ImageTest.class.getResourceAsStream("/" + STAMP));
        Canvas canvas = new Canvas(certificate);

        canvas.setColor(new Color(255, 0, 0));
        Rectangle rect = new Rectangle(1100, 600, 450, 300);
        canvas.drawRect(rect);
        canvas.drawImage(stamp, rect, Scaling.CONTAIN);

        canvas.setColor(new Color(70, 87, 111));
        FontBuilder fontBuilder = new FontBuilder();
        Font font = fontBuilder.withLocale(Locale.TRADITIONAL_CHINESE).withStyle(Font.ITALIC).ofSize(64).build();
        canvas.setFont(font);
        rect = new Rectangle(680, 560, 410, 90);
        canvas.drawRect(rect);
        canvas.drawText("麗得科技", rect);

        canvas.setColor(new Color(163, 63, 0));
        // use the font installed at the OS.
        canvas.setFont(new Font("Snell Roundhand", Font.ITALIC, 32));
        rect = new Rectangle(300, 910, 350, 60);
        canvas.drawRect(rect);
        canvas.drawText("March 18, 2019", rect);

        OutputStream output = new FileOutputStream(environment.tmpFolder().resolve(CERTIFICATE).toFile());
        ImageWriter writer = new ImageWriter(output);
        writer.write(canvas.getBufferedImage());
        writer.close();
    }

    @Test
    public void testDrawImage() throws Exception {
        BufferedImage certificate = ImageReader.getBufferedImage(ImageTest.class.getResourceAsStream("/" + CERTIFICATE));
        BufferedImage stamp = ImageReader.getBufferedImage(ImageTest.class.getResourceAsStream("/" + STAMP));
        Canvas canvas = new Canvas(certificate);

        canvas.setColor(new Color(255, 0, 0));
        Rectangle rect = new Rectangle(1100, 600, 450, 300);
        canvas.drawRect(rect);
        canvas.drawImage(stamp, rect);

        OutputStream output = new FileOutputStream(environment.tmpFolder().resolve(CERTIFICATE).toFile());
        ImageWriter writer = new ImageWriter(output);
        writer.write(canvas.getBufferedImage());
        writer.close();
    }

    @Test
    public void testDrawImageContain() throws Exception {
        BufferedImage certificate = ImageReader.getBufferedImage(ImageTest.class.getResourceAsStream("/" + CERTIFICATE));
        BufferedImage stamp = ImageReader.getBufferedImage(ImageTest.class.getResourceAsStream("/" + STAMP));
        Canvas canvas = new Canvas(certificate);

        canvas.setColor(new Color(255, 0, 0));
        Rectangle rect = new Rectangle(1100, 600, 450, 300);
        canvas.drawRect(rect);
        canvas.drawImage(stamp, rect, Scaling.CONTAIN);

        OutputStream output = new FileOutputStream(environment.tmpFolder().resolve(CERTIFICATE).toFile());
        ImageWriter writer = new ImageWriter(output);
        writer.write(canvas.getBufferedImage());
        writer.close();
    }

    @Test
    public void testLoadCMYKJPEGImage() throws Exception {
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCES.resolve("cmykjpeg.jpg").toString());
            OutputStream outputStream = Files.newOutputStream(OUTPUT.resolve("cmykjpeg.png"))) {
            BufferedImage bufferedImage = ImageReader.getBufferedImage(inputStream);
            ImageWriter.writeBufferedImage(outputStream, bufferedImage);
        }
    }
}