package io.leandev.appstack.doc.delta;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.*;

public class DeltaParserTest {
    private static final DeltaParser DELTA_PARSER = new DeltaParser();

    @BeforeMethod
    public void setUp() {
    }

    @Test
    public void testParse() throws IOException {
        Path path = Paths.get("/").resolve(DeltaParserTest.class.getPackage().getName().replaceAll("\\.", "/"));
        InputStream inputStream = DeltaParserTest.class.getResourceAsStream(path.resolve("delta.json").toString());

        Delta delta = DELTA_PARSER.parse(inputStream);

        inputStream.close();

        assertThat(delta.getOperations()).hasSize(44);
    }
}