package io.leandev.appstack.crypto;

import io.leandev.appstack.util.Environment;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.testng.Assert.*;

public class EncryptorTest {
    private final Class clazz = this.getClass();
    private final Path RESOURCE = Paths.get(clazz.getPackage().getName().replaceAll("\\.", "/"));
    private final Path OUTPUT = Environment.getDefaultInstance().tmpFolder();
    private final ClassLoader LOADER = clazz.getClassLoader();

    @Test
    public void testAESBCEEncrypt() throws Throwable {
        String plainText = "This message is encrypted by AES BCE.";
        Encryptor encryptor = new AESCBCEncryptor();
        String cipherText = encryptor.encrypt(plainText);
        assertEquals(encryptor.decrypt(cipherText), plainText);
    }

    @Test
    public void testAESBCEEncryptWithPassword() throws Throwable {
        String plainText = "This message is encrypted by AES BCE.";
        String password = "my secret";
        Encryptor encryptor = new AESCBCEncryptor(password);
        String cipherText = encryptor.encrypt(plainText);

        Encryptor decryptor = new AESCBCEncryptor(password);
        assertEquals(decryptor.decrypt(cipherText), plainText);
    }

    @Test
    public void testAESBCEEncryptFileWithPassword() throws Throwable {
        String password = "my secret";
        InputStream source = LOADER.getResourceAsStream(RESOURCE.resolve("logo.png").toString());
        byte[] data = new byte[source.available()];
        source.read(data);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Encryptor encryptor = new AESCBCEncryptor(password);
        encryptor.encrypt(inputStream, outputStream);
        byte[] cipherData = outputStream.toByteArray();
        source.close();
        inputStream.close();
        outputStream.close();

        Encryptor decryptor = new AESCBCEncryptor(password);
        inputStream = new ByteArrayInputStream(cipherData);
        outputStream = new ByteArrayOutputStream();
        decryptor.decrypt(inputStream, outputStream);

        assertEquals(outputStream.toByteArray(), data);

        inputStream.close();
        outputStream.close();
    }

    @Test
    public void testAESGCMEncrypt() throws Throwable {
        String plainText = "This message is encrypted by AES GCM.";
        Encryptor encryptor = new AESGCMEncryptor();
        String cipherText = encryptor.encrypt(plainText);
        assertEquals(encryptor.decrypt(cipherText), plainText);
    }

    @Test
    public void testAESGCMEncryptWithPassword() throws Throwable {
        String plainText = "This message is encrypted by AES GCM.";
        String password = "my secret";
        Encryptor encryptor = new AESGCMEncryptor(password);
        String cipherText = encryptor.encrypt(plainText);

        Encryptor decryptor = new AESGCMEncryptor(password);
        assertEquals(decryptor.decrypt(cipherText), plainText);
    }

    @Test
    public void testAESGCMEncryptFileWithPassword() throws Throwable {
        String password = "my secret";
        InputStream source = LOADER.getResourceAsStream(RESOURCE.resolve("logo.png").toString());
        byte[] data = new byte[source.available()];
        source.read(data);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Encryptor encryptor = new AESGCMEncryptor(password);
        encryptor.encrypt(inputStream, outputStream);
        byte[] cipherData = outputStream.toByteArray();
        source.close();
        inputStream.close();
        outputStream.close();

        Encryptor decryptor = new AESGCMEncryptor(password);
        inputStream = new ByteArrayInputStream(cipherData);
        outputStream = new ByteArrayOutputStream();
        decryptor.decrypt(inputStream, outputStream);

        assertEquals(outputStream.toByteArray(), data);

        inputStream.close();
        outputStream.close();
    }
}