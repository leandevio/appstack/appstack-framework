package io.leandev.appstack.xml;

import io.leandev.appstack.env.Environ;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class XPathTest {
    private static final Class CLASS = XPathTest.class;
    private static final ClassLoader CLASSLOADER = CLASS.getClassLoader();
    private static final Path RESOURCES = Paths.get(CLASS.getPackage().getName().replaceAll("\\.", "/"));
    private static final Path OUTPUT = Environ.getInstance().tmpFolder().resolve(CLASS.getSimpleName());

    @BeforeAll
    static void setup() throws IOException {
        if(!Files.exists(OUTPUT)) {
            Files.createDirectory(OUTPUT);
        }
    }

    @Test
    public void testQuery() throws Exception {
        String XML = "Product.xml";
        Document document;
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCES.resolve(XML).toString())) {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(inputStream);
        }
        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = "/products/product[@id='Yellow Green Christmas Gift Box']";

        NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(
                document, XPathConstants.NODESET);
        Node node = nodeList.item(0);
        assertThat(node.getNodeType()).isEqualTo(Node.ELEMENT_NODE);
        Element product = (Element) node;

        assertThat(product.getAttribute("id")).isEqualTo("Yellow Green Christmas Gift Box");
        Element name = (Element) product.getElementsByTagName("name").item(0);
        assertThat(name.getTextContent()).isEqualTo("黃綠色耶誕禮盒");
        Element description = (Element) product.getElementsByTagName("description").item(0);
        CDATASection cdataSection = (CDATASection) description.getFirstChild();
        assertThat(cdataSection.getTextContent()).isEqualTo("活潑亮眼聖誕佳節賀禮");
        Element price = (Element) product.getElementsByTagName("price").item(0);
        assertThat(price.getTextContent()).isEqualTo("3000");
        Element creationTime = (Element) product.getElementsByTagName("creationTime").item(0);
        assertThat(creationTime.getTextContent()).isEqualTo("2018-07-12T00:00:00.000Z");
    }

    @Test
    public void testModify() throws Exception {
        String XML = "Product.xml";
        Document document;
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCES.resolve(XML).toString())) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(inputStream);
        }
        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = "/products/product[@id='Yellow Green Christmas Gift Box']";
        NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(
                document, XPathConstants.NODESET);
        Element product = (Element) nodeList.item(0);

        String now = Instant.now().toString();
        Element price = (Element) product.getElementsByTagName("price").item(0);
        price.setTextContent("4000");
        Element creationTime = (Element) product.getElementsByTagName("creationTime").item(0);
        Element modificationTime = document.createElement("modificationTime");
        modificationTime.setTextContent(now);
        product.insertBefore(modificationTime, creationTime);


        expression = "/products/product[@id='Yellow Green Christmas Gift Box']/price";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                document, XPathConstants.NODESET);
        Element currentPrice = (Element) nodeList.item(0);
        assertThat(currentPrice.getTextContent()).isEqualTo("4000");


        expression = "/products/product[@id='Yellow Green Christmas Gift Box']/modificationTime";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                document, XPathConstants.NODESET);
        Element currentModificationTime = (Element) nodeList.item(0);
        assertThat(currentModificationTime.getTextContent()).isEqualTo(now);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        try(OutputStream output = Files.newOutputStream(OUTPUT.resolve(XML))) {
            StreamResult result = new StreamResult(output);
            transformer.transform(source, result);
        }
    }
}
