package io.leandev.appstack.text;

import io.leandev.appstack.parser.Record;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.testng.Assert.*;

public class TxtParserTest {
    private static String DATAFILE = "Product.txt";
    private Path DATAPATH = Paths.get("/").resolve(this.getClass().getPackage().getName().replaceAll("\\.", "/")).resolve(DATAFILE);

    @BeforeMethod
    public void setUp() {
    }

    @Test
    public void testIterator() throws IOException {
        InputStream in = TxtParserTest.class.getResourceAsStream(DATAPATH.toString());
        TxtParser parser = TxtParserBuilder.newTxtParser()
                .withColumnWidths(31, 6, 24) // 欄位長度定義
                .withPadding('_') // 指定填補空白的字元
                .build();
        for(Record record : parser.iterate(in)) {
            assertNotNull(record.getString(0));
            assertNotNull(record.getDouble(1));
            assertNotNull(record.getDate(2));
        }
        in.close();
    }

    @Test
    public void testStream() throws IOException {
        InputStream in = TxtParserTest.class.getResourceAsStream(DATAPATH.toString());
        TxtParser parser = TxtParserBuilder.newTxtParser()
                .withColumnWidths(31, 6, 24) // 欄位長度定義
                .withPadding('_') // 指定填補空白的字元
                .build();
        parser.stream(in).forEach(record -> {
            System.out.println(Arrays.toString(record.getValues().toArray()));
        });
        in.close();
    }
}