package io.leandev.appstack.mail;

import io.leandev.appstack.client.ClientAccessToken;
import io.leandev.appstack.client.ClientAgent;
import io.leandev.appstack.env.Environ;
import io.leandev.appstack.microsoft.MicrosoftMailCredential;
import javax.mail.Message;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import io.leandev.appstack.microsoft.MicrosoftMailAgent;
import org.junit.jupiter.api.Test;

public class MailboxTest {
    private final Environ environ = Environ.getInstance();
    private final Path RESOURCE = Paths.get("/").resolve(this.getClass().getPackage().getName().replaceAll("\\.", "/"));
    private final Path OUTPUT = environ.tmpFolder();
//    private final String HOSTNAME = "imap.gmail.com";
    private final String HOSTNAME = "ellen.leandev.com.tw";
    private final int PORT = 993;
//    private final String USERNAME = "jaysenlynn@gmail.com";
    private final String USERNAME = environ.property("ellen.username");
    private final String PASSWORD = environ.property("ellen.password");


    @Test
    public void testConnectOffice365() throws Exception {
        String tenantId = environ.property("office365.tenantId");
        String clientId = environ.property("office365.clientId");
        String clientSecret = environ.property("office365.clientSecret");
        String username = environ.property("office365.username");

        // leandev.io
        tenantId = "d53d1738-1031-480b-9f4e-9b1dc0d3d2a0";
        clientId = "067b5b28-86db-477c-8bc2-4d4e0fdde75a";
        clientSecret = "M4W8Q~VcFXkBTlp33o2.QQWXgmRqszEWVfbS_bZD";
        username = "notif@leandev.io";

        // talentonline.io
        tenantId = "7ea9a746-4dcd-446d-90f1-e73085b57373";
        clientId = "644fe2b7-2ae6-4ea8-a78d-b027ccee52ce";
        clientSecret = "Hya8Q~clSFmevfpnhvYUutTwH-UEDRXseiQvhbs0";
        username = "recruiting@talentonline.io";

        MicrosoftMailCredential clientCredential = MicrosoftMailCredential.builder()
                .tenantId(tenantId)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .build();

        ClientAgent clientAgent = new MicrosoftMailAgent();
        ClientAccessToken clientAccessToken = clientAgent.connect(clientCredential);

        clientAccessToken = clientAgent.acquireToken();

        Mailbox mailbox = MailboxBuilder.newOffice365Mailbox()
                .withDebug(true)
                .build();

        String password = clientAccessToken.getAccessToken();

        mailbox.connect(username, password);

        Folder inbox = mailbox.getInbox();

        inbox.open();

        inbox.close();

        mailbox.close();
    }

    @Test
    public void testConnectGmail() throws Exception {
        final String USERNAME = environ.property("gmail.username");
        final String PASSWORD = environ.property("gmail.password");
        Mailbox mailbox = MailboxBuilder.newGMailMailbox()
                .withDebug(true)
                .build();

        mailbox.connect(USERNAME, PASSWORD);

        Folder inbox = mailbox.getInbox();

        inbox.open();

        inbox.close();

        mailbox.close();
    }

    @Test
    public void testFetchEmail() throws Exception {
        String tenantId = environ.property("office365.tenantId");
        String clientId = environ.property("office365.clientId");
        String clientSecret = environ.property("office365.clientSecret");
        String username = environ.property("office365.username");
        // leandev.io
//        tenantId = "d53d1738-1031-480b-9f4e-9b1dc0d3d2a0";
//        clientId = "067b5b28-86db-477c-8bc2-4d4e0fdde75a";
//        clientSecret = "M4W8Q~VcFXkBTlp33o2.QQWXgmRqszEWVfbS_bZD";
//        username = "recruiting@leandev.io";

        // talentonline.io
        tenantId = "7ea9a746-4dcd-446d-90f1-e73085b57373";
        clientId = "644fe2b7-2ae6-4ea8-a78d-b027ccee52ce";
        clientSecret = "Hya8Q~clSFmevfpnhvYUutTwH-UEDRXseiQvhbs0";
        username = "recruiting@talentonline.io";


        // ctci.com
//        tenantId = "69bd7464-f819-45c0-9e64-6fe7f0a3886c";
//        clientId = "03b44f5a-7380-470a-b6bf-9249493b9a10";
//        clientSecret = "ug38Q~1qN_h1JDdDVqDf.ymRLSHxpaqNrkzxeb9A";
//        username = "Resume@meeting.ctci.com";

        MicrosoftMailCredential clientCredential = MicrosoftMailCredential.builder()
                .tenantId(tenantId)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .build();

        ClientAgent clientAgent = new MicrosoftMailAgent();
        ClientAccessToken clientAccessToken = clientAgent.connect(clientCredential);

        // 使用 MailboxBuilder 建立 Mailbox．
        Mailbox mailbox = MailboxBuilder.newOffice365Mailbox()
                .withDebug(true)
                .build();

        // 使用 Mailbox 連接郵件伺服器．
        mailbox.connect(username, clientAccessToken.getAccessToken());

        // 建立可以連接收件匣的 Folder．
        Folder inbox = mailbox.getInbox();

        // 使用 Folder 開啟收件匣．
        inbox.open();

        // 使用 Folder 讀取郵件．
        List<Email> emails = inbox.fetch();

        int counter = 0;
        for(Email email : emails) {
            counter++;
            System.out.println(String.format("UID: %s", email.getUID()));
            System.out.println(String.format("Received Date: %s", email.getReceivedDate()));
            System.out.println(String.format("From: %s", email.getFrom().getPersonal()));
            System.out.println(String.format("TO: %s", email.getRecipients(Message.RecipientType.TO)));
            System.out.println(String.format("CC: %s", email.getRecipients(Message.RecipientType.CC)));
            System.out.println(String.format("Subject: %s", email.getSubject()));
            if(email.getArticles().size()>0) {
                Article article = email.getArticles().get(0);
                System.out.println(String.format("Content: %s\n%s\n%s", article.getMediaType(), article.getText(), article.getData()));
            } else {
                System.out.println("No Content.");
            }
            if(email.getAttachments().size()>0) {
                System.out.println("Attachments:");
                for(Attachment attachment : email.getAttachments()) {
                    System.out.println(String.format("%s: %s", attachment.getName(), attachment.getMediaType()));
                }
            }
            if(counter > 10) break;
        }

        inbox.close();

        mailbox.close();
    }

    @Test
    public void testCopyEmail() throws Exception {
        String tenantId = environ.property("office365.tenantId");
        String clientId = environ.property("office365.clientId");
        String clientSecret = environ.property("office365.clientSecret");
        String username = environ.property("office365.username");

        // leandev.io
//        tenantId = "d53d1738-1031-480b-9f4e-9b1dc0d3d2a0";
//        clientId = "067b5b28-86db-477c-8bc2-4d4e0fdde75a";
//        clientSecret = "M4W8Q~VcFXkBTlp33o2.QQWXgmRqszEWVfbS_bZD";
//        username = "recruiting@leandev.io";

        // talentonline.io
        tenantId = "7ea9a746-4dcd-446d-90f1-e73085b57373";
        clientId = "c20dc1f3-0dcb-4508-8626-a94dbe4d3a4b";
        clientSecret = "mD08Q~Ou~vhoy0VBfD9Vxt2YJd6RFKhL1y6WWbNw";
        username = "recruiting@leandev.io";

        MicrosoftMailCredential clientCredential = MicrosoftMailCredential.builder()
                .tenantId(tenantId)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .build();

        ClientAgent clientAgent = new MicrosoftMailAgent();
        ClientAccessToken clientAccessToken = clientAgent.connect(clientCredential);

        // 使用 MailboxBuilder 建立 Mailbox．
        Mailbox mailbox = MailboxBuilder.newOffice365Mailbox()
                .withDebug(true)
                .build();

        // 使用 Mailbox 連接郵件伺服器．
        mailbox.connect(username, clientAccessToken.getAccessToken());

        Folder inbox = mailbox.getInbox();

        inbox.open(Folder.READ_WRITE);

        List<Email> emails = inbox.fetch();

        Folder done = mailbox.getFolder("Done");

        done.open(Folder.READ_WRITE);

        for(Email email : emails) {
            inbox.copyEmail(email, done);
        }

        inbox.close();

        mailbox.close();
    }
}
