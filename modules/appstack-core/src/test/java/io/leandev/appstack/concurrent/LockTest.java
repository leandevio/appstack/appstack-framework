package io.leandev.appstack.concurrent;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import static org.assertj.core.api.Assertions.assertThat;

public class LockTest {
    final int POOLSIZE = 2;
    final ExecutorService executor = Executors.newFixedThreadPool(POOLSIZE);

    @BeforeTest
    public void setUp() throws Exception {

    }

    @Test
    public void testReentrantLock() throws Exception {
        ReentrantLock lock = new ReentrantLock();
        Runnable w1 = new Worker(lock, "Job1");
        Runnable w2 = new Worker(lock, "Job2");
        Runnable w3 = new Worker(lock, "Job3");
        Runnable w4 = new Worker(lock, "Job4");
        executor.execute(w1);
        executor.execute(w2);
        executor.execute(w3);
        executor.execute(w4);

    }

    @AfterTest
    public void tearDown() throws Exception {
        executor.shutdown();
        try {
            // Wait a while for existing tasks to terminate
            if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                executor.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!executor.awaitTermination(60, TimeUnit.SECONDS))
                    System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            executor.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }
}