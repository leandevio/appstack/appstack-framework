package io.leandev.appstack.math;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;

public class SimpleLinearRegressionModelTest {

    @Test
    public void testPredict() {
        SimpleLinearRegressionModel simpleLinearRegressionModel = new SimpleLinearRegressionModel();
        double slope = 2, intercept = 1;
        for(int x=1; x<=5; x++) {
            simpleLinearRegressionModel.addData(x, x*slope + intercept);
        }
        double x = 4.5;
        assertThat(simpleLinearRegressionModel.predict(x)).isEqualTo(x*slope + intercept);
        assertThat(simpleLinearRegressionModel.slope()).isEqualTo(slope);
        assertThat(simpleLinearRegressionModel.intercept()).isEqualTo(intercept);
        assertThat(simpleLinearRegressionModel.slopeStdErr()).isEqualTo(0);
        assertThat(simpleLinearRegressionModel.interceptStdErr()).isEqualTo(0);
    }
}