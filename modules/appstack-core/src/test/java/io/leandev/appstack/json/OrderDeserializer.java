package io.leandev.appstack.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import io.leandev.appstack.converter.DateConverter;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

public class OrderDeserializer extends StdDeserializer<Order> {
    DateConverter dateConverter = new DateConverter();

    public OrderDeserializer() {
        this(null);
    }

    public OrderDeserializer(Class<?> valueType) {
        super(valueType);
    }

    @Override
    public Order deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        BigDecimal amount = node.get("amount").decimalValue();
        String buyer = node.get("customer").asText();
        Date orderDate = dateConverter.convert(node.get("orderDate").asText());
        Order order = new Order();
        order.setBuyer(buyer);
        order.setOrderDate(orderDate);
        order.setAmount(amount);

        JsonNode orderItems = node.get("orderItems");
        for(JsonNode orderItem : orderItems) {
            String product = orderItem.get("product").asText();
            int quantity = orderItem.get("quantity").intValue();
            BigDecimal price = orderItem.get("price").decimalValue();
            OrderItem item = new OrderItem(product, quantity, price);
            order.getItems().add(item);
        }


        return order;
    }
}
