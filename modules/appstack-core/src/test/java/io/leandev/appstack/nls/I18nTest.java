package io.leandev.appstack.nls;

import org.testng.annotations.Test;

import java.util.Locale;

import static org.testng.Assert.*;

public class I18nTest {

    @Test
    public void testTranslateTerm() {
        String term = "release date";
        String term_zh = "發行日期";
        String term_zh_CN = "发行日期";
        Locale zh = Locale.forLanguageTag("zh");
        Locale zh_TW = Locale.forLanguageTag("zh-TW");
        Locale zh_CN = Locale.forLanguageTag("zh-CN");
        Locale en_US = Locale.forLanguageTag("en-US");
        Locale ja = Locale.forLanguageTag("ja");
        I18n i18n = new I18n(zh_TW);
        assertEquals(i18n.term(term), term_zh);
        assertEquals(i18n.term(term, zh), term_zh);
        assertEquals(i18n.term(term, zh_CN), term_zh_CN);
        assertEquals(i18n.term(term, en_US), term);
        assertEquals(i18n.term(term, ja), term);
    }

    @Test
    public void testTranslateMessage() {
        String message = "size must be between:${0} and ${1}";
        String message_zh = "長度需介於${0}到${1}";
        String message_zh_CN = "长度需介于${0}到${1}";
        Locale zh = Locale.forLanguageTag("zh");
        Locale zh_TW = Locale.forLanguageTag("zh-TW");
        Locale zh_CN = Locale.forLanguageTag("zh-CN");
        Locale en_US = Locale.forLanguageTag("en-US");
        Locale ja = Locale.forLanguageTag("ja");
        I18n i18n = new I18n(zh_TW);
        assertEquals(i18n.message(message), message_zh);
        assertEquals(i18n.message(message, zh), message_zh);
        assertEquals(i18n.message(message, zh_CN), message_zh_CN);
        assertEquals(i18n.message(message, en_US), message);
        assertEquals(i18n.message(message, ja), message);
    }

    @Test
    public void testTranslateBoth() {
        String term = "Release Date";
        String term_zh = "發行日期";
        String term_zh_CN = "发行日期";
        String message = "size must be between:${0} and ${1}";
        String message_zh = "長度需介於${0}到${1}";
        String message_zh_CN = "长度需介于${0}到${1}";
        Locale zh = Locale.forLanguageTag("zh");
        Locale zh_TW = Locale.forLanguageTag("zh-TW");
        Locale zh_CN = Locale.forLanguageTag("zh-CN");
        Locale en_US = Locale.forLanguageTag("en-US");
        Locale ja = Locale.forLanguageTag("ja");
        I18n i18n = new I18n(zh_TW);
        assertEquals(i18n.translate(term), term_zh);
        assertEquals(i18n.translate(term, zh), term_zh);
        assertEquals(i18n.translate(term, zh_CN), term_zh_CN);
        assertEquals(i18n.translate(term, en_US), term);
        assertEquals(i18n.translate(term, ja), term);
        assertEquals(i18n.translate(message), message_zh);
        assertEquals(i18n.translate(message, zh), message_zh);
        assertEquals(i18n.translate(message, zh_CN), message_zh_CN);
        assertEquals(i18n.translate(message, en_US), message);
        assertEquals(i18n.translate(message, ja), message);
    }

    @Test
    public void testDatetimePattern() {
        Locale zh_TW = Locale.forLanguageTag("zh-TW");
        I18n i18n = new I18n(zh_TW);
        String datetimePattern_zh_TW = "yyyy/MM/dd HH:mm:ss";
        String datePattern_zh_TW = "yyyy/MM/dd";

        assertEquals(i18n.datetimePattern(), datetimePattern_zh_TW);
        assertEquals(i18n.datePattern(), datePattern_zh_TW);
    }

}