package io.leandev.appstack.text;

import io.leandev.appstack.util.Environment;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class TextFileWriterTest {
    private static final Class CLASS = TextFileWriterTest.class;
    private static final ClassLoader CLASSLOADER = CLASS.getClassLoader();

    private static final Path RESOURCES = Paths.get(CLASS.getPackage().getName().replaceAll("\\.", "/"));
    private static final Path OUTPUT = Environment.getDefaultInstance().tmpFolder().resolve(CLASS.getSimpleName());

    @BeforeAll
    static void setup() throws IOException {
        if(!Files.exists(OUTPUT)) {
            Files.createDirectory(OUTPUT);
        }
    }

    @Test
    public void testWrite() throws Exception {
        String filename = "data.txt";
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCES.resolve(filename).toString());
            OutputStream outputStream = Files.newOutputStream(OUTPUT.resolve(filename));
            TextFileReader textFileReader = new TextFileReader(inputStream);
            TextFileWriter textFileWriter = new TextFileWriter(outputStream)) {
            String text = textFileReader.getText();
            textFileWriter.write(text);
        }
    }

    @Test
    public void testWriteBig5() throws Exception {
        String filename = "data.txt";
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCES.resolve(filename).toString());
            OutputStream outputStream = Files.newOutputStream(OUTPUT.resolve(filename));
            TextFileReader textFileReader = new TextFileReader(inputStream);
            TextFileWriter textFileWriter = new TextFileWriter(outputStream, Charset.forName("BIG5"))) {
            String text = textFileReader.getText();
            textFileWriter.write(text);
        }
    }

}