package io.leandev.appstack.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class OrderSerializer extends StdSerializer<Order> {

    public OrderSerializer() {
        this(null);
    }

    public OrderSerializer(Class<Order> t) {
        super(t);
    }

    @Override
    public void serialize(
            Order value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartObject();
        jgen.writeStringField("customer", value.getBuyer());
        jgen.writeNumberField("amount", value.getAmount());
        jgen.writeObjectField("orderDate", value.getOrderDate());
        jgen.writeObjectField("orderItems", value.getItems());
        jgen.writeEndObject();
    }
}
