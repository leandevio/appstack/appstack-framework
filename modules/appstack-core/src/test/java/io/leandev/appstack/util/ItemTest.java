package io.leandev.appstack.util;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class ItemTest {
    @Test
    public void testPutAllWriteable() throws Exception {
        Date now = new Date();
        Account account = new Account("ellisatkinson", 99, now);
        Customer customer = new Customer("jasonlin", 100);
        Item<Customer> item = new Item<>(customer);

        item.putAllWriteable(account);
        assertEquals(customer.getName(), account.getName());
        assertEquals(customer.getCredits(), account.getCredits());
        assertEquals(now, account.getBirthday());
    }

    @Test
    public void testPutAllWriteableExcludes() throws Exception {
        Date now = new Date();
        Account account = new Account("ellisatkinson", 99, now);
        Customer customer = new Customer("jasonlin", 100);
        Item<Customer> item = new Item<>(customer);

        item.putAllWriteable(account, "name");
        assertNotEquals(customer.getName(), account.getName());
        assertEquals(customer.getCredits(), account.getCredits());
        assertEquals(now, account.getBirthday());
    }

}