package io.leandev.appstack.xml;

import io.leandev.appstack.env.Environ;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class XmlReaderWriterTest {
    private static final Class CLASS = XmlReaderWriterTest.class;
    private static final ClassLoader CLASSLOADER = CLASS.getClassLoader();
    private static final Path RESOURCES = Paths.get(CLASS.getPackage().getName().replaceAll("\\.", "/"));
    private static final Path OUTPUT = Environ.getInstance().tmpFolder().resolve(CLASS.getSimpleName());

    @BeforeAll
    static void setup() throws IOException {
        if(!Files.exists(OUTPUT)) {
            Files.createDirectory(OUTPUT);
        }
    }

    @Test
    public void testQuery() throws Exception {
        String XML = "Product.xml";
        Document document;
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCES.resolve(XML).toString());
            XmlReader xmlReader = new XmlReader(inputStream)) {
            document = xmlReader.readAll();
        }
        XPathExpression xPathExpression = XPathExpression.root()
                .resolve("products")
                .resolve("product")
                .resolveByAttribute("id", "Yellow Green Christmas Gift Box");
        XmlFinder xmlFinder = new XmlFinder(document);

        Optional<Element> $product = xmlFinder.findElement(xPathExpression);
        Element product = $product.get();

        assertThat(product.getAttribute("id")).isEqualTo("Yellow Green Christmas Gift Box");
        Element name = (Element) product.getElementsByTagName("name").item(0);
        assertThat(name.getTextContent()).isEqualTo("黃綠色耶誕禮盒");
        Element description = (Element) product.getElementsByTagName("description").item(0);
        CDATASection cdataSection = (CDATASection) description.getFirstChild();
        assertThat(cdataSection.getTextContent()).isEqualTo("活潑亮眼聖誕佳節賀禮");
        Element price = (Element) product.getElementsByTagName("price").item(0);
        assertThat(price.getTextContent()).isEqualTo("3000");
        Element creationTime = (Element) product.getElementsByTagName("creationTime").item(0);
        assertThat(creationTime.getTextContent()).isEqualTo("2018-07-12T00:00:00.000Z");
    }

    @Test
    public void testModify() throws Exception {
        String XML = "Product.xml";
        Document document;
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCES.resolve(XML).toString());
            XmlReader xmlReader = new XmlReader(inputStream)) {
            document = xmlReader.readAll();
        }
        XPathExpression xPathExpression = XPathExpression.root()
                .resolve("products")
                .resolve("product")
                .resolveByAttribute("id", "Yellow Green Christmas Gift Box");
        XmlFinder xmlFinder = new XmlFinder(document);

        Optional<Element> $product = xmlFinder.findElement(xPathExpression);
        Element product = $product.get();

        String now = Instant.now().toString();
        Element price = xmlFinder.findElement(xPathExpression.resolve("price")).get();
        price.setTextContent("4000");
        Element creationTime = xmlFinder.findElement(xPathExpression.resolve("creationTime")).get();
        Element modificationTime = document.createElement("modificationTime");
        modificationTime.setTextContent(now);
        product.insertBefore(modificationTime, creationTime);

        xPathExpression = XPathExpression.root()
                .resolve("products")
                .resolve("product")
                .resolveByAttribute("id", "Yellow Green Christmas Gift Box");
        Element currentPrice = xmlFinder.findElement(xPathExpression.resolve("price")).get();
        assertThat(currentPrice.getTextContent()).isEqualTo("4000");

        Element currentModificationTime = xmlFinder.findElement(xPathExpression.resolve("modificationTime")).get();
        assertThat(currentModificationTime.getTextContent()).isEqualTo(now);

        try(OutputStream output = Files.newOutputStream(OUTPUT.resolve(XML))) {
            XmlWriter xmlWriter = new XmlWriter(output);
            xmlWriter.write(document);
        }
    }
}
