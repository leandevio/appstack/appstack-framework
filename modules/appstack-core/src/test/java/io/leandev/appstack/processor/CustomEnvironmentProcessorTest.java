package io.leandev.appstack.processor;


import io.leandev.appstack.config.TestConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes= TestConfiguration.class, loader= AnnotationConfigContextLoader.class)
public class CustomEnvironmentProcessorTest {

    @Test
    public void testEnvironmentProcessor() {
        assertThat(12).isEqualTo(12);
    }

}