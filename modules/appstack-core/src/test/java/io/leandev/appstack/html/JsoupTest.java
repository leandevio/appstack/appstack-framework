package io.leandev.appstack.html;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;
public class JsoupTest {
    @BeforeMethod
    public void setUp() {
    }

    @Test
    public void testSearch() throws Exception {
        String html = "<html><head><title>Breaking News</title></head>"
                + "<body><div class='news' title='First'>First Headline</div>"
                + "<div class='news' title='Second'>Second Headline</div></body></html>";
        Document doc = Jsoup.parse(html);
        assertThat(doc.title()).isEqualTo("Breaking News");
        Elements newsHeadlines = doc.select(".news");
        Element first = newsHeadlines.first();
        assertThat(first.className()).isEqualTo("news");
        assertThat(first.attr("title")).isEqualTo("First");
        assertThat(first.text()).isEqualTo("First Headline");
    }
}
