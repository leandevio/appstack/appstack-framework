package io.leandev.appstack.html;

import io.leandev.appstack.doc.delta.Delta;
import io.leandev.appstack.doc.delta.DeltaParser;
import io.leandev.appstack.docx.XWPFTest;
import io.leandev.appstack.util.Environment;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.*;

public class HtmlDocumentTest {
    private final Path RESOURCE = Paths.get(HtmlDocumentTest.class.getPackage().getName().replaceAll("\\.", "/"));
    private final Path OUTPUT = Environment.getDefaultInstance().tmpFolder();
    private final ClassLoader LOADER = XWPFTest.class.getClassLoader();

    @BeforeMethod
    public void setUp() {
    }

    @Test
    public void testAppendSectionOfDelta() throws Exception {
        Path path = RESOURCE.resolve("delta.json");
        InputStream inputStream = LOADER.getResourceAsStream(path.toString());

        DeltaParser deltaParser = new DeltaParser();
        Delta delta = deltaParser.parse(inputStream);

        inputStream.close();

        HtmlDocument htmlDocument = new HtmlDocument();
        htmlDocument.appendBlocks(delta);

        OutputStream outputStream = new FileOutputStream(OUTPUT.resolve("delta.html").toFile());

        htmlDocument.write(outputStream);

        outputStream.close();

        assertThat(htmlDocument.html()).isNotNull();
    }

    @Test
    public void testBug() throws IOException {
        String data = "{\"ops\":[{\"attributes\":{\"color\":\"#000000\"},\"insert\":\"Kristen3 Bell您好,\"},{\"insert\":\"\\n\"},{\"attributes\":{\"color\":\"#000000\"},\"insert\":\"非常感謝您投遞本公司的職缺，為了讓您的學經歷與我們的職缺做更精確的配對，請\"},{\"insert\":\"\\n\"},{\"attributes\":{\"color\":\"#000000\"},\"insert\":\"您協助提供詳細履歷或其他補充文件(應屆畢業生請提供歷年成績單)，我們將在收到\"},{\"insert\":\"\\n\"},{\"attributes\":{\"color\":\"#000000\"},\"insert\":\"您的資料後做進一步的職缺媒合。\"},{\"insert\":\"\\n\"},{\"attributes\":{\"color\":\"#000000\"},\"insert\":\"再次謝謝您對聯發科技職缺的關注與興趣。\"},{\"insert\":\"\\n\\n\"},{\"attributes\":{\"color\":\"#000000\"},\"insert\":\"聯發科技股份有限公司\"},{\"insert\":\"\\n\"},{\"attributes\":{\"color\":\"#000000\"},\"insert\":\"人力資源部\"},{\"insert\":\"\\n\"}]}";

        DeltaParser deltaParser = new DeltaParser();
        Delta delta = deltaParser.parse(data);


        HtmlDocument htmlDocument = new HtmlDocument();
        htmlDocument.appendBlocks(delta);
    }
}