package io.leandev.appstack.cache;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;


@Setter
@Getter
@Builder
@AllArgsConstructor
public class Account implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private int credits;
    private LocalDate birthday;
}
