package io.leandev.appstack.compress;

import io.leandev.appstack.env.Environ;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipOutputStream;

public class ZipOutputStreamBuilderTest {
    private static final Class CLASS = ZipOutputStreamBuilderTest.class;
    private static final ClassLoader CLASSLOADER = CLASS.getClassLoader();
    private static final Path RESOURCES = Paths.get(CLASS.getPackage().getName().replaceAll("\\.", "/"));
    private static final Path OUTPUT = Environ.getInstance().tmpFolder().resolve(CLASS.getSimpleName());

    @BeforeAll
    static void setup() throws IOException {
        if(!Files.exists(OUTPUT)) {
            Files.createDirectory(OUTPUT);
        }
    }

    @Test
    public void testZip() throws Exception {
        String JSON_FILENAME = "data.json";
        String XLSX_FILENAME = "data.xlsx";
        String ZIPFILENAME = "compressed.zip";
        try (OutputStream outputStream = new FileOutputStream(OUTPUT.resolve(ZIPFILENAME).toFile());
             InputStream json = CLASSLOADER.getResourceAsStream(RESOURCES.resolve(JSON_FILENAME).toString());
             InputStream xlsx = CLASSLOADER.getResourceAsStream(RESOURCES.resolve(XLSX_FILENAME).toString())) {
             ZipOutputStreamBuilder.of(outputStream)
                    .putNextEntry("產品資料.json", json)  // 內容可以是 InputStream 或是 byte[]
                    .putNextEntry("產品資料.xlsx", xlsx)
                    .build(true);
        }
    }
}
