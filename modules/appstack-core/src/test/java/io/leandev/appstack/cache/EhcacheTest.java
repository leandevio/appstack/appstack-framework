package io.leandev.appstack.cache;

import io.leandev.appstack.env.Environ;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.PersistentCacheManager;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class EhcacheTest {
    private static final Class CLASS = EhcacheTest.class;
    private static final ClassLoader CLASSLOADER = EhcacheTest.class.getClassLoader();
    private static final Path RESOURCES = Paths.get(CLASS.getPackage().getName().replaceAll("\\.", "/"));
    private static final Path OUTPUT = Environ.getInstance().tmpFolder().resolve(CLASS.getSimpleName());
    private static final Path CACHE = Environ.getInstance().cacheFolder().resolve(CLASS.getSimpleName());

    @Test
    public void testSimpleCache() {
        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .build();
        cacheManager.init();
        CacheConfiguration cacheConfiguration =  CacheConfigurationBuilder.newCacheConfigurationBuilder(Integer.class, Integer.class, ResourcePoolsBuilder.heap(10)).build();
        Cache<Integer, Integer> squareNumberCache = cacheManager.createCache("squareNumber", cacheConfiguration);
        Cache<Integer, Integer> $squareNumberCache = cacheManager.getCache("squareNumber", Integer.class, Integer.class);

        Integer number = 4, squareNumber = number * number;
        squareNumberCache.put(number, squareNumber);

        assertThat($squareNumberCache).isEqualTo(squareNumberCache);
        assertThat($squareNumberCache.get(number)).isEqualTo(squareNumber);

        cacheManager.close();
    }

    @Test
    public void testPersistCache() {
        PersistentCacheManager persistentCacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .with(CacheManagerBuilder.persistence(CACHE.toFile()))
                .build(true);

        CacheConfiguration cacheConfiguration = CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, Account.class,
                ResourcePoolsBuilder.newResourcePoolsBuilder()
                        .heap(10, EntryUnit.ENTRIES)
                        .offheap(1, MemoryUnit.MB)
                        .disk(20, MemoryUnit.MB, true))
                        .build();

        Cache<String, Account> threeTieredCache = persistentCacheManager.createCache("threeTieredCache", cacheConfiguration);
        Account ellisatkinson = new Account("ellisatkinson", 99, LocalDate.of(1980, 3, 3));
        threeTieredCache.put(ellisatkinson.getName(), ellisatkinson);

        Cache<String, Account> $threeTieredCache = persistentCacheManager.getCache("threeTieredCache", String.class, Account.class);
        Account jasonlin = new Account("jasonlin", 100, LocalDate.of(1990, 4, 1));
        threeTieredCache.put(jasonlin.getName(), jasonlin);

        Account $ellisatkinson = $threeTieredCache.get(ellisatkinson.getName());
        Account $jasonlin = $threeTieredCache.get(jasonlin.getName());

        assertThat($threeTieredCache).isEqualTo(threeTieredCache);

        assertThat($ellisatkinson.getName()).isEqualTo(ellisatkinson.getName());
        assertThat($ellisatkinson.getCredits()).isEqualTo(ellisatkinson.getCredits());
        assertThat($ellisatkinson.getBirthday()).isEqualTo(ellisatkinson.getBirthday());

        assertThat($jasonlin.getName()).isEqualTo(jasonlin.getName());
        assertThat($jasonlin.getCredits()).isEqualTo(jasonlin.getCredits());
        assertThat($jasonlin.getBirthday()).isEqualTo(jasonlin.getBirthday());

        persistentCacheManager.close();
    }

}
