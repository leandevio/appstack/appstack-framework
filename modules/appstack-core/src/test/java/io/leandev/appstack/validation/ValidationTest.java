package io.leandev.appstack.validation;

import io.leandev.appstack.logging.Logger;
import io.leandev.appstack.logging.LogManager;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class ValidationTest {
    private static final Logger LOGGER = LogManager.getLogger(ValidationTest.class);
    @Test
    public void testValidate() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        User user = new User();
        user.setName("Jason");
        user.setWorking(true);
        user.setAboutMe("Its all about me!");
        user.setAge(16);

        Set<ConstraintViolation<User>> violations = validator.validate(user);

        for (ConstraintViolation<User> violation : violations) {
            LOGGER.error(violation.getMessage());
        }
    }
}
