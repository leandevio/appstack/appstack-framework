package io.leandev.appstack.search;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;

public class PredicateParserTest {

    @BeforeMethod
    public void setUp() {
    }

    @Test
    public void testParse() {
        String criteria = "(category==\"Boxed Flower\");(name==粉馨花束;price>=4200;createdOn==2018-07-12T00:00:00.000Z)";
        PredicateParser predicateParser = new PredicateParser();
        Predicate predicate = predicateParser.parse(criteria);

        predicate = predicate.and(Predicate.gt("cost", 2000));

        assertThat(predicate.node()).isInstanceOf(AndNode.class);
    }

    @Test
    public void testParseInteger() {
        String criteria = "text==01;hundreds==200;two==2;zero==0;plus==+2;minus==-2";
        PredicateParser predicateParser = new PredicateParser();
        Predicate predicate = predicateParser.parse(criteria);

        assertThat(predicate.node()).isInstanceOf(AndNode.class);

        AndNode and = (AndNode) predicate.node();

        ComparisonNode node = (ComparisonNode) and.children().get(0);
        assertThat(node.expectation()).isEqualTo("01");

        node = (ComparisonNode) and.children().get(1);
        assertThat(node.expectation()).isEqualTo(200);

        node = (ComparisonNode) and.children().get(2);
        assertThat(node.expectation()).isEqualTo(2);

        node = (ComparisonNode) and.children().get(3);
        assertThat(node.expectation()).isEqualTo(0);

        node = (ComparisonNode) and.children().get(4);
        assertThat(node.expectation()).isEqualTo(2);

        node = (ComparisonNode) and.children().get(5);
        assertThat(node.expectation()).isEqualTo(-2);

    }

    @Test
    public void testParseFloat() {
        String criteria = "text==01;float==2.3;zero==0.0;plus==+2.3;minus==-2.2;decimal==.3;decimalplus==+.3;decimalminus==-.3";
        PredicateParser predicateParser = new PredicateParser();
        Predicate predicate = predicateParser.parse(criteria);

        assertThat(predicate.node()).isInstanceOf(AndNode.class);

        AndNode and = (AndNode) predicate.node();

        ComparisonNode node = (ComparisonNode) and.children().get(0);
        assertThat(node.expectation()).isEqualTo("01");

        node = (ComparisonNode) and.children().get(1);
        assertThat(node.expectation()).isEqualTo(2.3f);

        node = (ComparisonNode) and.children().get(2);
        assertThat(node.expectation()).isEqualTo(0.0f);

        node = (ComparisonNode) and.children().get(3);
        assertThat(node.expectation()).isEqualTo(+2.3f);

        node = (ComparisonNode) and.children().get(4);
        assertThat(node.expectation()).isEqualTo(-2.2f);

        node = (ComparisonNode) and.children().get(5);
        assertThat(node.expectation()).isEqualTo(.3f);

        node = (ComparisonNode) and.children().get(6);
        assertThat(node.expectation()).isEqualTo(+.3f);

        node = (ComparisonNode) and.children().get(7);
        assertThat(node.expectation()).isEqualTo(-.3f);

    }

    @Test
    public void testParseNull() {
        String criteria = "text=is=null;float==null";
        PredicateParser predicateParser = new PredicateParser();
        Predicate predicate = predicateParser.parse(criteria);

        assertThat(predicate.node()).isInstanceOf(AndNode.class);

        AndNode and = (AndNode) predicate.node();
        assertThat(and.children().size()).isEqualTo(1);

        ComparisonNode node = (ComparisonNode) and.children().get(0);
        assertThat(node.expectation()).isNull();
    }
}