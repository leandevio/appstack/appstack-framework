package io.leandev.appstack.util;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class ObjectMapperTest {

    @Test
    public void testMap() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Date now = new Date();
        Account account = new Account("Baeldung", 10, now);
        Customer customer = mapper.map(account, Customer.class);
        assertEquals(customer.getCredits(), account.getCredits());
        assertEquals(customer.getName(), account.getName());
        assertEquals(customer.getBirthday(), account.getBirthday());
    }
}