package io.leandev.appstack.mail;

import io.leandev.appstack.util.Environment;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import static org.testng.Assert.*;

public class MailerTest {
    private final Environment environment = Environment.getDefaultInstance();

    @Test
    public void testConnect() throws Exception {
        // 使用 MailerBuilder 建立 Mailer．
        Mailer mailer = MailerBuilder.newMailer()
                .withSMTPServer()
                .build();
        // 測試連線
        mailer.test();;
    }

    @Test
    public void testConnectSMTPServer() throws Exception {
        Mailer mailer = MailerBuilder.newMailer()
                .withSMTPServer()
                .build();
        Email email = EmailBuilder.newTextEmail()
                .to("jasonlin@leandev.io", "林晉陞")
                .withSubject("Mailer 測試郵件")
                .withContent("您好：\n這封信沒有附件．")
                .build();
        mailer.send(email);;
    }

    @Test
    public void testSendTextEmail() throws Exception {
        Mailer mailer = MailerBuilder.newMailer()
                .withSMTPServer()
                .withDebug(true)
                .build();
        Email email = EmailBuilder.newTextEmail()
                .to("jasonlin@leandev.io", "林晉陞")
                .withSubject("Mailer 測試郵件")
                .withContent("您好：\n這封信沒有附件．")
                .build();
        mailer.send(email);
    }

    @Test
    public void testSendTextEmailWithAttachment() throws Exception {
        Mailer mailer = MailerBuilder.newMailer()
                .withSMTPServer()
                .withDebug(true)
                .build();

        InputStream pdf = readPdf();
        InputStream excel = readExcel();

        Email email = EmailBuilder.newTextEmail()
                .from("ellisatkinson@ellen.leandev.com.tw", "麗得科技")
                .to("jasonlin@ellen.leandev.com.tw", "傑森")
                .cc("mayshih@ellen.leandev.com.tw")
                .withSubject("Mailer 測試郵件")
                .withContent("您好：\n這封信有兩個附件．")
                .withAttachment(pdf, "Office Layout.pdf")
                .withAttachment(excel, "報價.xlsx")
                .build();

        pdf.close();
        excel.close();

        mailer.send(email);
    }

    @Test
    public void testSendHtmlEmail() throws Exception {
        final String USERNAME = environment.property("office365.username");
        final String PASSWORD = environment.property("office365.password");

        Mailer mailer = MailerBuilder.newMailer()
                .withOffice365Server(USERNAME, PASSWORD)
                .withDebug(true)
                .stopFirewall()
                .build();
        Email email = EmailBuilder.newHtmlEmail()
                .from(USERNAME)
                .to("jasonlin@email.com", "林晉陞")
                .withSubject("Mailer 測試郵件")
                .withContent("<span>您好：</span><p>" +
                        "<span style='color:blue'>這段文字是藍色的．</span><p>" +
                        "<span>這封信沒有附件．</span>")
                .build();
        mailer.send(email);
    }

    @Test
    public void testSendHtmlEmailWithAttachment() throws Exception {
        final String HOSTNAME = "ellen.leandev.com.tw";
        final int PORT = 25;
        final String USERNAME = environment.property("ellen.username");
        final String PASSWORD = environment.property("ellen.password");

        Mailer mailer = MailerBuilder.newMailer()
                .withSMTPServer(HOSTNAME, PORT, USERNAME, PASSWORD)
                .withDebug(true)
                .build();

        InputStream pdf = readPdf();
        InputStream excel = readExcel();

        Email email = EmailBuilder.newHtmlEmail()
                .from("ellisatkinson@ellen.leandev.com.tw", "麗得科技")
                .to("jasonlin@ellen.leandev.com.tw", "傑森")
                .cc("mayshih@ellen.leandev.com.tw")
                .withSubject("Mailer 測試郵件")
                .withContent("<span>您好：</span><p>" +
                        "<span style='color:blue'>這段文字是藍色的．</span><p>" +
                        "<span>這封信有兩個附件．</span>", ">您好：\n這段文字是藍色的．\n這封信有兩個附件．")
                .withAttachment(pdf, "Office Layout.pdf")
                .withAttachment(excel, "報價.xlsx")
                .build();

        pdf.close();
        excel.close();

        mailer.send(email);
    }

    @Test
    public void testConnectGmailServer() throws Exception {
        final String USERNAME = environment.property("gmail.username");
        final String PASSWORD = environment.property("gmail.password");

        Mailer mailer = MailerBuilder.newMailer()
                .withGmailServer(USERNAME, PASSWORD)
                .withDebug(true)
                .build();

        InputStream pdf = readPdf();
        InputStream excel = readExcel();

        // the from must be the same as the sender.
        Email email = EmailBuilder.newHtmlEmail()
                .from(USERNAME, "Jason Lin")
                .to("jasonlin@leandev.io", "林晉陞")
                .withSubject("Mailer 測試郵件")
                .withContent("<span>您好：</span><p>" +
                        "<span style='color:blue'>這段文字是藍色的．</span><p>" +
                        "<span>這封信有兩個附件．</span>", ">您好：\n這段文字是藍色的．\n這封信有兩個附件．")
                .withAttachment(pdf, "Office Layout.pdf")
                .withAttachment(excel, "報價.xlsx")
                .build();
        mailer.send(email);

        pdf.close();
        excel.close();
    }

    @Test
    public void testConnectOffice365Server() throws Exception {
        final String USERNAME = environment.property("office365.username");
        final String PASSWORD = environment.property("office365.password");

        Mailer mailer = MailerBuilder.newMailer()
                .withOffice365Server(USERNAME, PASSWORD)
                .withDebug(true)
                .build();

        InputStream pdf = readPdf();
        InputStream excel = readExcel();

        // the from could be different from the sender.
        Email email = EmailBuilder.newHtmlEmail()
                .from("notification@talentonline.io", "Jason Lin")
                .to("jaysenlynn@gmail.com", "林晉陞")
                .to("jasonlin@talentonline.io", "林晉陞")
                .withSubject("Mailer 測試郵件")
                .withContent("<span>您好：</span><p>" +
                        "<span style='color:blue'>這段文字是藍色的．</span><p>" +
                        "<span>這封信有兩個附件．</span>", ">您好：\n這段文字是藍色的．\n這封信有兩個附件．")
                .withAttachment(pdf, "Office Layout.pdf")
                .withAttachment(excel, "報價.xlsx")
                .build();
        mailer.send(email);

        pdf.close();
        excel.close();
    }

    @Test
    public void testSendInvitation() throws Exception {
        final String USERNAME = environment.property("office365.username");
        final String PASSWORD = environment.property("office365.password");

        Mailer mailer = MailerBuilder.newMailer()
                .withOffice365Server(USERNAME, PASSWORD)
                .withDebug(true)
                .stopFirewall()
                .build();

        InputStream pdf = readPdf();
        InputStream excel = readExcel();

        Instant now = Instant.now();
        Date start = Date.from(now.plus(Duration.ofDays(1)));
        Date end = Date.from(start.toInstant().plus(Duration.ofHours(3)));
        String uid = UUID.randomUUID().toString();

        Invitation invitation = InvitationBuilder.newInvitation()
                .from(USERNAME, "Mailer 會議邀請測試")
                .withOrganizer("jasonlin@talentonline.io", "林晉陞")
//                .withAttendee("jasonlin@talentonline.io", "林晉陞")
                .withAttendee("jasonlin@leandev.io", "林晉陞")
                .withSubject("專案會議")
                .startAt(start)
                .endAt(end)
                .atLocation("542 會議室")
                .withUid(uid)
                .withDescription("以下是本次會議的討論主題：<ol><li>成本效益</li><li>技術架構<li>風險評估</li></ol>")
//                .withContent("<span>您好：</span><p>" +
//                        "<span style='color:blue'>這段文字是藍色的．</span><p>" +
//                        "<span>這封信有兩個附件．</span>", ">您好：\n這段文字是藍色的．\n這封信有兩個附件．")
                .withAttachment(pdf, "Office Layout.pdf")
                .withAttachment(excel, "報價.xlsx")
                .build();
        mailer.send(invitation);

        pdf.close();
        excel.close();

    }

    @Test
    public void testCancelInvitation() throws Exception {
        final String USERNAME = environment.property("gmail.username");
        final String PASSWORD = environment.property("gmail.password");

        Mailer mailer = MailerBuilder.newMailer()
                .withGmailServer(USERNAME, PASSWORD)
                .withDebug(true)
                .build();

        InputStream pdf = readPdf();
        InputStream excel = readExcel();

        Instant now = Instant.now();
        Date start = Date.from(now.plus(Duration.ofDays(1)));
        Date end = Date.from(start.toInstant().plus(Duration.ofHours(3)));
        String uid = UUID.randomUUID().toString();

        Invitation invitation = InvitationBuilder.newInvitation()
                .from(USERNAME, "Mailer 會議邀請測試")
                .withOrganizer("jaysenlin@gmail.com", "Jaysen Lin")
                .withAttendee("jasonlin@leandev.io", "林晉陞")
                .withSubject("專案會議")
                .startAt(start)
                .endAt(end)
                .atLocation("542 會議室")
                .withUid(uid)
                .withDescription("以下是本次會議的討論主題：<ol><li>成本效益</li><li>技術架構<li>風險評估</li></ol>")
                .withAttachment(pdf, "Office Layout.pdf")
                .withAttachment(excel, "報價.xlsx")
                .build();
        mailer.send(invitation);

        pdf.close();
        excel.close();

        Thread.sleep(30*1000);

       invitation = InvitationBuilder.newInvitation()
                .from(USERNAME, "Mailer 會議邀請測試")
                .withOrganizer("jaysenlin@gmail.com", "Jaysen Lin")
                .withAttendee("jasonlin@leandev.io", "林晉陞")
                .withSubject("專案會議")
               .startAt(start)
               .endAt(end)
               .atLocation("542 會議室")
                .withUid(uid)
                .build();

        mailer.cancel(invitation);
    }

    private InputStream readPdf() {
        ClassLoader loader = this.getClass().getClassLoader();
        String path = this.getClass().getPackage().getName().replaceAll("\\.", "/") + "/office-layout.pdf" ;
        return loader.getResourceAsStream(path);
    }

    private InputStream readExcel() {
        ClassLoader loader = this.getClass().getClassLoader();
        String path = this.getClass().getPackage().getName().replaceAll("\\.", "/") + "/quotation.xlsx" ;
        return loader.getResourceAsStream(path);
    }

}