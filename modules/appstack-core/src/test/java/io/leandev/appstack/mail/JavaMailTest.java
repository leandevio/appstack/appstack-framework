package io.leandev.appstack.mail;

import com.sun.mail.util.MailSSLSocketFactory;
import io.leandev.appstack.env.Environ;
import io.leandev.appstack.util.Environment;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import javax.activation.DataHandler;
import javax.activation.MailcapCommandMap;
import javax.activation.MimetypesFileTypeMap;
import javax.mail.*;
import javax.mail.Folder;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;

public class JavaMailTest {
    private final Environment environment = Environment.getDefaultInstance();
    private final Path RESOURCE = Paths.get("/").resolve(this.getClass().getPackage().getName().replaceAll("\\.", "/"));
    private final Path OUTPUT = environment.tmpFolder();
    private final Environ environ = Environ.getInstance();

    @Test
    public void testSendMessage() throws Exception {
        Session session = buildSession();
        Message message = buildMessage(session);
        Transport.send(message);
    }

    @Test
    public void testSendWithAuth() throws Exception {
        final String HOSTNAME = environ.property("app.mail.smtp.hostname");
        Properties props = new Properties();
        props.put("mail.smtp.host", HOSTNAME);
        // Create the session
        Session session = Session.getInstance(props, null);

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("no_reply@example.com", "NoReply-JD"));
        message.setRecipients(
                Message.RecipientType.TO, InternetAddress.parse("jasonlin@leandev.com.tw"));
        message.setSubject("Mailer 測試郵件");

        String msg = "您好：\n這封信沒有附件．";

        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(msg, "text/html; charset=utf-8");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);
        message.setContent(multipart);
        Transport.send(message);
    }

    private Session buildSession() throws MessagingException {
        final String USERNAME = environ.property("app.mail.smtp.username");
        final String PASSWORD = environ.property("app.mail.smtp.password");
        final String HOSTNAME = environ.property("app.mail.smtp.hostname");
        final int PORT = environ.propertyAsInteger("app.mail.smtp.port");
        final String AUTH = environ.property("app.mail.smtp.auth");
        final String STARTTLS = environ.property("app.mail.smtp.starttls");
        Properties props = new Properties();
        props.put("mail.smtp.host", HOSTNAME);
        props.put("mail.smtp.port", PORT);
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", AUTH);
        props.put("mail.smtp.starttls.enable", STARTTLS);
        // Create the session
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(USERNAME, PASSWORD);
                    }
                });

        return session;
    }

    private Message buildMessage(Session session) throws MessagingException {
        final String USERNAME = environ.property("app.mail.smtp.username");
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(USERNAME));
        message.setRecipients(
                Message.RecipientType.TO, InternetAddress.parse("jasonlin@leandev.com.tw"));
        message.setSubject("Mailer 測試郵件");

        String msg = "您好：\n這封信沒有附件．";

        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(msg, "text/html; charset=utf-8");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);

        message.setContent(multipart);

        return message;
    }

    @Test
    public void testReceiveMail() throws Exception {
        // for POP3
        //String protocol = "pop3";
        //String host = "pop.gmail.com";
        //String port = "995";

        // for IMAP
        // Gmail
//        String protocol = "imap";
//        String host = "imap.gmail.com";
//        String port = "993";

        // Office365
//        String protocol = "imap";
//        String host = "outlook.office365.com";
//        String port = "993";

//        final String USERNAME = environment.property("office365.username");
//        final String PASSWORD = environment.property("office365.password");

        // Ellen
//        String protocol = "imap";
//        String host = "ellen.leandev.com.tw";
//        String port = "143";
//
//        String userName = "jasonlin@ellen.leandev.com.tw";
//        String password = "mar4th57";

        // IMAP
        String protocol = "imap";
        String host = "tai-win.com.tw";
        String port = "993";

        final String USERNAME = "tw104@tai-win.com.tw";
        final String PASSWORD = "Taiwin123456@";

        MailReceiver receiver = new MailReceiver();
        receiver.downloadEmails(protocol, host, port, USERNAME, PASSWORD);
    }

    private class MailReceiver {
        /**
         * Returns a Properties object which is configured for a POP3/IMAP server
         *
         * @param protocol either "imap" or "pop3"
         * @param host
         * @param port
         * @return a Properties object
         */
        private Properties getServerProperties(String protocol, String host,
                                               String port) throws GeneralSecurityException {
            Properties properties = new Properties();

            // server setting
            properties.put(String.format("mail.%s.host", protocol), host);
            properties.put(String.format("mail.%s.port", protocol), port);

            // SSL setting
            properties.setProperty(
                    String.format("mail.%s.socketFactory.class", protocol),
                    "javax.net.ssl.SSLSocketFactory");
            properties.setProperty(
                    String.format("mail.%s.socketFactory.fallback", protocol),
                    "false");
            properties.setProperty(
                    String.format("mail.%s.socketFactory.port", protocol),
                    String.valueOf(port));

            MailSSLSocketFactory sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
            properties.put("mail.imap.ssl.trust", "*");
            properties.put("mail.imap.ssl.socketFactory", sf);

//            properties.setProperty("mail.imap.ssl.enable", "true");
//            properties.setProperty("mail.imap.starttls.enable", "true");
//            properties.put("mail.imap.ssl.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//            properties.put("mail.imap.ssl.trust", "*");

            return properties;
        }

        /**
         * Downloads new messages and fetches details for each message.
         * @param protocol
         * @param host
         * @param port
         * @param userName
         * @param password
         */
        public void downloadEmails(String protocol, String host, String port,
                                   String userName, String password) throws Exception {
            Properties properties = getServerProperties(protocol, host, port);

            Session session = Session.getInstance(properties);

            try {
                // connects to the message store
                Store store = session.getStore(protocol);
                store.connect(userName, password);

                // opens the inbox folder
                Folder folderInbox = store.getFolder("INBOX");
                folderInbox.open(Folder.READ_ONLY);

                // fetches new messages from server
                Message[] messages = folderInbox.getMessages();
                // fetches specific messages from server
//                Message[] messages = ((javax.mail.UIDFolder) folderInbox).getMessagesByUID(9008, 9010);

                System.out.println("messages.length---" + messages.length);

                for (int i = 0; i < messages.length; i++) {
                    Message message = messages[i];
                    long uid = ((javax.mail.UIDFolder) message.getFolder()).getUID(message);
                    System.out.println("---------------------------------");
                    System.out.println("UID: " + uid);
                    writePart(message);
                }

                // disconnect
                folderInbox.close(false);
                store.close();
            } catch (NoSuchProviderException ex) {
                System.out.println("No provider for protocol: " + protocol);
                ex.printStackTrace();
            } catch (MessagingException ex) {
                System.out.println("Could not connect to the message store");
                ex.printStackTrace();
            }
        }

        /**
         * Returns a list of addresses in String format separated by comma
         *
         * @param address an array of Address objects
         * @return a string represents a list of addresses
         */
        private String parseAddresses(Address[] address) {
            String listAddress = "";

            if (address != null) {
                for (int i = 0; i < address.length; i++) {
                    listAddress += address[i].toString() + ", ";
                }
            }
            if (listAddress.length() > 1) {
                listAddress = listAddress.substring(0, listAddress.length() - 2);
            }

            return listAddress;
        }
        /*
         * This method checks for content-type
         * based on which, it processes and
         * fetches the content of the message
         */
        private void writePart(Part p) throws Exception {
            if (p instanceof Message)
                //Call methos writeEnvelope
                writeEnvelope((Message) p);

            System.out.println("----------------------------");
            System.out.println("CONTENT-TYPE: " + p.getContentType());

            //check if the content is plain text
            if (p.isMimeType("text/plain")) {
                System.out.println("This is plain text");
                System.out.println("---------------------------");
                System.out.println((String) p.getContent());
            }
            //check if the content has attachment
            else if (p.isMimeType("multipart/*")) {
                System.out.println("This is a Multipart");
                System.out.println("---------------------------");
                Multipart mp = (Multipart) p.getContent();
                int count = mp.getCount();
                for (int i = 0; i < count; i++)
                    writePart(mp.getBodyPart(i));
            }
            //check if the content is a nested message
            else if (p.isMimeType("message/rfc822")) {
                System.out.println("This is a Nested Message");
                System.out.println("---------------------------");
                writePart((Part) p.getContent());
            }
            //check if the content is an inline image
            else if (p.isMimeType("font/jpeg")) {
                System.out.println("--------> image/jpeg");
                Object o = p.getContent();

                InputStream x = (InputStream) o;
                // Construct the required byte array
                System.out.println("x.length = " + x.available());
                int i = 0;
                byte[] bArray = new byte[x.available()];
                while ((i = (int) ((InputStream) x).available()) > 0) {
                    int result = (int) (((InputStream) x).read(bArray));
                    if (result == -1)

                    break;
                }
                FileOutputStream f2 = new FileOutputStream("/tmp/image.jpg");
                f2.write(bArray);
            }
            else if (p.getContentType().contains("font/")) {
                System.out.println("content type" + p.getContentType());
                File f = new File("font" + new Date().getTime() + ".jpg");
                DataOutputStream output = new DataOutputStream(
                        new BufferedOutputStream(new FileOutputStream(f)));
                com.sun.mail.util.BASE64DecoderStream test =
                        (com.sun.mail.util.BASE64DecoderStream) p
                                .getContent();
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = test.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }
            }
            else {
                Object o = p.getContent();
                if (o instanceof String) {
                    System.out.println("This is a string");
                    System.out.println("---------------------------");
                    System.out.println((String) o);
                }
                else if (o instanceof InputStream) {
                    System.out.println("This is just an input stream");
                    System.out.println("---------------------------");
                    InputStream is = (InputStream) o;
                    is = (InputStream) o;
                    int c;
                    while ((c = is.read()) != -1)
                        System.out.write(c);
                }
                else {
                    System.out.println("This is an unknown type");
                    System.out.println("---------------------------");
                    System.out.println(o.toString());
                }
            }

        }
        /*
         * This method would print FROM,TO and SUBJECT of the message
         */
        private void writeEnvelope(Message m) throws Exception {
            System.out.println("This is the message envelope");
            System.out.println("---------------------------");
            Address[] a;

            // FROM
            if ((a = m.getFrom()) != null) {
                for (int j = 0; j < a.length; j++)
                    System.out.println("FROM: " + a[j].toString());
            }

            // TO
            if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
                for (int j = 0; j < a.length; j++)
                    System.out.println("TO: " + a[j].toString());
            }

            // SUBJECT
            if (m.getSubject() != null)
                System.out.println("SUBJECT: " + m.getSubject());

        }
    }

    @Test
    public void testSendInvitation() throws Exception {
        final String USERNAME = environment.property("gmail.username");
        final String PASSWORD = environment.property("gmail.password");
        final InternetAddress organizer = new InternetAddress(USERNAME);
        final InternetAddress attendee =  new InternetAddress(environment.property("office365.username"), "林晉陞");
//        final InternetAddress attendee =  new InternetAddress("jaysenlin@gmail.com", "Jaysen Lin");
//        final InternetAddress attendee =  new InternetAddress("jasonlin@email.com", "Jason Lynn");
        final String subject = "Sales Meeting";


        final String HOSTNAME = "smtp.gmail.com";
        final int PORT = 587;
        Properties properties = new Properties();

        properties.put("mail.smtp.host", HOSTNAME);
        properties.put("mail.smtp.port", PORT);
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");

        Session session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(USERNAME, PASSWORD);
                    }
                });

        //register the text/calendar mime type
        MimetypesFileTypeMap mimetypes = (MimetypesFileTypeMap)MimetypesFileTypeMap.getDefaultFileTypeMap();
        mimetypes.addMimeTypes("text/calendar ics ICS");

        //register the handling of text/calendar mime type
        MailcapCommandMap mailcap = (MailcapCommandMap) MailcapCommandMap.getDefaultCommandMap();
        mailcap.addMailcap("text/calendar;; x-java-content-handler=com.sun.mail.handlers.text_plain");

        MimeMessage message = new MimeMessage(session);
        message.setFrom(organizer);
        message.setSubject(subject);
        message.addRecipient(Message.RecipientType.TO, attendee);

        // Create an alternative Multipart
        Multipart multipart = new MimeMultipart("alternative");

        //part 1, html text
        BodyPart messageBodyPart = buildHtmlTextPart();
        multipart.addBodyPart(messageBodyPart);

        // Add part two, the calendar
        BodyPart calendarPart = buildCalendarPart(attendee);
        multipart.addBodyPart(calendarPart);

        //Put the multipart in message
        message.setContent(multipart);

        // send the message
        Transport transport = session.getTransport("smtp");
        transport.connect();
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }

    private BodyPart buildHtmlTextPart() throws MessagingException {

        MimeBodyPart descriptionPart = new MimeBodyPart();

        //Note: even if the content is spcified as being text/html, outlook won't read correctly tables at all
        // and only some properties from div:s. Thus, try to avoid too fancy content
        String content = "simple meeting invitation";
        descriptionPart.setContent(content, "text/html; charset=utf-8");

        return descriptionPart;
    }

    //define somewhere the icalendar date format
    private static SimpleDateFormat iCalendarDateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmm'00'");

    private BodyPart buildCalendarPart(InternetAddress attendee) throws Exception {

        BodyPart calendarPart = new MimeBodyPart();

//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
//        cal.add(Calendar.DAY_OF_MONTH, 1);
//        Date start = cal.getTime();
//        cal.add(Calendar.HOUR_OF_DAY, 3);
//        Date end = cal.getTime();

        Instant now = Instant.now();
        Date start = Date.from(now.plus(Duration.ofDays(1)));
        Date end = Date.from(start.toInstant().plus(Duration.ofHours(3)));
        iCalendarDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        //check the icalendar spec in order to build a more complicated meeting request
        String calendarContent =
                "BEGIN:VCALENDAR\n" +
                        "METHOD:REQUEST\n" +
                        "PRODID: BCP - Meeting\n" +
                        "VERSION:2.0\n" +
                        "BEGIN:VEVENT\n" +
                        "DTSTAMP:20200918T103000Z\n" +
                        "DTSTART:20200918T103000Z\n" +
                        "DTEND:20200918T123000Z\n" +
                        "SUMMARY:test request\n" +
                        "UID:506\n" +
                        "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE:MAILTO:"+ attendee.getAddress() + "\n" +
                        "ORGANIZER:MAILTO:jaysenlynn@gmail.com\n" +
                        "LOCATION:on the net\n" +
                        "DESCRIPTION:learn some stuff\n" +
                        "SEQUENCE:0\n" +
                        "PRIORITY:5\n" +
                        "CLASS:PUBLIC\n" +
                        "STATUS:CONFIRMED\n" +
                        "TRANSP:OPAQUE\n" +
                        "BEGIN:VALARM\n" +
                        "ACTION:DISPLAY\n" +
                        "DESCRIPTION:REMINDER\n" +
                        "TRIGGER;RELATED=START:-PT00H15M00S\n" +
                        "END:VALARM\n" +
                        "END:VEVENT\n" +
                        "END:VCALENDAR";


        calendarPart.addHeader("Content-Class", "urn:content-classes:calendarmessage");
        calendarPart.setContent(calendarContent, "text/calendar;method=REQUEST");

        return calendarPart;
    }

    @Test
    public void testSendInvitation2() throws Exception {
        final String USERNAME = environment.property("gmail.username");
        final String PASSWORD = environment.property("gmail.password");

        final InternetAddress organizer = new InternetAddress(USERNAME);
        final InternetAddress attendee =  new InternetAddress(environment.property("office365.username"), "林晉陞");
        final String subject = "Sales Meeting";


        final String HOSTNAME = "smtp.gmail.com";
        final int PORT = 587;
        Properties properties = new Properties();

        properties.put("mail.smtp.host", HOSTNAME);
        properties.put("mail.smtp.port", PORT);
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");

        Session session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(USERNAME, PASSWORD);
                    }
                });

        // Define message

        MimeMessage message = new MimeMessage(session);
        message.addHeaderLine("method=REQUEST");
        message.addHeaderLine("charset=UTF-8");
        message.addHeaderLine("component=VEVENT");


        message.setFrom(organizer);
        message.addRecipient(Message.RecipientType.TO, attendee);
        message.setSubject("QF-meeting");

        StringBuffer sb = new StringBuffer();

        StringBuffer buffer = sb.append("BEGIN:VCALENDAR\n"+
                "PRODID:-//Microsoft Corporation//Outlook 9.0 MIMEDIR//EN\n"+
                "VERSION:2.0\n" +
                "METHOD:REQUEST\n" +
                "BEGIN:VEVENT\n" +
                "ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:jaysenlynn@gmail.com\n" +
                "ORGANIZER:MAILTO:jaysenlynn@gmail.com\n" +
                "DTSTART:20200910T103000\n" +
                "DTEND:20200910T123000\n" +
                "LOCATION:Conference room\n" +
                "TRANSP:OPAQUE\n" +
                "SEQUENCE:0\n" +
                "UID:040000008200E00074C5B7101A82E00800000000002FF466CE3AC5010000000000000000100\n" +
                " 000004377FE5C37984842BF9440448399EB02\n" +
                "DTSTAMP:20200910T103000\n" +
                "CATEGORIES:Meeting\n" +
                "DESCRIPTION:wee need to meet urgntly.\n\n" +
                "SUMMARY:Test meeting request\n" +
                "PRIORITY:5\n" +
                "CLASS:PUBLIC\n" +
                "BEGIN:VALARM\n" +
                "TRIGGER:PT1440M\n" +
                "ACTION:DISPLAY\n" +
                "DESCRIPTION:Reminder\n" +
                "END:VALARM\n" +
                "END:VEVENT\n" +
                "END:VCALENDAR");

        // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();

        // Fill the message
        messageBodyPart.setHeader("Content-Class", "urn:content-classes:calendarmessage");
        messageBodyPart.setHeader("Content-ID","calendar_message");
        messageBodyPart.setDataHandler(new DataHandler(
                new ByteArrayDataSource(buffer.toString(), "text/calendar")));//very important


        // Create a Multipart
        Multipart multipart = new MimeMultipart();

        // Add part one
        multipart.addBodyPart(messageBodyPart);



        // Put parts in message
        message.setContent(multipart);

        // send message
        Transport.send(message);

    }


}
