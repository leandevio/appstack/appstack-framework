package io.leandev.appstack.util;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static org.testng.Assert.*;

public class EnvironmentTest {
    private static final String APPNAME = "foobar";
    private static final String APPVERSION = "1.1.0";
    private static final String DATADRIVERKEY = "data.driver";
    private static final String DATADRIVER = "org.h2.Driver";
    private static final String DATAURLKEY = "data.url";
    private static final String DEFAULTDATAURL = "jdbc:h2:file:~/var/data/foobarDB;MV_STORE=TRUE;MODE=Oracle;IGNORECASE=TRUE;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;TRACE_LEVEL_SYSTEM_OUT=1";
    private static final String CUSTOMDATAURL = "jdbc:h2:tcp:~/var/data/foobarDB;MV_STORE=TRUE;MODE=Oracle;IGNORECASE=TRUE;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;TRACE_LEVEL_SYSTEM_OUT=1";

    private static final String CUSTOMPROPERTIES = "custom.properties";

    private String appHome;
    private Path conf;
    private Path customProperties;

    @BeforeTest
    public void beforeTest() throws IOException {
        appHome = System.getProperty("APP_HOME", "~");
        appHome = appHome.replace("~", System.getProperty("user.home"));
        conf = Paths.get(appHome).resolve("conf");
        customProperties = conf.resolve(APPNAME + ".properties");
        Files.createDirectories(conf);
        Files.deleteIfExists(customProperties);
    }

    @Test
    public void testSystemProperty() {
        Environment environment = Environment.getDefaultInstance();

        assertEquals(environment.appName(), APPNAME);
        assertEquals(environment.appVersion(), APPVERSION);

        assertEquals(environment.appBase(), environment.appHome());

        assertTrue(Files.exists(environment.appBase()));
    }


    @Test
    public void testDefaultProperty() {
        Environment environment = Environment.getDefaultInstance();

        assertEquals(environment.appName(), APPNAME);
        assertEquals(environment.appVersion(), APPVERSION);

        assertEquals(environment.property(DATADRIVERKEY), DATADRIVER);
        assertEquals(environment.property(DATAURLKEY), DEFAULTDATAURL);
    }


    @Test
    public void testCustomProperty() throws IOException {
        generateCustomProperties();
        Environment environment = Environment.getDefaultInstance();

        assertEquals(environment.appName(), APPNAME);
        assertEquals(environment.appVersion(), APPVERSION);

        assertEquals(environment.property(DATADRIVERKEY), DATADRIVER);
        assertEquals(environment.property(DATAURLKEY), CUSTOMDATAURL);
    }


    private void generateCustomProperties() throws IOException {
        if(Files.exists(customProperties)) return;

        Properties properties = new Properties();

        InputStream input = this.getClass().getClassLoader().getResourceAsStream(CUSTOMPROPERTIES);
        properties.load(input);
        input.close();

        OutputStream output = new FileOutputStream(customProperties.toFile());
        properties.store(output, "Application Properties");
        output.close();
    }
}