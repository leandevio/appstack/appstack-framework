package io.leandev.appstack.ftp;

import io.leandev.appstack.crypto.PlainPasswordEncoder;
import io.leandev.appstack.json.JsonParser;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


import static org.assertj.core.api.Assertions.*;

public class FtpTest {
    private static String DATAFILE = "FtpUsers.json";
    private Path DATAPATH = Paths.get("/").resolve(this.getClass().getPackage().getName().replaceAll("\\.", "/")).resolve(DATAFILE);
    private JsonParser parser = new JsonParser();
    private FtpServer ftpServer;
    private List<FtpUser> ftpUsers;

    @BeforeTest
    public void setUp() throws Exception {
        InputStream inputStream = this.getClass().getResourceAsStream(DATAPATH.toString());
        ftpUsers = parser.readValueAsList(inputStream, FtpUser.class);
        inputStream.close();
        FtpUserManager ftpUserManager = FtpInMemoryUserManagerBuilder.newInMemoryUserManager()
                .withPasswordEncoder(new PlainPasswordEncoder())
                .withUsers(ftpUsers)
                .build();
        ftpServer = FtpServerBuilder.newFtpServer()
                .onPort(2121)
                .withUserManager(ftpUserManager)
                .build();
        ftpServer.startup();
    }

    @Test
    public void testUploadDownload() throws Exception {
        FtpClient ftpClient = new FtpClient();
        FtpUser ftpUser = ftpUsers.get(0);

        // connect
        ftpClient.connect("localhost", 2121);

        // login
        ftpClient.login(ftpUser.getUsername(), ftpUser.getPassword());

        // prepare the data
        Path path = Paths.get("FtpUsers.json");
        InputStream inputStream = this.getClass().getResourceAsStream(DATAPATH.toString());
        byte[] expect = readData(inputStream);

        // upload the data
        inputStream = this.getClass().getResourceAsStream(DATAPATH.toString());
        ftpClient.upload(path, inputStream);
        inputStream.close();

        // download the data
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ftpClient.download(path, outputStream);
        byte[] actual = outputStream.toByteArray();
        assertThat(expect).isEqualTo(actual);
    }

    private byte[] readData(InputStream inputStream) throws IOException {
        byte[] bytes = new byte[inputStream.available()];
        inputStream.read(bytes);
        inputStream.close();
        return bytes;
    }

    @AfterTest
    public void tearDown() throws Exception {
        ftpServer.shutdown();
    }
}