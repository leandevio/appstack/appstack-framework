package io.leandev.appstack.workbook;

import io.leandev.appstack.doc.Align;
import io.leandev.appstack.doc.BorderStyle;
import io.leandev.appstack.doc.Color;
import io.leandev.appstack.formatter.DataFormat;
import io.leandev.appstack.json.JsonParser;
import io.leandev.appstack.util.Collection;
import io.leandev.appstack.util.Environment;
import io.leandev.appstack.util.Model;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

public class XlsxWorkbookTest {
    private final String DATAFILE = "Product.json";
    private final Path DATA = Paths.get("/data").resolve(DATAFILE);
    private final String FILENAME = "product.xlsx";
    private final Path SOURCE = Paths.get("/").resolve(this.getClass().getPackage().getName().replaceAll("\\.", "/")).resolve(FILENAME);
    private final Path TARGET = Environment.getDefaultInstance().tmpFolder().resolve(FILENAME);
    private final JsonParser jsonParser = new JsonParser();
    private final String[] FIELDS = {"id", "name", "description", "price", "availability", "width", "height", "createdOn"};

    private Collection data;

    @BeforeMethod
    public void setUp() throws IOException {
        InputStream input = this.getClass().getResourceAsStream(DATA.toString());
        data = jsonParser.readValue(input, Collection.class);
        input.close();
    }

    @Test
    public void testWrite() throws Exception {
        FileOutputStream output = new FileOutputStream(TARGET.toString());
        XlsxWorkbook workbook = new XlsxWorkbook();
        XlsxWorksheet worksheet = workbook.createWorksheet();
        for(Model datum : data) {
            XlsxRow row = worksheet.createRow();
            for(String name : FIELDS) {
                XlsxCell cell = row.createCell();;
                Object value;
                if(name.equals("createdOn")) {
                    value = datum.getAsDate(name);
                } else {
                    value = datum.getValue(name);
                }
                cell.setValue(value);
            }
        }
        workbook.autoSizeColumns();
        workbook.write(output);
        output.close();
    }

    @Test
    public void testWriteWithStyle() throws Exception {
        FileOutputStream output = new FileOutputStream(TARGET.toString());
        XlsxWorkbook workbook = new XlsxWorkbook();
        workbook.setLocale(Locale.TRADITIONAL_CHINESE);
        XlsxWorksheet worksheet = workbook.createWorksheet();
        worksheet.setGridlines(false);
        XlsxRow columnHeaders = worksheet.createRow();
        for(String field : FIELDS) {
            XlsxCell columnHeader = columnHeaders.createCell();
            columnHeader.setBorderStyle(BorderStyle.SOLID);
            columnHeader.setBorderColor(Color.WHITE);
            columnHeader.setBorderBottomStyle(BorderStyle.DOUBLE);
            columnHeader.setBorderBottomColor(Color.DARKBLUE);
            columnHeader.setBackground(Color.BLUE);
            columnHeader.setColor(Color.WHITE);
            columnHeader.setAlign(Align.CENTER);
            columnHeader.setValue(field);
        }
        for(Model datum : data) {
            XlsxRow row = worksheet.createRow();
            for(String name : FIELDS) {
                XlsxCell cell = row.createCell();
                cell.setBorderColor(Color.DARKBLUE);
                cell.setBorderStyle(BorderStyle.DOTTED);
                cell.setBackground(Color.LIGHTBLUE);
                cell.setColor(Color.DARKBLUE);
                Object value = datum.getValue(name);
                if(name.equals("id")) {
                    cell.setAlign(Align.CENTER);
                    cell.setBorderStyle(BorderStyle.NONE);
                    cell.setBorderColor(Color.DARKBLUE);
                    cell.setBorderRightStyle(BorderStyle.SOLID);
                } else if(name.equals("createdOn")) {
                    value = datum.getAsDate(name);
                    cell.setDataFormat(DataFormat.DATE);
                } else if(name.equals("price")) {
                    cell.setDataFormat(DataFormat.MONEY);
                }
                cell.setValue(value);
            }
        }
        worksheet.autoSizeColumns();
        worksheet.setBorderColor(Color.DARKBLUE);
        worksheet.setBorderStyle(BorderStyle.SOLID);
        workbook.write(output);
        output.close();
    }
}