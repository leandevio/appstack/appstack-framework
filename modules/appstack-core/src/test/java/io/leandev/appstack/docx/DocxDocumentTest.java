package io.leandev.appstack.docx;

import io.leandev.appstack.font.FontFamily;
import io.leandev.appstack.image.*;
import io.leandev.appstack.image.Canvas;
import io.leandev.appstack.measure.Length;
import io.leandev.appstack.measure.Paper;
import io.leandev.appstack.nls.ChineseTranslator;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import io.leandev.appstack.util.Environment;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.Buffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

public class DocxDocumentTest {
    private final Class clazz = this.getClass();
    private final Path RESOURCE = Paths.get(clazz.getPackage().getName().replaceAll("\\.", "/"));
    private final Path OUTPUT = Environment.getDefaultInstance().tmpFolder();
    private final ClassLoader LOADER = clazz.getClassLoader();

   @BeforeMethod
    public void setUp() {
    }

//    @Test
//    public void testCreateDocument() throws Exception {
//        Path TARGETFILE = OUTPUT.resolve("simple.docx");
//        //Write the Document in file system
//        FileOutputStream out = new FileOutputStream(TARGETFILE.toFile());
//
//        Document document = new DocxDocument();
//
//        Heading heading = document.createHeading(1);
//        heading.appendChunk(document.createTextRun("標題一"));
//
//        Paragraph paragraph = document.createParagraph();
//        paragraph.appendChunk(document.createTextRun("標題一"));
//
//        document.appendBlock(heading);
//        document.appendBlock(paragraph);
//
//        document.write(out);
//
//        out.close();
//
//    }

    @Test
    public void testStampDocument() throws Throwable {
        Path OUTPUTFILE = OUTPUT.resolve("stamp.docx");
        InputStream template = LOADER.getResourceAsStream(RESOURCE.resolve("template.docx").toString());
        FileOutputStream output = new FileOutputStream(OUTPUTFILE.toFile());

        DocxDocument document = new DocxDocument(template);

        // 在第一頁的標題加上一條彩色的線(圖片)
        DocxParagraph firstParagraph = document.getFirstParagraph(true).get();
        InputStream lineImage = LOADER.getResourceAsStream(RESOURCE.resolve("line.png").toString());
        Length margin = Length.ofCentimeter(2.54);
        DocxImage line = firstParagraph.createImage(lineImage, Paper.A4.width.subtract(margin.multiply(2)), Length.ofCentimeter(0.5));
        line.setPosition(margin, Length.ofCentimeter(9.45));;

        // 產生一個有提示字的 QR Code 與印章的簽名橫幅
        BufferedImage signatureBannerImage = generateSignatureBanner();
        DocxImage signatureBanner = firstParagraph.createImage(signatureBannerImage, Paper.A4.width.subtract(margin.multiply(2)));
        signatureBanner.setPosition(margin, Length.ofCentimeter(12));

        // 使用書籤(bookmark)找到特定的段落．
        DocxParagraph introduction = document.getParagraphByBookmark("Introduction").get();
        // 變更將該段落的文字內容．事實上，這個方式等於是在該段落建立一個 TextRun，然後指定 TextRun 的文字內容．
        introduction.setText("測試計劃記錄用於驗證和確保產品或系統符合其設計規範和其他要求的策略。測試計劃通常由測試工程師編製，或由測試工程師提供大量投入。");
        // 在 introduction 段落後加上一個新的段落，並指定文字內容．
        document.createParagraphAfter(introduction)
                .setText("該計劃確定要測試的專案、要測試的功能、要執行的測試類型、負責測試的人員、完成測試所需的資源和計劃以及與計劃相關的風險。");

        // 搜尋文字內容找到特定段落
        DocxParagraph inScope = document.getParagraphByText("Scope defines the features").get();
        inScope.setText("範圍定義將要測試的軟體的功能、功能或非功能性要求。");

        DocxParagraph outOfScope = document.getParagraphByBookmark("Out_of_Scope").get();
        outOfScope.setText("這些功能不進行測試，因為它們不包括在軟體要求規範中。");
        // 使用名稱取得樣式物件(Style) ，用來指定段落的樣式．
        DocxStyle bulletStyle = document.getStyleByName("List Bullet 2").get();
        // 新增段落並指定文字內容與樣式．
        DocxParagraph item = document.createParagraphAfter(outOfScope).withText("用戶介面").withStyle(bulletStyle);
        item = document.createParagraphAfter(item).withText("硬體介面").withStyle(bulletStyle);
        item = document.createParagraphAfter(item).withText("軟體介面").withStyle(bulletStyle);
        item = document.createParagraphAfter(item).withText("資料庫邏輯").withStyle(bulletStyle);
        item = document.createParagraphAfter(item).withText("通信介面").withStyle(bulletStyle);
        document.createParagraphAfter(item).withText("網站安全性和性能").withStyle(bulletStyle);

        // 使用書籤(bookmark)找到特定的表格(table)．
        DocxTable docxTable = document.getTableByBookmark("Feature_List").get();
        DocxStyle compactStyle = document.getStyleByName("Compact").get();
        // 在表格內新增一列(table row)．
        DocxTableRow docxTableRow = docxTable.createRow();
        docxTableRow.getCell(0).setText("餘額查詢");
        // 取得該列的第一的表格單元(table cell)．
        DocxTableCell docxTableCell_1 = docxTableRow.getCell(1);
        // 取得表格單元內的第一個段落，然後指定文字內容與樣式．指定 autoCreate=true 可以在不含段落的表格單元建立一個新的段落．
        docxTableCell_1.getFirstParagraph(true).get().withText("經理").withStyle(compactStyle);
        // 在同樣的表格單元新增一個段落．
        docxTableCell_1.createParagraph("客戶").withStyle(compactStyle);
        DocxTableCell docxTableCell_2 = docxTableRow.getCell(2);
        docxTableCell_2.getFirstParagraph(true).get().withText("客戶：客戶可以有多個銀行帳戶。他僅可以查看其帳戶的餘額。").withStyle(compactStyle);
        docxTableCell_2.createParagraph("經理：經理可以查看所有在他的監督下客戶的餘額").withStyle(compactStyle);

        DocxParagraph blank = document.createParagraphAfter(docxTable);

        // 建立一個新表格並複製現有的表格內容與格式
        DocxTable newTable = document.createTableAfter(blank);
        docxTable.copyTo(newTable);

        DocxParagraph testMethodologyOverview = document.getParagraphByBookmark("Test_Methodology_Overview").get();
        DocxParagraph nextParagraph = document.getParagraphAfter(testMethodologyOverview);
        while(!nextParagraph.isH2()) {
            document.removeParagraph(nextParagraph);
            nextParagraph = document.getParagraphAfter(testMethodologyOverview);
        }

        testMethodologyOverview.setText("本專案將採用敏捷方法（Agile methodology）的測試方式．");
        DocxParagraph agileMethodologyFigure = document.createParagraphAfter(testMethodologyOverview);
        DocxStyle figureStyle = document.getStyleByName("Figure").get();
        // 插入一張圖片．
        InputStream agileMethodologyImage = LOADER.getResourceAsStream(RESOURCE.resolve("agile_development_model.gif").toString());
        agileMethodologyFigure.createImage(agileMethodologyImage, Paper.A4.width.divide(2).subtract(margin));
        agileMethodologyFigure.setStyle(figureStyle);

        // 在第一頁的頁首插入 logo
        InputStream logoStream = LOADER.getResourceAsStream(RESOURCE.resolve("logo.png").toString());
        ImageReader imageReader = new ImageReader(logoStream);
        BufferedImage logoImage = imageReader.read();
        imageReader.close();
        logoStream.close();
        DocxHeader firstDocxHeader = document.getFirstHeader();
        DocxImage logo = firstDocxHeader.getFirstParagraph(true).get().createImage(logoImage, Length.ofCentimeter(2));
        logo.setPosition(Length.ofCentimeter(9.5), Length.ofCentimeter(1.5));

        // 在第二頁開始的頁首插入 QR code
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BufferedImage qrCodeImage = qrCodeWriter.encode("http://www.leandev.io", Length.ofCentimeter(1), Length.ofCentimeter(1));
        DocxHeader docxHeader = document.getHeader();
        DocxImage qrCode = docxHeader.getFirstParagraph(true).get().createImage(qrCodeImage);
        qrCode.setPosition(Length.ofCentimeter(2), Length.ofCentimeter(25.7));

        document.write(output);
    }

    private BufferedImage generateSignatureBanner() throws IOException, ImageException {
        Length margin = Length.ofCentimeter(2.54);
        Length width = Paper.A4.width.subtract(margin.multiply(2));
        Length height = Length.ofCentimeter(8);
        Canvas canvas = new Canvas(width, height);

        // 在右半邊的正中央嵌入印章(圖片)
        ImageReader imageReader = new ImageReader(LOADER.getResourceAsStream(RESOURCE.resolve("verified.png").toString()));
        BufferedImage verifiedImage = imageReader.read();
        imageReader.close();
        ImageProcessor imageProcessor = new ImageProcessor();
        // 保持長寬比，將印章圖片縮小到 5 公分寬
        Length scaledWidth = Length.ofCentimeter(5);
        verifiedImage = imageProcessor.scaleByWidth(verifiedImage, scaledWidth);
        Rectangle rightRect = canvas.createRectangle(width.divide(2), Length.ofCentimeter(0), width.divide(2), height);
        canvas.drawImage(verifiedImage, rightRect);
        // 在右半邊的最下方靠左嵌入日期
        LocalDate today = LocalDate.now();
        ChineseTranslator translator = new ChineseTranslator();
        String year = translator.translate(today.getYear()-1911, false);
        String month = translator.translate(today.getMonthValue());
        String day = translator.translate(today.getDayOfMonth());
        String todayText = String.format("中華民國%s年%s月%s日", year, month, day);
        Font dateFont = canvas.createFont(FontFamily.BiauKai, Font.PLAIN, 12);
        canvas.setFont(dateFont);
        canvas.drawText(todayText, rightRect, Position.BOTTOM);

        // 在左半邊的正中央嵌入 QR Code．
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        Length qrCodeWidth = Length.ofCentimeter(2);
        Length qrCodeHeight = qrCodeWidth;
        BufferedImage qrcodeImage = qrCodeWriter.encode("http://www.leandev.io", qrCodeWidth, qrCodeHeight);
        Rectangle leftRect = canvas.createRectangle(Length.ofCentimeter(0), Length.ofCentimeter(0), width.divide(2), height);
        canvas.drawImage(qrcodeImage, leftRect);
        String hint = "掃描辨認真偽";
        Length fontSize = qrCodeWidth.divide(6);
        Font hintFont = canvas.createFont(Locale.TRADITIONAL_CHINESE, Font.PLAIN, fontSize);
        Rectangle hintRect = canvas.createRectangle(width.divide(2).subtract(qrCodeWidth).divide(2),
                height.divide(2).add(qrCodeHeight.divide(2)),
                qrCodeWidth,
                fontSize.multiply(1.2));
        canvas.setFont(hintFont);
        canvas.drawText(hint, hintRect, Position.BOTTOM);

        return canvas.getBufferedImage();
    }
}
