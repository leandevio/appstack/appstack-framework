package io.leandev.appstack.pdf;

import io.leandev.appstack.text.TextFileWriter;
import io.leandev.appstack.util.Environment;
import io.leandev.appstack.pdf.signature.CreateSignature;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.text.PDFTextStripper;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;

public class PDFBoxTest {
    private final Path CONF = Environment.getDefaultInstance().appHome().resolve("conf");
    private final Path RESOURCE = Paths.get("/").resolve(this.getClass().getPackage().getName().replaceAll("\\.", "/"));
    private final Path OUTPUT = Environment.getDefaultInstance().tmpFolder();

    private static String SOURCEFILE = "report.pdf";
    private static Path SOURCEPATH = Paths.get("/").resolve(PdfDocumentTest.class.getPackage().getName().replaceAll("\\.", "/")).resolve(SOURCEFILE);
    private static String TARGETFILE = "report.pdf";
    private final Path TARGETPATH = Environment.getDefaultInstance().tmpFolder().resolve(TARGETFILE);


    @BeforeMethod
    public void setUp() {
    }

    @Test
    public void testWrite() throws Exception {
        Path TARGETFILE = OUTPUT.resolve("simple.pdf");
        FileOutputStream out = new FileOutputStream(TARGETFILE.toFile());

        PDDocument document = new PDDocument();

        PDPage pageOne = new PDPage();

        document.addPage(pageOne);

        PDPageContentStream contentStream = new PDPageContentStream(document, pageOne);

        //Begin the Content stream
        contentStream.beginText();

        String text = "This is an example of adding text to a page in the pdf document. we can add as many lines as we want like this using the ShowText() method of the ContentStream class.";

        //Adding text in the form of string
        PDFont font = PDType1Font.HELVETICA_BOLD; // Or whatever font you want.
        int fontSize = 16; // Or whatever font size you want.
        contentStream.setFont(font, fontSize);
        int paragraphWidth = 200;

        int start = 0;
        int end = 0;
        int height = 10;

        contentStream.setLeading(fontSize*1.5f);
        contentStream.newLineAtOffset(25, 725);

        for ( int i : possibleWrapPoints(text) ) {
            float width = font.getStringWidth(text.substring(start,i)) / 1000 * fontSize;
            if ( start < end && width > paragraphWidth ) {
                // Draw partial text and increase height
                contentStream.showText(text.substring(start,end));
                contentStream.newLine();
                height += font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
                start = end;
            }
            end = i;
        }
        // Last piece of text
        contentStream.showText(text.substring(start));
        //Ending the content stream
        contentStream.endText();

        //Closing the content stream
        contentStream.close();

        //Saving the document
        document.save(out);

        //Closing the document
        document.close();

    }

    int[] possibleWrapPoints(String text) {
        String[] split = text.split("(?<=\\W)");
        int[] ret = new int[split.length];
        ret[0] = split[0].length();
        for ( int i = 1 ; i < split.length ; i++ )
            ret[i] = ret[i-1] + split[i].length();
        return ret;
    }

    @Test
    public void testWriteChinese() throws Exception {
        // Create a Document object.
        PDDocument pdDocument = new PDDocument();

        // Create a Page object
        PDPage pdPage = new PDPage();
        // Add the page to the document and save the document to a desired file.
        pdDocument.addPage(pdPage);
        // Create a Content Stream
        PDPageContentStream pdPageContentStream = new PDPageContentStream(pdDocument, pdPage);

        // Start the stream
        pdPageContentStream.beginText();

        // Set the X and Y corodinates for the text to be positioned
        pdPageContentStream.newLineAtOffset(25, 700);

        // Set a Font and its Size
        // We cannot use the standard fonts provided.
        // pdPageContentStream.setFont(PDType1Font.HELVETICA, 12);
        InputStream font = this.getClass().getResourceAsStream(RESOURCE.resolve("kaiu.ttf").toString());
        PDFont unicodeFont = PDType0Font.load(pdDocument, font);
        pdPageContentStream.setFont(unicodeFont, 14);

        pdPageContentStream.showText("國立暨南大學");

        // End the Stream
        pdPageContentStream.endText();

        // Once all the content is written, close the stream
        pdPageContentStream.close();
        Path TARGETFILE = OUTPUT.resolve("simple.pdf");
        pdDocument.save(TARGETFILE.toFile());
        pdDocument.close();
        System.out.println("PDF saved to the location !!!");

    }

    @Test
    public void testRead() throws Exception {
        Path inputFile = OUTPUT.resolve("samlee.pdf");
        FileInputStream input = new FileInputStream(inputFile.toFile());

        PDDocument document = PDDocument.load(input);

        //Instantiate PDFTextStripper class
        PDFTextStripper pdfStripper = new PDFTextStripper();

        //Retrieving text from PDF document
        String text = pdfStripper.getText(document);

        Path outputFile = OUTPUT.resolve("extract_text_from_pdf.txt");
        FileOutputStream output = new FileOutputStream(outputFile.toFile());
        TextFileWriter textFileWriter = new TextFileWriter(output);
        textFileWriter.write(text);
        textFileWriter.close();
        output.close();

        //Closing the document
        document.close();

    }

    @Test
    public void testAccessPermission() throws Exception {
        InputStream inputStream = PdfDocumentTest.class.getResourceAsStream(SOURCEPATH.toString());
        OutputStream outputStream = new FileOutputStream(TARGETPATH.toFile());
        PDDocument pdDocument = PDDocument.load(inputStream);

        AccessPermission accessPermission = new AccessPermission();
        accessPermission.setCanModify(false);
        accessPermission.setCanExtractContent(false);
        accessPermission.setCanPrint(false);
        accessPermission.setCanPrintDegraded(false);
        accessPermission.setCanAssembleDocument(false);
        accessPermission.setCanExtractForAccessibility(false);
        accessPermission.setCanFillInForm(false);
        accessPermission.setCanModifyAnnotations(false);
        StandardProtectionPolicy spp = new StandardProtectionPolicy("1234", "0000", accessPermission);
        spp.setEncryptionKeyLength(128);
        spp.setPermissions(accessPermission);

        pdDocument.protect(spp);

        pdDocument.save(outputStream);
        pdDocument.close();

        inputStream.close();
        outputStream.close();
    }

    @Test
    public void testDigitalSignature() throws Exception {
        // load the keystore
        KeyStore keystore = KeyStore.getInstance("PKCS12");
        char[] password = "123456".toCharArray();
        keystore.load(new FileInputStream(CONF.resolve("keystore.p12").toFile()), password);

        // sign PDF
        CreateSignature signing = new CreateSignature(keystore, password);
        signing.setExternalSigning(false);

        File inFile = OUTPUT.resolve("simple.pdf").toFile();

        File outFile = OUTPUT.resolve("simple_signed.pdf").toFile();
        signing.signDetached(inFile, outFile, null);

    }
}
