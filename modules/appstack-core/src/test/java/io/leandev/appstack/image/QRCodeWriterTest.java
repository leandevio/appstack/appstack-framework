package io.leandev.appstack.image;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.awt.image.BufferedImage;

import static org.testng.Assert.*;

public class QRCodeWriterTest {

    @BeforeMethod
    public void setUp() {
    }

    @Test
    public void testQRCode() throws ImageException {
        final String url = "http://www.leandev.io";
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BufferedImage qrcode = qrCodeWriter.encode(url, 200, 200);

        QRCodeReader qrCodeReader = new QRCodeReader();
        String text = qrCodeReader.decode(qrcode);

        assertEquals(text, url);
    }

}