package io.leandev.appstack.geo;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class GeoEuclideanDistanceServiceTest {

    @Test
    public void testComputeEuclideanDistance() {
        GeoDistanceService geoDistanceService = new GeoDistanceService();
        GeoLocation origin = new GeoLocation(32.9697, -96.80322);
        GeoLocation destination = new GeoLocation(29.46786, -98.53506);

        EuclideanDistance euclideanDistance = geoDistanceService.computeEuclideanDistance(origin, destination);

        assertEquals(euclideanDistance.distance(), 422738.9313940138);
    }
}