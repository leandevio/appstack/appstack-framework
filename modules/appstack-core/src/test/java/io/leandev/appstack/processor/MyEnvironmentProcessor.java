package io.leandev.appstack.processor;

import io.leandev.appstack.util.Environment;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;

import java.util.HashMap;
import java.util.Map;

public class MyEnvironmentProcessor implements EnvironmentProcessor {

    @Override
    public void postProcess(Environment environment) {

    }

    @Override
    public void postProcess(Environment environment, Map<String, Object> props) {
        props.put("server.port", "9999");
    }
}
