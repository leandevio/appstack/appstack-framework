package io.leandev.appstack.reflect;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

import java.util.Set;

public class ReflectionTest {

    @Test
    public void testGetTypesAnnotatedWith() {
        Reflection reflection = new Reflection("io.leandev.appstack.reflect");

        Set<Class<?>> annotated = reflection.getTypesAnnotatedWith(MyAnnotation.class);

        assertEquals(annotated.size(), 1);

        assertTrue(annotated.contains(MyAnnotatedType.class));
    }

    @Test
    public void testGetSubTypesOf() {
        Reflection reflection = new Reflection("io.leandev.appstack.reflect");

        Set<Class<? extends MyInterface>> annotated = reflection.getSubTypesOf(MyInterface.class);

        assertEquals(annotated.size(), 1);

        assertTrue(annotated.contains(MyImplementation.class));
    }
}