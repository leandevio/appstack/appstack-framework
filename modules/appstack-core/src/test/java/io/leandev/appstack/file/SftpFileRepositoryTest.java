package io.leandev.appstack.file;

import io.leandev.appstack.env.Environ;
import io.leandev.appstack.text.TextReader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class SftpFileRepositoryTest {
    private static Environ environ = Environ.getInstance();

    private static final Class CLASS = SftpFileRepositoryTest.class;
    private static final ClassLoader CLASSLOADER = SftpFileRepositoryTest.class.getClassLoader();
    private final Path RESOURCE = Paths.get(CLASS.getPackage().getName().replaceAll("\\.", "/"));
    private static final Path OUTPUT = environ.tmpFolder().resolve(CLASS.getSimpleName());

    private static final String HOSTNAME = "sitapp.leandev.io";
    private static final Integer PORT = 22;
    private static final String USERNAME = "appfusemgr";
    private static final String PASSWORD = "appfuse1943";


    @BeforeAll
    static void setup() throws IOException {
        if(!Files.exists(OUTPUT)) {
            Files.createDirectory(OUTPUT);
        }
    }

    /**
     * 將 binary 的資料輸出到指定路徑的檔案(sftp://sitapp.leandev.io/tmp/Yellow Green Christmas Gift Box.jpg)。
     */
    @Test
    public void testWriteBytesToFile() throws Exception {
        String filename = "Yellow Green Christmas Gift Box.jpg";
        Path path = Paths.get("tmp").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new SftpFileRepository(HOSTNAME, PORT, USERNAME, PASSWORD);
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCE.resolve(filename).toString());
            ByteArrayOutputStream buffer = new ByteArrayOutputStream()) {
            this.transferTo(inputStream, buffer);
            byte[] bytes = buffer.toByteArray();
            // 輸出到檔案
            fileRepository.write(path, bytes, true);
        }
    }


    /**
     * 將文字內容輸出到指定路徑的檔案(sftp://sitapp.leandev.io/tmp/product.json)。
     */
    @Test
    public void testWriteTextToFile() throws Exception {
        String filename = "products.json";
        Path path = Paths.get("tmp").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new SftpFileRepository(HOSTNAME, PORT, USERNAME, PASSWORD);
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCE.resolve(filename).toString());
            TextReader textReader = new TextReader(inputStream)) {
            String text = textReader.readAll();
            // 輸出到檔案
            fileRepository.write(path, text, true);
        }
    }

    /**
     * 從指定路徑的檔案(sftp://sitapp.leandev.io/tmp/product.json)讀取檔案內容。
     */
    @Test
    public void testGetFileContent() throws Exception {
        String filename = "products.json";
        Path path = Paths.get("tmp").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new SftpFileRepository(HOSTNAME, PORT, USERNAME, PASSWORD);
        // 讀取檔案
        try(InputStream inputStream = fileRepository.newInputStream(path);
            TextReader textReader = new TextReader(inputStream)) {
            String text = textReader.readAll();
            System.out.println(text);
        }
    }

    /**
     * 檢查指定檔案路徑(sftp://sitapp.leandev.io/tmp/products.json)的檔案是否存在。
     */
    @Test
    public void testExists() throws Exception {
        String filename = "products.json";
        Path path = Paths.get("tmp").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new SftpFileRepository(HOSTNAME, PORT, USERNAME, PASSWORD);
        // 列出資料夾下的檔案路徑
        assertThat(fileRepository.exists(path)).isTrue();
    }

    /**
     * 查詢指定檔案路徑(http://sitapp.leandev.io/tmp/product.json)的檔案大小。
     */
    @Test
    public void testSize() throws Exception {
        String filename = "products.json";
        Path path = Paths.get("tmp").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new SftpFileRepository(HOSTNAME, PORT, USERNAME, PASSWORD);
        // 列出資料夾下的檔案路徑
        assertThat(fileRepository.size(path)).isGreaterThan(0);
    }


    /**
     * 檢查指定的檔案路徑(sftp://sitapp.leandev.io/tmp)是否為目錄。
     */
    @Test
    public void testIsDirectory() throws Exception {
        Path path = Paths.get("tmp");
        // 初始化 FileRepository
        FileRepository fileRepository = new SftpFileRepository(HOSTNAME, PORT, USERNAME, PASSWORD);
        // 列出資料夾下的檔案路徑
        assertThat(fileRepository.isDirectory(path)).isTrue();
    }

    /**
     * 列出資料夾(sftp://sitapp.leandev.io/tmp)下面的檔案路徑。
     */
    @Test
    public void testListFolder() throws Exception {
        Path path = Paths.get("tmp");
        // 初始化 FileRepository
        try(FileRepository fileRepository0 = new SftpFileRepository(HOSTNAME, PORT, USERNAME, PASSWORD)) {
            // 列出資料夾下的檔案路徑
            Stream<Path> files = fileRepository0.list(path);
            files.forEach(file -> {
                String type;
                FileRepository fileRepository = new SftpFileRepository(HOSTNAME, PORT, USERNAME, PASSWORD);
                if(fileRepository.isDirectory(file)) {
                    type = "directory";
                } else if(fileRepository.isRegularFile(file)) {
                    type = "file";
                } else {
                    type = "unknown";
                }
                assertThat(fileRepository.exists(file)).isTrue();
                assertThat(fileRepository.exists(path.resolve(UUID.randomUUID().toString()))).isFalse();
                String message = String.format("%s: %s", file.toString(), type);
                System.out.println(message);
                fileRepository.close();
            });
        }
    }

    /**
     * 刪除指定路徑(sftp://sitapp.leandev.io/tmp/products.json)的檔案。
     */
    @Test
    public void testDeleteFile() throws Exception {
        String filename = "products.json";
        Path uri = Paths.get("tmp").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new SftpFileRepository(HOSTNAME, PORT, USERNAME, PASSWORD);
        // 刪除檔案
        fileRepository.deleteIfExists(uri);
    }

    /**
     * 移動檔案(sftp://sitapp.leandev.io/tmp/products.json)到指定路徑(sftp://sitapp.leandev.io/archive/products.json)。
     */
    @Test
    public void testMoveFile() throws Exception {
        String filename = "products.json";
        Path source = Paths.get("tmp").resolve(filename);
        Path target = Paths.get("archive").resolve(filename);
        // 初始化 FileRepository
        FileRepository fileRepository = new SftpFileRepository(HOSTNAME, PORT, USERNAME, PASSWORD);
        // 移動檔案
        fileRepository.move(source, target);
    }

    @Test
    public void testWriteToOutputStream() throws Exception {
        String filename = "products.json";
        Path path = Paths.get("tmp").resolve(filename);
        FileRepository fileRepository = new SftpFileRepository(HOSTNAME, PORT, USERNAME, PASSWORD);
        try(InputStream inputStream = CLASSLOADER.getResourceAsStream(RESOURCE.resolve(filename).toString());
            OutputStream outputStream = fileRepository.newOutputStream(path, true)) {
            transferTo(inputStream, outputStream);
        }
    }


    private void transferTo(InputStream source, OutputStream target) throws IOException {
        byte[] buffer = new byte[8192];
        int length;
        while ((length = source.read(buffer)) > 0) {
            target.write(buffer, 0, length);
        }
    }

}
