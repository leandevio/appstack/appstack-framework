package io.leandev.appstack.cache;

import io.leandev.appstack.env.Environ;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class CacheTest {
    private static final Class CLASS = CacheTest.class;
    private static final ClassLoader CLASSLOADER = CacheTest.class.getClassLoader();
    private static final Path RESOURCES = Paths.get(CLASS.getPackage().getName().replaceAll("\\.", "/"));
    private static final Path OUTPUT = Environ.getInstance().tmpFolder().resolve(CLASS.getSimpleName());
    private static final Path CACHE = Environ.getInstance().cacheFolder().resolve(CLASS.getSimpleName());

    @BeforeAll
    static void setup() throws IOException {
        if(!Files.exists(OUTPUT)) {
            Files.createDirectory(OUTPUT);
        }
    }

    @Test
    public void testSimpleCache() {
        CacheManager cacheManager = CacheManagerBuilder.newCacheManager()
                .build();

        Cache<Integer, Integer> squareNumberCache = CacheBuilder.newCache(cacheManager, "squareNumber", Integer.class, Integer.class)
                .build();

        Cache<Integer, Integer> $squareNumberCache = cacheManager.getCache("squareNumber", Integer.class, Integer.class);

        Integer number = 4, squareNumber = number * number;
        squareNumberCache.put(number, squareNumber);

        assertThat($squareNumberCache).isEqualTo(squareNumberCache);
        assertThat($squareNumberCache.get(number)).isEqualTo(squareNumber);

        cacheManager.close();
    }

    @Test
    public void testPersistentCache() {
        CacheManager persistentCacheManager = CacheManagerBuilder.newCacheManager()
                .usingPersistence(CACHE)
                .build();

        Cache<String, Account> threeTieredCache = CacheBuilder.newCache(persistentCacheManager, "threeTieredCache", String.class, Account.class)
                .heap(10)
                .offheap(1)
                .disk(20)
                .build();

        Account ellisatkinson = new Account("ellisatkinson", 99, LocalDate.of(1980, 3, 3));
        threeTieredCache.put(ellisatkinson.getName(), ellisatkinson);

        Cache<String, Account> $threeTieredCache = persistentCacheManager.getCache("threeTieredCache", String.class, Account.class);
        Account jasonlin = new Account("jasonlin", 100, LocalDate.of(1990, 4, 1));
        threeTieredCache.put(jasonlin.getName(), jasonlin);

        Account $ellisatkinson = $threeTieredCache.get(ellisatkinson.getName());
        Account $jasonlin = $threeTieredCache.get(jasonlin.getName());

        assertThat($threeTieredCache).isEqualTo(threeTieredCache);

        assertThat($ellisatkinson.getName()).isEqualTo(ellisatkinson.getName());
        assertThat($ellisatkinson.getCredits()).isEqualTo(ellisatkinson.getCredits());
        assertThat($ellisatkinson.getBirthday()).isEqualTo(ellisatkinson.getBirthday());

        assertThat($jasonlin.getName()).isEqualTo(jasonlin.getName());
        assertThat($jasonlin.getCredits()).isEqualTo(jasonlin.getCredits());
        assertThat($jasonlin.getBirthday()).isEqualTo(jasonlin.getBirthday());
    }
}
