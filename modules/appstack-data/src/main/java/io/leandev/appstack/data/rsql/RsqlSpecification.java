package io.leandev.appstack.data.rsql;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import io.leandev.appstack.converter.Converters;
import org.springframework.data.jpa.domain.Specification;

import cz.jirutka.rsql.parser.ast.ComparisonOperator;

public class RsqlSpecification<T> implements Specification<T> {
    private Converters converters = new Converters();

    private String property;
    private ComparisonOperator operator;
    private List<String> arguments;

    public RsqlSpecification(final String property, final ComparisonOperator operator, final List<String> arguments) {
        super();
        this.property = property;
        this.operator = operator;
        this.arguments = arguments;
    }

    @Override
    public Predicate toPredicate(final Root<T> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {
        final List<Object> args = castArguments(root);
        final Object argument = args.get(0);
        switch (RsqlComparator.get(operator)) {
            case EQUAL: {
                if (argument instanceof String) {
                    return builder.like(root.get(property), argument.toString().replace('*', '%'));
                } else if (argument == null) {
                    return builder.isNull(root.get(property));
                } else {
                    return builder.equal(root.get(property), argument);
                }
            }
            case NOT_EQUAL: {
                if (argument instanceof String) {
                    return builder.notLike(root.<String> get(property), argument.toString().replace('*', '%'));
                } else if (argument == null) {
                    return builder.isNotNull(root.get(property));
                } else {
                    return builder.notEqual(root.get(property), argument);
                }
            }
            case GREATER_THAN: {
                if(argument instanceof String) {
                    return builder.greaterThan(root.get(property), (String) argument);
                } else if(argument instanceof Number) {
                    return builder.gt(root.get(property), (Number) argument);
                } else if(argument instanceof Date) {
                    return builder.greaterThan(root.get(property), (Date) argument);
                } else {
                    return builder.greaterThan(root.get(property), argument.toString());
                }

            }
            case GREATER_THAN_OR_EQUAL: {
                if(argument instanceof String) {
                    return builder.greaterThanOrEqualTo(root.get(property), (String) argument);
                } else if(argument instanceof Number) {
                    return builder.ge(root.get(property), (Number) argument);
                } else if(argument instanceof Date) {
                    return builder.greaterThanOrEqualTo(root.get(property), (Date) argument);
                } else {
                    return builder.greaterThanOrEqualTo(root.get(property), argument.toString());
                }
            }
            case LESS_THAN: {
                if(argument instanceof String) {
                    return builder.lessThan(root.get(property), (String) argument);
                } else if(argument instanceof Number) {
                    return builder.lt(root.get(property), (Number) argument);
                } else if(argument instanceof Date) {
                    return builder.lessThan(root.get(property), (Date) argument);
                } else {
                    return builder.lessThan(root.get(property), argument.toString());
                }
            }
            case LESS_THAN_OR_EQUAL: {
                if(argument instanceof String) {
                    return builder.lessThanOrEqualTo(root.get(property), (String) argument);
                } else if(argument instanceof Number) {
                    return builder.le(root.get(property), (Number) argument);
                } else if(argument instanceof Date) {
                    return builder.lessThanOrEqualTo(root.get(property), (Date) argument);
                } else {
                    return builder.lessThanOrEqualTo(root.get(property), argument.toString());
                }
            }
            case IN:
                return root.get(property).in(args);
            case NOT_IN:
                return builder.not(root.get(property).in(args));
            case HAS:
                return builder.isMember(args, root.get(property));
        }

        return null;
    }

    private List<Object> castArguments(final Root<T> root) {

        final Class<? extends Object> type = root.get(property).getJavaType();

        final List<Object> args = arguments.stream().map(arg -> converters.convert(arg, type)).collect(Collectors.toList());

        return args;
    }

}