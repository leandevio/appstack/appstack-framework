package io.leandev.appstack.data.jdbc;

import com.zaxxer.hikari.HikariDataSource;
import io.leandev.appstack.bean.Model;
import org.springframework.boot.jdbc.metadata.HikariDataSourcePoolMetadata;

public class HikariDataSourceAdaptor implements DataSourceAdaptor {
    private static final String[] CONFIGURATION_PROPERTIES = {"PoolName", "JdbcUrl", "Username", "Catalog", "Schema", "DataSourceJNDI", "DriverClassName",  "DataSourceClassName",
            "MaximumPoolSize", "MinimumIdle", "TransactionIsolation", "MaxLifeTime", "KeepaliveTime",
            "LoginTimeout", "ConnectionTimeout", "IdleTimeout", "ValidationTimeout", "InitializationFailTimeout"};

    private static final String[] Metrics_PROPERTIES = {"Active", "Idle", "Usage"};

    private final HikariDataSource hikariDataSource;

    public HikariDataSourceAdaptor(HikariDataSource hikariDataSource) {
        this.hikariDataSource = hikariDataSource;
    }

    public Model<?> getConfiguration() {
        Model<?> model = new Model<>();
        Model<HikariDataSource> properties = new Model(hikariDataSource);
        for(String property : CONFIGURATION_PROPERTIES) {
            String key = property.substring(0, 1).toLowerCase() + property.substring(1);
            model.put(property, properties.get(key));
        }
        return model;
    }

    public Model<?> getMetrics() {
        Model<?> model = new Model<>();
        HikariDataSourcePoolMetadata metadata = new HikariDataSourcePoolMetadata(hikariDataSource);
        Model<HikariDataSourcePoolMetadata> properties = new Model(metadata);
        for(String property : Metrics_PROPERTIES) {
            String key = property.substring(0, 1).toLowerCase() + property.substring(1);
            model.put(property, properties.get(key));
        }
        model.put("Maximum", hikariDataSource.getMaximumPoolSize());
        model.put("Minimum", hikariDataSource.getMinimumIdle());
        return model;
    }
}
