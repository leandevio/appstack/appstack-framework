package io.leandev.appstack.data.jpa;

import io.leandev.appstack.converter.Converters;
import io.leandev.appstack.search.*;
import io.leandev.appstack.search.Predicate;

import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;

public class PredicateBuilder<T> {
    private final CriteriaQuery criteriaQuery;
    private final CriteriaBuilder criteriaBuilder;
    private Root<T> root;
    private Predicate predicate = Predicate.empty();
    private Converters converters = new Converters();

    public PredicateBuilder(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
        this.criteriaQuery = criteriaQuery;
        this.criteriaBuilder = criteriaBuilder;
    }

    public static PredicateBuilder of(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return new PredicateBuilder(criteriaQuery, criteriaBuilder);
    }

//    public static PredicateBuilder of(EntityManager entityManager) {
//        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
//        return new PredicateBuilder(builder);
//    }

    public PredicateBuilder from(Root<T> root) {
        this.root = root;
        return this;
    }

    public PredicateBuilder from(Class<T> entityType, String alias) {
//        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityType);
        this.root = criteriaQuery.from(entityType);
        this.root.alias(alias);
        return this;
    }

    public PredicateBuilder where(Predicate predicate) {
        this.predicate = (predicate==null) ? Predicate.empty() : predicate;
        return this;
    }

    public javax.persistence.criteria.Predicate build() {
        return toPredicate(this.predicate.node());
    }

    private javax.persistence.criteria.Predicate toPredicate(Node node) {
        if(node instanceof ComparisonNode) {
            return toPredicate((ComparisonNode) node);
        } else if(node instanceof LogicalNode) {
            return toPredicate((LogicalNode) node);
        } else {
            return null;
        }
    }

    private javax.persistence.criteria.Predicate toPredicate(ComparisonNode node) {
        javax.persistence.criteria.Predicate predicate;

        String selector = node.selector();
        ComparisonOperator operator = node.operator();
        Path property = toPath(selector);
        Class type = property.getJavaType();
        // 必須做型別轉換，因為由 predicate 經常是由前端透過 http 以 json 的形式送過來，日期的型別需要經過轉換，由 ISO8601 的字串(尤其是有+08:00的時區標示法)轉換成 Date。
        Object expectation = this.convert(node.expectation(), type);
        switch (operator) {
            case IS:
            case EQUAL: {
                if (expectation instanceof String) {
                    String text = (String) expectation;
                    if(text.contains("*")) {
                        predicate = criteriaBuilder.like(property, text.replace('*', '%'));
                    } else {
                        predicate = criteriaBuilder.equal(property, text);
                    }
                } else if (expectation == null) {
                    predicate = criteriaBuilder.isNull(property);
                } else {
                    predicate = criteriaBuilder.equal(property, expectation);
                }
                break;
            }
            case IS_NOT:
            case NOT_EQUAL: {
                if (expectation instanceof String) {
                    predicate = criteriaBuilder.notLike(property, expectation.toString().replace('*', '%'));
                } else if (expectation == null) {
                    predicate = criteriaBuilder.isNotNull(property);
                } else {
                    predicate = criteriaBuilder.notEqual(property, expectation);
                }
                break;
            }
            case GREATER_THAN: {
                if(expectation instanceof String) {
                    predicate = criteriaBuilder.greaterThan(property, (String) expectation);
                } else if(expectation instanceof Number) {
                    predicate = criteriaBuilder.gt(property, (Number) expectation);
                } else if(expectation instanceof Date) {
                    predicate = criteriaBuilder.greaterThan(property, (Date) expectation);
                } else {
                    predicate = criteriaBuilder.greaterThan(property, expectation.toString());
                }
                break;
            }
            case GREATER_THAN_OR_EQUAL: {
                if(expectation instanceof String) {
                    predicate = criteriaBuilder.greaterThanOrEqualTo(property, (String) expectation);
                } else if(expectation instanceof Number) {
                    predicate = criteriaBuilder.ge(property, (Number) expectation);
                } else if(expectation instanceof Date) {
                    predicate = criteriaBuilder.greaterThanOrEqualTo(property, (Date) expectation);
                } else {
                    predicate = criteriaBuilder.greaterThanOrEqualTo(property, expectation.toString());
                }
                break;
            }
            case LESS_THAN: {
                if(expectation instanceof String) {
                    predicate = criteriaBuilder.lessThan(property, (String) expectation);
                } else if(expectation instanceof Number) {
                    predicate = criteriaBuilder.lt(property, (Number) expectation);
                } else if(expectation instanceof Date) {
                    predicate = criteriaBuilder.lessThan(property, (Date) expectation);
                } else {
                    predicate = criteriaBuilder.lessThan(property, expectation.toString());
                }
                break;
            }
            case LESS_THAN_OR_EQUAL: {
                if(expectation instanceof String) {
                    predicate = criteriaBuilder.lessThanOrEqualTo(property, (String) expectation);
                } else if(expectation instanceof Number) {
                    predicate = criteriaBuilder.le(property, (Number) expectation);
                } else if(expectation instanceof Date) {
                    predicate = criteriaBuilder.lessThanOrEqualTo(property, (Date) expectation);
                } else {
                    predicate = criteriaBuilder.lessThanOrEqualTo(property, expectation.toString());
                }
                break;
            }
            case IN:
                predicate = createInClause(criteriaBuilder, property, expectation, type);
                break;
            case NOT_IN:
                predicate = criteriaBuilder.not(createInClause(criteriaBuilder, property, expectation, type));
                break;
            case HAS:
                predicate = criteriaBuilder.isMember(expectation, property);
                break;
            default:
                predicate = null;
        }
        return predicate;

    }

    /**
     * 要透過
     * @param criteriaBuilder
     * @param property
     * @param values
     * @param type
     * @return
     * @param <T>
     */
    private <T> CriteriaBuilder.In<T> createInClause(CriteriaBuilder criteriaBuilder, Path<T> property, Object values, Class<T> type) {
        CriteriaBuilder.In<T> inClause = criteriaBuilder.in(property);
        for (Object value : (Collection) values) {
            inClause.value((T) value);
        }
        return inClause;
    }

    private Path toPath(String field) {
        String[] tokens = field.split("\\.");
        Path path = getStartPath(tokens[0]);

        for(int i = 1; i<tokens.length; i++) {
            path = path.get(tokens[i]);
        }
        return path;
    }

    private Path getStartPath(String token) {
        Path path = null;
        if(token.equals(root.getAlias())) {
            path = root;
        } else {
            for(Join join : root.getJoins()) {
                String alias = join.getAlias();
                if(alias != null && alias.equals(token)) {
                    path = join;
                    break;
                }
            }
        }
        if(path == null) {
            path = root.get(token);
        }
        return path;
    }

    private javax.persistence.criteria.Predicate toPredicate(LogicalNode node) {
        javax.persistence.criteria.Predicate predicate;

        LogicalOperator operator = node.operator();
        javax.persistence.criteria.Predicate[] predicates = node.children().stream()
                .map(child -> toPredicate(child))
                .toArray(size -> new javax.persistence.criteria.Predicate[size]);

        if(operator.equals(LogicalOperator.OR)) {
            predicate = criteriaBuilder.or(predicates);
        } else {
            predicate = criteriaBuilder.and(predicates);
        }

        return predicate;
    }

    private Object convert(Object value, Class type) {
        if(value == null) {
            return null;
        } else if(value.getClass().isArray()) {
            return ((List) Arrays.asList((Object[]) value)).stream().map(item -> converters.convert(item, type)).collect(Collectors.toList());
        } else if(value instanceof Set) {
            return ((Set) value).stream().map(item -> converters.convert(item, type)).collect(Collectors.toSet());
        } else if(value instanceof List) {
            return ((List) value).stream().map(item -> converters.convert(item, type)).collect(Collectors.toList());
        } else {
            return converters.convert(value, type);
        }
    }
}
