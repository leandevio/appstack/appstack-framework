package io.leandev.appstack.data.jdbc;

import io.leandev.appstack.bean.Model;

import javax.sql.DataSource;
public class GenericDataSourceAdaptor implements DataSourceAdaptor {
    private DataSource dataSource;

    public GenericDataSourceAdaptor(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    @Override
    public Model<?> getConfiguration() {
        return new Model<>();
    }

    @Override
    public Model<?> getMetrics() {
        return new Model<>();
    }
}
