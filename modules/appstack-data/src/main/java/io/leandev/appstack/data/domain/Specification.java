package io.leandev.appstack.data.domain;

import io.leandev.appstack.data.jpa.PredicateBuilder;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class Specification<T> implements org.springframework.data.jpa.domain.Specification<T> {
    private final io.leandev.appstack.search.Predicate predicate;

    public Specification(io.leandev.appstack.search.Predicate predicate) {
        this.predicate = predicate;
    }
    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return PredicateBuilder.of(criteriaQuery, criteriaBuilder).from(root).where(predicate).build();
    }
}
