package io.leandev.appstack.data.constant;

public class DataPropsKey {
    public static final String DATADRIVER = "data.driver";
    public static final String DATAURL = "data.url";
    public static final String DATAUSERNSME = "data.username";
    public static final String DATAPASSWORD = "data.password";

    public static final String DATADIALECT = "data.dialect";

    public static final String DATADDLACTION = "data.ddl.action";
    public static final String DATADEBUG = "data.debug";

    public static final String NATIONALIZED = "spring.jpa.properties.hibernate.use_nationalized_character_data";
}
