package io.leandev.appstack.data.jpa;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PageableBuilder {
    int pageNumber;
    int pageSize;
    Sort sort;

    public PageableBuilder(int pageNumber, int pageSize, Sort sort) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.sort = sort;
    }

    public PageableBuilder withDefault(Sort sort) {
        if(this.sort.isSorted()) {
            this.sort = sort;
        }
        return this;
    }

    public PageableBuilder sort(io.leandev.appstack.search.Sort sort) {
        List<Sort.Order> orders = sort.stream().map(order -> {
            Sort.Direction direction = Sort.Direction.valueOf(order.direction().name());
            return new Sort.Order(direction, order.selector());
        }).collect(Collectors.toList());

        this.sort = Sort.by(orders);
        return this;
    }

    public PageableBuilder sort(io.leandev.appstack.search.Sort sort, Function<String,String> converter) {
        List<Sort.Order> orders = sort.stream().map(order -> {
            Sort.Direction direction = Sort.Direction.valueOf(order.direction().name());
            String property = converter.apply(order.selector());
            return new Sort.Order(direction, property);
        }).collect(Collectors.toList());

        this.sort = Sort.by(orders);
        return this;
    }

    public PageableBuilder sort(Sort sort, Function<String,String> converter) {
        List<Sort.Order> orders = sort.stream().map(order -> {
            String property = converter.apply(order.getProperty());
            return new Sort.Order(order.getDirection(), property);
        }).collect(Collectors.toList());

        this.sort = Sort.by(orders);
        return this;
    }

    public PageableBuilder sort(Sort sort, Function<String,String> converter, Sort defaultSort) {
        if(sort.isUnsorted()) {
            this.sort = defaultSort;
        } else {
            List<Sort.Order> orders = sort.stream().map(order -> {
                String property = converter.apply(order.getProperty());
                return new Sort.Order(order.getDirection(), property);
            }).collect(Collectors.toList());
            this.sort = Sort.by(orders);
        }
        return this;
    }

    public PageableBuilder sort(Sort sort, Function<String,String> converter, String defaultSort) {
        return this.sort(sort, converter, Sort.by(defaultSort));
    }


    public Pageable build() {
        return PageRequest.of(pageNumber, pageSize, sort);
    }

    public static PageableBuilder of(Pageable pageable) {
        return new PageableBuilder(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort());
    }


}
