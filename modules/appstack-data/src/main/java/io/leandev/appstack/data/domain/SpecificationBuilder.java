package io.leandev.appstack.data.domain;

import io.leandev.appstack.search.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SpecificationBuilder<T> {
    private Class<T> entityType;
    private Predicate predicate = Predicate.empty();

    public static SpecificationBuilder newInstance() {
        return new SpecificationBuilder();
    }

    public SpecificationBuilder from(Class<T> entityType) {
        this.entityType = entityType;
        return this;
    }

    public SpecificationBuilder where(Predicate predicate) {
        this.predicate = (predicate==null) ? Predicate.empty() : predicate;
        return this;
    }

    public Specification<T> build() {
        return new Specification<>(predicate);
    }
}
