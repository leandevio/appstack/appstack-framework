package io.leandev.appstack.data.jdbc;

import com.zaxxer.hikari.HikariDataSource;
import io.leandev.appstack.bean.Model;

import javax.sql.DataSource;

public class DataSourceProfiler {
    private final DataSourceAdaptor dataSourceAdaptor;

    public DataSourceProfiler(DataSource dataSource) {
        if(dataSource instanceof HikariDataSource) {
            this.dataSourceAdaptor = new HikariDataSourceAdaptor((HikariDataSource) dataSource);
        } else {
            this.dataSourceAdaptor = new GenericDataSourceAdaptor(dataSource);
        }
    }

    public Model<?> getConfiguration() {
        return this.dataSourceAdaptor.getConfiguration();
    }

    public Model<?> getMetrics() {
        return this.dataSourceAdaptor.getMetrics();
    }
}
