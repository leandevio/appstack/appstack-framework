package io.leandev.appstack.data.jpa;


import io.leandev.appstack.search.Sort;
import io.leandev.appstack.search.Predicate;


import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class TupleQueryBuilder<T> {
    private final CriteriaBuilder criteriaBuilder;
    private CriteriaQuery<Tuple> criteriaQuery;
    private Root<T> root;
    private Predicate predicate = Predicate.empty();
    private List<Selection<?>> selections = new ArrayList<>();

    public TupleQueryBuilder(CriteriaBuilder criteriaBuilder) {
        this.criteriaBuilder = criteriaBuilder;
    }

    public TupleQueryBuilder(EntityManager entityManager) {
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    public static TupleQueryBuilder of(EntityManager entityManager) {
        return new TupleQueryBuilder(entityManager);
    }

    public static TupleQueryBuilder of(CriteriaBuilder criteriaBuilder) {
        return new TupleQueryBuilder(criteriaBuilder);
    }

    public TupleQueryBuilder from(Class<T> entityType) {
        this.criteriaQuery =  criteriaBuilder.createTupleQuery();
        this.root = criteriaQuery.from(entityType);
        return this;
    }

    public TupleQueryBuilder from(Class<T> entityType, String alias) {
        this.criteriaQuery =  criteriaBuilder.createTupleQuery();
        this.root = criteriaQuery.from(entityType);
        this.root.alias(alias);
        return this;
    }

    public TupleQueryBuilder join(String property, JoinType joinType, String alias) {
        Join join = this.root.join(property, joinType);
        join.alias(alias);
        return this;
    }

    public TupleQueryBuilder join(String property, JoinType joinType) {
        return this.join(property, joinType, property);
    }

    public TupleQueryBuilder join(String property, String alias) {
        return this.join(property, JoinType.INNER, alias);
    }

    public TupleQueryBuilder join(String property) {
        return this.join(property, JoinType.INNER, property);
    }

    public TupleQueryBuilder where(Predicate predicate) {
        this.predicate = (predicate==null) ? Predicate.empty() : predicate;
        return this;
    }

    public TupleQueryBuilder select(String... fields) {
        for(String field : fields) {
            Path selection = toPath(field);
            this.selections.add(selection.alias(field));
        }
        return this;
    }

    public TupleQueryBuilder selectAs(String field, String alias) {
        Path selection = toPath(field);
        this.selections.add(selection.alias(alias));
        return this;
    }

    public TupleQueryBuilder orderBy(Sort sort) {
        List<Order> orders = new ArrayList<>();
        sort.stream().forEach(order -> {
            if(order.isDescending()) {
                orders.add(criteriaBuilder.desc(toPath(order.selector())));
            } else {
                orders.add(criteriaBuilder.asc(toPath(order.selector())));
            }
        });
        this.criteriaQuery.orderBy(orders);
        return this;
    }

    public TupleQueryBuilder orderBy(org.springframework.data.domain.Sort sort) {
        List<Order> orders = new ArrayList<>();
        sort.stream().forEach(order -> {
            if(order.isDescending()) {
                orders.add(criteriaBuilder.desc(toPath(order.getProperty())));
            } else {
                orders.add(criteriaBuilder.asc(toPath(order.getProperty())));
            }
        });
        this.criteriaQuery.orderBy(orders);
        return this;
    }

    private Path toPath(String field) {
        String[] tokens = field.split("\\.");
        Path path = getStartPath(tokens[0]);

        for(int i = 1; i<tokens.length; i++) {
            path = path.get(tokens[i]);
        }
        return path;
    }

    private Path getStartPath(String token) {
        Path path = null;
        if(token.equals(root.getAlias())) {
            path = root;
        } else {
            for(Join join : root.getJoins()) {
                String alias = join.getAlias();
                if(alias != null && alias.equals(token)) {
                    path = join;
                    break;
                }
            }
        }
        if(path == null) {
            path = root.get(token);
        }
        return path;
    }

    // TODO: 2022/1/4  group by
    public TupleQueryBuilder countAs(String field, String alias) {
        Expression expression = criteriaBuilder.count(toPath(field));
        expression.alias(alias);
        this.selections.add(expression);
        return this;
    }

    public TupleQueryBuilder countAs(String... fields) {
        for(String field : fields) {
            Expression expression = criteriaBuilder.count(toPath(field));
            this.selections.add(expression);
        }
        return this;
    }

    public TupleQueryBuilder distinct() {
        this.criteriaQuery.distinct(true);
        return this;
    }

    public CriteriaQuery<Tuple> build() {
        javax.persistence.criteria.Predicate predicate = PredicateBuilder.of(criteriaQuery, criteriaBuilder)
                .from(root).where(this.predicate).build();
        if(predicate != null) {
            this.criteriaQuery.where(predicate);
        }

        this.criteriaQuery.multiselect(this.selections);
        return this.criteriaQuery;
    }
}
