package io.leandev.appstack.data.jpa;

import io.leandev.appstack.bean.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Tuple;
import javax.persistence.TupleElement;

public class TupleConstructor<T> {
    private static Logger logger = LoggerFactory.getLogger(TupleConstructor.class);

    private Class<T> entityType;

    public TupleConstructor(Class<T> entityType) {
        this.entityType = entityType;
    }

    public T construct(Tuple tuple) {
        T object = null;
        try {
            object = entityType.newInstance();
            Model<T> model;
            if(object instanceof Model) {
                model = (Model) object;
            } else {
                model = new Model<>(object);
            }

            for(TupleElement tupleElement : tuple.getElements()) {
                String key = tupleElement.getAlias();
                Object value = tuple.get(key);
                model.put(key, value);
            }
        } catch (InstantiationException | IllegalAccessException e) {
            logger.debug(String.format("Failed to build. %s", e.getMessage()), e);
        }
        return object;
    }
}
