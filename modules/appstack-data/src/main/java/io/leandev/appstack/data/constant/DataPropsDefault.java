package io.leandev.appstack.data.constant;

public class DataPropsDefault {
    public static final String DATADRIVER = "org.h2.Driver";
    public static final String DATAURL = "jdbc:h2:file:%s/%sDB;MV_STORE=TRUE;MODE=Oracle;IGNORECASE=TRUE;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;TRACE_LEVEL_SYSTEM_OUT=1";
    public static final String DATAUSERNAME = "sa";
    public static final String DATAPASSWORD = "";

    /**
     * validate: validate the schema, makes no changes to the database.
     * update: update the schema.
     * create: creates the schema, destroying previous data.
     * create-drop: drop the schema at the end of the session
     * none: is all other cases.
     */
    public static final String DATADDLACTION = "update";
    public static final String DATADEBUG = "false";

    public static final String NATIONALIZED = "false";

}
