package io.leandev.appstack.data.rsql;

import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import cz.jirutka.rsql.parser.ast.Node;
import cz.jirutka.rsql.parser.ast.RSQLOperators;

import java.util.Set;

public class RSQLParser {
    private cz.jirutka.rsql.parser.RSQLParser parser;
    public RSQLParser() {
        Set<ComparisonOperator> operators = RSQLOperators.defaultOperators();
        operators.add(RsqlComparator.HAS.getOperator());
        this.parser = new cz.jirutka.rsql.parser.RSQLParser(operators);
    }

    public Node parse(String predicate) {
        return this.parser.parse(predicate);
    }
}
