package io.leandev.appstack.data.jdbc;

import io.leandev.appstack.bean.Model;

public interface DataSourceAdaptor {
    Model<?> getConfiguration();
    Model<?> getMetrics();
}
