package io.leandev.appstack.data.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class QueryRunner<T> {
    private EntityManager entityManager;

    public QueryRunner(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Use the criteria query for data to generate the criteria query to count the data.
     *
     * To reuse the restriction, the two queries need to have the same path expression in the restriction.
     * Because we can not guarantee the data query has alias assigned for every root,
     * we have to create the count query after executing the data query to let JPA generate the alias first.
     *
     * @param dataQuery the criteria query for data
     * @param pageable
     * @return
     */
    public Page<T> findAll(CriteriaQuery dataQuery, Pageable pageable) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        Optional<Root> optionalRoot = dataQuery.getRoots().stream().findFirst();
        Root root = optionalRoot.get();

        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();

        if(!pageable.getSort().isUnsorted()) {
            List<Order> orders = new ArrayList<>();
            pageable.getSort().stream().forEach(order -> {
                if(order.isDescending()) {
                    orders.add(criteriaBuilder.desc(toPath(root, order.getProperty())));
                } else {
                    orders.add(criteriaBuilder.asc(toPath(root, order.getProperty())));
                }
            });
            dataQuery.orderBy(orders);
        }

        TypedQuery<T> typedQuery = entityManager.createQuery(dataQuery)
                .setFirstResult(pageNumber*pageSize)
                .setMaxResults(pageSize);

        List<T> tuples = typedQuery.getResultList();

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);

        Set<Root<?>> roots = dataQuery.getRoots();
        for(Root<?> $root : roots) {
            From from = countQuery.from($root.getModel());
            from.alias($root.getAlias());
            for(Join $join : $root.getJoins()) {
                Join join = from.join($join.getAttribute().getName());
                join.alias($join.getAlias());
            }
        }

        if(dataQuery.getRestriction() != null) {
            countQuery.where(dataQuery.getRestriction());
        }

        if(dataQuery.isDistinct()) {
            countQuery.distinct(dataQuery.isDistinct());
        }

        countQuery.select(criteriaBuilder.count(countQuery.getRoots().stream().findFirst().get()));
        Long totalElements = entityManager.createQuery(countQuery).getSingleResult();

        Page<T> page = new PageImpl<>(tuples, pageable, totalElements);

        return page;
    }

    public List<T> findAll(CriteriaQuery dataQuery) {
        TypedQuery<T> typedQuery = entityManager.createQuery(dataQuery);
        List<T> tuples = typedQuery.getResultList();
        return tuples;
    }

    public Optional<T> findOne(CriteriaQuery dataQuery) {
        List<T> tuples = this.findAll(dataQuery);
        return tuples.stream().findFirst();
    }

    private Path toPath(Root root, String field) {
        String[] paths = field.split("\\.");
        Path selection = root;
        for(String path : paths) {
            selection = selection.get(path);
        }
        return selection;
    }
}
