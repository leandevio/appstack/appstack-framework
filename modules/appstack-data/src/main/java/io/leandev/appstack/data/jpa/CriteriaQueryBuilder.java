package io.leandev.appstack.data.jpa;


import io.leandev.appstack.search.Sort;
import io.leandev.appstack.search.Predicate;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class CriteriaQueryBuilder<V, T> {
    private final CriteriaBuilder criteriaBuilder;
    private CriteriaQuery<V> criteriaQuery;
    private Root<T> root;
    private Class<V> valueType;
    private Selection selection;
    private Predicate predicate = Predicate.empty();

    public CriteriaQueryBuilder(CriteriaBuilder criteriaBuilder, Class<V> valueType) {
        this.criteriaBuilder = criteriaBuilder;
        this.valueType = valueType;
    }

    public CriteriaQueryBuilder(EntityManager entityManager, Class<V> valueType) {
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
        this.valueType = valueType;
    }

    public CriteriaQueryBuilder(CriteriaBuilder criteriaBuilder) {
        this.criteriaBuilder = criteriaBuilder;
    }

    public CriteriaQueryBuilder(EntityManager entityManager) {
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    public static CriteriaQueryBuilder of(EntityManager entityManager, Class<?> valueType) {
        return new CriteriaQueryBuilder(entityManager, valueType);
    }

    public static CriteriaQueryBuilder of(CriteriaBuilder criteriaBuilder, Class<?> valueType) {
        return new CriteriaQueryBuilder(criteriaBuilder, valueType);
    }

    public static CriteriaQueryBuilder of(EntityManager entityManager) {
        return new CriteriaQueryBuilder(entityManager);
    }

    public static CriteriaQueryBuilder of(CriteriaBuilder criteriaBuilder) {
        return new CriteriaQueryBuilder(criteriaBuilder);
    }

    public CriteriaQueryBuilder from(Class<T> entityType) {
        this.criteriaQuery = criteriaBuilder.createQuery(valueType);
        this.root = criteriaQuery.from(entityType);
        this.selection = this.root;
        if(this.valueType == null) {
            this.valueType = (Class<V>) entityType;
        }
        return this;
    }

    public CriteriaQueryBuilder from(Class<T> entityType, String alias) {
        this.criteriaQuery = criteriaBuilder.createQuery(valueType);
        this.root = criteriaQuery.from(entityType);
        this.root.alias(alias);
        this.selection = this.root;
        if(this.valueType == null) {
            this.valueType = (Class<V>) entityType;
        }
        return this;
    }

    public CriteriaQueryBuilder join(String property, JoinType joinType, String alias) {
        Join join = this.root.join(property, joinType);
        join.alias(alias);
        return this;
    }

    public CriteriaQueryBuilder join(String property, JoinType joinType) {
        return this.join(property, joinType, property);
    }

    public CriteriaQueryBuilder join(String property, String alias) {
        return this.join(property, JoinType.INNER, alias);
    }

    public CriteriaQueryBuilder join(String property) {
        return this.join(property, JoinType.INNER, property);
    }

    public CriteriaQueryBuilder distinct() {
        this.criteriaQuery.distinct(true);
        return this;
    }

    public CriteriaQueryBuilder orderBy(Sort sort) {
        List<Order> orders = new ArrayList<>();
        sort.stream().forEach(order -> {
            if(order.isDescending()) {
                orders.add(criteriaBuilder.desc(toPath(order.selector())));
            } else {
                orders.add(criteriaBuilder.asc(toPath(order.selector())));
            }
        });
        this.criteriaQuery.orderBy(orders);
        return this;
    }

    public CriteriaQueryBuilder orderBy(org.springframework.data.domain.Sort sort) {
        List<Order> orders = new ArrayList<>();
        sort.stream().forEach(order -> {
            if(order.isDescending()) {
                orders.add(criteriaBuilder.desc(toPath(order.getProperty())));
            } else {
                orders.add(criteriaBuilder.asc(toPath(order.getProperty())));
            }
        });
        this.criteriaQuery.orderBy(orders);
        return this;
    }

    private Path toPath(String field) {
        String[] tokens = field.split("\\.");
        Path path = getStartPath(tokens[0]);

        for(int i = 1; i<tokens.length; i++) {
            path = path.get(tokens[i]);
        }
        return path;
    }

    private Path getStartPath(String token) {
        Path path = null;
        if(token.equals(root.getAlias())) {
            path = root;
        } else {
            for(Join join : root.getJoins()) {
                String alias = join.getAlias();
                if(alias != null && alias.equals(token)) {
                    path = join;
                    break;
                }
            }
        }
        if(path == null) {
            path = root.get(token);
        }
        return path;
    }

    public CriteriaQueryBuilder count() {
        Expression expression = criteriaBuilder.count(root);
        this.selection = expression;
        return this;
    }

    public CriteriaQueryBuilder where(Predicate predicate) {
        this.predicate = (predicate==null) ? Predicate.empty() : predicate;
        return this;
    }


    public CriteriaQuery build() {
        javax.persistence.criteria.Predicate predicate = PredicateBuilder.of(criteriaQuery, criteriaBuilder)
                .from(root).where(this.predicate).build();
        if(predicate != null) {
            criteriaQuery.where(predicate);
        }
        if(selection != null) {
            criteriaQuery.select(selection);
        }
        return criteriaQuery;
    }

}
