package io.leandev.appstack.data.processor;

import io.leandev.appstack.data.constant.DataPropsDefault;
import io.leandev.appstack.data.constant.DataPropsKey;
import io.leandev.appstack.processor.EnvironmentProcessor;
import io.leandev.appstack.util.Environment;
import java.util.Map;

public class DataEnvironmentProcessor implements EnvironmentProcessor {

    @Override
    public void postProcess(Environment environment) {
        String url = String.format(DataPropsDefault.DATAURL, environment.dataFolder().toString(), environment.appName());
        environment.setProperty(DataPropsKey.DATADRIVER, DataPropsDefault.DATADRIVER, false);
        environment.setProperty(DataPropsKey.DATAURL, url, false);
        environment.setProperty(DataPropsKey.DATAUSERNSME, DataPropsDefault.DATAUSERNAME, false);
        environment.setProperty(DataPropsKey.DATAPASSWORD, DataPropsDefault.DATAPASSWORD, false);
        String action = environment.property(DataPropsKey.DATADDLACTION);
        if(action==null || action.equalsIgnoreCase("create") || action.equalsIgnoreCase("create-drop")) {
            environment.setProperty(DataPropsKey.DATADDLACTION, DataPropsDefault.DATADDLACTION, true);
        } else {
            environment.setProperty(DataPropsKey.DATADDLACTION, DataPropsDefault.DATADDLACTION, false);
        }
        environment.setProperty(DataPropsKey.DATADEBUG, DataPropsDefault.DATADEBUG, false);
        environment.setProperty(DataPropsKey.NATIONALIZED, DataPropsDefault.NATIONALIZED, false);
    }

    @Override
    public void postProcess(Environment environment, Map<String, Object> props) {
        configDataSource(environment, props);
        configJpa(environment, props);
    }

    private void configDataSource(Environment environment, Map<String, Object> props) {
        props.put("spring.datasource.driver-class-name", environment.property(DataPropsKey.DATADRIVER));
        props.put("spring.datasource.url", environment.property(DataPropsKey.DATAURL));
        props.put("spring.datasource.username", environment.property(DataPropsKey.DATAUSERNSME));
        props.put("spring.datasource.password", environment.property(DataPropsKey.DATAPASSWORD));

        if(environment.containsProperty(DataPropsKey.DATADIALECT)) {
            props.put("spring.jpa.properties.hibernate.dialect", environment.property(DataPropsKey.DATADIALECT));
        }
        // When using the default connection pool - Tomcat connection pool
        // Number of ms to wait before throwing an exception if no connection is available.
        props.put("spring.datasource.tomcat.max-wait", "10000");
        // Maximum number of active connections that can be allocated from this pool at the same time.
        props.put("spring.datasource.tomcat.max-active", "50");
        // Validate the connection before borrowing it from the pool.
        props.put("spring.datasource.tomcat.test-on-borrow", "true");
    }

    private void configJpa(Environment environment, Map<String, Object> props) {
        props.put("spring.jpa.generate-ddl", "true");
        String action = environment.property(DataPropsKey.DATADDLACTION);
        if(action.equalsIgnoreCase("create")||action.equalsIgnoreCase("create-drop")) {
            action = DataPropsDefault.DATADDLACTION;
        }
        props.put("spring.jpa.hibernate.ddl-auto", action);
        props.put("spring.jpa.show-sql", environment.property(DataPropsKey.DATADEBUG));
        // enable nationalized character data as the default.
        props.put("spring.jpa.properties.hibernate.use_nationalized_character_data",  environment.property(DataPropsKey.NATIONALIZED));
    }

}
