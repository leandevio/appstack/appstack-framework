package io.leandev.appstack.web.repository;

import io.leandev.appstack.data.config.PersistenceConfig;
import io.leandev.appstack.web.entity.Document;
import io.leandev.appstack.web.entity.Product;
import io.leandev.appstack.data.service.DataInstaller;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { PersistenceConfig.class })
@Transactional
@Rollback
public class JPASpecificationsTest {

    @Autowired
    DataInstaller dataInstaller;

    @Autowired
    private ProductRepository repository;

    @Before
    public void init() {
        dataInstaller.install(Product.class, product -> {
            Document image = new Document();
            String path = "/doc/" + product.getId();
            try(InputStream in = JPASpecificationsTest.class.getResourceAsStream(path + ".jpg")) {
                String type = "font/jpeg";
                if(in==null) return;
                int size = in.available();
                byte[] bytes = new byte[size];
                in.read(bytes);
                image.setName(product.getName());
                image.setSize((long) size);
                image.setType(type);
                image.setBytes(bytes);
                image.setCreatedOn(product.getCreatedOn());
            } catch (IOException ex) {}
            product.setImage(image);
        });
    }

    @Test
    public void testFindByOneSpec() {
        ProductSpecification spec =
                new ProductSpecification(new SearchCriteria("name", SearchOperation.STARTS_WITH, "甜馨"));

        Optional<Product> optional = repository.findOne(spec);

        assertEquals(optional.get().getName(), "甜馨花盒");
    }

    @Test
    public void testFindByTwoSpecs() {
        ProductSpecification spec1 =
                new ProductSpecification(new SearchCriteria("category", SearchOperation.EQUALITY, "Flower Bouquet"));
        ProductSpecification spec2 =
                new ProductSpecification(new SearchCriteria("price", SearchOperation.GREATER_THAN, "10000"));


        Optional<Product> optional = repository.findOne(Specification.where(spec1).and(spec2));

        assertEquals(optional.get().getName(), "108朵紅玫瑰花束");
    }



}