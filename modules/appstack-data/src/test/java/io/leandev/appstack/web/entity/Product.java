package io.leandev.appstack.web.entity;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.*;

@Entity
public class Product {
    /**
     * 版本
     */
    private Integer version;
    /**
     * 識別
     */
    private String uuid;
    /**
     * 編號
     */
    private String id;
    /**
     * 名稱
     */
    private String name;
    /**
     * 價格
     */
    private BigDecimal price;
    /**
     * 可得性
     */
    private Boolean availability = true;
    /**
     * 說明
     */
    private String description;
    /**
     * 分類
     */
    private String category;
    /**
     * 關鍵字
     */
    private Set<String> keywords = new HashSet<>();
    /**
     * 寬度
     */
    private Integer width;
    /**
     * 高度
     */
    private Integer height;
    /**
     * 深(長)度
     */
    private Integer depth;
    /**
     * 花材
     */
    private Set<String> materials = new HashSet<>();
    /**
     * 備註
     */
    private String note;
    /**
     * 圖像
     */
    private Document image;
    /**
     * 圖片
     */
    private Set<Document> pictures = new HashSet<>();
    /**
     * 附件
     */
    private Set<Document> artifacts = new HashSet<>();
    /**
     * 建立日期
     */
    private Date createdOn = new Date();
    /**
     * 建立者
     */
    private String createdBy;
    /**
     * 修改日期
     */
    private Date modifiedOn = new Date();
    /**
     * 修改者
     */
    private String modifiedBy;

    private Set<String> delivery = new HashSet<>();

    public Product() {
    }

    public Product(String id, String name, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    /**
     * Getter: {@link #version}.
     *
     * @return {@link #version}.
     */
    @Version
    public Integer getVersion() {
        return version;
    }

    /**
     * Setter: {@link #version}.
     *
     * @param version {@link #version}.
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Getter: {@link #uuid}.
     *
     * @return {@link #uuid}.
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length = 36)
    @Size(max = 36)
    public String getUuid() {
        return uuid;
    }

    /**
     * Setter: {@link #uuid}.
     *
     * @param uuid {@link #uuid}.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Getter: {@link #id}.
     *
     * @return {@link #id}.
     */
    @Column(length = 100, nullable = false, unique = true)
    @NotNull
    @Size(max = 100)
    public String getId() {
        return id;
    }

    /**
     * Setter: {@link #id}.
     *
     * @param id {@link #id}.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter: {@link #name}.
     *
     * @return {@link #name}.
     */
    @Column(length = 100, nullable = false)
    @NotNull
    @Size(max = 100)
    public String getName() {
        return name;
    }

    /**
     * Setter: {@link #name}.
     *
     * @param name {@link #name}.
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Getter: {@link #price}.
     *
     * @return {@link #price}.
     */
    @Column
    @Min(100)
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Setter: {@link #price}.
     *
     * @param price {@link #price}.
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Getter: {@link #availability}.
     *
     * @return {@link #availability}.
     */
    @Column(nullable = false)
    @NotNull
    public Boolean getAvailability() {
        return availability;
    }

    /**
     * Setter: {@link #availability}.
     *
     * @param availability {@link #availability}.
     */
    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    /**
     * Getter: {@link #description}.
     *
     * @return {@link #description}.
     */
    @Column(length = 2000)
    @Size(max = 2000)
    public String getDescription() {
        return description;
    }

    /**
     * Setter: {@link #description}.
     *
     * @param description {@link #description}.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter: {@link #category}.
     *
     * @return {@link #category}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getCategory() {
        return category;
    }

    /**
     * Setter: {@link #category}.
     *
     * @param category {@link #category}.
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Getter: {@link #width}.
     *
     * @return {@link #width}.
     */
    @Column
    public Integer getWidth() {
        return width;
    }

    /**
     * Setter: {@link #width}.
     *
     * @param width {@link #width}.
     */

    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * Getter: {@link #height}.
     *
     * @return {@link #height}.
     */
    @Column
    public Integer getHeight() {
        return height;
    }

    /**
     * Setter: {@link #height}.
     *
     * @param height {@link #height}.
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * Getter: {@link #depth}.
     *
     * @return {@link #depth}.
     */
    @Column
    public Integer getDepth() {
        return depth;
    }

    /**
     * Setter: {@link #depth}.
     *
     * @param depth {@link #depth}.
     */
    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    /**
     * Getter: {@link #materials}.
     *
     * @return {@link #materials}.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "Product_Materials",
            joinColumns = @JoinColumn(name = "Owner")
    )
    @Column(name = "Materials")
    public Set<String> getMaterials() {
        return materials;
    }

    /**
     * Setter: {@link #materials}.
     *
     * @param materials {@link #materials}.
     */
    public void setMaterials(Set<String> materials) {
        this.materials = materials;
    }

    /**
     * Getter: {@link #keywords}.
     *
     * @return {@link #keywords}.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "Product_Keywords",
            joinColumns = @JoinColumn(name = "Owner")
    )
    @Column(name = "Keywords")
    public Set<String> getKeywords() {
        return keywords;
    }

    /**
     * Setter: {@link #keywords}.
     *
     * @param keywords {@link #keywords}.
     */
    public void setKeywords(Set<String> keywords) {
        this.keywords = keywords;
    }

    /**
     * Getter: {@link #note}.
     *
     * @return {@link #note}.
     */
    @Column(length = 2000)
    @Size(max = 2000)
    public String getNote() {
        return note;
    }

    /**
     * Setter: {@link #note}.
     *
     * @param note {@link #note}.
     */
    public void setNote(String note) {
        this.note = note;
    }


    /**
     * Getter: {@link #image}.
     *
     * @return {@link #image}.
     */
    @OneToOne(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    public Document getImage() {
        return image;
    }

    /**
     * Setter: {@link #image}.
     *
     * @param image {@link #image}.
     */
    public void setImage(Document image) {
        this.image = image;
    }

    /**
     * Getter: {@link #pictures}.
     *
     * @return {@link #pictures}.
     */
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    public Set<Document> getPictures() {
        return pictures;
    }

    /**
     * Setter: {@link #pictures}.
     *
     * @param pictures {@link #pictures}.
     */
    public void setPictures(Set<Document> pictures) {
        this.pictures = pictures;
    }

    /**
     * Getter: {@link #artifacts}.
     *
     * @return {@link #artifacts}.
     */
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    public Set<Document> getArtifacts() {
        return artifacts;
    }

    /**
     * Setter: {@link #artifacts}.
     *
     * @param artifacts {@link #artifacts}.
     */
    public void setArtifacts(Set<Document> artifacts) {
        this.artifacts = artifacts;
    }

    /**
     * Getter: {@link #createdOn}.
     *
     * @return {@link #createdOn}.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * Setter: {@link #createdOn}.
     *
     * @param createdOn {@link #createdOn}.
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Getter: {@link #createdBy}.
     *
     * @return {@link #createdBy}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Setter: {@link #createdBy}.
     *
     * @param createdBy {@link #createdBy}.
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }


    /**
     * Getter: {@link #modifiedOn}.
     *
     * @return {@link #modifiedOn}.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    @LastModifiedDate
    public Date getModifiedOn() {
        return modifiedOn;
    }

    /**
     * Setter: {@link #modifiedOn}.
     *
     * @param modifiedOn {@link #modifiedOn}.
     */
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * Getter: {@link #modifiedBy}.
     *
     * @return {@link #modifiedBy}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * Setter: {@link #modifiedBy}.
     *
     * @param modifiedBy {@link #modifiedBy}.
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Getter: {@link #delivery}.
     *
     * @return {@link #delivery}.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "Product_Delivery",
            joinColumns = @JoinColumn(name = "Owner")
    )
    @Column(name = "delivery")
    public Set<String> getDelivery() {
        return delivery;
    }

    /**
     * Setter: {@link #delivery}.
     *
     * @param delivery {@link #delivery}.
     */
    public void setDelivery(Set<String> delivery) {
        this.delivery = delivery;
    }
}
