package io.leandev.appstack.data.service;

import io.leandev.appstack.json.JsonParser;
import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.function.Consumer;

@Service
@Transactional
public class DataInstaller {
    private static final Logger LOGGER = LogManager.getLogger(DataInstaller.class);

    @PersistenceContext
    private EntityManager entityManager;

    public <T> long install(Class<T> entityType) {
        return install(entityType, data -> {});
    }

    public <T> long install(Class<T> entityType, Consumer<T> processor) {
        String resource = "/data/" + entityType.getSimpleName() + ".json";

        if(DataInstaller.class.getResource(resource)==null) return -1;

        long counter = this.count(entityType);

        if(counter>0) return -1;

        JsonParser parser = new JsonParser();

        try(InputStream inputStream = DataInstaller.class.getResourceAsStream(resource)){
            List<T> data = parser.readValueAsList(inputStream, entityType);
            data.forEach(datum -> processor.accept(datum));
            data.forEach(datum -> entityManager.persist(datum));
            counter = data.size();
        } catch (IOException e) {
            LOGGER.error(String.format("Failed to install data for %s", entityType.getName()), e);
            throw new RuntimeException(e);
        }

        return counter;
    }

    public long count(Class<?> entityType) {
        CriteriaBuilder qb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = qb.createQuery(Long.class);
        cq.select(qb.count(cq.from(entityType)));
        return entityManager.createQuery(cq).getSingleResult();
    }
}
