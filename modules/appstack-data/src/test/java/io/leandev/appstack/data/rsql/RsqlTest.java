package io.leandev.appstack.data.rsql;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIn.isIn;
import static org.hamcrest.core.IsNot.not;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import io.leandev.appstack.data.config.PersistenceConfig;
import io.leandev.appstack.web.entity.Document;
import io.leandev.appstack.web.entity.Product;
import io.leandev.appstack.web.repository.JPASpecificationsTest;
import io.leandev.appstack.web.repository.ProductRepository;
import io.leandev.appstack.data.service.DataInstaller;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.Node;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { PersistenceConfig.class })
@Transactional
@Rollback
public class RsqlTest {

    @Autowired
    private ProductRepository repository;

    @Autowired
    DataInstaller dataInstaller;

    Product pinkBouquet;
    Product gentlemanBlue;

    @Before
    public void init() {
        dataInstaller.install(Product.class, product -> {
            Document image = new Document();
            String path = "/doc/" + product.getId();
            try(InputStream in = JPASpecificationsTest.class.getResourceAsStream(path + ".jpg")) {
                String type = "font/jpeg";
                if(in==null) return;
                int size = in.available();
                byte[] bytes = new byte[size];
                in.read(bytes);
                image.setName(product.getName());
                image.setSize((long) size);
                image.setType(type);
                image.setBytes(bytes);
                image.setCreatedOn(product.getCreatedOn());
            } catch (IOException ex) {}
            product.setImage(image);
        });

        pinkBouquet = repository.findById("Pink bouquet").get();
        gentlemanBlue = repository.findById("Gentleman blue").get();
    }

    @Test
    public void testAnd() {
        Node rootNode = new RSQLParser().parse("name==粉馨花束;price==4200");
        Specification<Product> spec = rootNode.accept(new RsqlSpecificationVisitor<>());
        List<Product> results = repository.findAll(spec);

        assertThat(pinkBouquet, isIn(results));
        assertThat(gentlemanBlue, not(isIn(results)));
    }

    @Test
    public void testOr() {
        Node rootNode = new RSQLParser().parse("name===粉馨花束,name==紳士藍");
        Specification<Product> spec = rootNode.accept(new RsqlSpecificationVisitor<>());
        List<Product> results = repository.findAll(spec);

        assertThat(pinkBouquet, isIn(results));
        assertThat(gentlemanBlue, isIn(results));
    }

    @Test
    public void testNotEqual() {
        final Node rootNode = new RSQLParser().parse("name!==粉馨花束");
        final Specification<Product> spec = rootNode.accept(new RsqlSpecificationVisitor<>());
        final List<Product> results = repository.findAll(spec);

        assertThat(pinkBouquet, not(isIn(results)));
        assertThat(gentlemanBlue, isIn(results));
    }

    @Test
    public void testGreaterThan() {
        final Node rootNode = new RSQLParser().parse("price>4000");
        final Specification<Product> spec = rootNode.accept(new RsqlSpecificationVisitor<>());
        final List<Product> results = repository.findAll(spec);

        assertThat(pinkBouquet, isIn(results));
        assertThat(gentlemanBlue, not(isIn(results)));
    }

    @Test
    public void testWildcard() {
        final Node rootNode = new RSQLParser().parse("name==粉馨花束*");
        final Specification<Product> spec = rootNode.accept(new RsqlSpecificationVisitor<>());
        final List<Product> results = repository.findAll(spec);

        assertThat(pinkBouquet, isIn(results));
        assertThat(gentlemanBlue, not(isIn(results)));
    }

    @Test
    public void testIn() {
        final Node rootNode = new RSQLParser().parse("name=in=(粉馨花束,紳士藍)");
        final Specification<Product> spec = rootNode.accept(new RsqlSpecificationVisitor<>());
        final List<Product> results = repository.findAll(spec);

        assertThat(pinkBouquet, isIn(results));
        assertThat(gentlemanBlue, isIn(results));
    }
}