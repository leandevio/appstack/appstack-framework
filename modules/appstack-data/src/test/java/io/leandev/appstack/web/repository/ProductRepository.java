package io.leandev.appstack.web.repository;

import io.leandev.appstack.web.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends PagingAndSortingRepository<Product, String>, ProductRepositoryCustom, JpaSpecificationExecutor<Product> {
    Page<Product> findByCategory(@Param("category") String category, Pageable pageable);
}
