package io.leandev.appstack.report;

import io.leandev.appstack.env.Environ;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class JasperReportEngineTest {
    private static final Class CLASS = JasperReportEngineTest.class;
    private static final ClassLoader CLASSLOADER = CLASS.getClassLoader();
    private static final Path RESOURCES = Paths.get("report");
    private static final Path OUTPUT = Environ.getInstance().tmpFolder().resolve(CLASS.getSimpleName());

    DataSource dataSource;

    @BeforeAll
    static void setup() throws IOException {
        if(!Files.exists(OUTPUT)) {
            Files.createDirectory(OUTPUT);
        }
    }

    @BeforeEach
    void init() {
        dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript(String.format("classpath:%s/product-schema.sql", RESOURCES))
                .build();
    }

    /**
     * 沒有子報表
     *
     * @throws IOException
     */
    @Test
    public void testExportReport() throws IOException {
        final String REPORT_NAME = "employee-report";
        final String PDF_REPORT = "employee-report.pdf";

        // 指定報表範本的位置
        JasperReportEngine jasperReportEngine = JasperReportEngineBuilder.of("classpath:report")
                .withDataSource(dataSource)
                .build();

        try (OutputStream outputStream = Files.newOutputStream(OUTPUT.resolve(PDF_REPORT))) {
            // 準備傳送給報表的參數。
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("title", "員工報表");
            parameters.put("minSalary", 15000.0);
            parameters.put("condition", " salary < 70000.0 ORDER BY FIRST_NAME");

            // 指定報表名稱、參數與匯出報表的 OutputStream
            jasperReportEngine.exportReport(REPORT_NAME, parameters, outputStream);
        }
    }

    /**
     * @todo
     * 需要以子報表做為參數的報表。
     *
     * @throws IOException
     */
//    @Test
//    public void testExportWithSubReport() throws IOException {
//        final String EMPLOYEE_EMAIL_REPORT_TEMPLATE = "employee-email-report";
//        final String EMAIL_REPORT_TEMPLATE = "email-report";
//        final String PDF_REPORT = "employee-email-report.pdf";
//
//        // 指定報表範本的位置
//        JasperReportEngine jasperReportEngine = JasperReportEngineBuilder.of("classpath:report")
//                .withDataSource(dataSource)
//                .build();
//
//        try (OutputStream outputStream = Files.newOutputStream(OUTPUT.resolve(PDF_REPORT))) {
//            // 準備傳送給報表的參數。
//            Map<String, Object> parameters = new HashMap<>();
//            parameters.put("title", "Employee Report");
//            parameters.put("minSalary", 15000.0);
//            parameters.put("condition", " salary < 70000.0 ORDER BY FIRST_NAME");
//
//            // 指定報表名稱、參數、匯出報表的 OutputStream，以及一個或數個的子報表
//            jasperReportEngine.exportReport(EMPLOYEE_EMAIL_REPORT_TEMPLATE, parameters, outputStream);
//        }
//    }

    /**
     * 在 JasperStudio 產生有子報表的報表
     *
     * @throws IOException
     */
    @Test
    public void testExportWithSubReportByStudio() throws IOException {
        final String EMPLOYEE_EMAIL_REPORT_TEMPLATE = "employee-email-report-studio";
        final String PDF_REPORT = "employee-email-report.pdf";

        // 指定報表範本的位置
        JasperReportEngine jasperReportEngine = JasperReportEngineBuilder.of("classpath:report")
                .withDataSource(dataSource)
                .build();

        try (OutputStream outputStream = Files.newOutputStream(OUTPUT.resolve(PDF_REPORT))) {
            // 準備傳送給報表的參數。
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("title", "Employee Report");
            parameters.put("minSalary", 15000.0);
            parameters.put("condition", " salary < 70000.0 ORDER BY FIRST_NAME");

            // 指定報表名稱、參數、匯出報表的 OutputStream。
            // JasperReportEngine 會自動分析主報表是否有含有子報表，並從指定的報表位置載入子報表。
            jasperReportEngine.exportReport(EMPLOYEE_EMAIL_REPORT_TEMPLATE, parameters, outputStream);
        }
    }
}