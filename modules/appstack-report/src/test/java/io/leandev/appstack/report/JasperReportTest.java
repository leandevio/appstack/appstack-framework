package io.leandev.appstack.report;

import io.leandev.appstack.env.Environ;
import io.leandev.appstack.xml.XmlFinder;
import io.leandev.appstack.xml.XmlReader;
import io.leandev.appstack.xml.Xmls;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JasperReportTest {
    private static final Class CLASS = JasperReportTest.class;
    private static final ClassLoader LOADER = CLASS.getClassLoader();
    private static final Path RESOURCES = Paths.get("report");
    private static final Path OUTPUT = Environ.getInstance().tmpFolder().resolve(JasperReportTest.class.getSimpleName());

    DataSource dataSource;

    @BeforeAll
    static void setup() throws IOException {
        if(!Files.exists(OUTPUT)) {
            Files.createDirectory(OUTPUT);
        }

    }

    @BeforeEach
    void init() {
        dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript(String.format("classpath:%s/product-schema.sql", RESOURCES))
                .build();
    }
    /**
     * JRXML files need to be compiled so the report engine can fill them with data.
     *
     * @throws JRException
     * @throws IOException
     */
    @Test
    public void testCompileReport() throws JRException, IOException {
        final String REPORT_TEMPLATE = "employee-report.jrxml";
        final String COMPILED_REPORT = "employee-report.jasper";

        try (InputStream reportStream = LOADER.getResourceAsStream(RESOURCES.resolve(REPORT_TEMPLATE).toString());
             OutputStream outputStream = Files.newOutputStream(OUTPUT.resolve(COMPILED_REPORT))) {
            JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
            JRSaver.saveObject(jasperReport, outputStream);
        }
    }

    @Test
    public void testExportReport() throws JRException, IOException, SQLException {
        final String REPORT_TEMPLATE = "employee-report.jrxml";
        final String COMPILED_REPORT = "employee-report.jasper";
        final String PDF_REPORT = "employee-report.pdf";


        try (InputStream reportStream = LOADER.getResourceAsStream(RESOURCES.resolve(REPORT_TEMPLATE).toString());
             OutputStream compiledStream = Files.newOutputStream(OUTPUT.resolve(COMPILED_REPORT));
             OutputStream printStream = Files.newOutputStream(OUTPUT.resolve(PDF_REPORT))) {
            // JRXML files need to be compiled so the report engine can fill them with data.
            JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);

            Map<String, Object> parameters = new HashMap<>();
            parameters.put("title", "Employee Report");
            parameters.put("minSalary", 15000.0);
            parameters.put("condition", " salary < 70000.0 ORDER BY FIRST_NAME");

            // Fill the report with parameters and jdbc connection.
            JasperPrint jasperPrint = JasperFillManager.fillReport(
                    jasperReport, parameters, dataSource.getConnection());
            JRSaver.saveObject(jasperReport, compiledStream);

            // Export the report
            JRPdfExporter exporter = new JRPdfExporter();

            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(
                    new SimpleOutputStreamExporterOutput(printStream));

            SimplePdfReportConfiguration reportConfig
                    = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig
                    = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("jasonlin");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            exporter.setConfiguration(reportConfig);
            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
        }
    }

    @Test
    public void testExportWithSubReport() throws IOException, JRException, SQLException {
        final String EMPLOYEE_EMAIL_REPORT_TEMPLATE = "employee-email-report.jrxml";
        final String EMAIL_REPORT_TEMPLATE = "email-report.jrxml";
        final String EMPLOYEE_EMAIL_COMPILED_REPORT = "employee-email-report.jasper";
        final String EMAIL_COMPILED_REPORT = "email-report.jasper";
        final String PDF_REPORT = "employee-email-report.pdf";

        try (InputStream employeeEmailReportStream = LOADER.getResourceAsStream(RESOURCES.resolve(EMPLOYEE_EMAIL_REPORT_TEMPLATE).toString());
             InputStream emailReportStream = LOADER.getResourceAsStream(RESOURCES.resolve(EMAIL_REPORT_TEMPLATE).toString());
             OutputStream employeeEmailCompiledStream = Files.newOutputStream(OUTPUT.resolve(EMPLOYEE_EMAIL_COMPILED_REPORT));
             OutputStream emailCompiledStream = Files.newOutputStream(OUTPUT.resolve(EMAIL_COMPILED_REPORT));
             OutputStream pdfReportStream = Files.newOutputStream(OUTPUT.resolve(PDF_REPORT))) {
            // JRXML files need to be compiled so the report engine can fill them with data.
            JasperReport employeeEmailJasperReport = JasperCompileManager.compileReport(employeeEmailReportStream);
            JRSaver.saveObject(employeeEmailJasperReport, employeeEmailCompiledStream);

            JasperReport emailJasperReport = JasperCompileManager.compileReport(emailReportStream);
            JRSaver.saveObject(emailJasperReport, emailCompiledStream);

            Map<String, Object> parameters = new HashMap<>();
            parameters.put("title", "Employee Report");
            parameters.put("minSalary", 15000.0);
            parameters.put("condition", " salary < 70000.0 ORDER BY FIRST_NAME");
            parameters.put("subreportParameter", emailJasperReport);
            // Fill the report with parameters and jdbc connection.
            JasperPrint jasperPrint = JasperFillManager.fillReport(
                    OUTPUT.resolve(EMPLOYEE_EMAIL_COMPILED_REPORT).toString(), parameters, dataSource.getConnection());

            // Export the report
            JRPdfExporter exporter = new JRPdfExporter();

            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(
                    new SimpleOutputStreamExporterOutput(pdfReportStream));

            SimplePdfReportConfiguration reportConfig
                    = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig
                    = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("jasonlin");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            exporter.setConfiguration(reportConfig);
            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
        }
    }

    @Test
    public void testExportWithSubReportByStudio() throws Exception {
        final String EMPLOYEE_EMAIL_REPORT_TEMPLATE = "employee-email-report-studio.jrxml";
        final String PDF_REPORT = "employee-email-report-studio.pdf";

        // extract & transform the report template designed by studio.
        Document document;
        List<String> namesOfSubreports = new ArrayList<>();
        try (InputStream employeeEmailReportStream = LOADER.getResourceAsStream(RESOURCES.resolve(EMPLOYEE_EMAIL_REPORT_TEMPLATE).toString());
             XmlReader xmlReader = new XmlReader(employeeEmailReportStream);) {
            document = xmlReader.readAll();
            XmlFinder xmlFinder = new XmlFinder(document);
            Element jasperReport = xmlFinder.findElement("/jasperReport").get();
            XmlFinder jasperReportFinder = new XmlFinder(jasperReport);
            List<Element> subreports = jasperReportFinder.findAllElements(".//subreport");
            Pattern pattern = Pattern.compile("^\"(.*)\\.jasper\"$");
            for (int i = 0; i < subreports.size(); i++) {
                Element parameter = document.createElement("parameter");
                String nameOfParameter = String.format("subreportParameter%d", i);
                parameter.setAttribute("name", nameOfParameter);
                parameter.setAttribute("class", "net.sf.jasperreports.engine.JasperReport");
                parameter.setAttribute("isForPrompting", "false");

                Optional<Element> lastParameter = jasperReportFinder.findLastElement("parameter");
                Element refElement = lastParameter.orElse(jasperReportFinder.findFirstChildElement().get());
                jasperReport.insertBefore(parameter, refElement.getNextSibling());

                Element subreport = subreports.get(i);
                XmlFinder subreportFinder = new XmlFinder(subreport);
                Element subreportExpression = subreportFinder.findElement("subreportExpression").get();
                subreportExpression.setAttribute("class", "net.sf.jasperreports.engine.JasperReport");
                XmlFinder subreportExpressionFinder = new XmlFinder(subreportExpression);
                CDATASection cdata = subreportExpressionFinder.findFirstChildCDATASection().get();
                String expression = cdata.getTextContent();
                Matcher matcher = pattern.matcher(expression);
                matcher.matches();
                String nameOfSubreport = matcher.group(1);
                cdata.setTextContent(String.format("$P{%s}", nameOfParameter));
                namesOfSubreports.add(nameOfSubreport);
            }
        }

        List<JasperReport> subreports = new ArrayList<>();
        for(String nameOfSubreport : namesOfSubreports) {
            String filename = String.format("%s.jrxml", nameOfSubreport);
            try(InputStream inputStream = LOADER.getResourceAsStream(RESOURCES.resolve(filename).toString())) {
                JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);
                subreports.add(jasperReport);
            }
        }

        try (InputStream employeeEmailReportStream = Xmls.newInputStream(document);
             OutputStream pdfReportStream = Files.newOutputStream(OUTPUT.resolve(PDF_REPORT))) {

            // JRXML files need to be compiled so the report engine can fill them with data.
            JasperReport employeeEmailJasperReport = JasperCompileManager.compileReport(employeeEmailReportStream);

            Map<String, Object> parameters = new HashMap<>();
            parameters.put("title", "Employee Report");
            parameters.put("minSalary", 15000.0);
            parameters.put("condition", " salary < 70000.0 ORDER BY FIRST_NAME");
            // fill the main report with subreport
            for(int i = 0; i < subreports.size(); i++) {
                String parameterName = String.format("subreportParameter%d", i);
                parameters.put(parameterName, subreports.get(i));
            }
            // Fill the report with parameters and jdbc connection.
            JasperPrint jasperPrint = JasperFillManager.fillReport(
                    employeeEmailJasperReport, parameters, dataSource.getConnection());

            // Export the report
            JRPdfExporter exporter = new JRPdfExporter();

            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(
                    new SimpleOutputStreamExporterOutput(pdfReportStream));

            SimplePdfReportConfiguration reportConfig
                    = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig
                    = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("jasonlin");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            exporter.setConfiguration(reportConfig);
            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
        }
    }
}

