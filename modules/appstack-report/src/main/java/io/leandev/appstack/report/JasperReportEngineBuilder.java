package io.leandev.appstack.report;

import javax.sql.DataSource;

public class JasperReportEngineBuilder {
    private String classpath;
    private DataSource dataSource;

    public static JasperReportEngineBuilder of(String classpath) {
        return new JasperReportEngineBuilder(classpath);
    }

    protected JasperReportEngineBuilder(String classpath) {
        this.classpath = classpath;
    }

    public JasperReportEngineBuilder withDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        return this;
    }

    public JasperReportEngine build() {
        JasperReportEngine jasperReportEngine = new JasperReportEngine(classpath);
        jasperReportEngine.setDataSource(dataSource);
        return jasperReportEngine;
    }

}
