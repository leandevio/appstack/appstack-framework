package io.leandev.appstack.report;

import io.leandev.appstack.cache.CacheBuilder;
import io.leandev.appstack.cache.CacheManagerBuilder;
import io.leandev.appstack.env.Environ;
import io.leandev.appstack.xml.XmlFinder;
import io.leandev.appstack.xml.XmlReader;
import io.leandev.appstack.xml.Xmls;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRElementsVisitor;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.engine.util.JRVisitorSupport;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class JasperReportEngine {
    private static final Class CLASS = JasperReportEngine.class;
    private static final ClassLoader CLASSLOADER = CLASS.getClassLoader();
    private static final Pattern SUBREPORT_NAME = Pattern.compile("^\"(.*)\\.(jasper|jrxml)\"$");
    private static final Pattern SUBREPORT_EXPRESSION = Pattern.compile("^\\$P\\{(.+)\\}$");
    private final Path TMP = Environ.getInstance().tmpFolder();
    private Path repository;
    private DataSource dataSource;
    private CacheManager cacheManager;
    private Cache<String, JasperReport> cache;


    public JasperReportEngine() {}

    public JasperReportEngine(String classpath) {
        this.repository = Paths.get(classpath.replace("classpath:", ""));
//        Reflections reflections = new Reflections( this.classpath, new ResourcesScanner());
//        Set<String> resourceList = reflections.getResources(Pattern.compile(".*\\.jrxml"));
    }

    protected CacheManager getCacheManager() {
        if(this.cacheManager != null) {
            return this.cacheManager;
        }
        this.cacheManager = CacheManagerBuilder.newCacheManager().build();
        return this.cacheManager;
    }

    protected Cache<String, JasperReport> getCache() {
        if(this.cache != null) {
            return this.cache;
        }
        CacheManager cacheManager = this.getCacheManager();
        this.cache = CacheBuilder.newCache(cacheManager, "JasperReport", String.class, JasperReport.class)
                .withNoExpiration()
                .build();
        return this.cache;
    }

    protected JasperReport getJasperReport(String reportName) {
        String key = reportName;
        if(reportName.endsWith(".jrxml")) {
            key = reportName.substring(0, reportName.length() - ".jrxml".length());
        }
        return this.getCache().get(key);
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public JasperReport compileReport(String reportName) {
        JasperReport cachedJasperReport = this.getJasperReport(reportName);
        if(cachedJasperReport != null) {
            return cachedJasperReport;
        }

        String template = reportName.endsWith(".jrxml") ? reportName : reportName + ".jrxml";
        // extract & transform the report template designed by studio.
        Document document;
        try (InputStream reportStream = CLASSLOADER.getResourceAsStream(repository.resolve(template).toString())) {
            if(reportStream == null) {
                String pathOfReport = String.format("classpath:%s", repository.resolve(template));
                throw new JasperReportException(String.format("Report not found. %s", pathOfReport));
            }
            XmlReader xmlReader = new XmlReader(reportStream);
            document = xmlReader.readAll();
            XmlFinder xmlFinder = new XmlFinder(document);
            Element jasperReport = xmlFinder.findElement("/jasperReport").get();
            XmlFinder jasperReportFinder = new XmlFinder(jasperReport);
            List<Element> subreports = jasperReportFinder.findAllElements(".//subreport");
            for (int i = 0; i < subreports.size(); i++) {
                Element subreport = subreports.get(i);
                XmlFinder subreportFinder = new XmlFinder(subreport);
                Element subreportExpression = subreportFinder.findElement("subreportExpression").get();
                subreportExpression.setAttribute("class", "net.sf.jasperreports.engine.JasperReport");
                XmlFinder subreportExpressionFinder = new XmlFinder(subreportExpression);
                CDATASection cdata = subreportExpressionFinder.findFirstChildCDATASection().get();
                String expression = cdata.getTextContent();
                Matcher matcher = SUBREPORT_NAME.matcher(expression);
                matcher.matches();
                String nameOfSubreport = matcher.group(1);
                Element parameter = document.createElement("parameter");
                parameter.setAttribute("name", nameOfSubreport);
                parameter.setAttribute("class", "net.sf.jasperreports.engine.JasperReport");
                parameter.setAttribute("isForPrompting", "false");

                Optional<Element> lastParameter = jasperReportFinder.findLastElement("parameter");
                Element refElement = lastParameter.orElse(jasperReportFinder.findFirstChildElement().get());
                jasperReport.insertBefore(parameter, refElement.getNextSibling());
                cdata.setTextContent(String.format("$P{%s}", nameOfSubreport));
            }
        } catch (IOException e) {
            throw new JasperReportException(String.format("Failed to parse the subreport. %s", reportName), e);
        }

        JasperReport jasperReport;
        try (InputStream reportStream = Xmls.newInputStream(document)) {
        // JRXML files need to be compiled so the report engine can fill them with data.
            jasperReport = JasperCompileManager.compileReport(reportStream);
        } catch (IOException | JRException e) {
            throw new JasperReportException(e);
        }

        return jasperReport;
    }

    public JasperPrint fillReport(JasperReport jasperReport, Map<String, Object> parameters) {
        JasperPrint jasperPrint;

        List<JRSubreport> jrSubreports = this.getSubReports(jasperReport);
        for(JRSubreport jrSubreport : jrSubreports) {
            String expression = jrSubreport.getExpression().getText().replace("\"", "");
            Matcher matcher = SUBREPORT_EXPRESSION.matcher(expression);
            matcher.matches();
            String nameOfSubreport = matcher.group(1);
            parameters.put(nameOfSubreport, this.compileReport(nameOfSubreport));
        }

        try(Connection connection = dataSource.getConnection()) {
            // Fill the report with parameters and jdbc connection.
            jasperPrint = JasperFillManager.fillReport(
                    jasperReport, parameters, connection);

        } catch (JRException | SQLException e) {
            throw new JasperReportException(e);
        }

        return jasperPrint;
    }

    private List<JRSubreport> getSubReports(JasperReport jasperReport) {
        Visitor visitor = new Visitor();
        JRElementsVisitor.visitReport(jasperReport, visitor);

        return visitor.getSubreports();
    }

    private class Visitor extends JRVisitorSupport {
        private List<JRSubreport> subreports = new ArrayList<>();

        @Override
        public void visitSubreport(JRSubreport subreport) {
            subreports.add(subreport);
        }

        public List<JRSubreport> getSubreports() {
            return this.subreports;
        }

    }


    public JasperPrint fillReport(String reportName, Map<String, Object> parameters) {
        JasperReport jasperReport = this.compileReport(reportName);
        return this.fillReport(jasperReport, parameters);
    }

    private Path createWorkspace() {
        Path path = TMP.resolve(UUID.randomUUID().toString());
        try {
            Files.createDirectory(path);
        } catch (IOException e) {
            throw new JasperReportException(String.format("JasperReportEngine failed to create the workspace. %s", path), e);
        }
        return path;
    }

    private void removeWorkspace(Path path) {
        try {
            Files.walk(path)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            log.warn(String.format("JasperReportEngine failed to clean the workspace. %s", path.toString()));
        }
    }

    //@todo 先透過寫到暫存區的方式產生報表，之後考慮改寫成把子報表編譯後當成參數直接傳給主報表。
    public JasperPrint fillReportWithSubReports(String reportName, Map<String, Object> parameters, String... subReports) {
        JasperPrint jasperPrint;

        Path workspace = createWorkspace();
        File jasperReport = workspace.resolve(reportName + ".jasper").toFile();

        this.writeObject(this.compileReport(reportName), jasperReport);

        for(String subReport : subReports) {
            this.writeObject(this.compileReport(subReport), workspace.resolve(subReport + ".jasper").toFile());
        }

        try(Connection connection = dataSource.getConnection()) {
            // Fill the report with parameters and jdbc connection.
            jasperPrint = JasperFillManager.fillReport(jasperReport.toString(), parameters, connection);

        } catch (JRException | SQLException e) {
            throw new JasperReportException(e);
        } finally {
            removeWorkspace(workspace);
        }
        return jasperPrint;
    }

    public void writeObject(JasperReport jasperReport, OutputStream outputStream) {
        try {
            JRSaver.saveObject(jasperReport, outputStream);
        } catch (JRException e) {
            throw new JasperReportException(e);
        }
    }

    public void writeObject(JasperReport jasperReport, File output) {
        try {
            JRSaver.saveObject(jasperReport, output);
        } catch (JRException e) {
            throw new JasperReportException(e);
        }
    }

    public void exportReport(String reportName, Map<String, Object> parameters, OutputStream outputStream) {
        try {
            JasperPrint jasperPrint = this.fillReport(reportName, parameters);

            // Export the report
            JRPdfExporter exporter = new JRPdfExporter();

            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(
                    new SimpleOutputStreamExporterOutput(outputStream));

            SimplePdfReportConfiguration reportConfig
                    = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig
                    = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("AppFuse");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            exporter.setConfiguration(reportConfig);
            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
        } catch (JRException e) {
            throw new JasperReportException(e);
        }
    }
}
