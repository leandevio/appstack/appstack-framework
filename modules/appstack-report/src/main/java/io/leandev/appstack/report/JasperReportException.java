package io.leandev.appstack.report;

public class JasperReportException extends RuntimeException {
    public JasperReportException(String message) {
        super(message);
    }
    public JasperReportException(Throwable cause) {
        super(cause);
    }
    public JasperReportException(String message, Throwable cause) {
        super(message, cause);
    }
}
