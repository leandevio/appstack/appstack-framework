# 國際化的支援

I18n 會依據 locale 與 namespace  決定如何使用 Java class path 下的國際化資料來翻譯指定的名詞或是訊息．locale 用來區分語言的種類；namespace 則是用來決定國際化資料的檔案名稱．一般來說，會把名詞放在 `term`這個 namespace 下，也就是放在 term.json 檔案內，而把訊息放在 message.json 內．

國際化資料必須放置在 `${classpath}/nls` 的目錄下面，以 languate tag (zh, zh-TW, en-US ...) 作為資料夾的名稱區分不同語言的國際化資料，然後以建立名詞 (term) 或訊息 (message) 專用的 json 格式的檔案．例如要支援繁體中文，應該在 java class path 下建立下列檔案：

- nls/zh-TW/term.json
- nls/zh-TW/message.json

I18n 有 fallback 的功能，以繁體中文為例，如果無法使用 zh-TW 翻譯指定的名詞或訊息時，就會嘗試使用 zh 翻譯，也就是利用下列的國際化資料：

- nls/zh/term.json
- nls/zh/message.json

如果都無法用來翻譯指定的名詞或訊息時，I18n 不會回傳 null，會回傳原來要翻譯的名詞或是訊息．

國際化資料的 json 檔案存放的是一個由 key 與 value 組成的  JSON 物件，key 不分大小寫．以下的範例使用下列的國際化資料：

zh/term.json

```json
{
  "task list": "工作清單",
  "Home": "首頁",
  "name": "名稱",
  "description": "說明",
  "Release Date": "發行日期",
  "Ok": "確定"
}
```

zh/message.json

```json
{
  "size must be between:${0} and ${1}":"長度需介於${0}到${1}",
  "must be less than or equal to ${0}":"必需小於或等於${0}",
  "must be greater than or equal to ${0}":"必需大於或等於${0}",
  "Please input ${0} or less than ${0} digits and ${1} or less than ${1} decimal places":"請輸入${0}或小於${0}位的整數及${1}位或小於${1}位的小數",
  "Please input ${0} or less than ${0} digits":"請輸入${0}位或小於${0}位的數字",
  "may not be null:${0}":"${0}必填",
  "This field is required.":"必填"
}
```

zh-CN/term.json

```json
{
  "task list": "工作清单",
  "home": "首页",
  "name": "名称",
  "description": "说明",
  "release date": "发行日期"
}
```

zh-CN/message.json

```json
{
  "size must be between:${0} and ${1}":"长度需介于${0}到${1}",
  "must be less than or equal to ${0}":"必需小于或等于${0}",
  "must be greater than or equal to ${0}":"必需大于或等于${0}",
  "Please input ${0} or less than ${0} digits and ${1} or less than ${1} decimal places":"请输入${0}或小于${0}位的整数及${1}位或小于${1}位的小数",
  "Please input ${0} or less than ${0} digits":"请输入${0}位或小于${0}位的数字",
  "may not be null:${0}":"${0}必填",
  "This field is required.":"必填"
}

```

使用 I18n.translate()，不指定 namespace，I18n 會依序使用 term 與 message 來翻譯指定的文字 (名詞或訊息)．

```java
String term = "release date";
String term_zh = "發行日期";
String term_zh_CN = "发行日期";
String message = "size must be between:${0} and ${1}";
String message_zh = "長度需介於${0}到${1}";
String message_zh_CN = "长度需介于${0}到${1}";
Locale zh = Locale.forLanguageTag("zh");
Locale zh_TW = Locale.forLanguageTag("zh-TW");
Locale zh_CN = Locale.forLanguageTag("zh-CN");
Locale en_US = Locale.forLanguageTag("en-US");
Locale ja = Locale.forLanguageTag("ja");
I18n i18n = new I18n(zh_TW);
assertEquals(i18n.translate(term), term_zh);
assertEquals(i18n.translate(term, zh), term_zh);
assertEquals(i18n.translate(term, zh_CN), term_zh_CN);
assertEquals(i18n.translate(term, en_US), term);
assertEquals(i18n.translate(term, ja), term);
assertEquals(i18n.translate(message), message_zh);
assertEquals(i18n.translate(message, zh), message_zh);
assertEquals(i18n.translate(message, zh_CN), message_zh_CN);
assertEquals(i18n.translate(message, en_US), message);
assertEquals(i18n.translate(message, ja), message);
```

使用 I18n.term()，只會使用 term 翻譯指定的文字 (名詞)．

```java
String term = "Release Date";
String term_zh = "發行日期";
String term_zh_CN = "发行日期";
Locale zh = Locale.forLanguageTag("zh");
Locale zh_TW = Locale.forLanguageTag("zh-TW");
Locale zh_CN = Locale.forLanguageTag("zh-CN");
Locale en_US = Locale.forLanguageTag("en-US");
Locale ja = Locale.forLanguageTag("ja");
I18n i18n = new I18n(zh_TW);
assertEquals(i18n.term(term), term_zh);
assertEquals(i18n.term(term, zh), term_zh);
assertEquals(i18n.term(term, zh_CN), term_zh_CN);
assertEquals(i18n.term(term, en_US), term);
assertEquals(i18n.term(term, ja), term);
```
使用 I18n.message()，只會使用 message 翻譯指定的文字 (訊息)．
```java
String message = "size must be between:${0} and ${1}";
String message_zh = "長度需介於${0}到${1}";
String message_zh_CN = "长度需介于${0}到${1}";
Locale zh = Locale.forLanguageTag("zh");
Locale zh_TW = Locale.forLanguageTag("zh-TW");
Locale zh_CN = Locale.forLanguageTag("zh-CN");
Locale en_US = Locale.forLanguageTag("en-US");
Locale ja = Locale.forLanguageTag("ja");
I18n i18n = new I18n(zh_TW);
assertEquals(i18n.message(message), message_zh);
assertEquals(i18n.message(message, zh), message_zh);
assertEquals(i18n.message(message, zh_CN), message_zh_CN);
assertEquals(i18n.message(message, en_US), message);
assertEquals(i18n.message(message, ja), message);
```



# 工作排程

```java
public class SchedulerTest {
    private Scheduler scheduler;

    @BeforeTest
    public void setUp() throws Exception {
        scheduler = new Scheduler();
        scheduler.start();
    }

    @Test
    public void testCronSchedule() throws SchedulerException, ParseException, InterruptedException {
        Schedule schedule = CronScheduleBuilder.newSchedule("0/10 * * * * ?").forJob(MyJob.class)
                .usingData("baseline", LocalDate.of(2019, 02, 12))
                .withIdentity("mySchedule", "cronGroup")
                .inTimeZone(TimeZone.getDefault())
                .build();

        scheduler.arrange(schedule);

        Thread.sleep(15*1000);
        schedule = scheduler.get("mySchedule", "cronGroup");
        System.out.println(String.format("previous fire time: %s", schedule.previousFireTime()));
        System.out.println(String.format("next fire time: %s", schedule.nextFireTime()));
        Thread.sleep(15*1000);
    }

    @Test
    public void testSimpleSchedule() throws SchedulerException, InterruptedException {
        Schedule schedule = SimpleScheduleBuilder.newSchedule().forJob(MyJob.class)
                .withIdentity("mySchedule", "simpleGroup")
                .usingData("baseline", LocalDate.of(2019, 02, 12))
                .withIntervalInSeconds(10)
                .repeatForever()
                .startNow()
                .build();
        scheduler.arrange(schedule);
        Thread.sleep(15*1000);
        schedule = scheduler.get("mySchedule", "simpleGroup");
        System.out.println(String.format("previous fire time: %s", schedule.previousFireTime()));
        System.out.println(String.format("next fire time: %s", schedule.nextFireTime()));
        Thread.sleep(15*1000);
    }

    @AfterTest
    public void tearDown() throws Exception {
        scheduler.shutdown();
    }

}
```



```java
public class MyJob implements Job {

    private int counter = 0;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        counter++;
        Schedule schedule = jobExecutionContext.schedule();
        LocalDate baseline = jobExecutionContext.getDataAsLocalDate("baseline");
        System.out.println(String.format("%s: A Simple Job was executed.", schedule.name()));
        System.out.println(String.format("counter: %s", counter));
        System.out.println(String.format("baseline: %s", baseline));
        System.out.println(String.format("times of triggered: %s", jobExecutionContext.schedule().timesTriggered()));
        System.out.println(String.format("fire time: %s", jobExecutionContext.fireTime()));
    }
}
```



# 各種資料來源的處理

## Excel 資料來源

使用 XlsxWorkbook (或是 XlsWorkbook) 讀取 Excel 的資料來源，選取要處理的資料表(Worksheet)，然後用 WorksheetParser 讀取資料內容．WorksheetParser 支援 for-each loop 與 Java Stream 兩種方式操作讀取的資料內容．

使用 for-each loop 操作： 

```java
InputStream in = WorksheetParserTest.class.getResourceAsStream("/data.xlsx");
Workbook workbook = new XlsxWorkbook(in);
Worksheet worksheet = workbook.getWorksheet("Product");
WorksheetParser parser = new WorksheetParser();
for(Record record : parser.iterate(worksheet)) {
  assertNotNull(record.getString(0));
  assertNotNull(record.getString(1));
  assertNotNull(record.getString(2));
  assertNotNull(record.getBigDecimal(3));
  assertNotNull(record.getDate(4));
}
workbook.close();
in.close();
```

使用 Java Stream 操作：

```java
InputStream in = WorksheetParserTest.class.getResourceAsStream("/data.xlsx");
Workbook workbook = new XlsxWorkbook(in);
Worksheet worksheet = workbook.getWorksheet("Product");
WorksheetParser parser = new WorksheetParser();
parser.stream(worksheet).forEach(record -> {
  assertNotNull(record.getString(0));
  assertNotNull(record.getString(1));
  assertNotNull(record.getString(2));
  assertNotNull(record.getBigDecimal(3));
	assertNotNull(record.getDate(4));
});
workbook.close();
in.close();
```

如果 Workbook 只有一個資料表，也可以直接使用 WorksheetParser 直接讀取資料內容：

```java
InputStream in = WorksheetParserTest.class.getResourceAsStream("/data.xlsx");
Workbook workbook = new XlsxWorkbook(in);
WorksheetParser parser = new WorksheetParser();
parser.stream(workbook).forEach(record -> {
  assertNotNull(record.getString(0));
  assertNotNull(record.getString(1));
  assertNotNull(record.getString(2));
  assertNotNull(record.getBigDecimal(3));
	assertNotNull(record.getDate(4));
});
workbook.close();
in.close();
```

## CSV 資料來源

使用 CsvParser 讀取資料內容．CsvParser 支援 for-each loop 與 Java Stream 兩種方式操作讀取的資料內容．

使用 for-each loop 操作： 

```java
InputStream in = CsvParserTest.class.getResourceAsStream("/Product.csv");
CsvParser parser = new CsvParser();
for(Record record : parser.iterate(in)) {
  assertNotNull(record.getString(0));
  assertNotNull(record.getString(1));
  assertNotNull(record.getString(2));
  assertNotNull(record.getBigDecimal(3));
  assertNotNull(record.getDate(4));
}
in.close();
```

使用 Java Stream 操作：

```java
InputStream in = CsvParserTest.class.getResourceAsStream("/Product.csv");
CsvParser parser = new CsvParser();
parser.stream(in).forEach(record -> {
  assertNotNull(record.getString(0));
  assertNotNull(record.getString(1));
  assertNotNull(record.getString(2));
  assertNotNull(record.getBigDecimal(3));
  assertNotNull(record.getDate(4));
});
in.close();
```

## Flat File (TXT) 資料來源

使用 TxtParser 讀取資料內容．TxtParser 支援 for-each loop 與 Java Stream 兩種方式操作讀取的資料內容．

建議使用  TxtParserBuilder 來建立與配置 TxtParser．TxtParserBuilder.withPadding(padding) 可以用來指定填補空白的字元，TxtParserBuilder.keepPadding() 可以指定在讀取結果保留填補的字元，主要用途都是方便除錯．

使用 for-each loop 操作： 

```java
InputStream in = TxtParserTest.class.getResourceAsStream("/Product.txt");
TxtParser parser = TxtParserBuilder.newTxtParser()
  .withColumnWidths(31, 6, 24) // 欄位長度定義
  .withPadding('_') // 指定填補空白的字元
  .build();
for(Record record : parser.iterate(in)) {
  assertNotNull(record.getString(0));
  assertNotNull(record.getDouble(1));
  assertNotNull(record.getDate(2));
}
in.close();
```

使用 Java Stream 操作：

```java
InputStream in = TxtParserTest.class.getResourceAsStream("/Product.txt");
TxtParser parser = TxtParserBuilder.newTxtParser()
  .withColumnWidths(31, 6, 24) // 欄位長度定義
  .withPadding('_') // 指定填補空白的字元
  .build();
parser.stream(in).forEach(record -> {
  System.out.println(Arrays.toString(record.getValues()));
});
in.close();
```

`Product.txt`

```
id_____________________________price_createdOn_______________
Sweet flower box_______________3000__2018-07-12T00:00:00.000Z
Yellow Green Christmas Gift Box3000__2018-07-12T00:00:00.000Z
Romantic pink christmas tree___4000__2018-07-12T00:00:00.000Z
```

# Workbook 的處理

## Xlsx 的處理

產生一個 Xlsx (Excel) 的資料檔．

```java
FileOutputStream output = new FileOutputStream(TARGET.toString());
XlsxWorkbook workbook = new XlsxWorkbook();
XlsxWorksheet worksheet = workbook.createWorksheet();
for(Model datum : data) {
  XlsxRow row = worksheet.createRow();
  for(String name : FIELDS) {
    XlsxCell cell = row.createCell();;
    Object value;
    if(name.equals("createdOn")) {
      value = datum.getAsDate(name);
    } else {
      value = datum.getValue(name);
    }
    cell.setValue(value);
  }
}
workbook.autoSizeColumns();
workbook.write(output);
output.close();
```

在產生的 Xlsx (Excel) 資料檔加上格式．

```java
FileOutputStream output = new FileOutputStream(TARGET.toString());
XlsxWorkbook workbook = new XlsxWorkbook();
workbook.setLocale(Locale.TRADITIONAL_CHINESE);
XlsxWorksheet worksheet = workbook.createWorksheet();
worksheet.setGridlines(false);
XlsxRow columnHeaders = worksheet.createRow();
for(String field : FIELDS) {
  XlsxCell columnHeader = columnHeaders.createCell();
  columnHeader.setBorderStyle(BorderStyle.SOLID);
  columnHeader.setBorderColor(Color.WHITE);
  columnHeader.setBorderBottomStyle(BorderStyle.DOUBLE);
  columnHeader.setBorderBottomColor(Color.DARKBLUE);
  columnHeader.setBackground(Color.BLUE);
  columnHeader.setColor(Color.WHITE);
  columnHeader.setAlign(Align.CENTER);
  columnHeader.setValue(field);
}
for(Model datum : data) {
  XlsxRow row = worksheet.createRow();
  for(String name : FIELDS) {
    XlsxCell cell = row.createCell();
    cell.setBorderColor(Color.DARKBLUE);
    cell.setBorderStyle(BorderStyle.DOTTED);
    cell.setBackground(Color.LIGHTBLUE);
    cell.setColor(Color.DARKBLUE);
    Object value = datum.getValue(name);
    if(name.equals("id")) {
      cell.setAlign(Align.CENTER);
      cell.setBorderStyle(BorderStyle.NONE);
      cell.setBorderColor(Color.DARKBLUE);
      cell.setBorderRightStyle(BorderStyle.SOLID);
    } else if(name.equals("createdOn")) {
      value = datum.getAsDate(name);
      cell.setDataFormat(DataFormat.DATE);
    } else if(name.equals("price")) {
      cell.setDataFormat(DataFormat.MONEY);
    }
    cell.setValue(value);
  }
}
worksheet.autoSizeColumns();
worksheet.setBorderColor(Color.DARKBLUE);
worksheet.setBorderStyle(BorderStyle.SOLID);
workbook.write(output);
output.close();
```



# Rich Text 的處理

## Delta 文件格式

Delta 文件格式是由一連串文件編輯的指令組合而成的 JSON，透過這些指令，可以用來建立 rich text 的文件．

```json
{
  "ops": [
    {
      "attributes": {
        "underline": true
      },
      "insert": "Underline"
    },
    {
      "insert": "\n"
    },
    {
      "attributes": {
        "italic": true,
        "bold": true
      },
      "insert": "Bold & Italic"
    }]
}
```

DeltaParser 可以用來將 JSON string 轉換成 Delta 物件．

```java
Path path = Paths.get("/").resolve(HtmlDocument.class.getPackage().getName().replaceAll("\\.", "/")).resolve("delta.json");

InputStream inputStream = HtmlDocument.class.getResourceAsStream(path.toString());

DeltaParser deltaParser = new DeltaParser();
Delta delta = deltaParser.parse(inputStream);

inputStream.close();
```

## HTML 文件格式

支援 Delta 文件格式，可以將 Delta 的文件格式轉換成 HTML 區塊 (Blocks)．

```java
Path path = Paths.get("/").resolve(HtmlDocument.class.getPackage().getName().replaceAll("\\.", "/")).resolve("delta.json");

InputStream inputStream = HtmlDocument.class.getResourceAsStream(path.toString());

DeltaParser deltaParser = new DeltaParser();
Delta delta = deltaParser.parse(inputStream);

inputStream.close();

HtmlDocument htmlDocument = new HtmlDocument();
htmlDocument.appendBlocks(delta);

assertThat(htmlDocument.html()).isNotNull();
```

# HTML的處理

```java
String html = "<html><head><title>Breaking News</title></head>"
  + "<body><div class='news' title='First'>First Headline</div>"
  + "<div class='news' title='Second'>Second Headline</div></body></html>";
Document doc = Jsoup.parse(html);
assertThat(doc.title()).isEqualTo("Breaking News");
Elements newsHeadlines = doc.select(".news");
Element first = newsHeadlines.first();
assertThat(first.className()).isEqualTo("news");
assertThat(first.attr("title")).isEqualTo("First");
assertThat(first.text()).isEqualTo("First Headline");
```



# PDF 的處理

## 加密

使用密碼加密 pdf 文件．

```java
String password = "24485314";
InputStream inputStream = .....
OutputStream outputStream = .....

// 載入 pdf 文件
PdfDocument pdfDocument = new PdfDocument(inputStream);

// 使用密碼加密
pdfDocument.protect(password);

// 儲存加密後的 pdf 文件
pdfDocument.save(outputStream);

pdfDocument.close();

inputStream.close();
outputStream.close();

```

## 數位簽章(Digital Signature)

使用數位簽章簽署 pdf 文件必須先將數位簽章儲存在 KeyStore．下列範例使用 Java keytool 建立私人簽署的數位簽章，並儲存在 KeyStore：

```bash
$ keytool -genkeypair -storepass 24485314 -storetype pkcs12 -alias myKey -validity 365 -v -keyalg RSA -keystore keystore.p12
```

上面的範例建立的一個檔案名稱為 `keystore.p12`，格式為 PKCS12，密碼是`24485314`．同時建立了一個數位簽章，名稱為 myKey，有效時間 365 天，密碼因為沒有另外指定，所以一樣是 `24485314`．

接著使用產生的數位簽章簽署 pdf 文件： 

```java
String storePass = "24485314";
Path keystoreFile = Paths.get("keystore.p12");

KeyStore keystore = KeyStoreBuilder.newPKCS12KeyStore()
  .from(keystoreFile, storePass)
  .build();

String keyPass = "24485314";
PdfSignature pdfSignature = new PdfSignature(keystore, keyPass);

// 要簽署的pdf文件
InputStream inputStream = PdfDocumentTest.class.getResourceAsStream(SOURCEPATH.toString());
// 簽署後的pdf文件
OutputStream outputStream = new FileOutputStream(TARGETPATH.toFile());

pdfSignature.sign(inputStream, outputStream);
```



# 報表

## JasperReports

### 部署報表

ReportEngine 會從 `classpath` 的`/report` 載入指定的報表，加以編譯，然後保存在快取裡．

```java
final String REPORTNAME = "DataBean Report";
final String REPORTFILE = "DataBeanReport.jrxml";

ReportEngine reportEngine = new JasperReportEngine();
Report report = reportEngine.deploy(REPORTNAME, REPORTFILE);
assertThat(report).isNotNull();
```



### 產生報表

ReportEngine 依據指定名稱從快取載入報表，填入報表需要的參數與資料後，產生 pdf 的報表．

```java
Map<String, Object> parameters = new HashMap<>();
List<DataBean> dataBeans = new ArrayList<>();
final String REPORTNAME = "DataBean Report";
final String FILENAME = "DataBeanReport.pdf";
final Path TARGET = Environment.getDefaultInstance().tmpFolder().resolve(FILENAME);

ReportEngine reportEngine = new JasperReportEngine();
Print print = reportEngine.fill(REPORTNAME, parameters, dataBeans.toArray());
FileOutputStream outputStream = new FileOutputStream(TARGET.toFile());
reportEngine.exportToPdf(print, outputStream);
assertThat(TARGET.toFile().exists()).isTrue();
```



### 關於子報表

使用 JasperReports Studio 設計報表時：

首先，在主報表建立一個用來指定子報表的參數，例如：SUBREPORT_A，型別為 net.sf.jasperreports.engine.JasperReport。

然後，再來修改主報表上的子報表屬性，將 Subreport Expression 修改為 $P{SUBREPORT_A}，也就是最前面建立的參數名稱，再將 Expression Class 修改為 net.sf.jasperreports.engine.JasperReport。

使用 Java 產生報表時：

透過 ReportEngine.get() 取得編譯過的子報表(Report 物件)，然後使用 parameters 將這個物件傳入主報表．

```java
Map<String, Object> parameters = new HashMap<>();
Report report = reportEngine.get(REPORTNAME);

Report subreport_a = reportEngine.getReport("subreport_a");
parameters.set("SUBREPORT_A", subreport_a)

Print print = reportEngine.fill(REPORTNAME, parameters, dataBeans.toArray());
```



# 電子郵件

## 發送郵件

`Mailer`可以透過 SMTP 通訊協定連接郵件伺服器讀取郵件．一般的使用方式如下：

- 使用 `MailerBuilder`  建立 `Mailer`．`MailerBuilder` 提供較便捷的方式建立一個可以連接 Gmail、Office 365 或是一般 SMTP 郵件伺服器的`Mailer`．
- 使用 `EmailBuilder`  建立 `Email`．`EmailBuilder` 提供較便捷的方式建立一封可以發送的郵件．
- 使用 `Mailer` 發送郵件．

Mailer 有個防火牆的功能，啟用時會阻止郵件發送到郵件伺服器以外的信箱。這個功能預設是啟用的。要發送到外部信箱必須關閉這個功能。

### 使用設定檔的內容連接伺服器

以下範例是使用 `Mailer` 連接 SMTP 信箱發送帶有兩個附件(PDF & Excel)的 HTML 郵件：

```java
// 使用 MailerBuilder  建立 Mailer．
Mailer mailer = MailerBuilder.newMailer()
  .withSMTPServer()
  .withDebug(true)
  .build();

InputStream pdf = readPdf();
InputStream excel = readExcel();

// 使用 EmailBuilder  建立 Email．
Email email = EmailBuilder.newHtmlEmail()
  .from(USERNAME, "Jason Lin")
  .to("jasonlin@leandev.io", "林晉陞")
  .withSubject("Mailer 測試郵件")
  .withContent("<span>您好：</span><p>" +
               "<span style='color:blue'>這段文字是藍色的．</span><p>" +
               "<span>這封信有兩個附件．</span>", ">您好：\n這段文字是藍色的．\n這封信有兩個附件．")
  .withAttachment(pdf, "Office Layout.pdf")
  .withAttachment(excel, "報價.xlsx")
  .build();

// 使用 Mailer 發送郵件．
mailer.send(email);

pdf.close();
excel.close();
```

這個範例使用設定檔內的參數來連接郵件伺服器

`app.properties`

```properties
# Mail
# SMTP
#mail.smtp.auth=true
#mail.smtp.host=ellen.leandev.com.tw
#mail.smtp.port=25
#mail.smtp.username=jasonlin@ellen.leandev.com.tw
#mail.smtp.password=24485314

# Gmail
#mail.smtp.auth=true
#mail.smtp.host=smtp.gmail.com
#mail.smtp.port=587
#mail.smtp.starttls.enable=true
#mail.smtp.username=jaysenlin@gmail.com
#mail.smtp.password=mar4th57
#mail.debug=true
#mail.firewall.enable=false

# Office365
mail.smtp.auth=true
mail.smtp.host=smtp.office365.com
mail.smtp.port=587
mail.smtp.starttls.enable=true
mail.smtp.username=notification@talentonline.io
mail.smtp.password=123456
mail.debug=true
# 停用防火墻
mail.firewall.enable=false
```

### 使用專門連接 Gmail 的 API 

以下範例是使用 `Mailer` 連接 Gmail 信箱發送帶有兩個附件(PDF & Excel)的 HTML 郵件：

```java
final String USERNAME = "<Your Username>";
final String PASSWORD = "<Your Password>";

// 使用 MailerBuilder  建立 Mailer．
Mailer mailer = MailerBuilder.newMailer()
  .withGmailServer(USERNAME, PASSWORD)
  .withDebug(true)
  .build();

// 停用防火牆
mailer.stopFirewall();

InputStream pdf = readPdf();
InputStream excel = readExcel();

// 使用 EmailBuilder  建立 Email．
Email email = EmailBuilder.newHtmlEmail()
  .from(USERNAME, "Jason Lin")
  .to("jasonlin@leandev.io", "林晉陞")
  .withSubject("Mailer 測試郵件")
  .withContent("<span>您好：</span><p>" +
               "<span style='color:blue'>這段文字是藍色的．</span><p>" +
               "<span>這封信有兩個附件．</span>", ">您好：\n這段文字是藍色的．\n這封信有兩個附件．")
  .withAttachment(pdf, "Office Layout.pdf")
  .withAttachment(excel, "報價.xlsx")
  .build();

// 使用 Mailer 發送郵件．
mailer.send(email);

pdf.close();
excel.close();
```



## 讀取郵件

`Mailbox`可以透過 IMAP 通訊協定連接郵件伺服器讀取郵件．一般的使用方式如下：

- 使用 `MailboxBuilder`  建立 `Mailbox`．`MailboxBuilder` 提供較便捷的方式建立一個可以連接 Gmail、Office 365 或是一般 IMAP 郵件伺服器的`Mailbox`．
- 使用 `Mailbox` 連接郵件伺服器．
- 建立可以連接收件匣的 `Folder` ．`Folder` 可以用來連接 IMAP 郵件伺服器上的資料匣．
- 使用 `Folder` 開啟收件匣．
- 使用 `Folder` 讀取郵件．

```mermaid
classDiagram
      Email o-- Envelop
      Email o-- Content
      Content "1" o-- "*" Article
      Content "1" o-- "*" Attachment
      Content "1" o-- "*" Content
      Content "1" o-- "*" Email
    	
    	class Email {
    			+getFrom(): Address
    			+getRecipients(type: Message.RecipientType): List~Address~
    			+getReplyTo(): Address
    			+getReceivedDate(): Date
    			+getSubject(): String
    			+getContent(): Content
    			+getArticles(): List~Article~
    			+getArticle(): Article
    			+getAttachments(): List~Attachment~
    	}
    
      class Content {
          +getArticles() List~Article~
          +getAttachments() List~Attachment~
          +getContents() List~Content~
          +getEmails() List~Email~
      }
      class Article {
          +getMediaType() MediaType
          +getCharset() Charset
          +getData() Object
          +getResources() List~Attachment~
          +getText() String
      }
      class Attachment {
          +getMediaType() MediaType
          +getName() String
          +getBytes() byte[]
      }
```



以下範例是使用 `Mailbox` 讀取 Gmail 信箱的郵件：

```java
final String USERNAME = "<Your Username>";
final String PASSWORD = "<Your Password>";

// 使用 MailboxBuilder 建立 Mailbox．
Mailbox mailbox = MailboxBuilder.newMailbox()
  .withGmailServer(USERNAME, PASSWORD)
  .withDebug(true)
  .build();

// 使用 Mailbox 連接郵件伺服器．
mailbox.connect();

// 建立可以連接收件匣的 Folder．
Folder inbox = mailbox.getInbox();

// 使用 Folder 開啟收件匣．
inbox.open();

// 使用 Folder 讀取郵件．
List<Email> emails = inbox.fetch();

for(Email email : emails) {
  System.out.println(String.format("UID: %s", email.getUID()));
  System.out.println(String.format("Received Date: %s", email.getReceivedDate()));
  System.out.println(String.format("From: %s", email.getFrom().toString()));
  System.out.println(String.format("TO: %s", email.getRecipients(Message.RecipientType.TO)));
  System.out.println(String.format("CC: %s", email.getRecipients(Message.RecipientType.CC)));
  System.out.println(String.format("Subject: %s", email.getSubject()));
  if(email.getArticles().size()>0) {
    Article article = email.getArticles().get(0);
    System.out.println(String.format("Content: %s\n%s\n%s", article.getMediaType(), article.getText(), article.getData()));
  } else {
    System.out.println("No Content.");
  }
  if(email.getAttachments().size()>0) {
    System.out.println("Attachments:");
    for(Attachment attachment : email.getAttachments()) {
      System.out.println(String.format("%s: %s", attachment.getName(), attachment.getMediaType()));
    }
  }
}

inbox.close();
mailbox.close();
```

### 讀取特定的郵件

郵件伺服器上沒有提供查詢郵件的方法，但是每封被存入資料匣的郵件都會被指定一個可以用來識別的 `UID`，可以透過 Email.getUID() 取得．`UID`是一個數字，在每個資料匣具有唯一的特性，越晚被存入資料匣的郵件的 `UID` 值越大．注意：越晚的意思是指實際存入資料匣的時間，與郵件收到的時間沒有關係．

Folder.fetch()讀取郵件時，可以透過指定 `UID` 的方式讀取特定郵件．

- Folder.fetch(long start, long end)：讀取 `UID` 介於 `start` 與 `end` 之間(包含 `start` 與 `end`)的郵件．
- Folder.fetch(long start)：讀取 `UID` 大於 `start` 以後(包含 `start`)之間的郵件．

隨著郵件進出資料匣，有時候資料匣會因為需要，重新整理郵件的 `UID`，也就是說郵件的 `UID` 會改變．資料匣有一個 `UID Validity` 的數字可以判斷這樣的情況是否發生．每次發生重新整理郵件 `UID` 時，資料匣的 `UID Validity` 就會改變．`UID Validity` 的值可以透過 Folder.getUIDValidity() 來取得．

## 建立資料匣

以下範例建立一個 `Done` 的資料匣：

```java
Folder done = mailbox.getFolder("Done");
if(!done.exists()) {
  mailbox.createFolder("Done");
}
```

## 複製郵件

以下範例將收件匣的郵件複製到 `Done` 的資料匣：

```java
Folder inbox = mailbox.getInbox();
Folder done = mailbox.getFolder("Done");
done.open(Folder.READ_WRITE);
List<Email> emails = inbox.fetch(100, 110);
for(Email email : emails) {
  inbox.copyEmail(email, done);
}
inbox.close();
mailbox.close();
```

## 刪除郵件

以下範例將收件匣的郵件刪除：

```java
Folder inbox = mailbox.getInbox(Folder.READ_WRITE);
List<Email> emails = inbox.fetch(100, 110);
for(Email email : emails) {
  inbox.deleteEmail(email);
}
inbox.close();
```

## 邀請函

以下範例透過電子郵件發送邀請函，收件者的信箱與收件軟體如果支援行事曆，就可以自動將行程加入行事曆：

```java
final String USERNAME = "<Your Username>";
final String PASSWORD = "<Your Password>";

Mailer mailer = MailerBuilder.newMailer()
  .withGmailServer(USERNAME, PASSWORD)
  .withDebug(true)
  .build();

InputStream pdf = readPdf();
InputStream excel = readExcel();

Instant now = Instant.now();
Date start = Date.from(now.plus(Duration.ofDays(1)));
Date end = Date.from(start.toInstant().plus(Duration.ofHours(3)));
String uid = UUID.randomUUID().toString();

Invitation invitation = InvitationBuilder.newInvitation()
  .from(USERNAME, "Mailer 會議邀請測試")
  .withOrganizer("jasonlin@leandev.io", "Jason Lin")
  .withAttendee("nikiechan@jobscloud.com", "Nikie Chan")
  .withAttendee("samlee@jobscloud.com", "Sam Lee")
  .withAttendee("mayshih@leandev.io", "May Shih")
  .withSubject("專案會議")
  .startAt(start)
  .endAt(end)
  .atLocation("542 會議室")
  .withUid(uid)
  .withDescription("以下是本次會議的討論主題：<ol><li>成本效益</li><li>技術架構<li>風險評估</li></ol>")
  .withAttachment(pdf, "Office Layout.pdf")
  .withAttachment(excel, "報價.xlsx")
  .build();
mailer.send(invitation);

pdf.close();
excel.close();

```

邀請函的 UID (Invitation.getUID()) 是用來識別邀請函的，可以使用任何字串的文字．如果沒有指定，Invitation 會使用 UUID 產生一個隨機的識別碼．如果要更新或取消特定的邀請函，必須在指定與之前發送邀請函相同的 UID．

要更新邀請函，只要重新發送邀請函，並且指定邀請函使用之前已經發送的邀請函相同的 UID，就可以更新邀請函．

邀請函可以被取消．要取消邀請函，也一樣必需使用之前已經發送的邀請函相同的 UID，然後透過 Mailer.cancel() 取消邀請函．

```java
String uid = "20200912103000"; // 要取消的邀請函使用的 UID

Invitation invitation = InvitationBuilder.newInvitation()
  .from(USERNAME, "Mailer 會議邀請測試")
  .withOrganizer("jasonlin@leandev.io", "Jason Lin")
  .withAttendee("nikiechan@jobscloud.com", "Nikie Chan")
  .withAttendee("samlee@jobscloud.com", "Sam Lee")
  .withAttendee("mayshih@leandev.io", "May Shih")
  .withSubject("專案會議")
  .startAt(start)
  .endAt(end)
  .atLocation("542 會議室")
  .withUid(uid)
  .build();

mailer.cancel(invitation);


```



# 快取

## CacheManager

`CacheManager` 可以讓應用程式建立使用 **key**-**value pair** (KVP) 存放資料的快取(Cache)．快取使用的儲存空間可以由記憶體與磁碟空間組合而成，種類包括：

- on-heap memory
- off-heap memory
- disk

應用程式建立快取時，可以指定上述三種儲存空間的大小如何配置．

快取會先把指定的資料存放在記憶體裡，當配置的記憶體空間用盡時，會將使用頻率低的資料移至指定的磁碟空間上．最後，在所有配置的空間用盡時，快取會將使用頻率低的資料移除．

下面的範例使用 `CacheManagerBuilder` 建立一個混合使用記憶體(on-heap & off-heap)與磁碟的`CacheManager`，並且在建立的時候同時初始化：

```java
CacheManager cacheManager = CacheManagerBuilder.newCacheManager()
  .usingPersistence()
  .build(true);
```

`usingPersistence(Path storage)`指定`CacheManager`可以使用`storage`參數指定的磁碟目錄作為作為快取的儲存空間．`usingPersistence()`不指定參數時，就是用`Environment.cacheFolder()`指定的目錄．

有了初始化的 `CacheManager` 後，就可以用來建立需要使用的快取．建立快取需要指定快取名稱，以及存放在快取內的 key-value pair 的型別．下列範例建立一個名稱為`statistics`的快取，並使用 `String`與 `Float` 分別作為 `key`與 `value`的型別：

```java
Cache<String.class, Float.class> cache = cacheManager.createCache("statistics", String.class, Float.class);
```

建立快取後，應用程式就可以透過快取名稱取得快取，並使用快取存取資料．以下的範例是一個取得目前訪客停留在網站的平均時間的函數，這個函數會先到快取讀取訪客平均停留時間 - `currentAvgTimeOnSite`，如果找不到，就呼叫`calculateCurrentAvgTimeOnSite()`函數計算，並將結果存入快取．

```java
public float getCurrentAvgTimeOnSite() {
  Cache<String.class, Float.class> cache = cacheManager.getCache("statistics");
  float currentAvgTimeOnSite = cache.get("currentAvgTimeOnSite")
  if(currentAvgTimeOnSite==null) {
    currentAvgTimeOnSite = calculateCurrentAvgTimeOnSite();
  }  
  cache.put("currentAvgTimeOnSite", currentAvgTimeOnSite);
  return currentAvgTimeOnSite;
}
```

應用程式可以透過移除快取內的資料使快取內的資料失效．

```java
cache.remove("currentAvgTimeOnSite");
```

### 關閉快取

```java
cacheManager.close();
```

### 自訂快取

```java
CacheConfiguration<String, Float> cacheConfiguration =
                CacheConfigurationBuilder.newCacheConfiguration(String.class, Float.class)
                .heap(10)
                .offHeap(1)
                .disk(10)
                .withTimeToLiveExpiration(Duration.of(60, ChronoUnit.MINUTES))
                .build();

Cache<String, Float> cache = cacheManager.createCache("statistics", cacheConfiguration);
```

`withTimeToLiveExpiration(Duration.of(60, ChronoUnit.MINUTES))`指定存放在快取的資料在60分鐘後失效．應用程式也可以使用 `withTimeToIdleExpiration(Duration)` 指定存放在快取的資料在沒有被讀取的一段時間後失效．

# 網路

## FTPServer

`FtpServer`可以使應用程式支援`FTP`通訊協定的介面．

建立`FtpServer`前需要先建立一個`FtpUserManager`來提供FTP的用戶資訊．下列範例使用`FtpInMemoryUserManagerBuilder`來建立一個使用記憶體作為儲存媒體的`FtpUserManager`：

```java
List<FtpUser> ftpUsers = parser.readValueAsList(Paths.get("FtpUsers.json"), FtpUser.class);

FtpUserManager ftpUserManager = FtpInMemoryUserManagerBuilder.newInMemoryUserManager()
  .withPasswordEncoder(new PlainPasswordEncoder()) // 密碼編碼器
  .withUsers(ftpUsers) // FTP 的用戶資訊
  .build();
```

在上面的範例裡面為了測試方便，指定使用`PlainPasswordEncoder`做為密碼編碼器，在正式環境應該使用適當的密碼編碼器，例如 `BCryptPasswordEncoder`．

範例裡面也在初始化時指定了FTP的用戶資訊．用戶資訊的結構如下：

FtpUsers.json

```json
[
  {
    "username": "jasonlin",
    "password": "1234"
  },
  {
    "username": "mayshih",
    "password": "5678"
  }
]
```

在不指定用戶的根目錄的情況下，`FtpInMemoryUserManagerBuilder`會指定使用 `$APP_HOME/var/ftp/${username}`作為`FTP`用戶的根目錄．

接下來使用建立的`FtpUserManager`建立`FtpServer`，接著啟動，然後停止：

```java
FtpServer ftpServer = FtpServerBuilder.newFtpServer()
  .onPort(2121) // 通訊埠
  .withUserManager(ftpUserManager) // FtpUserManager
  .build();
ftpServer.startup(); // 啟動
....
ftpServer.shutdown(); // 停止

```

# 地理資訊

## 地理距離

`GeoDistanceService` 可以利用經緯度計算兩地間的距離．東經(east longitudes)是正數，南緯(south latitudes)是負數．

```java
GeoDistanceService geoDistanceService = new GeoDistanceService();
GeoLocation origin = new GeoLocation(32.9697, -96.80322);
GeoLocation destination = new GeoLocation(29.46786, -98.53506);

EuclideanDistance euclideanDistance = geoDistanceService.computeEuclideanDistance(origin, destination);

assertEquals(euclideanDistance.distance(), 422738.9313940138);
```

# 數學運算

## 簡單線性迴歸模型(Simple LInear Regression Model)

```java
SimpleLinearRegressionModel simpleLinearRegressionModel = new SimpleLinearRegressionModel();
double slope = 2, intercept = 1;
for(int x=1; x<=5; x++) {
  simpleLinearRegressionModel.addData(x, x*slope + intercept);
}
double x = 4.5;
assertThat(simpleLinearRegressionModel.predict(x)).isEqualTo(x*slope + intercept);
assertThat(simpleLinearRegressionModel.slope()).isEqualTo(slope);
assertThat(simpleLinearRegressionModel.intercept()).isEqualTo(intercept);
assertThat(simpleLinearRegressionModel.slopeStdErr()).isEqualTo(0);
assertThat(simpleLinearRegressionModel.interceptStdErr()).isEqualTo(0);影像處ㄌ
```

# 影像處理

## 產生人機驗證碼（Captcha）

```java
// generate the captcha text in length of 6.
String captchaText = Captcha.generateText(6);
// use the text to generate the captcha image.
BufferedImage captchaImage = Captcha.generateImage(captchaText);
OutputStream output = new FileOutputStream(environment.tmpFolder().resolve("captcha.png").toFile());
// write to the outputstream
ImageWriter writer = new ImageWriter(output, "png");
writer.write(captchaImage);
writer.close();
```



